<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201218105118 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE zrp_company_transaction (id INT AUTO_INCREMENT NOT NULL, zrp_company_id INT DEFAULT NULL, cmp_trx_date DATETIME NOT NULL, cmp_trx_nbr INT NOT NULL, INDEX IDX_6C26605B88560360 (zrp_company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE zrp_company_transaction ADD CONSTRAINT FK_6C26605B88560360 FOREIGN KEY (zrp_company_id) REFERENCES zrp_company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_transaction ADD trx_desc LONGTEXT DEFAULT NULL, ADD trx_num VARCHAR(50) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE zrp_company_transaction');
        $this->addSql('ALTER TABLE zrp_transaction DROP trx_desc, DROP trx_num');
    }
}

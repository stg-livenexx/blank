<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210415112651 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE zrp_command (id INT AUTO_INCREMENT NOT NULL, zrp_client_id INT DEFAULT NULL, cmd_number INT NOT NULL, cmd_create_at DATETIME NOT NULL, cmd_produit VARCHAR(255) NOT NULL, cmd_quantity INT NOT NULL, cmd_amount DOUBLE PRECISION NOT NULL, INDEX IDX_EAB5F92BE71B1F2 (zrp_client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE zrp_command ADD CONSTRAINT FK_EAB5F92BE71B1F2 FOREIGN KEY (zrp_client_id) REFERENCES zrp_client (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE zrp_command');
    }
}

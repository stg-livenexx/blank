<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210210093037 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE zrp_cash_register (id INT AUTO_INCREMENT NOT NULL, zrp_company_id INT DEFAULT NULL, cs_rgt_num VARCHAR(50) NOT NULL, INDEX IDX_827C247188560360 (zrp_company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE zrp_cash_register ADD CONSTRAINT FK_827C247188560360 FOREIGN KEY (zrp_company_id) REFERENCES zrp_company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_transaction ADD zrp_cash_register_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE zrp_transaction ADD CONSTRAINT FK_502ADE719B291B64 FOREIGN KEY (zrp_cash_register_id) REFERENCES zrp_cash_register (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_502ADE719B291B64 ON zrp_transaction (zrp_cash_register_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_transaction DROP FOREIGN KEY FK_502ADE719B291B64');
        $this->addSql('DROP TABLE zrp_cash_register');
        $this->addSql('DROP INDEX IDX_502ADE719B291B64 ON zrp_transaction');
        $this->addSql('ALTER TABLE zrp_transaction DROP zrp_cash_register_id');
    }
}

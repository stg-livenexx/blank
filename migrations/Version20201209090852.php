<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201209090852 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_city ADD ct_ascii VARCHAR(255) DEFAULT NULL, CHANGE ct_latitude ct_latitude NUMERIC(10, 6) DEFAULT NULL, CHANGE ct_longitude ct_longitude NUMERIC(10, 6) DEFAULT NULL');
        $this->addSql('ALTER TABLE zrp_country ADD cntr_iso3 VARCHAR(3) NOT NULL, ADD cntr_iso2 VARCHAR(2) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_city DROP ct_ascii, CHANGE ct_latitude ct_latitude NUMERIC(10, 6) NOT NULL, CHANGE ct_longitude ct_longitude NUMERIC(10, 6) NOT NULL');
        $this->addSql('ALTER TABLE zrp_country DROP cntr_iso3, DROP cntr_iso2');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210416120709 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE zrp_product_zrp_command (zrp_product_id INT NOT NULL, zrp_command_id INT NOT NULL, INDEX IDX_415ABA265A497FEC (zrp_product_id), INDEX IDX_415ABA262C2C712C (zrp_command_id), PRIMARY KEY(zrp_product_id, zrp_command_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE zrp_product_zrp_command ADD CONSTRAINT FK_415ABA265A497FEC FOREIGN KEY (zrp_product_id) REFERENCES zrp_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_product_zrp_command ADD CONSTRAINT FK_415ABA262C2C712C FOREIGN KEY (zrp_command_id) REFERENCES zrp_command (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE zrp_product_zrp_command');
    }
}

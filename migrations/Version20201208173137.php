<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201208173137 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_user ADD zrp_city_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE zrp_user ADD CONSTRAINT FK_3EF3CFBDEFD37150 FOREIGN KEY (zrp_city_id) REFERENCES zrp_city (id)');
        $this->addSql('CREATE INDEX IDX_3EF3CFBDEFD37150 ON zrp_user (zrp_city_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_user DROP FOREIGN KEY FK_3EF3CFBDEFD37150');
        $this->addSql('DROP INDEX IDX_3EF3CFBDEFD37150 ON zrp_user');
        $this->addSql('ALTER TABLE zrp_user DROP zrp_city_id');
    }
}

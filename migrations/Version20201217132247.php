<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201217132247 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_transaction DROP FOREIGN KEY FK_502ADE71C311C06A');
        $this->addSql('DROP INDEX IDX_502ADE71C311C06A ON zrp_transaction');
        $this->addSql('ALTER TABLE zrp_transaction CHANGE zrp_user_id zrp_customer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE zrp_transaction ADD CONSTRAINT FK_502ADE71B1881853 FOREIGN KEY (zrp_customer_id) REFERENCES zrp_customer (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_502ADE71B1881853 ON zrp_transaction (zrp_customer_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_transaction DROP FOREIGN KEY FK_502ADE71B1881853');
        $this->addSql('DROP INDEX IDX_502ADE71B1881853 ON zrp_transaction');
        $this->addSql('ALTER TABLE zrp_transaction CHANGE zrp_customer_id zrp_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE zrp_transaction ADD CONSTRAINT FK_502ADE71C311C06A FOREIGN KEY (zrp_user_id) REFERENCES zrp_user (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_502ADE71C311C06A ON zrp_transaction (zrp_user_id)');
    }
}

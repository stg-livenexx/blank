<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201218183521 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_transaction DROP FOREIGN KEY FK_502ADE711B66191');
        $this->addSql('ALTER TABLE zrp_transaction ADD CONSTRAINT FK_502ADE711B66191 FOREIGN KEY (zrp_folder_id) REFERENCES zrp_folder (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_transaction DROP FOREIGN KEY FK_502ADE711B66191');
        $this->addSql('ALTER TABLE zrp_transaction ADD CONSTRAINT FK_502ADE711B66191 FOREIGN KEY (zrp_folder_id) REFERENCES zrp_folder (id) ON DELETE CASCADE');
    }
}

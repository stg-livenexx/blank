<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201222150431 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_company ADD cmp_effective_min INT DEFAULT NULL, ADD cmp_effective_max INT DEFAULT NULL, ADD cmp_siren VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE zrp_customer ADD zrp_company_id INT DEFAULT NULL, DROP cst_siren');
        $this->addSql('ALTER TABLE zrp_customer ADD CONSTRAINT FK_AC5F1E9788560360 FOREIGN KEY (zrp_company_id) REFERENCES zrp_company (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_AC5F1E9788560360 ON zrp_customer (zrp_company_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_company DROP cmp_effective_min, DROP cmp_effective_max, DROP cmp_siren');
        $this->addSql('ALTER TABLE zrp_customer DROP FOREIGN KEY FK_AC5F1E9788560360');
        $this->addSql('DROP INDEX IDX_AC5F1E9788560360 ON zrp_customer');
        $this->addSql('ALTER TABLE zrp_customer ADD cst_siren VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP zrp_company_id');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201214115353 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_customer ADD zrp_customer_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE zrp_customer ADD CONSTRAINT FK_AC5F1E97EBAFFF20 FOREIGN KEY (zrp_customer_type_id) REFERENCES zrp_customer_type (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_AC5F1E97EBAFFF20 ON zrp_customer (zrp_customer_type_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_customer DROP FOREIGN KEY FK_AC5F1E97EBAFFF20');
        $this->addSql('DROP INDEX IDX_AC5F1E97EBAFFF20 ON zrp_customer');
        $this->addSql('ALTER TABLE zrp_customer DROP zrp_customer_type_id');
    }
}

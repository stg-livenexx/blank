<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201214111940 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE zrp_customer (id INT AUTO_INCREMENT NOT NULL, zrp_user_id INT DEFAULT NULL, cst_siren VARCHAR(100) DEFAULT NULL, INDEX IDX_AC5F1E97C311C06A (zrp_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE zrp_customer ADD CONSTRAINT FK_AC5F1E97C311C06A FOREIGN KEY (zrp_user_id) REFERENCES zrp_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_company ADD zrp_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE zrp_company ADD CONSTRAINT FK_2BC01AB0C311C06A FOREIGN KEY (zrp_user_id) REFERENCES zrp_user (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_2BC01AB0C311C06A ON zrp_company (zrp_user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE zrp_customer');
        $this->addSql('ALTER TABLE zrp_company DROP FOREIGN KEY FK_2BC01AB0C311C06A');
        $this->addSql('DROP INDEX IDX_2BC01AB0C311C06A ON zrp_company');
        $this->addSql('ALTER TABLE zrp_company DROP zrp_user_id');
    }
}

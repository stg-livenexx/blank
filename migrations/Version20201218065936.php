<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201218065936 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE zrp_folder (id INT AUTO_INCREMENT NOT NULL, zrp_customer_id INT DEFAULT NULL, fld_name VARCHAR(100) NOT NULL, INDEX IDX_F81936D4B1881853 (zrp_customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE zrp_folder ADD CONSTRAINT FK_F81936D4B1881853 FOREIGN KEY (zrp_customer_id) REFERENCES zrp_customer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_transaction ADD zrp_folder_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE zrp_transaction ADD CONSTRAINT FK_502ADE711B66191 FOREIGN KEY (zrp_folder_id) REFERENCES zrp_folder (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_502ADE711B66191 ON zrp_transaction (zrp_folder_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_transaction DROP FOREIGN KEY FK_502ADE711B66191');
        $this->addSql('DROP TABLE zrp_folder');
        $this->addSql('DROP INDEX IDX_502ADE711B66191 ON zrp_transaction');
        $this->addSql('ALTER TABLE zrp_transaction DROP zrp_folder_id');
    }
}

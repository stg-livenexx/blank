<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201208140652 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE zrp_city (id INT AUTO_INCREMENT NOT NULL, zrp_country_id INT DEFAULT NULL, zrp_city_id INT DEFAULT NULL, ct_name VARCHAR(255) NOT NULL, ct_latitude NUMERIC(10, 6) NOT NULL, ct_longitude NUMERIC(10, 6) NOT NULL, INDEX IDX_9E3B1BC0E6E227C6 (zrp_country_id), INDEX IDX_9E3B1BC0EFD37150 (zrp_city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_company (id INT AUTO_INCREMENT NOT NULL, zrp_company_type_id INT DEFAULT NULL, zrp_city_id INT DEFAULT NULL, zrp_company_activity_id INT DEFAULT NULL, cmp_name VARCHAR(100) NOT NULL, cmp_effective INT DEFAULT NULL, cmp_checkout_nbr INT DEFAULT NULL, cmp_zerop_checkout_nbr INT DEFAULT NULL, cmp_cp INT DEFAULT NULL, cmp_ca DOUBLE PRECISION DEFAULT NULL, cmp_date_creation DATETIME DEFAULT NULL, cmp_zerop_checkout_date DATETIME DEFAULT NULL, INDEX IDX_2BC01AB03A57868 (zrp_company_type_id), INDEX IDX_2BC01AB0EFD37150 (zrp_city_id), INDEX IDX_2BC01AB09FD1051C (zrp_company_activity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_company_activity (id INT AUTO_INCREMENT NOT NULL, cmp_act_name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_company_erp (id INT AUTO_INCREMENT NOT NULL, zrp_company_id INT DEFAULT NULL, zrp_erp_id INT DEFAULT NULL, cmp_erp_specific_api VARCHAR(255) DEFAULT NULL, INDEX IDX_DABFE55988560360 (zrp_company_id), INDEX IDX_DABFE5598C6D0F42 (zrp_erp_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_company_type (id INT AUTO_INCREMENT NOT NULL, cmp_tp_name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_country (id INT AUTO_INCREMENT NOT NULL, cntr_name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_customer_type (id INT AUTO_INCREMENT NOT NULL, cstm_tp_name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_erp (id INT AUTO_INCREMENT NOT NULL, erp_name VARCHAR(50) NOT NULL, erp_api_url VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_expense_type (id INT AUTO_INCREMENT NOT NULL, exp_tp_name VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_role (id INT AUTO_INCREMENT NOT NULL, rl_name VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_transaction (id INT AUTO_INCREMENT NOT NULL, zrp_city_biller_id INT DEFAULT NULL, zrp_city_invoiced_id INT DEFAULT NULL, zrp_company_activity_id INT DEFAULT NULL, trx_amount DOUBLE PRECISION NOT NULL, trx_issue_date DATETIME NOT NULL, trx_postal_code_invoiced INT DEFAULT NULL, trx_postal_code_biller INT DEFAULT NULL, trx_address_biller VARCHAR(100) DEFAULT NULL, INDEX IDX_502ADE711D1A52FE (zrp_city_biller_id), INDEX IDX_502ADE71F8101C51 (zrp_city_invoiced_id), INDEX IDX_502ADE719FD1051C (zrp_company_activity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zrp_user (id INT AUTO_INCREMENT NOT NULL, zrp_role_id INT DEFAULT NULL, usr_firstname VARCHAR(255) DEFAULT NULL, usr_lastname VARCHAR(255) DEFAULT NULL, usr_address VARCHAR(255) DEFAULT NULL, usr_phone_number VARCHAR(20) DEFAULT NULL, usr_token VARCHAR(255) DEFAULT NULL, usr_img_url VARCHAR(255) DEFAULT NULL, usr_date_birth DATETIME DEFAULT NULL, usr_date_registration DATETIME DEFAULT NULL, usr_date_last_connected DATETIME DEFAULT NULL, usr_date_create DATETIME DEFAULT NULL, usr_date_update DATETIME DEFAULT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, roles JSON NOT NULL, UNIQUE INDEX UNIQ_3EF3CFBDF85E0677 (username), UNIQUE INDEX UNIQ_3EF3CFBDE7927C74 (email), INDEX IDX_3EF3CFBDB27C3153 (zrp_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE zrp_city ADD CONSTRAINT FK_9E3B1BC0E6E227C6 FOREIGN KEY (zrp_country_id) REFERENCES zrp_country (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_city ADD CONSTRAINT FK_9E3B1BC0EFD37150 FOREIGN KEY (zrp_city_id) REFERENCES zrp_city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_company ADD CONSTRAINT FK_2BC01AB03A57868 FOREIGN KEY (zrp_company_type_id) REFERENCES zrp_company_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_company ADD CONSTRAINT FK_2BC01AB0EFD37150 FOREIGN KEY (zrp_city_id) REFERENCES zrp_city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_company ADD CONSTRAINT FK_2BC01AB09FD1051C FOREIGN KEY (zrp_company_activity_id) REFERENCES zrp_company_activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_company_erp ADD CONSTRAINT FK_DABFE55988560360 FOREIGN KEY (zrp_company_id) REFERENCES zrp_company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_company_erp ADD CONSTRAINT FK_DABFE5598C6D0F42 FOREIGN KEY (zrp_erp_id) REFERENCES zrp_erp (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_transaction ADD CONSTRAINT FK_502ADE711D1A52FE FOREIGN KEY (zrp_city_biller_id) REFERENCES zrp_city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_transaction ADD CONSTRAINT FK_502ADE71F8101C51 FOREIGN KEY (zrp_city_invoiced_id) REFERENCES zrp_city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_transaction ADD CONSTRAINT FK_502ADE719FD1051C FOREIGN KEY (zrp_company_activity_id) REFERENCES zrp_company_activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE zrp_user ADD CONSTRAINT FK_3EF3CFBDB27C3153 FOREIGN KEY (zrp_role_id) REFERENCES zrp_role (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_city DROP FOREIGN KEY FK_9E3B1BC0EFD37150');
        $this->addSql('ALTER TABLE zrp_company DROP FOREIGN KEY FK_2BC01AB0EFD37150');
        $this->addSql('ALTER TABLE zrp_transaction DROP FOREIGN KEY FK_502ADE711D1A52FE');
        $this->addSql('ALTER TABLE zrp_transaction DROP FOREIGN KEY FK_502ADE71F8101C51');
        $this->addSql('ALTER TABLE zrp_company_erp DROP FOREIGN KEY FK_DABFE55988560360');
        $this->addSql('ALTER TABLE zrp_company DROP FOREIGN KEY FK_2BC01AB09FD1051C');
        $this->addSql('ALTER TABLE zrp_transaction DROP FOREIGN KEY FK_502ADE719FD1051C');
        $this->addSql('ALTER TABLE zrp_company DROP FOREIGN KEY FK_2BC01AB03A57868');
        $this->addSql('ALTER TABLE zrp_city DROP FOREIGN KEY FK_9E3B1BC0E6E227C6');
        $this->addSql('ALTER TABLE zrp_company_erp DROP FOREIGN KEY FK_DABFE5598C6D0F42');
        $this->addSql('ALTER TABLE zrp_user DROP FOREIGN KEY FK_3EF3CFBDB27C3153');
        $this->addSql('DROP TABLE zrp_city');
        $this->addSql('DROP TABLE zrp_company');
        $this->addSql('DROP TABLE zrp_company_activity');
        $this->addSql('DROP TABLE zrp_company_erp');
        $this->addSql('DROP TABLE zrp_company_type');
        $this->addSql('DROP TABLE zrp_country');
        $this->addSql('DROP TABLE zrp_customer_type');
        $this->addSql('DROP TABLE zrp_erp');
        $this->addSql('DROP TABLE zrp_expense_type');
        $this->addSql('DROP TABLE zrp_role');
        $this->addSql('DROP TABLE zrp_transaction');
        $this->addSql('DROP TABLE zrp_user');
    }
}

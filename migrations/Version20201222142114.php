<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201222142114 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_transaction ADD trx_label VARCHAR(100) DEFAULT NULL, ADD trx_date_created DATETIME DEFAULT NULL, ADD trx_is_synchronise TINYINT(1) DEFAULT NULL, ADD trx_file VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE zrp_transaction DROP trx_label, DROP trx_date_created, DROP trx_is_synchronise, DROP trx_file');
    }
}

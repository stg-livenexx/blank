<?php

namespace App\Zerop\FrontOffice\FrontBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpTransaction;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpTransaction\ServiceMetierZrpTransaction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpTicketController
 * @package App\Zerop\FrontOffice\FrontBundle\Controller
 */
class ZrpTicketController extends AbstractController
{
    private $_transaction_manager;
    private $_translator;
    private $_utils_manager;

    /**
     * ZrpTicketController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpTransaction $_transaction_manager
     * @param TranslatorInterface $_translator
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpTransaction $_transaction_manager,
                                TranslatorInterface $_translator)
    {
        $this->_transaction_manager = $_transaction_manager;
        $this->_translator          = $_translator;
        $this->_utils_manager       = $_utils_manager;
    }

    /**
     * Display a page info ticket
     * @param ZrpTransaction $_transaction
     * @return string
     */
    public function index(ZrpTransaction $_transaction)
    {
        $_json_details = $_transaction->getTrxDesc() ? json_decode($_transaction->getTrxDesc()) : [];

        return $this->render('@Front/ZrpTicket/index.html.twig', [
            'transaction'  => $_transaction,
            'json_details' => $_json_details
        ]);
    }

    /**
     * Set ip view ticket ajax
     * @param Request $_request
     * @return string
     */
    public function setIpViewAjax(Request $_request)
    {
        // Récupérer les données formulaire
        $_post = $_request->request->all();

        $_response = $this->_transaction_manager->setIpView($_post);

        $_result = ['response' => $_response];

        return new JsonResponse($_result);
    }
}

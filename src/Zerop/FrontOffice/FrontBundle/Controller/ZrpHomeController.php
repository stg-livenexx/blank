<?php

namespace App\Zerop\FrontOffice\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class ZrpHomeController
 */
class ZrpHomeController extends AbstractController
{
    /**
     * Display a page home
     * @return string
     */
    public function indexAction()
    {
        return $this->render('FrontBundle:ZrpHome:index.html.twig');
    }
}

<?php

namespace App\Zerop\WebService\WebServiceBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\MetierManagerBundle\Utils\ServiceName;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

/**
 * Class WsUserController
 */
class WsUserController extends AbstractFOSRestController
{
    /**
     * Récupérer tout les pays
     * @param Request $_request
     * @return View
     */
    public function getAllPaysAction(Request $_request)
    {
        // Récupérer manager
        $_utils_manager = $this->get(ServiceName::SRV_METIER_UTILS);

        $_payss = $_utils_manager->getAllEntitiesByOrder(EntityName::GS_CITY, [], ['pyNomFr' => 'ASC']);

        return View::create($_payss, Response::HTTP_OK, []);
    }

    /**
     * Insciption utilisateur
     * @param Request $_request
     * @return View
     */
    public function inscriptionAction(Request $_request)
    {
        // Récupérer manager
        $_user_manager     = $this->get(ServiceName::SRV_METIER_USER);
        $_ws_user_manager  = $this->get(ServiceName::SRV_METIER_WS_USER);
        $_response_manager = $this->get(ServiceName::SRV_METIER_WS_RESPONSE);

        $_status_code = Response::HTTP_OK;

        // Récupérer les données formulaire
        $_post = $_request->request->all();

        if (empty($_post)) {
            $_status_code = Response::HTTP_NOT_ACCEPTABLE;

            $_message  = "NULL VALUES ARE NOT ALLOWED";
            $_response = $_response_manager->getWsResponse(false, $_message);

            return new View($_response, $_status_code);
        }

        // Vérification si l'email utilisateur est déjà membre
        $_is_membre = $_user_manager->isEmailExist($_post['user_email']);
        if ($_is_membre) {
            $_message  = "Email déjà existant";
            $_response = $_response_manager->getWsResponse(false, $_message);

            return new View($_response, $_status_code);
        }

        // Enregistrement utilisateur
        $_ws_user_manager->inscription($_post);

        // Envoie mail de confirmation
        $_user_manager->sendEmailConfirmationInscription($_post);

        $_message  = "Utilisateur a été bien ajouté";
        $_response = $_response_manager->getWsResponse(true, $_message);

        return new View($_response, $_status_code);
    }

    /**
     * Modification profil
     * @param Request $_request
     * @return View
     */
    public function modificationProfilAction(Request $_request)
    {
        // Récupérer manager
        $_ws_user_manager  = $this->get(ServiceName::SRV_METIER_WS_USER);
        $_user_manager     = $this->get(ServiceName::SRV_METIER_USER);
        $_response_manager = $this->get(ServiceName::SRV_METIER_WS_RESPONSE);

        $_status_code = Response::HTTP_OK;

        // Récupérer les données formulaire
        $_post = $_request->request->all();

        if (empty($_post)) {
            $_status_code = Response::HTTP_NOT_ACCEPTABLE;

            $_message  = "NULL VALUES ARE NOT ALLOWED";
            $_response = $_response_manager->getWsResponse(false, $_message);

            return new View($_response, $_status_code);
        }

        $_id_utilisateur = isset($_post['user_id']) ? $_post['user_id'] : null;
        if (empty($_id_utilisateur)) {
            $_message  = "Id utilisateur ne doit pas être null";
            $_response = $_response_manager->getWsResponse(false, $_message);

            return new View($_response, $_status_code);
        }

        $_user = $_user_manager->getUserById($_id_utilisateur);

        // Enregistrement utilisateur
        $_ws_user_manager->modificationProfil($_user, $_post);

        $_message  = "Utilisateur a été bien modifié";
        $_response = $_response_manager->getWsResponse(true, $_message);

        return new View($_response, $_status_code);
    }
}
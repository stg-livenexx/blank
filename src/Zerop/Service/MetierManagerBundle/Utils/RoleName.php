<?php

namespace App\Zerop\Service\MetierManagerBundle\Utils;

/**
 * Class RoleName
 * Class that contains the constant names of user roles
 */
class RoleName
{
	const ROLE_SUPER_ADMINISTRATEUR = 'ROLE_SUPERADMIN';
	const ROLE_ENTREPRISE           = 'ROLE_ENTREPRISE';
	const ROLE_CLIENT               = 'ROLE_CLIENT';

    const ID_ROLE_SUPERADMIN = 1;
    const ID_ROLE_ENTREPRISE = 2;
    const ID_ROLE_CLIENT     = 3;

    static $ROLE_TYPE = array(
        'Superadmin' => 'ROLE_SUPERADMIN',
        'Entreprise' => 'ROLE_ENTREPRISE',
        'Client'     => 'ROLE_CLIENT'
    );

    static $ROLE_LIST = array(
        RoleName::ID_ROLE_SUPERADMIN => 'ROLE_SUPERADMIN',
        RoleName::ID_ROLE_ENTREPRISE => 'ROLE_ENTREPRISE',
        RoleName::ID_ROLE_CLIENT     => 'ROLE_CLIENT'
    );
}

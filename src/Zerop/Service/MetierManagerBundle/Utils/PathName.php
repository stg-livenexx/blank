<?php

namespace App\Zerop\Service\MetierManagerBundle\Utils;

/**
 * Class PathReportingName
 * Class that contains the constant names of the reporting directories
 */
class PathName
{
    const UPLOAD_IMAGE_USER = '/upload/user/admin/image/';
    const UPLOAD_CUSTOMER = '/upload/customer/ticket/';
    const UPLOAD_IMAGE_QR_CODE = '/upload/qr_code/';
    const UPLOAD_PDF = '/upload/pdf/';
}

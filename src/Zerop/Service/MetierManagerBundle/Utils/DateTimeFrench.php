<?php

namespace App\Zerop\Service\MetierManagerBundle\Utils;


class DateTimeFrench extends \DateTime
{
    public function format($_format='j M Y'){

        $_days_full = array(
            'Monday'    => 'Lundi',
            'Tuesday'   => 'Mardi',
            'Wednesday' => 'Mercredi',
            'Thursday'  => 'Jeudi',
            'Friday'    => 'Vendredi',
            'Saturday'  => 'Samedi',
            'Sunday'    => 'Dimanche'
        );
        $_days_small = array(
            'Mon' => 'Lun',
            'Tue' => 'Mar',
            'Wed' => 'Mer',
            'Thu' => 'Jeu',
            'Fri' => 'Ven',
            'Sat' => 'Sam',
            'Sun' => 'Dim'
        );
        $_months_full = array(
            'January'   => 'Janvier',
            'February'  => 'Février',
            'March'     => 'Mars',
            'April'     => 'Avril',
            'May'       => 'Mai',
            'June'      => 'Juin',
            'July'      => 'Juillet',
            'August'    => 'Août',
            'September' => 'Septembre',
            'October'   => 'Octobre',
            'November'  => 'Novembre',
            'December'  => 'Décembre'
        );
        $_months_small = array(
            'Feb' => 'Fév',
            'Apr' => 'Avr',
            'May' => 'Mai',
            'Jun' => 'Juin',
            'Jul' => 'Juil',
            'Aug' => 'Août',
            'Dec' => 'Déc'
        );

        $_display = parent::format($_format);

        if( strstr($_format, 'l') ){
            $_display = str_replace(array_keys($_days_full), array_values($_days_full), $_display);
        }
        if( strstr($_format, 'D') ){
            $_display = str_replace(array_keys($_days_small), array_values($_days_small), $_display);
        }
        if( strstr($_format, 'F') ){
            $_display = str_replace(array_keys($_months_full), array_values($_months_full), $_display);
        }
        if( strstr($_format, 'M') ){
            $_display = str_replace(array_keys($_months_small), array_values($_months_small), $_display);
        }

        return $_display;
    }

}
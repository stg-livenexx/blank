<?php

namespace App\Zerop\Service\MetierManagerBundle\Utils;

/**
 * Class DefaultHoraire
 * @package App\Zerop\Service\MetierManagerBundle\Utils
 */
class DefaultHoraire
{
    const DEFAULT_LOCAL_SERVER_TIMEZONE = 'Europe/Paris';
}

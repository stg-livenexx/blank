<?php

namespace App\Zerop\Service\MetierManagerBundle\Utils;

/**
 * Class EntityName
 * Class that contains the constant names of all entities
 */
class EntityName
{
    const ZRP_USER = 'UserBundle:User';
    const ZRP_USER_ROLE = 'MetierManagerBundle:ZrpRole';
    const ZRP_ROLE = 'MetierManagerBundle:ZrpRole';
    const ZRP_COMPANY = 'MetierManagerBundle:ZrpCompany';
    const ZRP_COMPANY_ERP = 'MetierManagerBundle:ZrpCompanyErp';
    const ZRP_ERP = 'MetierManagerBundle:ZrpErp';
    const ZRP_COMPANY_TYPE = 'MetierManagerBundle:ZrpCompanyType';
    const ZRP_COMPANY_ACTIVITY = 'MetierManagerBundle:ZrpCompanyActivity';
    const ZRP_CUSTOMER_TYPE = 'MetierManagerBundle:ZrpCustomerType';
    const ZRP_EXPENSE_TYPE = 'MetierManagerBundle:ZrpExpenseType';
    const ZRP_TRANSACTION = 'MetierManagerBundle:ZrpTransaction';
    const ZRP_COMPANY_TRANSACTION = 'MetierManagerBundle:ZrpCompanyTransaction';
    const ZRP_CUSTOMER = 'MetierManagerBundle:ZrpCustomer';
    const ZRP_CITY = 'MetierManagerBundle:ZrpCity';
    const ZRP_FOLDER = 'MetierManagerBundle:ZrpFolder';
    const ZRP_CASH_REGISTER = 'MetierManagerBundle:ZrpCashRegister';
    const ZRP_CLIENT = 'MetierManagerBundle:ZrpClient';
    const ZRP_COMMAND = 'MetierManagerBundle:ZrpCommand';
    const ZRP_PRODUCT = 'MetierManagerBundle:ZrpProduct';
}
<?php

namespace App\Zerop\Service\MetierManagerBundle\Utils;

/**
 * Class ServiceName
 * Class that contains the constant names of the services (business)
 */
class ServiceName
{
    const SRV_METIER_UTILS = 'zrp.manager.utils';
    const SRV_METIER_USER = 'zrp.manager.user';
    const SRV_METIER_USER_UPLOAD = 'zrp.manager.user.upload';
    const SRV_METIER_DASHBOARD = 'zrp.manager.dashboard';
    const SRV_METIER_COMPANY = 'zrp.manager.company';
    const SRV_METIER_COMPANY_TYPE = 'zrp.manager.company.type';
    const SRV_METIER_COMPANY_ACTIVITY = 'zrp.manager.company.activity';
    const SRV_METIER_CITY = 'zrp.manager.city';

    // Service WebService
    const SRV_METIER_WS_RESPONSE = 'zrp.manager.ws.response';
}
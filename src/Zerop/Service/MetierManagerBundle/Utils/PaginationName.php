<?php

namespace App\Zerop\Service\MetierManagerBundle\Utils;

/**
 * Class PaginationName
 * Class that contains the numbers per page constant of pagination
 */
class PaginationName
{
    const NBR_SELECT_DISPLAYED = 10;
    const NBR_TRANSACTION_DASHBOARD_CUSTOMER = 5;
}

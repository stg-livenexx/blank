<?php


namespace App\Zerop\Service\MetierManagerBundle\Utils;


class Effective
{
    static $EFFECTIF_LIST = [
        [2,3],
        [3,5],
        [5,10],
        [10,15],
        [15,25]
    ];
}
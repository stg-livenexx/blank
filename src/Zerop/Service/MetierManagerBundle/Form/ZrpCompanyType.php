<?php

namespace App\Zerop\Service\MetierManagerBundle\Form;

use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpCompanyType
 * @package App\Gs\Service\MetierManagerBundle\Form
 */
class ZrpCompanyType extends AbstractType
{
    private $_translator;

    /**
     * ZrpCompany constructor.
     * ZrpCompanyType constructor.
     * @param TranslatorInterface $_translator
     */
    public function __construct(TranslatorInterface $_translator)
    {
        $this->_translator = $_translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cmpName', TextType::class, array(
                'label'    => $this->_translator->trans('bo.company.index.th.raison.social'),
                'required' => true
            ))
            ->add('cmpDesc', TextType::class, array(
                'label'    => $this->_translator->trans('bo.company.label.desc'),
                'required' => false
            ))
            ->add('cmpCheckoutNbr', NumberType::class, array(
                'label'    => $this->_translator->trans('bo.company.index.th.nbr.caisse'),
                'required' => true,
                'attr'     => [
                    'data-bv-regexp'                => 'true',
                    'data-bv-regexp-regexp'         => '^\+?[0-9]*$',
                    'data-bv-regexp-message'        => 'Veuillez fournir une valeur valide. ',
                    'data-bv-notempty-message'      => 'Ce champ est requis',
                    'data-bv-greaterthan'           => true,
                    'data-bv-greaterthan-inclusive' => true,
                    'data-bv-greaterthan-message'   => ' '
                ]
            ))
            ->add('cmpZeropCheckoutNbr', NumberType::class, array(
                'label'    => $this->_translator->trans('bo.company.index.th.nbr.caisse.zerop'),
                'required' => true,
                'attr'     => [
                    'data-bv-regexp'                => 'true',
                    'data-bv-regexp-regexp'         => '^\+?[0-9]*$',
                    'data-bv-regexp-message'        => 'Veuillez fournir une valeur valide. ',
                    'data-bv-notempty-message'      => 'Ce champ est requis',
                    'data-bv-greaterthan'           => true,
                    'data-bv-greaterthan-inclusive' => true,
                    'data-bv-greaterthan-message'   => ' '
                ]
            ))
            ->add('cmp_ca', TextType::class, array(
                'label'    => $this->_translator->trans('bo.company.index.th.chiffre.affaires'),
                'required' => true,
                'attr'     => [
                    'autocomplete'             => 'off',
                    'data-bv-regexp'           => 'true',
                    'data-bv-regexp-regexp'    => '[0-9]$',
                    'data-bv-regexp-message'   => 'Veuillez fournir une valeur valide. ',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ]
            ))
            ->add('cmpCp', NumberType::class, array(
                'label'    => $this->_translator->trans('bo.company.index.th.code.postal'),
                'required' => true,
                'attr'     => [
                    'data-bv-regexp'           => 'true',
                    'data-bv-regexp-regexp'    => '^\+?[0-9]*$',
                    'data-bv-regexp-message'   => 'Veuillez fournir une valeur valide. ',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ]
            ))
            ->add('cmpDateCreation', DateTimeType::class, array(
                'label'    => $this->_translator->trans('bo.company.index.th.date.creation'),
                'widget'   => 'single_text',
                'format'   => 'dd/MM/yyyy',
                'html5'    => false,
                'required' => true,
                'attr'     => array(
                    'placeholder'  => $this->_translator->trans('bo.company.index.th.date.creation'),
                    'readonly'     => true,
                    'autocomplete' => 'off'
                )
            ))
            ->add('cmpZeropCheckoutDate', DateTimeType::class, array(
                'label'    => $this->_translator->trans('bo.company.index.th.nbr.date.caisse.zerop'),
                'widget'   => 'single_text',
                'format'   => 'dd/MM/yyyy',
                'html5'    => false,
                'required' => true,
                'attr'     => array(
                    'placeholder'  => $this->_translator->trans('bo.company.index.th.nbr.date.caisse.zerop'),
                    'readonly'     => true,
                    'name'         => 'zrp_date-creation',
                    'autocomplete' => 'off'
                )
            ))
            ->add('zrpCity', TextType::class, [
                'label'  => $this->_translator->trans('bo.company.index.th.commune'),
                'attr'   => [
                    'data-bv-notempty-message' => 'Ce champ est requis',
                ],
                'mapped' => false
            ])
//            ->add('zrpCity', EntityType::class, array(
//                'label'         => $this->_translator->trans('bo.company.index.th.commune'),
//                'class'         => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity',
//                'query_builder' => function (EntityRepository $_er) {
//                    return $_er
//                        ->createQueryBuilder('ct')
//                        ->orderBy('ct.ctName', 'ASC');
//                },
//                'choice_label'  => 'ctName',
//                'attr'          => [
//                    'class'                    => 'kl-select-city',
//                    'data-bv-notempty-message' => 'Ce champ est requis'
//                ],
//
//                'multiple'    => false,
//                'expanded'    => false,
//                'required'    => true,
//                'placeholder' => $this->_translator->trans('bo.company.select.commune.placeholder.label')
//            ))
            ->add('zrpCompanyActivity', EntityType::class, array(
                'label'         => $this->_translator->trans('bo.company.select.activite.label'),
                'class'         => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyActivity',
                'query_builder' => function (EntityRepository $_er) {
                    return $_er
                        ->createQueryBuilder('cpn_actv')
                        ->orderBy('cpn_actv.cmpActName', 'ASC');
                },
                'attr'          => [
                    'class'                    => 'kl-select-two',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ],
                'choice_label'  => 'cmpActName',
                'multiple'      => false,
                'expanded'      => false,
                'required'      => true,
                'placeholder'   => $this->_translator->trans('bo.company.select.activite.placeholder.label')
            ))
            ->add('zrpCompanyType', EntityType::class, array(
                'label'         => $this->_translator->trans('bo.company.index.th.company.type'),
                'class'         => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyType',
                'query_builder' => function (EntityRepository $_er) {
                    return $_er
                        ->createQueryBuilder('cmpTp')
                        ->orderBy('cmpTp.cmpTpName', 'ASC');
                },
                'choice_label'  => 'cmpTpName',
                'attr'          => [
                    'class'                    => 'kl-select-company-type kl-select-two',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ],

                'multiple'    => false,
                'expanded'    => false,
                'required'    => true,
                'placeholder' => $this->_translator->trans('bo.company.select.company.type.placeholder.label')
            ))
            ->add('zrpUser', EntityType::class, array(
                'label'         => $this->_translator->trans('bo.user.edit.title'),
                'class'         => 'App\Zerop\Service\UserBundle\Entity\User',
                'query_builder' => function (EntityRepository $_er) {
                    return $_er
                        ->createQueryBuilder('usr')
                        ->where('usr.zrpRole = :role_entrp')
                        ->orderBy('usr.username', 'ASC')
                        ->setParameter('role_entrp', RoleName::ID_ROLE_ENTREPRISE);
                },
                'attr'          => [
                    'class'                    => 'kl-select-two',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ],
                'choice_label'  => 'username',
                'multiple'      => false,
                'expanded'      => false,
                'required'      => true,
                'placeholder'   => $this->_translator->trans('bo.company.select.user.placeholder.label')
            ))
            ->add('cmpSiren', TextType::class, array(
                'label'    => $this->_translator->trans('bo.company.index.th.numero.siren'),
                'required' => true,
                'attr'     => [
                    'data-bv-regexp'           => 'true',
                    'data-bv-regexp-regexp'    => '^\+?[0-9]*$',
                    'data-bv-regexp-message'   => 'Veuillez fournir une valeur valide (des chiffres). ',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ]
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zrp_service_metiermanagerbundle_company';
    }
}

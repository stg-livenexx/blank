<?php

namespace App\Zerop\Service\MetierManagerBundle\Form;

use Doctrine\DBAL\Types\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

/**
 * Class ZrpClientType
 * @package App\Zerop\Service\MetierManagerBundle\Form
 */
class ZrpClientType extends AbstractType
{
    /**
     * @var Translator
     */
    private $_translator;

    /**
     * ZrpClientType constructor.
     */
    public function __construct()
    {
        $this->_translator = new Translator(Locale::getDefault());
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('cltCin', IntegerType::class, array(
            'label'     =>  $this->_translator->trans('bo.client.cin'),
            'required'  =>  true,
            'attr'      =>  [
                'data-bv-notempty'          =>  'true',
                'data-bv-notempty-message'  =>   $this->_translator->trans('bo.client.cin.error.message'),
                'data-bv-regexp-regexp'     =>   '^[1-9][0-9]+$',
                'data-bv-regexp-message'    =>   $this->_translator->trans('bo.client.cin.error.message')
            ]
        ))
        ->add('cltLastName', TextType::class, array(
            'label'     =>  $this->_translator->trans('bo.client.last.name'),
            'required'  =>  true,
            'attr'      =>  [
                'data-bv-notempty'          =>  'true',
                'data-bv-notempty-message'  =>   $this->_translator->trans('bo.client.last.name.error.message'),
                'data-bv-regexp-regexp'     =>   '[a-zA-Z]+',
                'data-bv-regexp-message'    =>   $this->_translator->trans('bo.client.last.name.error.message')
            ]
        ))
        ->add('cltFirstName', TextType::class, array(
            'label'     =>  $this->_translator->trans('bo.client.first.name'),
            'required'  =>  true,
            'attr'      =>  [
                'data-bv-notempty'          =>  'true',
                'data-bv-notempty-message'  =>   $this->_translator->trans('bo.client.first.name.error.message'),
                'data-bv-regexp-regexp'     =>   '[a-zA-Z]+',
                'data-bv-regexp-message'    =>   $this->_translator->trans('bo.client.first.name.error.message')
            ]
        ))
        ->add('cltPhoneNumber', TextType::class, array(
            'label'     =>  $this->_translator->trans('bo.client.phone.number'),
            'required'  =>  true,
            'attr'      =>  [
                'data-bv-notempty'          =>  'true',
                'data-bv-notempty-message'  =>   $this->_translator->trans('bo.client.phone.number.error.message'),
                'data-bv-regexp-regexp'     =>   '[0-9]+',
                'data-bv-regexp-message'    =>   $this->_translator->trans('bo.client.phone.number.error.message')
            ]
        ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpClient',
            'is_edit'    => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zrp_adminbundle_client';
    }
}
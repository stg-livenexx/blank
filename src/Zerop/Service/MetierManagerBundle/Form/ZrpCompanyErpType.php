<?php

namespace App\Zerop\Service\MetierManagerBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class ZrpCompanyErpType extends AbstractType
{
    private $_translator;

    /**
     * ZrpCompanyActivityType constructor.
     */
    public function __construct()
    {
        $this->_translator = new Translator(\Locale::getDefault());
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cmpErpSpecificApi', TextType::class, [
                'label'    => $this->_translator->trans("bo.company.erp.company.erp.specific.api"),
                'required' => true
            ])
            ->add('zrpCompany', EntityType::class, [
                'label'         => $this->_translator->trans('bo.company.erp.index.th.company'),
                'class'         => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany',
                'query_builder' => function (EntityRepository $_er) {
                    return $_er
                        ->createQueryBuilder('cmp')
                        ->orderBy('cmp.id', 'ASC');
                },
                'choice_label'  => 'cmpName',
                'attr'          => [
                    'class'                    => 'kl-select-two',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ],

                'multiple'    => false,
                'expanded'    => false,
                'required'    => true,
                'placeholder' => $this->_translator->trans('bo.company.erp.select.company.placeholder.label')
            ])
            ->add('zrpErp', EntityType::class, [
                'label'         => $this->_translator->trans('bo.company.erp.index.th.erp'),
                'class'         => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpErp',
                'query_builder' => function (EntityRepository $_er) {
                    return $_er
                        ->createQueryBuilder('erp')
                        ->orderBy('erp.id', 'ASC');
                },
                'choice_label'  => 'erpName',
                'attr'          => [
                    'class'                    => 'kl-select-two',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ],

                'multiple'    => false,
                'expanded'    => false,
                'required'    => true,
                'placeholder' => $this->_translator->trans('bo.company.erp.select.erp.placeholder.label')
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyErp'
        ));
    }
}
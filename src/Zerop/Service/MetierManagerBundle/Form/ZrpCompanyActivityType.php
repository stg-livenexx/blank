<?php

namespace App\Zerop\Service\MetierManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

/**
 * Class ZrpCompanyActivityType
 * @package App\Zerop\Service\MetierManagerBundle\Form
 */
class ZrpCompanyActivityType extends AbstractType
{
    private $_translator;

    /**
     * ZrpCompanyActivityType constructor.
     */
    public function __construct()
    {
        $this->_translator = new Translator(\Locale::getDefault());
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cmpActName', TextType::class, [
                'label'    => $this->_translator->trans("bo.company.activity.label"),
                'required' => true
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyActivity'
        ]);
    }
}

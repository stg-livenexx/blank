<?php

namespace App\Zerop\Service\MetierManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

/**
 * Class ZrpExpenseTypeType
 * @package App\Zerop\Service\MetierManagerBundle\Form
 */
class ZrpExpenseTypeType extends AbstractType
{
    private $_translator;

    /**
     * PpProductType constructor.
     */
    public function __construct()
    {
        $this->_translator = new Translator(\Locale::getDefault());
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expTpName', TextType::class, array(
                'label'    => $this->_translator->trans('bo.expense.type.libelle'),
                'required' => true
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpExpenseType'
        ));
    }
}
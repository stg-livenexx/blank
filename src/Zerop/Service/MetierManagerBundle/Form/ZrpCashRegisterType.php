<?php


namespace App\Zerop\Service\MetierManagerBundle\Form;

use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class ZrpCashRegisterType extends AbstractType
{
    private $_translator;

    /**
     * ZrpErpType constructor.
     */
    public function __construct()
    {
        $this->_translator = new Translator(\Locale::getDefault());
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user_role = $options['user_role'];
        if ($this->user_role == RoleName::ID_ROLE_ENTREPRISE) {
            $builder
                ->
                add('csRgtNum', TextType::class, [
                    'label'    => "Numéro de caisse",
                    'required' => true,
                    'attr'     => [
                        'data-bv-notempty-message' => 'Ce champ est requis',
                        'maxlength'                => '50'
                    ]
                ])
                ->add('csRgtName', TextType::class, [
                    'label'    => "Nom de caisse",
                    'required' => true,
                    'attr'     => [
                        'data-bv-notempty-message' => 'Ce champ est requis',
                        'maxlength'                => '100'
                    ]
                ]);
        }
        if ($this->user_role == RoleName::ID_ROLE_SUPERADMIN) {
            $builder
                ->
                add('csRgtNum', TextType::class, [
                    'label'    => "Numéro de caisse",
                    'required' => true,
                    'attr'     => [
                        'data-bv-notempty-message' => 'Ce champ est requis',
                        'maxlength'                => '50'
                    ]
                ])
                ->add('csRgtName', TextType::class, [
                    'label'    => "Nom de caisse",
                    'required' => true,
                    'attr'     => [
                        'data-bv-notempty-message' => 'Ce champ est requis',
                        'maxlength'                => '100'
                    ]
                ])
                ->add('zrpCompany', EntityType::class, [
                    'label'         => "Nom de l'entreprise",
                    'required'      => true,
                    'class'         => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany',
                    'query_builder' => function (EntityRepository $_er) {
                        return $_er
                            ->createQueryBuilder('cmp')
                            ->leftJoin('cmp.zrpCompanyType', 'cmp_type')
                            ->join('cmp.zrpCompanyActivity', 'cmp_activity')
                            ->leftJoin('cmp.zrpCompanyErp', 'cmp_erp')
                            ->leftJoin('cmp_erp.zrpErp', 'erp')
                            ->join('cmp.zrpUser','usr')
                            ->orderBy('cmp.cmpName', 'DESC');
                    },
                    'choice_label'  => 'cmpName',
                    'attr'          => [
                        'class'                    => 'kl-select-two',
                        'data-bv-notempty-message' => 'Ce champ est requis'
                    ],
                    'placeholder'   => $this->_translator->trans('bo.company.index.select.placeholder.name')
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCashRegister',
            'user_role'  => null,
            'is_edit'    => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zrp_adminbundle_erp';
    }
}
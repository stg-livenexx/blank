<?php

namespace App\Zerop\Service\MetierManagerBundle\Form;

use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpClientFinalType
 * @package App\Gs\Service\UserBundle\Form
 */
class ZrpClientFinalType extends AbstractType
{
    private $_translator;

    /**
     * ZrpClientFinalType constructor.
     * @param TranslatorInterface $_translator
     */
    public function __construct(TranslatorInterface $_translator)
    {
        $this->_translator = $_translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user_has_customer = $options['user_has_customer'];
        $builder
            ->add('zrpUser', EntityType::class, array(
                'label'         => $this->_translator->trans('bo.user.edit.title'),
                'class'         => 'App\Zerop\Service\UserBundle\Entity\User',
                'query_builder' => function (EntityRepository $_er) {
                    $_query_builder = $_er->createQueryBuilder('usr')
                        ->where('usr.zrpRole = :role_clnt');
                    if ($this->user_has_customer) {
                        $_query_builder->andWhere('usr.id NOT IN (:user_id)')
                            ->setParameter('user_id', $this->user_has_customer);
                    }
                    $_query_builder->orderBy('usr.username', 'ASC')
                        ->setParameter('role_clnt', RoleName::ID_ROLE_CLIENT);

                    return $_query_builder;
                },
                'attr'          => [
                    'class'                    => 'kl-select-two',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ],
                'choice_label'  => 'UsrFullname',
                'multiple'      => false,
                'expanded'      => false,
                'required'      => true,
                'placeholder'   => $this->_translator->trans('bo.company.select.user.placeholder.label')
            ))
            ->add('zrpCustomerType', EntityType::class, array(
                'label'         => $this->_translator->trans('bo.customer.type.index.title'),
                'class'         => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCustomerType',
                'query_builder' => function (EntityRepository $_er) {
                    return $_er
                        ->createQueryBuilder('ct');
                },
                'attr'          => [
                    'class'                    => 'kl-select-two',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ],
                'choice_label'  => 'cstmTpName',
                'multiple'      => false,
                'expanded'      => false,
                'required'      => true,
                'placeholder'   => $this->_translator->trans('bo.customer.type.index.title')
            ))
            ->add('cstSiren', NumberType::class, [
                'label'    => $this->_translator->trans('bo.customer.siren.number'),
                'required' => true
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCustomer',
            'user_has_customer' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zrp_service_metiermanagerbundle_client_final';

    }
}

<?php


namespace App\Zerop\Service\MetierManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class ZrpErpType extends AbstractType
{
    private $_translator;

    /**
     * ZrpErpType constructor.
     */
    public function __construct()
    {
        $this->_translator = new Translator(\Locale::getDefault());
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('erpName', TextType::class, [
                'label'    => $this->_translator->trans("bo.erp.name.label"),
                'required' => true,
                'attr'     => ['data-bv-notempty-message' => 'Ce champ est requis']
            ])
            ->add('erpApiUrl', TextType::class, [
                'label'    => $this->_translator->trans("bo.api.url.label"),
                'required' => true,
                'attr'     => [
                    'data-bv-notempty-message' => 'Ce champ est requis',
                    'data-bv-regexp'           => 'true',
                    'data-bv-regexp-regexp'    => 'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)',
                    'data-bv-regexp-message'   => 'Veuillez fournir un url valide.']
            ])
            ->add('erpApiDatabase', TextType::class, [
                'label'    => $this->_translator->trans("bo.erp.database.label"),
                'required' => true
            ])
            ->add('erpApiUserEmail', EmailType::class, [
                'label'    => $this->_translator->trans("bo.api.user.email.label"),
                'attr'     => [
                    'data-bv-notempty-message' => 'Ce champ est requis',
                    'data-bv-regexp'           => 'true',
                    'data-bv-regexp-regexp'    => '[^@]+@[^@]+\.[a-zA-Z]{2,}',
                    'data-bv-regexp-message'   => 'Email invalide'],
                'required' => true
            ])
            ->add('erpApiUserPwd', RepeatedType::class, [
                'type'            => PasswordType::class,
                'first_options'   => [
                    'label' => $this->_translator->trans('bo.api.user.pwd.label'),
                    'attr'  => [
                        'minlength'                    => 6,
                        'data-bv-identical'            => "false",
                        'data-bv-identical-field'      => 'zrp_adminbundle_erp[erpApiUserPwd][second]',
                        'data-bv-stringlength-message' => 'Ce champ doit comporter plus de 6 caractères',
                        "data-bv-identical-message"    => 'Les deux mots de passe ne sont pas identiques.',
                        'data-bv-notempty-message'     => 'Ce champ est requis'
                    ]
                ],
                'second_options'  => [
                    'label' => $this->_translator->trans("bo.user.field.password.confirm"),
                    'attr'  => [
                        'data-bv-identical'         => "true",
                        'data-bv-identical-field'   => 'zrp_adminbundle_erp[erpApiUserPwd][first]',
                        'data-bv-notempty-message'  => 'Ce champ est requis',
                        "data-bv-identical-message" => 'Les deux mots de passe ne sont pas identiques.',
                    ]
                ],
                'invalid_message' => 'Confirmation mot de passe incorrect'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpErp',
            'is_edit'    => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zrp_adminbundle_erp';
    }
}
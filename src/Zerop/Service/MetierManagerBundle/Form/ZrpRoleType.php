<?php

namespace App\Zerop\Service\MetierManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class ZrpRoleType
 * @package App\Zerop\Service\MetierManagerBundle\Form
 */
class ZrpRoleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rlName', TextType::class, array(
                'label'    => "Libellé",
                'required' => true
            ))
            ->add('rlDescription', TextType::class, array(
                'label'    => "Description",
                'required' => true
            )
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpRole'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zrp_service_metiermanagerbundle_role';
    }
}

<?php

namespace App\Zerop\Service\MetierManagerBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

/**
 * Class ZrpTicketType
 * @package App\Zerop\Service\MetierManagerBundle\Form
 */
class ZrpTicketType extends AbstractType
{
    private $_translator;

    /**
     * ZrpCustomerTypeType constructor.
     */
    public function __construct()
    {
        $this->_translator = new Translator(\Locale::getDefault());
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('trxLabel', TextType::class, array(
                'label'    => $this->_translator->trans('bo.customer.type.label'),
                'required' => true,
                'attr'     => [
                    'maxlength' => '150',
                ]
            ))
            ->add('trxAmount', TextType::class, array(
                'label'    => $this->_translator->trans('bo.transaction.montant'),
                'required' => true,
                'attr'     => [
                    'maxlength'                => '99',
                    'data-bv-regexp'           => 'true',
                    'data-bv-regexp-regexp'    => '[0-9]$',
                    'data-bv-regexp-message'   => 'Veuillez fournir une valeur valide. ',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ]
            ))
            ->add('trxPostalCodeInvoiced', TextType::class, array(
                'label'    => $this->_translator->trans('bo.transaction.code.postal.facture'),
                'required' => true,
                'attr'     => [
                    'maxlength'                => "100",
                    'data-bv-regexp'           => 'true',
                    'data-bv-regexp-regexp'    => '^\+?[0-9]*$',
                    'data-bv-regexp-message'   => 'Veuillez fournir une valeur valide. ',
                    'data-bv-notempty-message' => 'Ce champ est requis',
                ]
            ))
            ->add('trxIssueDate', DateTimeType::class, array(
                'label'    => $this->_translator->trans('bo.transaction.date.heure.emission'),
                'widget'   => 'single_text',
                'format'   => 'dd/MM/yyyy H:m',
                'html5'    => false,
                'required' => true,
                'attr'     => array(
                    'placeholder'            => $this->_translator->trans('bo.transaction.date.heure.emission'),
                    'readonly'               => true,
                    'data-bv-regexp'         => 'true',
                    'data-bv-regexp-regexp'  => '^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d [012]{0,1}[0-9]:[0-6][0-9]$',
                    'data-bv-regexp-message' => 'Veuillez fournir une format de date valide dd/mm/yyyy H:m ',
                )
            ))
            ->add('trxFile', FileType::class, array(
                'label'    => $this->_translator->trans('bo.transaction.ticket.fichier'),
                'mapped'   => false,
                'attr'     => array('accept' => 'image/pdf,image/jpeg,application/pdf'),
                'required' => true,
            ))
            ->add('zrpCashRegister', EntityType::class, [
                'label'         => $this->_translator->trans('bo.cash.register.title'),
                'class'         => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCashRegister',
                'query_builder' => function (EntityRepository $_er) {
                    return $_er
                        ->createQueryBuilder('cr')
                        ->orderBy('cr.csRgtNum', 'DESC');
                },
                'choice_label'  => 'csRgtNum',
                'required'      => true,
                'attr'          => [
                    'class'                    => 'kl-select-two',
                    'data-bv-notempty-message' => 'Ce champ est requis'
                ],
                'placeholder'   => $this->_translator->trans('bo.cash.register.select.placeholder.label')
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpTransaction'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zrp_service_metiermanagerbundle_ticket';
    }
}
<?php

namespace App\Zerop\Service\MetierManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpFolderType
 * @package App\Gs\Service\MetierManagerBundle\Form
 */
class ZrpFolderType extends AbstractType
{
	private $_translator;

	/**
	 * ZrpCompany constructor.
	 * ZrpFolderType constructor.
	 * @param TranslatorInterface $_translator
	 */
	public function __construct(TranslatorInterface $_translator)
	{
		$this->_translator = $_translator;
	}
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder

			->add('fldName', TextType::class, array(
				'label'    => $this->_translator->trans('bo.folder.nom.dossier.label'),
				'required' => true
			))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
								   'data_class' => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpFolder'
							   ));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix()
	{
		return 'zrp_service_metiermanagerbundle_folder';
	}
}

<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ZrpFolder
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="zrp_folder")
 */
class ZrpFolder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fld_name", type="string", length=100, nullable=false)
     */
    private $fldName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fld_updated_date", type="datetime", nullable=false)
     */
    private $fldUpdatedDate;

    /**
     * ZrpFolder constructor.
     */
    public function __construct()
    {
        $this->fldUpdatedDate = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getFldUpdatedDate()
    {
        return $this->fldUpdatedDate;
    }

    /**
     * @param \DateTime $fldUpdatedDate
     */
    public function setFldUpdatedDate($fldUpdatedDate)
    {
        $this->fldUpdatedDate = $fldUpdatedDate;
    }

    /**
     * @var ZrpCustomer
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCustomer")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_customer_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCustomer;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFldName()
    {
        return $this->fldName;
    }

    /**
     * @param string $fldName
     */
    public function setFldName($fldName)
    {
        $this->fldName = $fldName;
    }

    /**
     * @return ZrpCustomer
     */
    public function getZrpCustomer()
    {
        return $this->zrpCustomer;
    }

    /**
     * @param ZrpCustomer $zrpCustomer
     */
    public function setZrpCustomer($zrpCustomer)
    {
        $this->zrpCustomer = $zrpCustomer;
    }
}

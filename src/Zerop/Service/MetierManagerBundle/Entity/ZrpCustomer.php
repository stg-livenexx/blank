<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class ZrpCustomerType
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity(repositoryClass="App\Zerop\Service\MetierManagerBundle\Repository\ZrpCustomerRepository")
 * @ORM\Table(name="zrp_customer")
 *
 */
class ZrpCustomer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ZrpUser
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_user_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpUser;

    /**
     * @var ZrpCustomerType
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCustomerType")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_customer_type_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCustomerType;


    /**
     * @var ZrpCompany
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_company_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCompany;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return ZrpUser
     */
    public function getZrpUser()
    {
        return $this->zrpUser;
    }

    /**
     * @param ZrpUser $zrpUser
     */
    public function setZrpUser($zrpUser)
    {
        $this->zrpUser = $zrpUser;
    }

    /**
     * @return ZrpCustomerType
     */
    public function getZrpCustomerType()
    {
        return $this->zrpCustomerType;
    }

    /**
     * @param ZrpCustomerType $zrpCustomerType
     */
    public function setZrpCustomerType($zrpCustomerType)
    {
        $this->zrpCustomerType = $zrpCustomerType;
    }

    /**
     * @return ZrpCompany
     */
    public function getZrpCompany()
    {
        return $this->zrpCompany;
    }

    /**
     * @param ZrpCompany $zrpCompany
     */
    public function setZrpCompany($zrpCompany)
    {
        $this->zrpCompany = $zrpCompany;
    }
}
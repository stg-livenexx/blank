<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class ZrpExpenseType
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 * @UniqueEntity(
 *     fields={"expTpName"},
 *     errorPath="port",
 *     message="Cet nom existe déjà"
 * )
 * @ORM\Entity
 * @ORM\Table(name="zrp_expense_type")
 */
class ZrpExpenseType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="exp_tp_name", type="string", length=50, nullable=true)
     */
    private $expTpName;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getExpTpName()
    {
        return $this->expTpName;
    }

    /**
     * @param string $expTpName
     */
    public function setExpTpName($expTpName)
    {
        $this->expTpName = $expTpName;
    }
}

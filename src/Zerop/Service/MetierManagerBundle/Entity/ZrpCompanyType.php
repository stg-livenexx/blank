<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class ZrpCompanyTypeType
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 * @UniqueEntity(
 *     fields={"cmpTpName"},
 *     errorPath="port",
 *     message="Le nom du type d'entreprise existe déjà"
 * )
 * @ORM\Entity
 * @ORM\Table(name="zrp_company_type")
 */
class ZrpCompanyType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cmp_tp_name", type="string", length=50, nullable=false)
     */
    private $cmpTpName;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCmpTpName()
    {
        return $this->cmpTpName;
    }

    /**
     * @param string $cmpTpName
     */
    public function setCmpTpName($cmpTpName)
    {
        $this->cmpTpName = $cmpTpName;
    }
}

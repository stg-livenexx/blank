<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class ZrpCommand
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity(repositoryClass="App\Zerop\Service\MetierManagerBundle\Repository\ZrpCommandRepository")
 * @ORM\Table(name="zrp_command")
 */
class ZrpCommand
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=false)
     *
     * @Groups("command:read")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cmd_number", type="integer", nullable=false)
     * @Groups("command:read")
     */
    private $cmdNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cmd_create_at", type="datetime", nullable=false)
     * @Groups("command:read")
     */
    private $cmdCreateAt;

    /**
     * @var float
     *
     * @ORM\Column(name="cmd_amount", type="float", nullable=false)
     * @Groups("command:read")
     */
    private $cmdAmount;

    /**
     * @var ZrpClient
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpClient", inversedBy="zrpCommands", fetch="EAGER")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_client_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Groups("command:read")
     */
    private $zrpClient;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpProduct", mappedBy="zrpCommands", cascade={"persist"})
     * @Groups("command:read")
     */
    private $zrpProducts;

    /**
     * @var string
     *
     * @ORM\Column(name="cmd_description", type="string", nullable=true)
     * @Groups("command:read")
     */
    private $cmdDescription;

    /**
     * ZrpCommand constructor.
     */
    public function __construct()
    {
        $this->zrpProducts = new ArrayCollection();
    }

    /**
     * get id commande client
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * get command number
     * @return string
     */
    public function getCmdNumber()
    {
        return $this->cmdNumber;
    }

    /**
     * modify command number
     * @param $cmdNumber
     */
    public function setCmdNumber($cmdNumber)
    {
        $this->cmdNumber = $cmdNumber;
    }

    /**
     * get command create at
     * @return \DateTime
     */
    public function getCmdCreateAt()
    {
        return $this->cmdCreateAt;
    }

    /**
     * modify command create at
     * @param $cmdCreateAt
     */
    public function setCmdCreateAt($cmdCreateAt)
    {
        //$cmdCreateAt = DateTime::createFromFormat('Y-m-d', new DateTime($cmdCreateAt));
        $cmdCreateAt = str_replace("/","-",$cmdCreateAt);
        $this->cmdCreateAt = new DateTime($cmdCreateAt);
    }

    /**
     * get commande amount
     * @return float
     */
    public function getCmdAmount()
    {
        return $this->cmdAmount;
    }

    /**
     * modify commande amount
     * @param $cmdAmount
     */
    public function setCmdAmount($cmdAmount)
    {
        $this->cmdAmount = $cmdAmount;
    }

    /**
     * @return ZrpClient
     */
    public function getZrpClient()
    {
        return $this->zrpClient;
    }

    /**
     * @param ZrpClient $zrpClient
     */
    public function setZrpClient(ZrpClient $zrpClient)
    {
        $this->zrpClient = $zrpClient;
    }

    /**
     * @return ArrayCollection
     */
    public function getZrpProducts()
    {
        return $this->zrpProducts;
    }

    /**
     * @param mixed $zrpProducts
     */
    public function setZrpProducts($zrpProducts)
    {
        $this->zrpProducts = $zrpProducts;
    }

    /**
     * @param ZrpProduct $zrpProduct
     * @return $this
     */
    public function addZrpProduct(ZrpProduct $zrpProduct)
    {
        if(!$this->zrpProducts->contains($zrpProduct))
        {
            $this->zrpProducts[] = $zrpProduct;
            $zrpProduct->addZrpCommand($this);
        }

        return $this;
    }

    /**
     * @param ZrpProduct $zrpProduct
     * @return $this
     */
    public function removeZrpProduct(ZrpProduct $zrpProduct)
    {
        if($this->zrpProducts->contains($zrpProduct))
        {
            $this->zrpProducts->removeElement($zrpProduct);
            $zrpProduct->removeZrpCommand($this);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getCmdDescription()
    {
        return $this->cmdDescription;
    }

    /**
     * @param $cmdDescription
     */
    public function setCmdDescription($cmdDescription)
    {
        $this->cmdDescription = $cmdDescription;
    }
}
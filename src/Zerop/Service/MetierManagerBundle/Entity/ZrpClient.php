<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class ZrpClient
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity(repositoryClass="App\Zerop\Service\MetierManagerBundle\Repository\ZrpClientRepository")
 * @ORM\Table(name="zrp_client")
 */
class ZrpClient
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @Groups({"client:read","command:read"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="clt_cin", type="string", nullable=false)
     * @Groups({"client:read","command:read"})
     */
    private $cltCin;

    /**
     * @var string
     *
     * @ORM\Column(name="clt_last_name", type="string", nullable=false)
     * @Groups({"client:read","command:read"})
     */
    private $cltLastName;

    /**
     * @var string
     *
     * @ORM\Column(name="clt_first_name", type="string", nullable=true)
     * @Groups({"client:read","command:read"})
     */
    private $cltFirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="clt_phone_number", type="string", nullable=true)
     * @Groups({"client:read","command:read"})
     */
    private $cltPhoneNumber;

    /**
     * @var mixed
     *
     * @ORM\OneToMany(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCommand", mappedBy="zrpClient", orphanRemoval=true)
     *  @Groups("client:read")
     */
    private $zrpCommands;

    /**
     * Get client id
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get client CIN
     * @return string
     */
    public function getCltCin(): string
    {
        return $this->cltCin;
    }

    /**
     * Modify CIN client
     * @param $cltCin
     */
    public function setCltCin($cltCin): void
    {
        $this->cltCin = $cltCin;
    }

    /**
     * Get client lastname
     * @return string
     */
    public function getCltLastName(): string
    {
        return $this->cltLastName;
    }

    /**
     * Modify last name client
     * @param $lastName
     */
    public function setCltLastName($cltLastName): void
    {
        $this->cltLastName = $cltLastName;
    }

    /**
     * Get first name client
     * @return string
     */
    public function getCltFirstName(): string
    {
        return $this->cltFirstName;
    }

    /**
     * Modify first name client
     * @param $cltFirstName
     */
    public function setCltFirstName($cltFirstName): void
    {
        $this->cltFirstName = $cltFirstName;
    }

    /**
     * Get phone number client
     * @return string
     */
    public function getCltPhoneNumber(): string
    {
        return $this->cltPhoneNumber;
    }

    public function setCltPhoneNumber($cltPhoneNumber): void
    {
        $this->cltPhoneNumber = $cltPhoneNumber;
    }

    /**
     * @return ArrayCollection
     */
    public function getZrpCommands()
    {
        return $this->zrpCommands;
    }

    /**
     * @param mixed $zrpCommands
     */
    public function setZrpCommands($zrpCommands)
    {
        $this->zrpCommands = $zrpCommands;
    }
}

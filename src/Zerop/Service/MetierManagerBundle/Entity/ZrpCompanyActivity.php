<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class ZrpCompanyActivityType
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 * @UniqueEntity(
 *     fields={"cmpActName"},
 *     errorPath="port",
 *     message="L'activité de l'entreprise existe déjà"
 * )
 * @ORM\Entity(repositoryClass="App\Zerop\Service\MetierManagerBundle\Repository\ZrpCompanyActivityRepository")
 * @ORM\Table(name="zrp_company_activity")
 */
class ZrpCompanyActivity
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cmp_act_name", type="string", length=50, nullable=false)
     */
    private $cmpActName;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCmpActName()
    {
        return $this->cmpActName;
    }

    /**
     * @param string $cmpActName
     */
    public function setCmpActName($cmpActName)
    {
        $this->cmpActName = $cmpActName;
    }
}

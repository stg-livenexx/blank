<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ZrpCompanyErp
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="zrp_company_erp")
 */
class ZrpCompanyErp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cmp_erp_specific_api", type="string", length=255, nullable=true)
     */
    private $cmpErpSpecificApi;

    /**
     * @var ZrpCompany
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany", inversedBy="zrpCompanyErp")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_company_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCompany;

    /**
     * @var ZrpErp
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpErp")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_erp_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpErp;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCmpErpSpecificApi()
    {
        return $this->cmpErpSpecificApi;
    }

    /**
     * @param string $cmpErpSpecificApi
     */
    public function setCmpErpSpecificApi($cmpErpSpecificApi)
    {
        $this->cmpErpSpecificApi = $cmpErpSpecificApi;
    }

    /**
     * @return ZrpCompany
     */
    public function getZrpCompany()
    {
        return $this->zrpCompany;
    }

    /**
     * @param ZrpCompany $zrpCompany
     */
    public function setZrpCompany($zrpCompany)
    {
        $this->zrpCompany = $zrpCompany;
    }

    /**
     * @return ZrpErp
     */
    public function getZrpErp()
    {
        return $this->zrpErp;
    }

    /**
     * @param ZrpErp $zrpErp
     */
    public function setZrpErp($zrpErp)
    {
        $this->zrpErp = $zrpErp;
    }
}

<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class ZrpErp
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="zrp_erp")
 * @UniqueEntity(
 *     fields={"erpApiUrl"},
 *     errorPath="port",
 *     message="Cette URL existe déjà"
 * )
 */
class ZrpErp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="erp_name", type="string", length=50, nullable=false)
     */
    private $erpName;

    /**
     * @var string
     * @ORM\Column(name="erp_api_url", type="string", length=255, nullable=false)
     */
    private $erpApiUrl;

    /**
     * @var string
     * @ORM\Column(name="erp_api_database", type="string", length=50, nullable=false)
     */
    private $erpApiDatabase;

    /**
     * @var string
     * @ORM\Column(name="erp_api_user_email", type="string", length=100, nullable=false)
     * @Assert\Email()
     */
    private $erpApiUserEmail;

    /**
     * @var string
     * @ORM\Column(name="erp_api_user_pwd", type="string", length=100)
     */
    private $erpApiUserPwd;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getErpName()
    {
        return $this->erpName;
    }

    /**
     * @param string $erpName
     */
    public function setErpName($erpName)
    {
        $this->erpName = $erpName;
    }

    /**
     * @return string
     */
    public function getErpApiUrl()
    {
        return $this->erpApiUrl;
    }

    /**
     * @param string $erpApiUrl
     */
    public function setErpApiUrl($erpApiUrl)
    {
        $this->erpApiUrl = $erpApiUrl;
    }

    /**
     * @return string
     */
    public function getErpApiDatabase(): ?string
    {
        return $this->erpApiDatabase;
    }

    /**
     * @param string $erpApiDatabase
     */
    public function setErpApiDatabase(string $erpApiDatabase): void
    {
        $this->erpApiDatabase = $erpApiDatabase;
    }

    /**
     * @return null|string
     */
    public function getErpApiUserEmail(): ?string
    {
        return $this->erpApiUserEmail;
    }

    /**
     * @param string $erpApiUserEmail
     */
    public function setErpApiUserEmail(string $erpApiUserEmail): void
    {
        $this->erpApiUserEmail = $erpApiUserEmail;
    }

    /**
     * @return null|string
     */
    public function getErpApiUserPwd(): ?string
    {
        return $this->erpApiUserPwd;
    }

    /**
     * @param $erpApiUserPwd
     */
    public function setErpApiUserPwd($erpApiUserPwd): void
    {
        $this->erpApiUserPwd = $erpApiUserPwd;
    }
}

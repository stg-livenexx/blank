<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ZrpCashRegister
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="zrp_cash_register")
 */
class ZrpCashRegister
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cs_rgt_num", type="string", length=50)
     */
    private $csRgtNum;

    /**
     * @var string
     *
     * @ORM\Column(name="cs_rgt_name", type="string", length=100, nullable=true)
     */
    private $csRgtName;

    /**
     * @var ZrpCompany
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_company_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCompany;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCsRgtNum()
    {
        return $this->csRgtNum;
    }

    /**
     * @param string $csRgtNum
     */
    public function setCsRgtNum($csRgtNum)
    {
        $this->csRgtNum = $csRgtNum;
    }

    /**
     * @return ZrpCompany
     */
    public function getZrpCompany()
    {
        return $this->zrpCompany;
    }

    /**
     * @param ZrpCompany $zrpCompany
     */
    public function setZrpCompany($zrpCompany)
    {
        $this->zrpCompany = $zrpCompany;
    }

    /**
     * @return string
     */
    public function getCsRgtName()
    {
        return $this->csRgtName;
    }

    /**
     * @param string $csRgtName
     */
    public function setCsRgtName($csRgtName)
    {
        $this->csRgtName = $csRgtName;
    }
}
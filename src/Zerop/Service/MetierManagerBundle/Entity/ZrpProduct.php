<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class ZrpProduct
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity(repositoryClass="App\Zerop\Service\MetierManagerBundle\Repository\ZrpProductRepository")
 * @ORM\Table(name="zrp_product")
 */
class ZrpProduct
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @Groups("command:read")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pd_libelle", type="string", nullable=false)
     * @Groups("command:read")
     */
    private $pdLibelle;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCommand", inversedBy="zrpProducts", fetch="EXTRA_LAZY")
     */
    private $zrpCommands;

    public function __construct()
    {
        $this->zrpCommands = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPdLibelle()
    {
        return $this->pdLibelle;
    }

    /**
     * @param $pdLibelle
     */
    public function setPdLibelle($pdLibelle)
    {
        $this->pdLibelle = $pdLibelle;
    }

    /**
     * @return ArrayCollection
     */
    public function getZrpCommands()
    {
        return $this->zrpCommands;
    }

    /**
     * @param $zrpCommands
     */
    public function setZrpCommands($zrpCommands)
    {
        $this->zrpCommands = $zrpCommands;
    }

    /**
     * @param ZrpCommand $zrpCommand
     * @return $this
     */
    public function addZrpCommand(ZrpCommand $zrpCommand)
    {
        if(!$this->zrpCommands->contains($zrpCommand))
        {
            $this->zrpCommands[] = $zrpCommand;
            $zrpCommand->addZrpProduct($this);
        }

        return $this;
    }

    /**
     * @param ZrpCommand $zrpCommand
     * @return $this
     */
    public function removeZrpCommand(ZrpCommand $zrpCommand)
    {
        if($this->zrpCommands->contains($zrpCommand))
        {
            $this->zrpCommands->removeElement($zrpCommand);
            $zrpCommand->removeZrpProduct($this);
        }

        return $this;
    }
}
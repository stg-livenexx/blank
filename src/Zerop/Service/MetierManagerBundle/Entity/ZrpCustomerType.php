<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class ZrpCustomerType
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity(repositoryClass="App\Zerop\Service\MetierManagerBundle\Repository\ZrpCustomerTypeRepository")
 * @ORM\Table(name="zrp_customer_type")
 *
 *  @UniqueEntity(
 *     fields={"cstmTpName"},
 *     errorPath="port",
 *     message="Le nom du type de client existe déjà"
 * )
 */
class ZrpCustomerType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cstm_tp_name", type="string", length=50, nullable=false)
     */
    private $cstmTpName;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCstmTpName()
    {
        return $this->cstmTpName;
    }

    /**
     * @param string $cstmTpName
     */
    public function setCstmTpName($cstmTpName)
    {
        $this->cstmTpName = $cstmTpName;
    }
}

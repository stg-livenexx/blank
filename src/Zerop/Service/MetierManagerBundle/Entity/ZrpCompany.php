<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Zerop\Service\MetierManagerBundle\Repository\ZrpCompanyRepository")
 * Class ZrpCompany
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="zrp_company")
 */
class ZrpCompany
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="cmp_name", type="string", length=100, nullable=false)
	 */
	private $cmpName;

    /**
     * @var int
     *
     * @ORM\Column(name="cmp_effective", type="integer", nullable=true)
     */
    private $cmpEffective;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="cmp_effective_min", type="integer", nullable=true)
	 */
	private $cmpEffectiveMin;

    /**
     * @var int
     *
     * @ORM\Column(name="cmp_effective_max", type="integer", nullable=true)
     */
    private $cmpEffectiveMax;

    /**
     * @var string
     *
     * @ORM\Column(name="cmp_siren", type="string", length=50, nullable=true)
     */
    private $cmpSiren;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="cmp_checkout_nbr", type="integer", nullable=true)
	 */
	private $cmpCheckoutNbr;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="cmp_zerop_checkout_nbr", type="integer", nullable=true)
	 */
	private $cmpZeropCheckoutNbr;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="cmp_cp", type="integer", nullable=true)
	 */
	private $cmpCp;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="cmp_ca", type="float", nullable=true)
	 */
	private $cmp_ca;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="cmp_date_creation", type="datetime", nullable=true)
	 */
	private $cmpDateCreation;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="cmp_zerop_checkout_date", type="datetime", nullable=true)
	 */
	private $cmpZeropCheckoutDate;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_desc", type="text", nullable=true)
     */
    private $cmpDesc;

	/**
	 * @var ZrpCompanyType
	 *
	 * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyType")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="zrp_company_type_id", referencedColumnName="id", onDelete="CASCADE")
	 * })
	 */
	private $zrpCompanyType;

	/**
	 * @var ZrpCity
	 *
	 * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="zrp_city_id", referencedColumnName="id", onDelete="CASCADE")
	 * })
	 */
	private $zrpCity;

	/**
	 * @var ZrpCompanyActivity
	 *
	 * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyActivity")
	 * @ORM\JoinColumns({
	 * @ORM\JoinColumn(name="zrp_company_activity_id", referencedColumnName="id", onDelete="CASCADE")
	 * })
	 */
	private $zrpCompanyActivity;

	/**
	 * @ORM\OneToMany(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyErp",
	 *      mappedBy="zrpCompany", cascade={"persist", "remove"})
	 */
	private $zrpCompanyErp;

    /**
     * @var ZrpUser
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_user_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpUser;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cmpDateCreation = new \DateTime();
    }

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getCmpName()
	{
		return $this->cmpName;
	}

	/**
	 * @param string $cmpName
	 */
	public function setCmpName($cmpName)
	{
		$this->cmpName = $cmpName;
	}

	/**
	 * @return int
	 */
	public function getCmpEffective()
	{
		return $this->cmpEffective;
	}

	/**
	 * @param int $cmpEffective
	 */
	public function setCmpEffective($cmpEffective)
	{
		$this->cmpEffective = $cmpEffective;
	}

	/**
	 * @return int
	 */
	public function getCmpCheckoutNbr()
	{
		return $this->cmpCheckoutNbr;
	}

	/**
	 * @param int $cmpCheckoutNbr
	 */
	public function setCmpCheckoutNbr($cmpCheckoutNbr)
	{
		$this->cmpCheckoutNbr = $cmpCheckoutNbr;
	}

	/**
	 * @return int
	 */
	public function getCmpZeropCheckoutNbr()
	{
		return $this->cmpZeropCheckoutNbr;
	}

	/**
	 * @param int $cmpZeropCheckoutNbr
	 */
	public function setCmpZeropCheckoutNbr($cmpZeropCheckoutNbr)
	{
		$this->cmpZeropCheckoutNbr = $cmpZeropCheckoutNbr;
	}

	/**
	 * @return int
	 */
	public function getCmpCp()
	{
		return $this->cmpCp;
	}

	/**
	 * @param int $cmpCp
	 */
	public function setCmpCp($cmpCp)
	{
		$this->cmpCp = $cmpCp;
	}

	/**
	 * @return float
	 */
	public function getCmpCa()
	{
		return $this->cmp_ca;
	}

	/**
	 * @param float $cmp_ca
	 */
	public function setCmpCa($cmp_ca)
	{
		$this->cmp_ca = floatval(str_replace(' ', '', $cmp_ca));
	}

	/**
	 * @return \DateTime
	 */
	public function getCmpDateCreation()
	{
		return $this->cmpDateCreation;
	}

	/**
	 * @param \DateTime $cmpDateCreation
	 */
	public function setCmpDateCreation($cmpDateCreation)
	{
		$this->cmpDateCreation = $cmpDateCreation;
	}

	/**
	 * @return \DateTime
	 */
	public function getCmpZeropCheckoutDate()
	{
		return $this->cmpZeropCheckoutDate;
	}

	/**
	 * @param \DateTime $cmpZeropCheckoutDate
	 */
	public function setCmpZeropCheckoutDate($cmpZeropCheckoutDate)
	{
		$this->cmpZeropCheckoutDate = $cmpZeropCheckoutDate;
	}

	/**
	 * @return ZrpCompanyType
	 */
	public function getZrpCompanyType()
	{
		return $this->zrpCompanyType;
	}

	/**
	 * @param ZrpCompanyType $zrpCompanyType
	 */
	public function setZrpCompanyType($zrpCompanyType)
	{
		$this->zrpCompanyType = $zrpCompanyType;
	}

	/**
	 * @return ZrpCity
	 */
	public function getZrpCity()
	{
		return $this->zrpCity;
	}

	/**
	 * @param ZrpCity $zrpCity
	 */
	public function setZrpCity($zrpCity)
	{
		$this->zrpCity = $zrpCity;
	}

	/**
	 * @return ZrpCompanyActivity
	 */
	public function getZrpCompanyActivity()
	{
		return $this->zrpCompanyActivity;
	}

	/**
	 * @param ZrpCompanyActivity $zrpCompanyActivity
	 */
	public function setZrpCompanyActivity($zrpCompanyActivity)
	{
		$this->zrpCompanyActivity = $zrpCompanyActivity;
	}

	/**
	 * @return mixed
	 */
	public function getZrpCompanyErp()
	{
		return $this->zrpCompanyErp;
	}

	/**
	 * @param mixed $zrpCompanyErp
	 */
	public function setZrpCompanyErp($zrpCompanyErp)
	{
		$this->zrpCompanyErp = $zrpCompanyErp;
	}

    /**
     * @return ZrpUser
     */
    public function getZrpUser()
    {
        return $this->zrpUser;
    }

    /**
     * @param ZrpUser $zrpUser
     */
    public function setZrpUser($zrpUser)
    {
        $this->zrpUser = $zrpUser;
    }

    /**
     * @return int
     */
    public function getCmpEffectiveMin()
    {
        return $this->cmpEffectiveMin;
    }

    /**
     * @param int $cmpEffectiveMin
     */
    public function setCmpEffectiveMin($cmpEffectiveMin)
    {
        $this->cmpEffectiveMin = $cmpEffectiveMin;
    }

    /**
     * @return int
     */
    public function getCmpEffectiveMax()
    {
        return $this->cmpEffectiveMax;
    }

    /**
     * @param int $cmpEffectiveMax
     */
    public function setCmpEffectiveMax($cmpEffectiveMax)
    {
        $this->cmpEffectiveMax = $cmpEffectiveMax;
    }

    /**
     * @return string
     */
    public function getCmpSiren()
    {
        return $this->cmpSiren;
    }

    /**
     * @param string $cmpSiren
     */
    public function setCmpSiren($cmpSiren)
    {
        $this->cmpSiren = $cmpSiren;
    }

    /**
     * @return string
     */
    public function getCmpDesc()
    {
        return $this->cmpDesc;
    }

    /**
     * @param string $cmpDesc
     */
    public function setCmpDesc($cmpDesc)
    {
        $this->cmpDesc = $cmpDesc;
    }
}
<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ZrpCompanyTransaction
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Zerop\Service\MetierManagerBundle\Repository\ZrpCompanyTransactionRepository")
 * @ORM\Table(name="zrp_company_transaction")
 */
class ZrpCompanyTransaction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cmp_trx_date", type="datetime", nullable=false)
     */
    private $cmpTrxDate;

    /**
     * @var int
     *
     * @ORM\Column(name="cmp_trx_nbr", type="integer", nullable=false)
     */
    private $cmpTrxNbr;

    /**
     * @var ZrpCompany
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_company_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCompany;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cmpTrxDate = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCmpTrxDate(): \DateTime
    {
        return $this->cmpTrxDate;
    }

    /**
     * @param \DateTime $cmpTrxDate
     */
    public function setCmpTrxDate(\DateTime $cmpTrxDate)
    {
        $this->cmpTrxDate = $cmpTrxDate;
    }

    /**
     * @return int
     */
    public function getCmpTrxNbr()
    {
        return $this->cmpTrxNbr;
    }

    /**
     * @param int $cmpTrxNbr
     */
    public function setCmpTrxNbr($cmpTrxNbr)
    {
        $this->cmpTrxNbr = $cmpTrxNbr;
    }

    /**
     * @return ZrpCompany
     */
    public function getZrpCompany()
    {
        return $this->zrpCompany;
    }

    /**
     * @param ZrpCompany $zrpCompany
     */
    public function setZrpCompany($zrpCompany)
    {
        $this->zrpCompany = $zrpCompany;
    }
}
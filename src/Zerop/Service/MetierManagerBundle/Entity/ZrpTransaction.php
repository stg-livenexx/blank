<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ZrpTransaction
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="zrp_transaction")
 */
class ZrpTransaction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     * @Assert\Length(max=100 ,maxMessage="100 caractères maximum")
     * @ORM\Column(name="trx_amount", type="float", nullable=false)
     */
    private $trxAmount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="trx_issue_date", type="datetime", nullable=false)
     */
    private $trxIssueDate;

    /**
     * @var int
     *
     * @ORM\Column(name="trx_postal_code_invoiced", type="integer", nullable=true)
     */
    private $trxPostalCodeInvoiced;

    /**
     * @var int
     *
     * @ORM\Column(name="trx_postal_code_biller", type="integer", nullable=true)
     */
    private $trxPostalCodeBiller;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_address_biller", type="string", length=100, nullable=true)
     */
    private $trxAddressBiller;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_qr_code", type="string", length=255, nullable=true)
     */
    private $trxQrCode;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_desc", type="text", nullable=true)
     */
    private $trxDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_num", type="string", length=50, nullable=true)
     */
    private $trxNum;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_label", type="string", length=100, nullable=true)
     */
    private $trxLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_ip_view", type="string", length=15, nullable=true)
     */
    private $trxIpView;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="trx_date_created", type="datetime", nullable=true)
     */
    private $trxDateCreated;

    /**
     * @var boolean
     *
     * @ORM\Column(name="trx_is_synchronise", type="boolean", nullable=true)
     */
    private $trxIsSynchronise;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_file", type="string", length=255, nullable=true)
     */
    private $trxFile;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_ref", type="string", length=100, nullable=true)
     */
    private $trxRef;

    /**
     * @var ZrpCity
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_city_biller_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCityBiller;

    /**
     * @var ZrpCity
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_city_invoiced_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCityInvoiced;

    /**
     * @var ZrpCompanyActivity
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyActivity")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_company_activity_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCompanyActivity;

    /**
     * @var ZrpCompany
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_company_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCompany;

    /**
     * @var ZrpCustomer
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCustomer")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_customer_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCustomer;

    /**
     * @var ZrpFolder
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpFolder")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_folder_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $zrpFolder;

    /**
     * @var ZrpCashRegister
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCashRegister")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_cash_register_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $zrpCashRegister;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getTrxAmount()
    {
        return $this->trxAmount;
    }

    /**
     * @param float $trxAmount
     */
    public function setTrxAmount($trxAmount)
    {
        $this->trxAmount = $trxAmount;
    }

    /**
     * @return \DateTime
     */
    public function getTrxIssueDate()
    {
        return $this->trxIssueDate;
    }

    /**
     * @param \DateTime $trxIssueDate
     */
    public function setTrxIssueDate($trxIssueDate)
    {
        $this->trxIssueDate = $trxIssueDate;
    }

    /**
     * @return int
     */
    public function getTrxPostalCodeInvoiced()
    {
        return $this->trxPostalCodeInvoiced;
    }

    /**
     * @param int $trxPostalCodeInvoiced
     */
    public function setTrxPostalCodeInvoiced($trxPostalCodeInvoiced)
    {
        $this->trxPostalCodeInvoiced = $trxPostalCodeInvoiced;
    }

    /**
     * @return int
     */
    public function getTrxPostalCodeBiller()
    {
        return $this->trxPostalCodeBiller;
    }

    /**
     * @param int $trxPostalCodeBiller
     */
    public function setTrxPostalCodeBiller($trxPostalCodeBiller)
    {
        $this->trxPostalCodeBiller = $trxPostalCodeBiller;
    }

    /**
     * @return string
     */
    public function getTrxAddressBiller()
    {
        return $this->trxAddressBiller;
    }

    /**
     * @param string $trxAddressBiller
     */
    public function setTrxAddressBiller($trxAddressBiller)
    {
        $this->trxAddressBiller = $trxAddressBiller;
    }

    /**
     * @return ZrpCity
     */
    public function getZrpCityBiller()
    {
        return $this->zrpCityBiller;
    }

    /**
     * @param ZrpCity $zrpCityBiller
     */
    public function setZrpCityBiller($zrpCityBiller)
    {
        $this->zrpCityBiller = $zrpCityBiller;
    }

    /**
     * @return ZrpCity
     */
    public function getZrpCityInvoiced()
    {
        return $this->zrpCityInvoiced;
    }

    /**
     * @param ZrpCity $zrpCityInvoiced
     */
    public function setZrpCityInvoiced($zrpCityInvoiced)
    {
        $this->zrpCityInvoiced = $zrpCityInvoiced;
    }

    /**
     * @return ZrpCompanyActivity
     */
    public function getZrpCompanyActivity()
    {
        return $this->zrpCompanyActivity;
    }

    /**
     * @param ZrpCompanyActivity $zrpCompanyActivity
     */
    public function setZrpCompanyActivity($zrpCompanyActivity)
    {
        $this->zrpCompanyActivity = $zrpCompanyActivity;
    }

    /**
     * @return string
     */
    public function getTrxQrCode()
    {
        return $this->trxQrCode;
    }

    /**
     * @param string $trxQrCode
     */
    public function setTrxQrCode($trxQrCode)
    {
        $this->trxQrCode = $trxQrCode;
    }

    /**
     * @return ZrpCompany
     */
    public function getZrpCompany()
    {
        return $this->zrpCompany;
    }

    /**
     * @param ZrpCompany $zrpCompany
     */
    public function setZrpCompany($zrpCompany)
    {
        $this->zrpCompany = $zrpCompany;
    }

    /**
     * @return ZrpCustomer
     */
    public function getZrpCustomer()
    {
        return $this->zrpCustomer;
    }

    /**
     * @param ZrpCustomer $zrpCustomer
     */
    public function setZrpCustomer($zrpCustomer)
    {
        $this->zrpCustomer = $zrpCustomer;
    }

    /**
     * @return ZrpFolder
     */
    public function getZrpFolder()
    {
        return $this->zrpFolder;
    }

    /**
     * @param ZrpFolder $zrpFolder
     */
    public function setZrpFolder($zrpFolder)
    {
        $this->zrpFolder = $zrpFolder;
    }

    /**
     * @return string
     */
    public function getTrxDesc()
    {
        return $this->trxDesc;
    }

    /**
     * @param string $trxDesc
     */
    public function setTrxDesc($trxDesc)
    {
        $this->trxDesc = $trxDesc;
    }

    /**
     * @return string
     */
    public function getTrxNum()
    {
        return $this->trxNum;
    }

    /**
     * @param string $trxNum
     */
    public function setTrxNum($trxNum)
    {
        $this->trxNum = $trxNum;
    }

    /**
     * @return string
     */
    public function getTrxLabel()
    {
        return $this->trxLabel;
    }

    /**
     * @param string $trxLabel
     */
    public function setTrxLabel($trxLabel)
    {
        $this->trxLabel = $trxLabel;
    }

    /**
     * @return \DateTime
     */
    public function getTrxDateCreated()
    {
        return $this->trxDateCreated;
    }

    /**
     * @param \DateTime $trxDateCreated
     */
    public function setTrxDateCreated($trxDateCreated)
    {
        $this->trxDateCreated = $trxDateCreated;
    }

    /**
     * @return bool
     */
    public function isTrxIsSynchronise()
    {
        return $this->trxIsSynchronise;
    }

    /**
     * @param bool $trxIsSynchronise
     */
    public function setTrxIsSynchronise($trxIsSynchronise)
    {
        $this->trxIsSynchronise = $trxIsSynchronise;
    }

    /**
     * @return string
     */
    public function getTrxFile()
    {
        return $this->trxFile;
    }

    /**
     * @param string $trxFile
     */
    public function setTrxFile($trxFile)
    {
        $this->trxFile = $trxFile;
    }

    /**
     * @return string
     */
    public function getTrxIpView()
    {
        return $this->trxIpView;
    }

    /**
     * @param string $trxIpView
     */
    public function setTrxIpView(string $trxIpView)
    {
        $this->trxIpView = $trxIpView;
    }

    /**
     * @return string
     */
    public function getTrxRef()
    {
        return $this->trxRef;
    }

    /**
     * @param string $trxRef
     */
    public function setTrxRef($trxRef)
    {
        $this->trxRef = $trxRef;
    }

    /**
     * @return ZrpCashRegister
     */
    public function getZrpCashRegister()
    {
        return $this->zrpCashRegister;
    }

    /**
     * @param ZrpCashRegister $zrpCashRegister
     */
    public function setZrpCashRegister($zrpCashRegister)
    {
        $this->zrpCashRegister = $zrpCashRegister;
    }
}
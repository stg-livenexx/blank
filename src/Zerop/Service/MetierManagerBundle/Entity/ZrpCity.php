<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ZrpCity
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="zrp_city")
 */
class ZrpCity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ct_name", type="string", length=255, nullable=false)
     */
    private $ctName;

    /**
     * @var string
     *
     * @ORM\Column(name="ct_ascii", type="string", length=255, nullable=true)
     */
    private $ctAscii;

    /**
     * @var float
     *
     * @ORM\Column(name="ct_latitude", type="decimal", precision=10, scale=6, nullable=true)
     */
    private $ctLatitude;

    /**
     * @var float
     *
     * @ORM\Column(name="ct_longitude", type="decimal", precision=10, scale=6, nullable=true)
     */
    private $ctLongitude;

    /**
     * @var ZrpCountry
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCountry")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_country_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCountry;

    /**
     * @var ZrpCity
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_city_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $zrpCity;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCtName()
    {
        return $this->ctName;
    }

    /**
     * @param string $ctName
     */
    public function setCtName($ctName)
    {
        $this->ctName = $ctName;
    }

    /**
     * @return float
     */
    public function getCtLatitude()
    {
        return $this->ctLatitude;
    }

    /**
     * @param float $ctLatitude
     */
    public function setCtLatitude($ctLatitude)
    {
        $this->ctLatitude = $ctLatitude;
    }

    /**
     * @return float
     */
    public function getCtLongitude()
    {
        return $this->ctLongitude;
    }

    /**
     * @param float $ctLongitude
     */
    public function setCtLongitude($ctLongitude)
    {
        $this->ctLongitude = $ctLongitude;
    }

    /**
     * @return ZrpCountry
     */
    public function getZrpCountry()
    {
        return $this->zrpCountry;
    }

    /**
     * @param ZrpCountry $zrpCountry
     */
    public function setZrpCountry($zrpCountry)
    {
        $this->zrpCountry = $zrpCountry;
    }

    /**
     * @return ZrpCity
     */
    public function getZrpCity()
    {
        return $this->zrpCity;
    }

    /**
     * @param ZrpCity $zrpCity
     */
    public function setZrpCity($zrpCity)
    {
        $this->zrpCity = $zrpCity;
    }

    /**
     * @return string
     */
    public function getCtAscii(): string
    {
        return $this->ctAscii;
    }

    /**
     * @param string $ctAscii
     */
    public function setCtAscii(string $ctAscii)
    {
        $this->ctAscii = $ctAscii;
    }

    /**
     * @return string
     */
    public function getLabelString()
    {
        return $this->ctName . $this->getZrpCountry() != null ? ' ('. $this->getZrpCountry()->getCntrName() .' )' : '';
    }
}

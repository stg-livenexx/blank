<?php

namespace App\Zerop\Service\MetierManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ZrpCountry
 * @package App\Zerop\Service\MetierManagerBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="zrp_country")
 */
class ZrpCountry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cntr_name", type="string", length=50, nullable=false)
     */
    private $cntrName;

    /**
     * @var string
     *
     * @ORM\Column(name="cntr_iso3", type="string", length=3, nullable=false)
     */
    private $cntrIso3;

    /**
     * @var string
     *
     * @ORM\Column(name="cntr_iso2", type="string", length=2, nullable=false)
     */
    private $cntrIso2;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCntrName()
    {
        return $this->cntrName;
    }

    /**
     * @param string $cntrName
     */
    public function setCntrName($cntrName)
    {
        $this->cntrName = $cntrName;
    }

    /**
     * @return string
     */
    public function getCntrIso3(): string
    {
        return $this->cntrIso3;
    }

    /**
     * @param string $cntrIso3
     */
    public function setCntrIso3(string $cntrIso3)
    {
        $this->cntrIso3 = $cntrIso3;
    }

    /**
     * @return string
     */
    public function getCntrIso2(): string
    {
        return $this->cntrIso2;
    }

    /**
     * @param string $cntrIso2
     */
    public function setCntrIso2(string $cntrIso2)
    {
        $this->cntrIso2 = $cntrIso2;
    }
}

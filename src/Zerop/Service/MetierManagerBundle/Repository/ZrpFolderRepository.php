<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpFolder;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpFolderRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpFolderRepository extends ServiceEntityRepository
{
    private $_entity_manager;
    private $_utils_manager;

    /**
     * ZrpFolderRepository constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ManagerRegistry $_registry, EntityManagerInterface $_entity_manager)
    {
        $this->_entity_manager = $_entity_manager;
        $this->_utils_manager  = $_utils_manager;

        parent::__construct($_registry, ZrpFolder::class);
    }

    /**
     * get all folder
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllFolder($_page, $_nb_max, $_search, $_order_by)
    {
        $_zrp_transaction = EntityName::ZRP_TRANSACTION;
        $_order_by        = $_order_by ? $_order_by : "trans.id ASC ";

        $_customer_connected = $this->_utils_manager->getCustomerConnected();
        $_user_connected     = $_customer_connected ? $_customer_connected->getId() : 0;

        $_where = '';
        if ($_user_connected) {
            $_where = "WHERE trans.zrpCustomer = $_user_connected";
        }

        $_having = "HAVING name LIKE '%$_search%' OR
                    nbr_transaction LIKE '%$_search%' OR
                date_enregistrement LIKE '%$_search%'ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_dql = "SELECT 
                    folder.id as id,
                    folder.fldName as name,
                    CONCAT('Nombre de tickets enregistrés', ' ', count(trans)) as nbr_transaction,
                    DATE_FORMAT(folder.fldUpdatedDate,'%d/%m/%Y %H:%i') as date_enregistrement
                    FROM $_zrp_transaction trans
                    JOIN trans.zrpFolder folder
                    $_where   AND trans.zrpFolder IS NOT NULL
                    GROUP BY folder.id
                    ";

        $_result     = $this->_entity_manager->createQuery($_dql . ' ' . $_having)
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)
            ->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }

    /**
     * get folders all
     * @param $_order
     * @return mixed
     */
    public function getFoldersAll($_order)
    {
        $_zrp_transaction = EntityName::ZRP_TRANSACTION;

        $_customer_connected = $this->_utils_manager->getCustomerConnected();
        $_user_connected     = $_customer_connected ? $_customer_connected->getId() : 0;

        $_where = '';
        if ($_user_connected) {
            $_where = " WHERE trans.zrpCustomer = $_user_connected ";
        }

        $_dql = "SELECT folder.id as id,
                        folder.fldName as name,
                        CONCAT('Nombre de tickets enregistrés : ', ' ', count(trans)) as nbr_transaction,
                        CONCAT(SUBSTRING(DATE_FORMAT(folder.fldUpdatedDate,'%d/%m/%Y '),1,6),'',SUBSTRING(DATE_FORMAT(folder.fldUpdatedDate,'%d/%m/%Y '),9,2))  as date_enregistrement,
                        folder.fldUpdatedDate as date_updated
                        FROM $_zrp_transaction trans
                        JOIN trans.zrpFolder folder
                        $_where   AND trans.zrpFolder IS NOT NULL
                        GROUP BY folder.id
                        ORDER BY $_order";

        $_all_result = $this->_entity_manager->createQuery($_dql)->getResult();

        return $_all_result;
    }

    /**
     * get ticket has not folder
     * @return mixed
     */
    public function getTicketHasNotFolder()
    {

        $_ticket                = EntityName::ZRP_TRANSACTION;
        $_customer_connected    = $this->_utils_manager->getCustomerConnected();
        $_customer_connected_id = $_customer_connected->getId();

        $_dql      = "SELECT tckt 
                   FROM $_ticket tckt 
                   JOIN tckt.zrpCustomer customer
                   WHERE tckt.zrpFolder IS NULL
                   AND customer.id= :customer_id";
        $_query    = $this->_entity_manager->createQuery($_dql)->setParameter('customer_id', $_customer_connected_id);
        $_tickets  = $_query->getResult();
        $_response = $_tickets ? $_tickets : [];

        return $_response;
    }

    /**
     * get ticket on folder
     * @param $_folder_id
     * @return mixed
     */
    public function getTicketOnFolder($_folder_id)
    {
        $_ticket                = EntityName::ZRP_TRANSACTION;
        $_customer_connected    = $this->_utils_manager->getCustomerConnected();
        $_customer_connected_id = $_customer_connected->getId();

        $_dql = "SELECT tckt 
                   FROM $_ticket tckt 
                   JOIN tckt.zrpCustomer customer
                   WHERE (tckt.zrpFolder IS NULL OR tckt.zrpFolder = $_folder_id)
                   AND customer.id= :customer_id";


        $_query    = $this->_entity_manager->createQuery($_dql)->setParameter('customer_id', $_customer_connected_id);
        $_tickets  = $_query->getResult();
        $_response = $_tickets ? $_tickets : [];
        return $_response;
    }

    /**
     * get name folder by customer
     * @param $_folder_label
     * @param $_folder_id
     * @return array|mixed
     */
    public function getNameFolderByCustomer($_folder_label, $_folder_id)
    {
        $_ticket                = EntityName::ZRP_TRANSACTION;
        $_customer_connected    = $this->_utils_manager->getCustomerConnected();
        $_customer_connected_id = $_customer_connected->getId();
        $_and_where             = '';

        if (!empty($_folder_id))
            $_and_where = "AND folder.id != '$_folder_id'";

        $_dql = "SELECT folder.fldName 
                   FROM $_ticket tckt 
                   JOIN tckt.zrpCustomer customer
                   JOIN tckt.zrpFolder folder
                   WHERE folder.fldName = :folder_label
                   AND customer.id= :customer_id $_and_where";

        $_query    = $this->_entity_manager->createQuery($_dql)
            ->setParameter('folder_label', $_folder_label)
            ->setParameter('customer_id', $_customer_connected_id);
        $_tickets  = $_query->getResult();
        $_response = $_tickets ? $_tickets : [];
        return $_response;
    }
}

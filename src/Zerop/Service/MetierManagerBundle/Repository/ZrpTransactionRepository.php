<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpTransaction;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Utils\DateTimeFrench;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\MetierManagerBundle\Utils\PaginationName;
use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use App\Zerop\Service\UserBundle\Entity\User;
use DateInterval;
use DatePeriod;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpTransactionRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpTransactionRepository extends ServiceEntityRepository
{
    private $_entity_manager;
    private $_utils_manager;
    private $_company_repository;

    /**
     * ZrpTransactionRepository constructor.
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     * @param ServiceMetierUtils $_utils_manager
     * @param ZrpCompanyRepository $_company_repository
     */
    public function __construct(ManagerRegistry $_registry, EntityManagerInterface $_entity_manager,
                                ServiceMetierUtils $_utils_manager, ZrpCompanyRepository $_company_repository)
    {
        $this->_entity_manager     = $_entity_manager;
        $this->_utils_manager      = $_utils_manager;
        $this->_company_repository = $_company_repository;
        parent::__construct($_registry, ZrpTransaction::class);
    }

    /**
     * get all transaction
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllTransaction($_page, $_nb_max, $_search, $_order_by)
    {
        $_transaction = EntityName::ZRP_TRANSACTION;

        $_order_by = $_order_by ? $_order_by : "trs.trxIssueDate DESC";

        $_having = "HAVING 
                    trs.trxLabel LIKE '%$_search%' OR  
                    trs.trxAmount LIKE '%$_search%' OR
                    date_transaction LIKE '%$_search%' OR
                    trs_company.cmpCp LIKE '%$_search%' OR
                    comp_city.ctName LIKE '%$_search%' OR
                    trs.trxAddressBiller LIKE '%$_search%' OR
                    comp_actv.cmpActName LIKE '%$_search%' OR 
                    zrp_user.usrAddress LIKE '%$_search%' 
                    ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_where          = '';
        $_user_connected = $this->_utils_manager->getUserConnected();
        $_user_role_id   = $_user_connected->getZrpRole() ? $_user_connected->getZrpRole()->getId() : 0;

        if ($_user_role_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_company    = $this->_company_repository->findOneByZrpUser($_user_connected->getId());
            $_company_id = $_company ? $_company->getId() : 0;
            $_where      = "WHERE trs_company.id = '$_company_id'";
        }

        if ($_user_role_id == RoleName::ID_ROLE_CLIENT) {

            $_filter      = ['zrpUser' => $_user_connected->getId()];
            $_customer    = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);
            $_customer_id = $_customer ? $_customer->getId() : 0;
            $_where       = "WHERE trs.zrpCustomer = '$_customer_id'";
        }

        $_dql = "SELECT  trs.id AS id ,
                         trs.trxLabel AS label, 
                         trs.trxAmount AS amount, 
                         DATE_FORMAT(trs.trxIssueDate, '%d/%m/%Y %H:%i') As date_transaction,
                         trs_company.cmpCp AS postal_code_biller,
                         comp_city.ctName AS city_biller,
                         trs.trxAddressBiller AS address_biller,
                         comp_actv.cmpActName AS activity_biller,
                         zrp_user.usrAddress AS  usrAddress
                     FROM $_transaction trs
                     LEFT JOIN trs.zrpCompany trs_company
                     LEFT JOIN trs_company.zrpUser zrp_user
					 LEFT JOIN  trs_company.zrpCity comp_city
					 LEFT JOIN  trs.zrpCityInvoiced trs_city_invc
					 LEFT JOIN  trs_company.zrpCompanyActivity comp_actv";

        $_result     = $this->_entity_manager->createQuery($_dql . ' ' . $_where . ' ' . $_having)
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_where . ' ' . $_no_having)
            ->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }

    /**
     * get admin all transaction
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_custom_filter
     * @param $_order_by
     * @return array
     */
    public function getAdminAllTransaction($_page, $_nb_max, $_search, $_custom_filter, $_order_by)
    {
        $_transaction = EntityName::ZRP_TRANSACTION;
        $_order_by    = $_order_by ? $_order_by : "trs.trxIssueDate DESC";
        $_timezone    = $_custom_filter['timezone'];

        $_value   = ":search";
        $_minutes = "";

        /** regex hour H:i or H:*/
        if (
            preg_match("/^([0-1]?[0-9]|2?[0-3]):([0-5]?[0-9]|2\d)$/", $_search)
            ||
            preg_match("/^([0-1]?[0-9]|2?[0-3]):$/", $_search)
        )
        {
            $_custom_search = $_search[2] ?? "";
            $_is_two_point = "";

            if ($_custom_search == ":" && strlen($_search) == 3)
            {
                $_search .= "00";
                $_is_two_point = ":";
            }
            elseif ($_custom_search == ":" && strlen($_search) > 3)
                $_minutes = ":" . explode(":", $_search)[1];
            else
                $_search .= ":00";

            $_date        = new DateTime($_search);
            $_date_search = $_date->modify("$_timezone hours")->format("H");

            if ($_date_search) {
                $_value = "'%{$_date_search}{$_is_two_point}{$_minutes}%'";
            }
        }

        // regex form"23/03/1984 15"
        if (
            preg_match("/^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d\s([0-1]?[0-9]|2?[0-3])$/", $_search)
            ||
            preg_match("/^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d\s([0-1]?[0-9]|2?[0-3]):$/", $_search)
            ||
            preg_match("/^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d\s([0-1]?[0-9]|2?[0-3]):([0-5]?[0-9]|2\d)$/", $_search)
        ) {

            $_custom_search = $_search[13] ?? "";

            if ($_custom_search == ":" && strlen($_search) == 14)
                $_search .= "00";
            elseif ($_custom_search == ":" && strlen($_search) > 14)
                $_minutes = ":" . explode(":", $_search)[1];
            else
                $_search .= ":00";

            $_date_array  = explode("/", $_search);
            $_date_format = $_date_array[1] . '/' . $_date_array[0] . '/' . $_date_array[2];
            $_date        = new DateTime($_date_format);
            $_date_search = $_date->modify("$_timezone hours")->format("d/m/Y H");

            if ($_date_search) {
                $_value = "'%{$_date_search}{$_minutes}%'";
            }
        }

        $_having = "HAVING amount LIKE :search OR
                    comp_city.ctName LIKE :search OR
                    date_transaction LIKE {$_value} OR                    
                    city_biller LIKE :search OR                    
                    lastname LIKE :search OR      
                    firstname LIKE :search OR        
                    code_postal LIKE :search OR      
                    city_invoiced LIKE :search OR
                    num_caisse LIKE :search
                    ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_and_where      = '';
        $_user_connected = $this->_utils_manager->getUserConnected();
        $_user_role_id   = $_user_connected->getZrpRole() ? $_user_connected->getZrpRole()->getId() : 0;

        if ($_user_role_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_company    = $this->_company_repository->findOneByZrpUser($_user_connected->getId());
            $_company_id = $_company ? $_company->getId() : 0;
            $_and_where  .= " AND trs_company.id = '$_company_id'";
        }
        if ($_user_role_id == RoleName::ID_ROLE_CLIENT) {
            $_filter      = ['zrpUser' => $_user_connected->getId()];
            $_customer    = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);
            $_customer_id = $_customer ? $_customer->getId() : 0;
            $_and_where   .= " AND trs.zrpCustomer = '$_customer_id'";
        }
        if (array_key_exists('date_emission_one', $_custom_filter) && !empty($_custom_filter['date_emission_one'])
            && array_key_exists('date_emission_two', $_custom_filter) && !empty($_custom_filter['date_emission_two'])
        ) {
            $_current_date_one = (\DateTime::createFromFormat('d/m/Y', $_custom_filter['date_emission_one']))->format('Y-m-d');
            $_current_date_two = (\DateTime::createFromFormat('d/m/Y', $_custom_filter['date_emission_two']))->format('Y-m-d');
            $_and_where        .= " AND (DATE(trs.trxIssueDate) BETWEEN '$_current_date_one' AND '$_current_date_two')";
        } elseif ((array_key_exists('date_emission_one', $_custom_filter) && !empty($_custom_filter['date_emission_one']))
            && (!array_key_exists('date_emission_two', $_custom_filter) || !$_custom_filter['date_emission_two'])
        ) {
            return ['result' => [], 'all_result' => []];
        } elseif ((array_key_exists('date_emission_two', $_custom_filter) && !empty($_custom_filter['date_emission_two']))
            && (!array_key_exists('date_emission_one', $_custom_filter) || !$_custom_filter['date_emission_one'])
        ) {
            return ['result' => [], 'all_result' => []];
        }

        $_montant_one = trim($_custom_filter['montant_one']);
        $_montant_two = trim($_custom_filter['montant_two']);
        if ($_montant_one != '' && $_montant_two != '')
            $_and_where .= " AND (trs.trxAmount BETWEEN '" . $_montant_one . "' AND '" . $_montant_two . "')";
        elseif ($_montant_one && $_montant_two == '')
            return ['result' => [], 'all_result' => []];
        elseif (!$_montant_one && $_montant_two)
            return ['result' => [], 'all_result' => []];

        $_lastname_term = trim($_custom_filter['lastname_term']);
        if ($_lastname_term != '') $_and_where .= " AND customer_user.usrLastname LIKE :lastname_term ";

        $_firstname_term = trim($_custom_filter['firstname_term']);
        if ($_firstname_term != '') $_and_where .= " AND customer_user.usrFirstname LIKE :firstname_term ";

        $_code_postal = trim($_custom_filter['code_postal']);
        if ($_code_postal != '') $_and_where .= " AND trs.trxPostalCodeInvoiced LIKE :code_postal ";

        $_commune_facturant = $_custom_filter['commune_facturant'];
        if ($_commune_facturant != '') {
            $_commune_facturant = trim($_commune_facturant);
            $_and_where         .= " AND comp_city.ctName LIKE :commune_facturant ";
        }

        $_commune_facture = $_custom_filter['commune_facture'];
        if ($_commune_facture != '') {
            $_commune_facture = trim($_commune_facture);
            $_and_where       .= " AND trs_city_invc.ctName LIKE :commune_facture ";
        }

        if ($_custom_filter['customer_id'] > 0) {
            $_customer_id = $_custom_filter['customer_id'];
            $_and_where   .= " AND trs.zrpCustomer = '$_customer_id'";
        }

        $_num_caisse = $_custom_filter['num_caisse'];
        if ($_num_caisse != "" && $_num_caisse > 0){
            $_and_where .= " AND cash_register.id = {$_num_caisse} ";
        }

        $_dql = "SELECT trs.id AS id,
                        trs.trxAmount AS amount, 
                        DATE_FORMAT(trs.trxIssueDate, '%d/%m/%Y %H:%i') As date_transaction,
                        comp_city.ctName AS city_biller,
                        customer_user.usrLastname AS lastname,
                        customer_user.usrFirstname AS firstname,
                        trs.trxPostalCodeInvoiced AS code_postal,
                        trs_city_invc.ctName AS city_invoiced,
                        cash_register.csRgtNum AS num_caisse
                 FROM $_transaction trs
                 LEFT JOIN trs.zrpCustomer trs_customer
                 LEFT JOIN trs_customer.zrpUser customer_user
                 LEFT JOIN trs.zrpCompany trs_company
				 LEFT JOIN trs_company.zrpCity comp_city
				 LEFT JOIN trs.zrpCityInvoiced trs_city_invc
				 LEFT JOIN trs_company.zrpCompanyActivity comp_actv
				 LEFT JOIN trs.zrpCashRegister cash_register
				 WHERE trs.trxRef IS NOT NULL ";

        $_result = $this->_entity_manager->createQuery($_dql . ' ' . $_and_where . ' ' . $_having)
            ->setParameter("search", "%{$_search}%");

        if ($_commune_facturant != '')
            $_result = $_result->setParameter("commune_facturant", "%{$_commune_facturant}%");

        if ($_commune_facture != '')
            $_result = $_result->setParameter("commune_facture", "%{$_commune_facture}%");

        if ($_lastname_term != '')
            $_result = $_result->setParameter("lastname_term", "%{$_lastname_term}%");

        if ($_firstname_term != '')
            $_result = $_result->setParameter("firstname_term", "%{$_firstname_term}%");

        if ($_code_postal != '') {
            $_code_postal = str_replace(' ', '', $_code_postal);
            $_result      = $_result->setParameter("code_postal", "%{$_code_postal}%");
        }

        $_result = $_result->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();

        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)
            ->getResult();

        return ['result' => $_result, 'all_result' => $_all_result];
    }

    /**
     * function getSumTransactionAmount
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSumTransactionAmount()
    {
        //get entity transaction
        $_transation        = EntityName::ZRP_TRANSACTION;
        $_company           = $this->_utils_manager->getCompanyConnected();
        $_and_where_user    = '';
        $_and_where_company = '';

        //Check role and user connected
        $_user_connected         = $this->_utils_manager->getUserConnected();
        $_user_role_connected    = $this->_utils_manager->getUserRoleConnected();
        $_user_role_connected_id = $_user_role_connected ? $_user_role_connected->getId() : 0;

        if ($_user_role_connected_id == RoleName::ID_ROLE_CLIENT) {
            $_filter         = ['zrpUser' => $_user_connected];
            $_customer       = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);
            $_customer_id    = $_customer ? $_customer->getId() : 0;
            $_and_where_user = " WHERE trsc.zrpCustomer = $_customer_id";
        }

        //Check company
        if ($_user_role_connected_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_company_id        = $_company ? $_company->getId() : 0;
            $_and_where_company = " WHERE trsc.zrpCompany = $_company_id";
        }

        $_dql    = "SELECT SUM(trsc.trxAmount) FROM $_transation trsc $_and_where_company $_and_where_user";
        $_result = $this->_entity_manager->createQuery($_dql)->getSingleScalarResult();

        return $_result ? $_result : 0;
    }

    /**
     * get nbr transaction by date time
     * @param \DateTime $_date_time
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSumTransactionByDateTime(\DateTime $_date_time)
    {
        $_transation        = EntityName::ZRP_TRANSACTION;
        $_company           = $this->_utils_manager->getCompanyConnected();
        $_and_where_company = '';
        $_and_where_user    = '';

        //Check role and user connected
        $_user_connected         = $this->_utils_manager->getUserConnected();
        $_user_role_connected    = $this->_utils_manager->getUserRoleConnected();
        $_user_role_connected_id = $_user_role_connected ? $_user_role_connected->getId() : 0;

        if ($_user_role_connected_id == RoleName::ID_ROLE_CLIENT) {
            $_filter         = ['zrpUser' => $_user_connected];
            $_customer       = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);
            $_customer_id    = $_customer ? $_customer->getId() : 0;
            $_and_where_user = "AND trsc.zrpCustomer = $_customer_id";
        }

        //Check company
        if ($_user_role_connected_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_company_id        = $_company ? $_company->getId() : 0;
            $_and_where_company = "AND trsc.zrpCompany = $_company_id";
        }

        $_dql = "SELECT SUM(trsc.trxAmount)  FROM $_transation trsc WHERE DATE(trsc.trxIssueDate) = :now $_and_where_company $_and_where_user";

        $_query = $this->_entity_manager->createQuery($_dql);
        $_query->setParameter('now', $_date_time->format('Y-m-d'));

        return $_query->getSingleScalarResult();
    }

    /**
     * find transaction by name and company
     * @param $_name
     * @param $_company_id
     * @return mixed
     */
    public function findTransactionByNameAndCompany($_name, $_company_id)
    {
        $_transaction = EntityName::ZRP_TRANSACTION;

        $_dql   = "SELECT trsc FROM $_transaction trsc 
                   WHERE trsc.trxNum = :name 
                   AND trsc.zrpCompany = :company_id";
        $_query = $this->_entity_manager->createQuery($_dql);
        $_query->setParameter('name', $_name);
        $_query->setParameter('company_id', $_company_id);
        return $_query->getResult();
    }

    /**
     * fetch transaction data chart
     * @param $_type
     * @param string $_siren_or_ref
     * @param string $_date
     * @param $_date_value
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function fetchTransactionDataChart($_type, $_siren_or_ref = '', $_date = '', $_date_value)

    {
        $_date_value = $_date_value ? $_date_value : 0;
        $_response   = [];
        $_years      = $this->getAllYearTransaction();

        /** if chart is type date range */
        if ($_date) {
            $_rate     = [];
            $_date     = explode(' - ', $_date);
            $_interval = DateInterval::createFromDateString('1 day');
            $_period   = new DatePeriod(new DateTime($_date[0]), $_interval, (new DateTime($_date[1]))->modify('1 days'));
            foreach ($_period as $_date) {
                $_date_day = $_date->format("d/m/Y");
                array_push($_rate, [
                    'label' => $_date_day,
                    'rate'  => $this->getTransactionRateByFilter('day', $_date_day, $_siren_or_ref, 0)

                ]);
            }
            $_response['rate'] = $_rate;

            return $_response;
        }

        /** if chart is type year */
        if ($_type == 'year') {
            $_rate   = [];
            $_months = [
                1 => 'Janvier', 2 => 'Février', 3 => 'Mars', 4 => 'Avril', 5 => 'Mai', 6 => 'Juin', 7 => 'Juillet', 8 => 'Août',
                9 => 'Septembre', 10 => 'Octobre', 11 => 'Novembre', 12 => 'Décembre'
            ];
            foreach ($_months as $_month => $_item) {
                array_push($_rate, [
                    'label' => $_item,
                    'rate'  => $this->getTransactionRateByFilter('month', $_month, $_siren_or_ref, $_date_value)
                ]);
                $_response['rate'] = $_rate;
            }
        }

        /** if chart is type month */
        if ($_type == 'month') {
            $_rate           = [];
            $_date_now_value = (new DateTime('NOW'))->modify("$_date_value month");
            $_date_month     = $_date_now_value->format('m');
            $_date_year      = $_date_now_value->format('Y');
            $_items          = $this->_utils_manager->getBeginAndLastDayInWeek($_date_month, $_date_year);
            foreach ($_items as $_item) {
                array_push($_rate, [
                    'label' => $_type == 'month' ? $this->_utils_manager->getWeekLabel($_item) : $_item,
                    'rate'  => $this->getTransactionRateByFilter('week', $_item, $_siren_or_ref, $_date_value)
                ]);
                $_response['rate'] = $_rate;
            }
        }

        /** if chart is type week */
        if ($_type == 'week') {
            $_rate           = [];
            $_date_now_value = (new DateTime('NOW'))->modify("$_date_value days");
            $_week_number    = $_date_now_value->format("W");
            $_year_number    = $_date_now_value->format("Y");
            $_days_of_week   = [
                'first_day' => clone $_date_now_value->setISODate($_year_number, $_week_number, 1),
                'last_day'  => clone $_date_now_value->setISODate($_year_number, $_week_number, 7)
            ];

            $_interval = DateInterval::createFromDateString('1 day');
            $_period   = new DatePeriod($_days_of_week['first_day'], $_interval, ($_days_of_week['last_day'])->modify('1 days'));
            foreach ($_period as $_date) {
                $_date_day = $_date->format("d/m/Y");
                array_push($_rate, [
                    'label' => (new DateTimeFrench($_date->format('Y-m-d')))->format('l') . ' ' . $_date_day,
                    'rate'  => $this->getTransactionRateByFilter('day', $_date_day, $_siren_or_ref, 0)
                ]);
            }
            $_response['rate'] = $_rate;
        }

        /** if chart is type day */
        if ($_type == 'day') {
            $_rate = [];
            $_day  = new DateTime('NOW');
            $_day->modify("$_date_value days");
            $_day = $_day->format("d/m/Y");
            array_push($_rate, [
                'label' => $_day,
                'rate'  => $this->getTransactionRateByFilter('day', $_day, $_siren_or_ref, $_date_value)

            ]);
            $_response['rate'] = $_rate;
        }

        return $_response;
    }

    /**
     * amount by type
     * @param $_type
     * @param string $_siren_or_ref
     * @return false|float|int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function amountByType($_type, $_siren_or_ref = '')
    {
        $_response = 0;

        if ($_type == 'year') {
            $_response = $this->getAmountByFilter($_type, $_siren_or_ref);
        }

        if ($_type == 'month') {
            $_response = $this->getAmountByFilter($_type, $_siren_or_ref);
        }

        if ($_type == 'week') {
            $_response = $this->getAmountByFilter($_type, $_siren_or_ref);
        }

        return $_response;
    }

    /**
     * get all year transaction
     * @return array
     * @throws \Exception
     */
    private function getAllYearTransaction()
    {
        $_current_date = new \DateTime();
        $_transaction  = EntityName::ZRP_TRANSACTION;
        $_current_year = $_older_year = $_current_date->format('Y');

        $_company           = $this->_utils_manager->getCompanyConnected();
        $_and_where_company = '';
        $_and_where_user    = '';

        //Check role and user connected
        $_user_connected         = $this->_utils_manager->getUserConnected();
        $_user_role_connected    = $this->_utils_manager->getUserRoleConnected();
        $_user_role_connected_id = $_user_role_connected ? $_user_role_connected->getId() : 0;

        if ($_user_role_connected_id == RoleName::ID_ROLE_CLIENT) {
            $_filter         = ['zrpUser' => $_user_connected];
            $_customer       = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);
            $_customer_id    = $_customer ? $_customer->getId() : 0;
            $_and_where_user = "WHERE trsc.zrpCustomer = $_customer_id";
        }

        //Check company
        if ($_user_role_connected_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_company_id        = $_company ? $_company->getId() : 0;
            $_and_where_company = "WHERE trsc.zrpCompany = $_company_id";
        }

        $_dql = "SELECT trsc.trxIssueDate FROM $_transaction trsc
                  $_and_where_user $_and_where_company ORDER BY trsc.trxIssueDate ASC";

        $_query  = $this->_entity_manager->createQuery($_dql);
        $_result = $_query->setMaxResults(1)->getResult();
        if (!empty($_result)) {
            $_older_year = $_result[0]['trxIssueDate']->format('Y');
        }

        return range($_older_year, $_current_year);
    }

    /**
     * @param $_type
     * @param $_item
     * @param string $_siren_or_ref
     * @param $_date_value
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getTransactionRateByFilter($_type, $_item, $_siren_or_ref = '', $_date_value)
    {
        $_transaction = EntityName::ZRP_TRANSACTION;

        $_company           = $this->_utils_manager->getCompanyConnected();
        $_and_where_company = '';
        $_and_where_user    = '';
        $_and_where_search  = '';

        //Check role and user connected
        $_user_connected         = $this->_utils_manager->getUserConnected();
        $_user_role_connected    = $this->_utils_manager->getUserRoleConnected();
        $_user_role_connected_id = $_user_role_connected ? $_user_role_connected->getId() : 0;
        $_where                  = '';

        $_current_year = new DateTime('NOW');
        $_current_year->modify("$_date_value years");
        $_current_year = $_current_year->format('Y');

        if ($_type == 'year') {
            $_where = "WHERE YEAR(trsc.trxIssueDate) = $_item";
        }
        if ($_type == 'month') {
            $_where = " WHERE YEAR(trsc.trxIssueDate) = '$_current_year'";
            $_where .= " AND MONTH(trsc.trxIssueDate) = $_item";
        }
        if ($_type == 'week') {
            $_first_day_week = $_item["start_date"]->format('Y-m-d');
            $_last_day_week  = $_item["end_date"]->format('Y-m-d');
            $_where          = " WHERE DATE(trsc.trxIssueDate) BETWEEN '" . $_first_day_week . "' AND '" . $_last_day_week . "'";
        }
        if ($_type == 'day') {
            $_where = " WHERE DATE_FORMAT(trsc.trxIssueDate, '%d/%m/%Y') = '$_item'";
        }
        if ($_user_role_connected_id == RoleName::ID_ROLE_CLIENT) {
            $_filter         = ['zrpUser' => $_user_connected];
            $_customer       = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);
            $_customer_id    = $_customer ? $_customer->getId() : 0;
            $_and_where_user = "AND trsc.zrpCustomer = $_customer_id ";
        }

        //Check company
        if ($_user_role_connected_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_company_id        = $_company ? $_company->getId() : 0;
            $_and_where_company = " AND trsc.zrpCompany = {$_company_id} ";
        }

        if ($_siren_or_ref && ($_user_role_connected_id == RoleName::ID_ROLE_ENTREPRISE)) {

            $_siren_or_ref     = preg_replace('!\s+!', ' ', utf8_decode(utf8_encode(trim($_siren_or_ref))));
            $_and_where_search = " AND trsc.trxRef LIKE '%$_siren_or_ref%'";
        }

        if ($_siren_or_ref && ($_user_role_connected_id == RoleName::ID_ROLE_SUPERADMIN)) {
            $_siren_or_ref     = preg_replace('!\s+!', ' ', utf8_decode(utf8_encode(trim($_siren_or_ref))));
            $_and_where_search = " AND company.cmpSiren LIKE '%$_siren_or_ref%'";
        }

        $_dql = "SELECT ROUND(SUM(trsc.trxAmount), 2) AS trx_sum FROM $_transaction trsc
                    JOIN trsc.zrpCompany company 
                 $_where $_and_where_user $_and_where_company $_and_where_search";

        $_query  = $this->_entity_manager->createQuery($_dql);
        $_result = $_query->getOneOrNullResult();

        return $_result['trx_sum'] ? $_result['trx_sum'] : 0;
    }

    /**
     * get transaction by type and item
     * @param $_type
     * @param string $_siren_or_ref
     * @return false|float|int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getAmountByFilter($_type, $_siren_or_ref = '')
    {
        $_transaction = EntityName::ZRP_TRANSACTION;

        $_company           = $this->_utils_manager->getCompanyConnected();
        $_and_where_company = '';
        $_and_where_user    = '';
        $_and_where_search  = '';

        //Check role and user connected
        $_user_connected         = $this->_utils_manager->getUserConnected();
        $_user_role_connected    = $this->_utils_manager->getUserRoleConnected();
        $_user_role_connected_id = $_user_role_connected ? $_user_role_connected->getId() : 0;
        $_where                  = '';

        $_current_year = date('Y');
        if ($_type == 'year') {
            $_where = "WHERE YEAR(trsc.trxIssueDate) = $_current_year";
        }
        if ($_type == 'month') {
            $_where = " WHERE YEAR(trsc.trxIssueDate) = YEAR(CURRENT_DATE())";
            $_where .= " AND MONTH(trsc.trxIssueDate) = MONTH(CURRENT_DATE())";
        }
        if ($_type == 'week') {
            $_where = " WHERE YEARWEEK(trsc.trxIssueDate) = YEARWEEK(NOW())";
        }

        if ($_user_role_connected_id == RoleName::ID_ROLE_CLIENT) {
            $_filter         = ['zrpUser' => $_user_connected];
            $_customer       = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);
            $_customer_id    = $_customer ? $_customer->getId() : 0;
            $_and_where_user = "AND trsc.zrpCustomer = $_customer_id";
        }

        //Check company
        if ($_user_role_connected_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_company_id        = $_company ? $_company->getId() : 0;
            $_and_where_company = "AND trsc.zrpCompany = $_company_id ";
            $_and_where_search  = " AND trsc.trxRef = '{$_siren_or_ref}' ";
        }

        if ($_user_role_connected_id == RoleName::ID_ROLE_SUPERADMIN) {
            $_and_where_search = " AND company.cmpSiren = {$_siren_or_ref} ";
        }

        if ($_siren_or_ref) {
            $_dql = "SELECT SUM(trsc.trxAmount) AS trx_sum 
                     FROM $_transaction trsc 
                     JOIN trsc.zrpCompany company
                     $_where $_and_where_user $_and_where_company $_and_where_search ";

            $_query  = $this->_entity_manager->createQuery($_dql);
            $_result = $_query->getOneOrNullResult();

            return $_result['trx_sum'] ? round($_result['trx_sum'], 2) : 0;
        }

        $_dql = "SELECT SUM(trsc.trxAmount) AS trx_sum FROM $_transaction trsc 
                 $_where $_and_where_user $_and_where_company";

        $_query  = $this->_entity_manager->createQuery($_dql);
        $_result = $_query->getOneOrNullResult();

        return $_result['trx_sum'] ? round($_result['trx_sum'], 2) : 0;
    }

    /**
     * get number transaction
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNbrTransaction()
    {
        $_transation     = EntityName::ZRP_TRANSACTION;
        $_company        = $this->_utils_manager->getCompanyConnected();
        $_where_company  = '';
        $_and_where_user = '';

        //Check role and user connected
        $_user_connected         = $this->_utils_manager->getUserConnected();
        $_user_role_connected    = $this->_utils_manager->getUserRoleConnected();
        $_user_role_connected_id = $_user_role_connected ? $_user_role_connected->getId() : 0;

        if ($_user_role_connected_id == RoleName::ID_ROLE_CLIENT) {
            $_filter         = ['zrpUser' => $_user_connected];
            $_customer       = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);
            $_customer_id    = $_customer ? $_customer->getId() : 0;
            $_and_where_user = "AND trsc.zrpCustomer = $_customer_id";
        }

        //Check company
        if ($_user_role_connected_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_company_id    = $_company ? $_company->getId() : 0;
            $_where_company = "WHERE trsc.zrpCompany = $_company_id";
        }

        $_dql = "SELECT COUNT(trsc.id)  FROM $_transation trsc $_where_company $_and_where_user";

        $_query = $this->_entity_manager->createQuery($_dql);

        return $_query->getSingleScalarResult();
    }

    /**
     * get all transactions by customer
     * @param $_user_connected_id
     * @return mixed
     */
    public function getAllTransactionsByCustomer($_user_connected_id)
    {
        $_transaction = EntityName::ZRP_TRANSACTION;

        $_dql   = "SELECT trs FROM $_transaction trs
                    JOIN trs.zrpCustomer trs_customer
                    WHERE trs_customer.zrpUser = :user_connected_id
                    ORDER BY trs.trxIssueDate DESC";
        $_query = $this->_entity_manager->createQuery($_dql);
        $_query->setParameter('user_connected_id', $_user_connected_id);

        return $_query->getResult();
    }

    /**
     * get all customer by filter
     * @param User $_user_connected
     * @param $_filters
     * @param $_sort
     * @return mixed
     */
    public function getCustomerTransactionByFilter(User $_user_connected, $_filters, $_sort)
    {
        $_user_connected_id   = $_user_connected->getId();
        $_user_connected_role = $_user_connected->getZrpRole() ? $_user_connected->getZrpRole()->getId() : 0;

        $_transaction = EntityName::ZRP_TRANSACTION;
        $_order_by    = "trs.$_sort";
        if ($_sort == "cmpName asc" || $_sort == "cmpName desc") $_order_by = "trs_company.$_sort";

        /** filter date **/
        $_and_where_date    = "";
        $_start_date_filter = $_filters['start_date_filter'];
        $_end_date_filter   = $_filters['end_date_filter'];
        if ($_start_date_filter && $_end_date_filter) {
            $_and_where_date = " AND DATE(trs.trxIssueDate) BETWEEN '" . $_start_date_filter . "' AND '" . $_end_date_filter . "'";
        }
        /** filter date **/

        /**filter price range**/
        $_and_price_range = "";
        $_price_range     = $_filters['price_range'] ? $_filters['price_range'] : '';
        $_test_float      = strpos($_price_range, '.');
        if ($_price_range) {
            $_and_price_range = $_test_float ? " AND ROUND(trs.trxAmount,2) LIKE '$_price_range%'" :
                " AND ROUND(trs.trxAmount,2) LIKE '$_price_range.%'";
        }
        /**filter price range**/

        /**filter postal code range**/
        $_and_postal_code_range = "";
        $_postal_code_range     = $_filters['postal_code_range'] ? $_filters['postal_code_range'] : '';
        if ($_postal_code_range)
            $_and_postal_code_range = " AND COALESCE(trs.trxPostalCodeBiller,0) = $_postal_code_range";
        /**filter postal_code range**/

        /**filter num caisse**/
        $_and_num_caisse = "";
        $_num_caisse     = $_filters['num_caisse'] ? $_filters['num_caisse'] : '';
        if ($_num_caisse)
            $_and_num_caisse = " AND trs_cash_register.id = $_num_caisse";
        /**filter num caisse**/

        $_where = 'WHERE 1=1';
        if ($_user_connected_role == RoleName::ID_ROLE_CLIENT)
            $_where = "WHERE trs_customer.zrpUser = '$_user_connected_id'";
        else if ($_user_connected_role == RoleName::ID_ROLE_ENTREPRISE)
            $_where = "WHERE trs_company.zrpUser = $_user_connected_id";

        $_dql = "SELECT trs FROM $_transaction trs
                    JOIN trs.zrpCustomer trs_customer
                    JOIN trs.zrpCompany trs_company
                    LEFT JOIN trs.zrpCashRegister trs_cash_register
                    $_where
                    $_and_where_date
                    $_and_price_range
                    $_and_postal_code_range
                    $_and_num_caisse
                    ORDER BY $_order_by";

        $_query = $this->_entity_manager->createQuery($_dql);

        return $_query->getResult();
    }

    /**
     * get min max
     * @param User $_user_connected
     * @return mixed
     */
    public function getMinMaxTransactionAmountAndPostalCodeBiller(User $_user_connected)
    {
        $_transaction = EntityName::ZRP_TRANSACTION;

        $_user_connected_id   = $_user_connected->getId();
        $_user_connected_role = $_user_connected->getZrpRole() ? $_user_connected->getZrpRole()->getId() : 0;
        $_where               = '';

        if ($_user_connected_role == RoleName::ID_ROLE_CLIENT)
            $_where = "WHERE trs_customer.zrpUser = '$_user_connected_id'";
        else if ($_user_connected_role == RoleName::ID_ROLE_ENTREPRISE)
            $_where = "WHERE trs_company.zrpUser = $_user_connected_id";

        $_dql_min_max   = "SELECT MAX(COALESCE(trs.trxAmount,0)) as max_montant , 
                        MIN(COALESCE(trs.trxAmount,0)) as min_montant,
                        MAX(COALESCE(trs.trxPostalCodeBiller,0)) as max_postal_code,
                        MIN(COALESCE(trs.trxPostalCodeBiller,0)) as min_postal_code
                        FROM $_transaction trs
                        JOIN trs.zrpCustomer trs_customer
                        JOIN trs.zrpCompany trs_company
                        $_where";
        $_query_min_max = $this->_entity_manager->createQuery($_dql_min_max);

        return $_query_min_max->getResult()[0];
    }

    /**
     * get customer transaction by term and limit
     * @param $_customer
     * @param string $_term
     * @param int $_limit
     * @return \Doctrine\ORM\Query|mixed
     */
    public function getCustomerTransactionByTermAndLimit($_customer, $_term = "", $_limit = PaginationName::NBR_TRANSACTION_DASHBOARD_CUSTOMER)
    {
        $_transaction = EntityName::ZRP_TRANSACTION;
        $_dql         = "SELECT trsc FROM $_transaction trsc 
                   WHERE  trsc.trxRef LIKE :term
                   AND trsc.zrpCustomer = :customer_id
                   ORDER BY trsc.trxIssueDate DESC ";
        $_query       = $this->_entity_manager->createQuery($_dql);
        $_query->setParameter('term', "%$_term%");
        $_query->setParameter('customer_id', $_customer->getId());
        $_query->setMaxResults($_limit);
        $_query = $_query->getResult();

        return $_query;
    }

    /**
     * get all ticket by folder
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @param $_folder
     * @return array
     */
    public function getAllTicketByFolder($_page, $_nb_max, $_search, $_order_by, $_folder, $_time_zone)
    {
        $_transaction = EntityName::ZRP_TRANSACTION;
        $_order_by    = $_order_by ? $_order_by : "trs.trxIssueDate DESC";

        $_timezone    = $_time_zone;

        $_value   = ":search";
        $_minutes = "";

        /** regex hour H:i or H:*/
        if (
            preg_match("/^([0-1]?[0-9]|2?[0-3]):([0-5]?[0-9]|2\d)$/", $_search)
            ||
            preg_match("/^([0-1]?[0-9]|2?[0-3]):$/", $_search)
        )
        {
            $_custom_search = $_search[2] ?? "";
            $_is_two_point = "";

            if ($_custom_search == ":" && strlen($_search) == 3)
            {
                $_search .= "00";
                $_is_two_point = ":";
            }
            elseif ($_custom_search == ":" && strlen($_search) > 3)
                $_minutes = ":" . explode(":", $_search)[1];
            else
                $_search .= ":00";

            $_date        = new DateTime($_search);
            $_date_search = $_date->modify("$_timezone hours")->format("H");

            if ($_date_search) {
                $_value = "'%{$_date_search}{$_is_two_point}{$_minutes}%'";
            }
        }

        // regex form"23/03/1984 15"
        if (
            preg_match("/^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d\s([0-1]?[0-9]|2?[0-3])$/", $_search)
            ||
            preg_match("/^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d\s([0-1]?[0-9]|2?[0-3]):$/", $_search)
            ||
            preg_match("/^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d\s([0-1]?[0-9]|2?[0-3]):([0-5]?[0-9]|2\d)$/", $_search)
        ) {

            $_custom_search = $_search[13] ?? "";

            if ($_custom_search == ":" && strlen($_search) == 14)
                $_search .= "00";
            elseif ($_custom_search == ":" && strlen($_search) > 14)
                $_minutes = ":" . explode(":", $_search)[1];
            else
                $_search .= ":00";

            $_date_array  = explode("/", $_search);
            $_date_format = $_date_array[1] . '/' . $_date_array[0] . '/' . $_date_array[2];
            $_date        = new DateTime($_date_format);
            $_date_search = $_date->modify("$_timezone hours")->format("d/m/Y H");

            if ($_date_search) {
                $_value = "'%{$_date_search}{$_minutes}%'";
            }
        }

        $_having = "HAVING amount LIKE :search OR
                    date_transaction LIKE {$_value} OR                    
                    reference LIKE :search OR  
                    label LIKE :search     
                    ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_and_where      = '';
        $_user_connected = $this->_utils_manager->getUserConnected();
        $_user_role_id   = $_user_connected->getZrpRole() ? $_user_connected->getZrpRole()->getId() : 0;

        if ($_user_role_id == RoleName::ID_ROLE_CLIENT) {
            $_filter      = ['zrpUser' => $_user_connected->getId()];
            $_customer    = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);
            $_customer_id = $_customer ? $_customer->getId() : 0;
            $_and_where   .= " AND trs.zrpCustomer = '$_customer_id'";
        }
        $_dql = "SELECT trs.trxLabel AS label, 
                        trs.trxRef AS reference, 
                        trs.trxAmount AS amount, 
                        DATE_FORMAT(trs.trxIssueDate, '%d/%m/%Y %H:%i') As date_transaction
                 FROM $_transaction trs
                 JOIN trs.zrpFolder folder
                 LEFT JOIN trs.zrpCustomer trs_customer
                 LEFT JOIN trs_customer.zrpUser customer_user
                 LEFT JOIN trs.zrpCompany trs_company
				 LEFT JOIN trs_company.zrpCity comp_city
				 LEFT JOIN trs.zrpCityInvoiced trs_city_invc
				 LEFT JOIN trs_company.zrpCompanyActivity comp_actv
				 WHERE trs.trxRef IS NOT NULL AND folder.id = :folder_id ";

        $_result = $this->_entity_manager->createQuery($_dql . ' ' . $_and_where . ' ' . $_having)
            ->setParameter("search", "%{$_search}%")
            ->setParameter('folder_id', $_folder->getId());

        $_result = $_result->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' .$_and_where.' '. $_no_having)
            ->setParameter('folder_id', $_folder->getId())
            ->getResult();

        //get montant total
        $_total_amout = 0;
        foreach ($_all_result as $_value) {
            $_total_amout = $_total_amout + $_value['amount'];
        }

        return ['result' => $_result, 'all_result' => $_all_result, 'total_amount' => round($_total_amout,2)];
    }
}
<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ZrpCompanyTypeRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpCompanyTypeRepository
{
    private $_entity_manager;
    private $_utils_manager;

    /**
     * ZrpCompanyTypeRepository constructor.
     * @param EntityManagerInterface $_entity_manager
     * @param ServiceMetierUtils $_utils_manager
     */
    public function __construct(EntityManagerInterface $_entity_manager, ServiceMetierUtils $_utils_manager)
    {
        $this->_utils_manager  = $_utils_manager;
        $this->_entity_manager = $_entity_manager;
    }

    /**
     * get all company type
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return mixed
     */
    public function getAllCompanyType($_page, $_nb_max, $_search, $_order_by)
    {
        $_order_by     = $_order_by ? $_order_by : " cmp_type.id DESC ";
        $_company_type = EntityName::ZRP_COMPANY_TYPE;

        $_having = "HAVING cmp_type.cmpTpName LIKE :search ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_dql = "SELECT cmp_type.id As id, cmp_type.cmpTpName As cmpTpName FROM $_company_type cmp_type";

        $_result     = $this->_entity_manager->createQuery($_dql . ' ' . $_having)
            ->setParameter('search', "%{$_search}%")
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }
}
<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpProductRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpProductRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $_entity_manager;

    /**
     * ZrpProductRepository constructor.
     * @param ManagerRegistry $registry
     * @param EntityManagerInterface $_entity_manager
     */
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $_entity_manager)
    {
        parent::__construct($registry, EntityName::ZRP_PRODUCT);
        $this->_entity_manager = $_entity_manager;
    }

    public function getAllProduct($_page, $_nb_max_page, $_search, $_order_by)
    {
        $_zrp_product = EntityName::ZRP_PRODUCT;

        $_dql = "SELECT p.id AS id, p.pdLibelle AS pdLibelle FROM $_zrp_product p";

        $_order_by = $_order_by ? $_order_by : "p.id DESC";

        $_having    = "HAVING p.pdLibelle LIKE :search ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_result    = $this->_entity_manager->createQuery($_dql.' '.$_having)
                           ->setParameter('search', "%{$_search}%")
                           ->setFirstResult($_page)
                           ->setMaxResults($_nb_max_page)
                           ->getResult();

        $_all_result = $this->_entity_manager->createQuery($_dql.' '.$_no_having)->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }
}
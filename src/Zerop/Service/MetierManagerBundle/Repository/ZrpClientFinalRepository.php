<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use App\Zerop\Service\UserBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpClientFinalRepository
 * @package App\Zerop\Service\UserBundle\Repository
 */
class ZrpClientFinalRepository extends ServiceEntityRepository
{
    private $_entity_manager;
    private $_utils_manager;

    /**
     * ZrpClientFinalRepository constructor.
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     * @param ServiceMetierUtils $_utils_manager
     */
    public function __construct(ManagerRegistry $_registry, EntityManagerInterface $_entity_manager, ServiceMetierUtils $_utils_manager)
    {
        $this->_entity_manager = $_entity_manager;
        $this->_utils_manager  = $_utils_manager;
        parent::__construct($_registry, User::class);
    }

    /**
     * @param $_page
     * @param $_nb_max
     * @param $_order_by
     * @param $_data_filter
     * @return array
     */
    public function getAllClientFinal($_page, $_nb_max, $_order_by, $_data_filter)
    {
        $_city             = "";
        $_usr_first_name   = "";
        $_usr_last_name    = "";
        $_usr_address      = "";
        $_usr_phone_number = "";
        $_email            = "";
        $_having           = "";
        $_postal_code      = "";

        $_transaction = EntityName::ZRP_TRANSACTION;
        $_order_by    = $_order_by ? " ORDER BY " . $_order_by : " ORDER BY usr.id DESC";

        if ($_data_filter) {
            foreach ($_data_filter as $_filter) {

                if (!empty(trim($_filter['value'])) && trim($_filter['value']) != '' && $_filter['key'] == 'usrFirstname') {
                    $_having         .= ($_having != "" ? "AND " : "HAVING ") . $_filter['key'] . " LIKE :usr_first_name ";
                    $_usr_first_name = trim($_filter['value']);
                }

                if (!empty(trim($_filter['value'])) && trim($_filter['value']) != '' && $_filter['key'] == 'usrLastname') {
                    $_having        .= ($_having != "" ? "AND " : "HAVING ") . $_filter['key'] . " LIKE :usr_last_name ";
                    $_usr_last_name = trim($_filter['value']);
                }

                if (!empty(trim($_filter['value'])) && trim($_filter['value']) != '' && $_filter['key'] == 'usrAddress') {
                    $_having      .= ($_having != "" ? "AND " : "HAVING ") . $_filter['key'] . " LIKE :usr_address ";
                    $_usr_address = trim($_filter['value']);
                }

                if (!empty(trim($_filter['value'])) && trim($_filter['value']) != '' && $_filter['key'] == 'ctName') {
                    $_having .= ($_having != "" ? "AND " : "HAVING ") . $_filter['key'] . " LIKE :city ";
                    $_city   = trim($_filter['value']);
                }

                if (!empty(trim($_filter['value'])) && trim($_filter['value']) != '' && $_filter['key'] == 'usrDateBirth') {
                    $_having .= ($_having != "" ? "AND " : "HAVING ") . $_filter['key'] . " LIKE '%" . trim($_filter['value']) . "%' ";
                }

                if (!empty(trim($_filter['value'])) && trim($_filter['value']) != '' && $_filter['key'] == 'usrPhoneNumber') {
                    $_having           .= ($_having != "" ? "AND " : "HAVING ") . $_filter['key'] . " LIKE :usr_phone_number ";
                    $_usr_phone_number = trim($_filter['value']);
                }

                if (!empty(trim($_filter['value'])) && trim($_filter['value']) != '' && $_filter['key'] == 'email') {
                    $_having .= ($_having != "" ? "AND " : "HAVING ") . $_filter['key'] . " LIKE :email ";
                    $_email  = trim($_filter['value']);
                }

                if (!empty(trim($_filter['value'])) && trim($_filter['value']) != '' && $_filter['key'] == 'postalCode') {
                    $_having      .= ($_having != "" ? "AND " : "HAVING ") . $_filter['key'] . " LIKE :postal_code ";
                    $_postal_code = trim($_filter['value']);
                }
            }
        }

        $_where_user = '';

        //Check role and user connected
        $_user_connected         = $this->_utils_manager->getUserConnected();
        $_user_role_connected    = $this->_utils_manager->getUserRoleConnected();
        $_user_role_connected_id = $_user_role_connected ? $_user_role_connected->getId() : 0;

        if ($_user_role_connected_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_filter     = ['zrpUser' => $_user_connected];
            $_company    = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_COMPANY, $_filter);
            $_company_id = $_company ? $_company->getId() : 0;
            $_where_user = "WHERE trx.zrpCompany = $_company_id";
        }

        $_dql = "SELECT cstmr.id AS id,usr.usrFirstname AS usrFirstname, 
                        usr.usrLastname AS usrLastname, 
                        usr.usrAddress AS usrAddress, 
                        ct.ctName AS ctName,
                        DATE_FORMAT(usr.usrDateBirth, '%d-%m-%Y') AS usrDateBirth, 
                        usr.usrPhoneNumber AS usrPhoneNumber, 
                        usr.email AS email,
                        trx.trxPostalCodeInvoiced AS postalCode,
                        ct.id AS ctId
                 FROM $_transaction trx
                 JOIN trx.zrpCustomer cstmr
                 LEFT JOIN cstmr.zrpUser usr
                 LEFT JOIN usr.zrpCity ct
                 LEFT JOIN ct.zrpCountry country
                  $_where_user
                 GROUP BY cstmr.id";

        $_result = $this->_entity_manager->createQuery($_dql . ' ' . $_having . $_order_by);

        if ($_usr_first_name != '')
            $_result = $_result->setParameter("usr_first_name", "%{$_usr_first_name}%");

        if ($_usr_last_name != '')
            $_result = $_result->setParameter("usr_last_name", "%{$_usr_last_name}%");

        if ($_usr_address != '')
            $_result = $_result->setParameter("usr_address", "%{$_usr_address}%");

        if ($_city != '')
            $_result = $_result->setParameter("city", "%{$_city}%");

        if ($_usr_phone_number != '') {
            $_usr_phone_number = str_replace(' ', '', $_usr_phone_number);
            $_result           = $_result->setParameter("usr_phone_number", "%{$_usr_phone_number}%");
        }

        if ($_email != '')
            $_result = $_result->setParameter("email", "%{$_email}%");

        if ($_postal_code != '') {
            $_postal_code = str_replace(' ', '', $_postal_code);
            $_result      = $_result->setParameter("postal_code", "%{$_postal_code}%");
        }

        $_result     = $_result->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . $_order_by)
            ->getResult();

        return ['result' => $_result, 'all_result' => $_all_result];
    }

    /**
     * get nbr client finaux
     * @return int
     */
    public function getNbrClientFinaux()
    {
        $_transaction = EntityName::ZRP_TRANSACTION;

        //Check role and user connected
        $_user_connected         = $this->_utils_manager->getUserConnected();
        $_user_role_connected    = $this->_utils_manager->getUserRoleConnected();
        $_user_role_connected_id = $_user_role_connected ? $_user_role_connected->getId() : 0;

        $_where_user = '';
        if ($_user_role_connected_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_filter     = ['zrpUser' => $_user_connected];
            $_company    = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_COMPANY, $_filter);
            $_company_id = $_company ? $_company->getId() : 0;
            $_where_user = "WHERE trx.zrpCompany = $_company_id";
        }

        $_dql = "SELECT COUNT(cstmr.id) FROM $_transaction trx JOIN trx.zrpCustomer cstmr $_where_user
                 GROUP BY cstmr.id";

        $_result = $this->_entity_manager->createQuery($_dql)->getResult();
        return count($_result);
    }

    /**
     * get lasted client transaction
     * @return int
     */
    public function getLastedClientTransaction()
    {
        $_transaction = EntityName::ZRP_TRANSACTION;

        //Check role and user connected
        $_user_connected         = $this->_utils_manager->getUserConnected();
        $_user_role_connected    = $this->_utils_manager->getUserRoleConnected();
        $_user_role_connected_id = $_user_role_connected ? $_user_role_connected->getId() : 0;

        $_where_user = '';
        if ($_user_role_connected_id == RoleName::ID_ROLE_ENTREPRISE) {
            $_filter     = ['zrpUser' => $_user_connected];
            $_company    = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_COMPANY, $_filter);
            $_company_id = $_company ? $_company->getId() : 0;
            $_where_user = "WHERE trx.zrpCompany = $_company_id";
        }

        if ($_user_role_connected_id == RoleName::ID_ROLE_CLIENT) {
            $_filter      = ['zrpUser' => $_user_connected];
            $_customer    = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);
            $_customer_id = $_customer ? $_customer->getId() : 0;
            $_where_user  = "WHERE trx.zrpCustomer = $_customer_id";
        }

        $_dql = "SELECT CONCAT(cutomer_user.usrLastname,' ',cutomer_user.usrFirstname) AS userName ,
                          cmp.cmpName AS cmp_name,
                         trx.trxAmount AS amount,
                        usr.username AS user_name,
                        cutomer_user.usrLastname  AS last_name, 
                        DATE_FORMAT(DATE_ADD(trx.trxIssueDate,3, 'HOUR'), '%d/%m/%Y %H:%i')  AS trx_date,
                        cash.csRgtNum as ref_caisse
                     FROM $_transaction trx 
                 JOIN trx.zrpCustomer cstmr 
                 JOIN cstmr.zrpUser cutomer_user
                 JOIN trx.zrpCompany cmp
                 JOIN  cmp.zrpCompanyType zrpCompanyType
				 JOIN  cmp.zrpCompanyActivity comp_actv
				 JOIN  cmp.zrpCompanyErp comp_erp
				 JOIN  comp_erp.zrpErp erp
				 JOIN  cmp.zrpUser usr
				 LEFT JOIN trx.zrpCashRegister cash
                 $_where_user ORDER BY trx.trxIssueDate DESC";

        $_result = $this->_entity_manager->createQuery($_dql)
            ->setMaxResults(4)
            ->getResult();
        return $_result;
    }

    /**
     * get user has customer
     * @return mixed
     */
    public function getUserHasCustomer()
    {
        $_customer = EntityName::ZRP_CUSTOMER;

        $_dql = "SELECT usr.id as usr_id
                   FROM $_customer customer 
                   JOIN customer.zrpUser usr";

        $_query = $this->_entity_manager->createQuery($_dql);
        $_users = [];

        foreach ($_query->getResult() as $key => $item) {
            array_push($_users, $item['usr_id']);
        }
        $_response = $_users ? implode(',', $_users) : [];

        return $_response;
    }

    /**
     * get all cityname
     * @param $_order_by
     * @param $_search
     * @return array
     */
    public function getAllCityName($_order_by, $_search)
    {
        $_city = EntityName::ZRP_CITY;
        $_dql  = "SELECT c.id, c.ctName text 
                 FROM $_city c 
                 HAVING text LIKE '%$_search%'
                 ORDER BY c.id $_order_by";

        $_query   = $this->_entity_manager->createQuery($_dql);
        $_results = $_query->getResult();

        return ['results' => $_results];
    }
}

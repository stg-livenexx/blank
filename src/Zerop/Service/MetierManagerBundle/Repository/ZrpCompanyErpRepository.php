<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\ORM\EntityManagerInterface;

class ZrpCompanyErpRepository
{
    private $_entity_manager;
    private $_utils_manager;

    /**
     * ZrpCompanyErpRepository constructor.
     * @param EntityManagerInterface $_entity_manager
     * @param ServiceMetierUtils $_utils_manager
     */
    public function __construct(EntityManagerInterface $_entity_manager, ServiceMetierUtils $_utils_manager)
    {
        $this->_utils_manager  = $_utils_manager;
        $this->_entity_manager = $_entity_manager;
    }

    /**
     * get all company erp
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllCompanyErp($_page, $_nb_max, $_search, $_order_by)
    {
        $_company_erp = EntityName::ZRP_COMPANY_ERP;
        $_order_by    = $_order_by ? $_order_by : "cmp_erp.id DESC";

        $_having = "HAVING cmp_erp.cmpErpSpecificApi LIKE :search OR
                    cmp.cmpName LIKE :search OR
                    erp.erpName LIKE :search
                    ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_dql = "SELECT cmp_erp.id As id, 
                        cmp_erp.cmpErpSpecificApi As cmpErpSpecificApi,
                        cmp.cmpName As cmpName,
                        erp.erpName As erpName
                 FROM $_company_erp cmp_erp
                 LEFT JOIN cmp_erp.zrpCompany cmp
                 LEFT JOIN cmp_erp.zrpErp erp";

        $_result     = $this->_entity_manager->createQuery($_dql . ' ' . $_having)
            ->setParameter("search", "%{$_search}%")
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }
}
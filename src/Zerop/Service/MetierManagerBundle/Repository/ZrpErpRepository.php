<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpErp;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpErpRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpErpRepository extends ServiceEntityRepository
{
    private $_entity_manager;

    /**
     * ZrpErpRepository constructor.
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     */
    public function __construct(ManagerRegistry $_registry, EntityManagerInterface $_entity_manager)
    {
        $this->_entity_manager = $_entity_manager;
        parent::__construct($_registry, ZrpErp::class);
    }

    /**
     * get all erp
     * @param $_page
     * @param $_nb_max_page
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllErp($_page, $_nb_max_page, $_search, $_order_by)
    {
        $_zrp_erp  = EntityName::ZRP_ERP;
        $_order_by = $_order_by ? $_order_by : "erp.id DESC";

        $_having    = "HAVING erp.erpName LIKE :search OR
                      erp.erpApiUrl LIKE :search OR
                      erp.erpApiUserEmail LIKE :search
                      ORDER BY $_order_by";
        $_no_having = "ORDER BY $_order_by";

        $_dql = "SELECT erp.id AS id, erp.erpName AS erpName,erp.erpApiUrl AS erpApiUrl,
                        erp.erpApiDatabase AS erpApiDatabase, erp.erpApiUserEmail AS erpApiUserEmail
                FROM $_zrp_erp erp ";

        $_result = $this->_entity_manager->createQuery($_dql . ' ' . $_having)
            ->setParameter("search", "%{$_search}%")
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max_page)
            ->getResult();

        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }
}
<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpCompanyRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpCompanyRepository extends ServiceEntityRepository
{
    private $_entity_manager;

    /**
     * ZrpCompanyRepository constructor.
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     */
    public function __construct(ManagerRegistry $_registry, EntityManagerInterface $_entity_manager)
    {
        $this->_entity_manager = $_entity_manager;
        parent::__construct($_registry, ZrpCompany::class);
    }

    /**
     * Get all company
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @param $_activity_id
     * @param $_custom_filter
     * @return array
     */
    public function getAllCompany($_page, $_nb_max, $_search, $_order_by, $_activity_id, $_custom_filter)
    {
        $_company   = EntityName::ZRP_COMPANY;
        $_order_by  = $_order_by ? $_order_by : "cmp.id DESC";
        $_and_where = '';

        /**
         * debut filtre
         */
        if ($_activity_id) $_and_where = "AND comp_actv.id = '" . $_activity_id . "'";

        $_raison_social = trim($_custom_filter['raison_social']);
        if ($_raison_social != '')
            $_and_where .= " AND cmp.cmpName LIKE :raison_social ";

        $_code_postal = trim($_custom_filter['code_postal']);
        if ($_code_postal != '')
            $_and_where .= " AND cmp.cmpCp LIKE :code_postal ";

        $_type_entreprise = $_custom_filter['type_entreprise'];
        if ($_type_entreprise > 0)
            $_and_where .= " AND zrpCompanyType.id = '" . $_type_entreprise . "'";

        $_erp = $_custom_filter['erp'];
        if ($_erp > 0)
            $_and_where .= " AND erp.id = '" . $_erp . "'";

        $_effectif = $_custom_filter['effectif'];
        if ($_effectif != '' || $_effectif != null) {
            $_effectif = (int)$_effectif;
            if ($_effectif != 0 && $_effectif != "0")
                $_and_where .= " AND (cmp.cmpEffectiveMin <= '" . $_effectif . "'  AND cmp.cmpEffectiveMax >= '" . $_effectif . "' )";
            else return ['result' => [], 'all_result' => []];
        }

        if (array_key_exists('data_creation', $_custom_filter) && !empty($_custom_filter['data_creation'])) {
            $_current_date = (\DateTime::createFromFormat('d/m/Y', $_custom_filter['data_creation']))->format('d/m/Y');
            $_and_where    .= " AND DATE_FORMAT(cmp.cmpDateCreation, '%d/%m/%Y') = '$_current_date' ";
        }

        $_ca = trim($_custom_filter['ca']);
        if ($_ca != '')
            $_and_where .= " AND cmp.cmp_ca LIKE :ca ";

        $_nbr_caisse = trim($_custom_filter['nbr_caisse']);
        if ($_nbr_caisse != '')
            $_and_where .= " AND cmp.cmpCheckoutNbr LIKE :nbr_caisse ";

        $_nbr_caisse_zerop = trim($_custom_filter['nbr_caisse_zerop']);
        if ($_nbr_caisse_zerop != '')
            $_and_where .= " AND cmp.cmpZeropCheckoutNbr LIKE :nbr_caisse_zerop ";

        if (array_key_exists('date_caisse', $_custom_filter) && !empty($_custom_filter['date_caisse'])) {
            $_current_date = (\DateTime::createFromFormat('d/m/Y', $_custom_filter['date_caisse']))->format('d/m/Y');
            $_and_where    .= " AND DATE_FORMAT(cmp.cmpZeropCheckoutDate, '%d/%m/%Y') = '$_current_date' ";
        }

        $_user = $_custom_filter['user'];
        if ($_user > 0)
            $_and_where .= " AND usr.id = '" . $_user . "'";

        $_siren = trim($_custom_filter['siren']);
        if ($_siren != '')
            $_and_where .= " AND cmp.cmpSiren LIKE :siren ";

        $_commune_facturant = $_custom_filter['commune_facturant'];
        if ($_commune_facturant != '' || $_commune_facturant) $_commune_facturant = trim($_commune_facturant);
        $_and_where .= " AND comp_ct.ctName LIKE :commune_facturant ";

        /**
         * fin filtre
         */

        $_having = "HAVING cmp.cmpName LIKE :search OR
                    cmp.cmpCp LIKE :search OR
                    erp.erpName LIKE :search OR
                    cmpTpName LIKE :search OR
                    dateCreation LIKE :search OR
                    dateZeropCheckout LIKE :search OR
                    comp_ct.ctName LIKE :search OR
                    cmp_effective LIKE :search OR
                    cmp.cmp_ca LIKE :search OR
                    cmp.cmpCheckoutNbr LIKE :search OR
                    cmp.cmpZeropCheckoutNbr LIKE :search OR
                    usr.username LIKE :search OR
                    cmp.cmpSiren LIKE :search
                    ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_dql = "SELECT cmp.id AS id, comp_actv.cmpActName AS activity , cmp.cmpName AS cmp_name ,
                        cmp.cmpCp AS cmp_cp,zrpCompanyType.cmpTpName AS cmpTpName,
                        comp_ct.ctName AS ct_name, erp.erpName AS erp_name, 
                        CONCAT(cmp.cmpEffectiveMin,' à ',cmp.cmpEffectiveMax) AS cmp_effective,
                        DATE_FORMAT(cmp.cmpDateCreation, '%d/%m/%Y') As dateCreation,
                        cmp.cmp_ca AS cmp_ca,
                        cmp.cmpCheckoutNbr AS cmpCheckoutNbr ,
                        cmp.cmpZeropCheckoutNbr AS cmpZeropCheckoutNbr,
                        DATE_FORMAT(cmp.cmpZeropCheckoutDate, '%d/%m/%Y') AS dateZeropCheckout,
                        usr.username AS user_name, cmp.cmpSiren AS num_siren
                   FROM $_company cmp
                     LEFT JOIN  cmp.zrpCompanyType zrpCompanyType
					 LEFT JOIN  cmp.zrpCity comp_ct
					 LEFT JOIN  comp_ct.zrpCountry cntr
					 LEFT JOIN  cmp.zrpCompanyActivity comp_actv
					 LEFT JOIN  cmp.zrpCompanyErp comp_erp
					 LEFT JOIN  comp_erp.zrpErp erp
					 LEFT JOIN  cmp.zrpUser usr
					 WHERE cmp.id != 0
					 ";

        $_result = $this->_entity_manager->createQuery($_dql . ' ' . $_and_where . ' ' . $_having);

        if ($_raison_social != '')
            $_result = $_result->setParameter("raison_social", "%{$_raison_social}%");

        if ($_code_postal != '') {
            $_code_postal = str_replace(' ', '', $_code_postal);
            $_result      = $_result->setParameter("code_postal", "%{$_code_postal}%");
        }

        if ($_ca != '') {
            $_ca     = str_replace(' ', '', $_ca);
            $_result = $_result->setParameter("ca", "%{$_ca}%");
        }

        if ($_nbr_caisse != '') {
            $_nbr_caisse = str_replace(' ', '', $_nbr_caisse);
            $_result     = $_result->setParameter("nbr_caisse", "%{$_nbr_caisse}%");
        }

        if ($_nbr_caisse_zerop != '') {
            $_nbr_caisse_zerop = str_replace(' ', '', $_nbr_caisse_zerop);
            $_result           = $_result->setParameter("nbr_caisse_zerop", "%{$_nbr_caisse_zerop}%");
        }

        if ($_siren != '') {
            $_siren  = str_replace(' ', '', $_siren);
            $_result = $_result->setParameter("siren", "%{$_siren}%");
        }

        $_result     = $_result->setParameter("search", "%{$_search}%")
            ->setParameter("commune_facturant", "%{$_commune_facturant}%")
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)
            ->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }

    /**
     * get nbr company
     * @return mixed
     */
    public function getNbrCompany()
    {
        $_company = EntityName::ZRP_COMPANY;
        $_dql     = "SELECT DISTINCT company.id FROM $_company company";
        $_result  = $this->_entity_manager->createQuery($_dql)->getResult();

        return count($_result);
    }

    /**
     * get all city
     * @param $_order_by
     * @param $_search
     * @return array
     */
    public function getAllCity($_order_by, $_search)
    {
        $_city     = EntityName::ZRP_CITY;
        $_order_by = $_order_by ? $_order_by : "ct.ctName ASC";

        $_dql = "SELECT  ct.id As id, CONCAT(ct.ctName, IF(country.cntrIso3 IS NOT NULL, CONCAT(' (', country.cntrIso3, ') '), ''))  As text 
                 FROM $_city ct
                 LEFT JOIN ct.zrpCountry country
                 GROUP BY text
                 HAVING text LIKE '%$_search%'
                 ORDER BY $_order_by";

        $_query = $this->_entity_manager->createQuery($_dql)->setMaxResults(10);

        return ['results' => $_query->getResult()];
    }
}

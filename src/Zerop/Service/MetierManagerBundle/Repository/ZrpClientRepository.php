<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpClientRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpClientRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $_entity_manager;

    /**
     * ZrpClientRepository constructor.
     * @param ManagerRegistry $registry
     * @param EntityManagerInterface $_entity_manager
     */
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $_entity_manager)
    {
        parent::__construct($registry, EntityName::ZRP_CLIENT);
        $this->_entity_manager = $_entity_manager;
    }

    /**
     * Get all client with search and order
     * @param $_page
     * @param $_nb_max_page
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllClient($_page, $_nb_max_page, $_search, $_order_by)
    {
        $_zrp_client = EntityName::ZRP_CLIENT;

        $_dql = "SELECT c.id AS id, c.cltCin AS cltCin, c.cltLastName AS cltLastName, c.cltFirstName AS cltFirstName, c.cltPhoneNumber AS cltPhoneNumber
                 FROM $_zrp_client c";

        $_order_by  =  $_order_by ? $_order_by : "c.id DESC";

        $_having    = "HAVING c.cltCin LIKE :search OR c.cltLastName LIKE :search OR c.cltFirstName LIKE :search OR c.cltPhoneNumber LIKE :search
                       ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_result    =  $this->_entity_manager->createQuery($_dql.' '.$_having)
                    ->setParameter("search", "%{$_search}%")
                    ->setFirstResult($_page)
                    ->setMaxResults($_nb_max_page)
                    ->getResult();

        $_all_result = $this->_entity_manager->createQuery($_dql.' '.$_no_having)->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }
}
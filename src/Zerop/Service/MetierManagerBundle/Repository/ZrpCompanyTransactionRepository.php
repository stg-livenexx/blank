<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyTransaction;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpCompanyTransactionRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpCompanyTransactionRepository extends ServiceEntityRepository
{
    private $_entity_manager;
    private $_utils_manager;

    /**
     * ZrpCompanyTransactionRepository constructor.
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     * @param ServiceMetierUtils $_utils_manager
     */
    public function __construct(ManagerRegistry $_registry, EntityManagerInterface $_entity_manager,
                                ServiceMetierUtils $_utils_manager)
    {
        $this->_entity_manager = $_entity_manager;
        $this->_utils_manager  = $_utils_manager;
        parent::__construct($_registry, ZrpCompanyTransaction::class);
    }

    /**
     * Get last company transaction
     * @param $_company_id
     * @return mixed
     */
    public function getLastCompanyTransaction($_company_id)
    {
        $_company_transaction = EntityName::ZRP_COMPANY_TRANSACTION;

        $_dql   = "SELECT cmp_trx FROM $_company_transaction cmp_trx 
                 WHERE cmp_trx.zrpCompany = :company_id
                 ORDER BY cmp_trx.id DESC";
        $_query = $this->_entity_manager->createQuery($_dql);
        $_query->setParameter('company_id', $_company_id);
        $_query->setMaxResults(1);

        $_result = $_query->getResult();
        return $_result ? $_result[0] : [];
    }
}

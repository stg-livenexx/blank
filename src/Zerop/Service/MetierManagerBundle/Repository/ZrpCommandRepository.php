<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

/**
 * Class ZrpCommandRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpCommandRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $_entity_manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $_entity_manager)
    {
        parent::__construct($registry, EntityName::ZRP_COMMAND);
        $this->_entity_manager = $_entity_manager;
    }

    public function getAllCommand($_page, $_nb_max_page, $_search, $_order_by)
    {
        $_zrp_command = EntityName::ZRP_COMMAND;

        $_dql = "SELECT cmd.id AS id, 
                 cmd.cmdNumber AS cmdNumber, 
                 DATE_FORMAT(cmd.cmdCreateAt, '%d/%m/%Y') AS cmdCreateAt,
                 cmd.cmdAmount AS cmdAmount,
                 p.pdLibelle AS pdLibelle,
                 c.cltLastName AS cltLastName,
                 c.cltFirstName AS cltFirstName
                 FROM $_zrp_command cmd
                 LEFT JOIN cmd.zrpProducts p
                 LEFT JOIN cmd.zrpClient c";

        $_having = "HAVING cmd.cmdNumber LIKE :search
                        OR cmdCreateAt LIKE :search
                        OR cmd.cmdAmount LIKE :search
                        OR p.pdLibelle LIKE :search
                        OR c.cltLastName LIKE :search
                        OR c.cltFirstName LIKE :search
                        ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_result = $this->_entity_manager->createQuery($_dql.' '.$_having)
                        ->setParameter('search', "%{$_search}%")
                        ->setFirstResult($_page)
                        ->setMaxResults($_nb_max_page)
                        ->getResult();

        $_all_result = $this->_entity_manager->createQuery($_dql.' '.$_no_having)->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }

    /**
     * Get all command with filter
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_custom_filter
     * @param $_order_by
     * @return array|array[]
     * @throws \Exception
     */
    public function getAdminAllCommand($_page, $_nb_max, $_search, $_custom_filter, $_order_by)
    {
        $_zrp_command  = EntityName::ZRP_COMMAND;
        $_order_by = $_order_by ? $_order_by : "cmd.cmdCreateAt DESC";
        $_timezone = $_custom_filter['timezone'];

        $_date_value = ":search";
        $_minutes = "";

        /** regex hour H:i or H:*/
        if (
            preg_match("/^([0-1]?[0-9]|2?[0-3]):([0-5]?[0-9]|2\d)$/", $_search)
            ||
            preg_match("/^([0-1]?[0-9]|2?[0-3]):$/", $_search)
        )
        {
            $_custom_search = $_search[2] ?? "";
            $_is_two_point = "";

            if ($_custom_search == ":" && strlen($_search) == 3)
            {
                $_search .= "00";
                $_is_two_point = ":";
            }
            elseif ($_custom_search == ":" && strlen($_search) > 3)
                $_minutes = ":" . explode(":", $_search)[1];
            else
                $_search .= ":00";

            $_date        = new DateTime($_search);
            $_date_search = $_date->modify("$_timezone hours")->format("H");

            if ($_date_search) {
                $_date_value = "'%{$_date_search}{$_is_two_point}{$_minutes}%'";
            }
        }

        // regex form"23/03/1984 15"
        if (
            preg_match("/^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d\s([0-1]?[0-9]|2?[0-3])$/", $_search)
            ||
            preg_match("/^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d\s([0-1]?[0-9]|2?[0-3]):$/", $_search)
            ||
            preg_match("/^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d\s([0-1]?[0-9]|2?[0-3]):([0-5]?[0-9]|2\d)$/", $_search)
        ) {

            $_custom_search = $_search[13] ?? "";

            if ($_custom_search == ":" && strlen($_search) == 14)
                $_search .= "00";
            elseif ($_custom_search == ":" && strlen($_search) > 14)
                $_minutes = ":" . explode(":", $_search)[1];
            else
                $_search .= ":00";

            $_date_array  = explode("/", $_search);
            $_date_format = $_date_array[1] . '/' . $_date_array[0] . '/' . $_date_array[2];
            $_date        = new DateTime($_date_format);
            $_date_search = $_date->modify("$_timezone hours")->format("d/m/Y H");

            if ($_date_search) {
                $_date_value = "'%{$_date_search}{$_minutes}%'";
            }
        }

        $_dql = "SELECT cmd.id AS id, 
                 cmd.cmdNumber AS cmdNumber, 
                 DATE_FORMAT(cmd.cmdCreateAt, '%d/%m/%Y %H:%i') AS cmdCreateAt,
                 cmd.cmdAmount AS cmdAmount,
                 cmd.cmdDescription As cmdDescription,
                 p.pdLibelle AS pdLibelle,
                 c.cltLastName AS cltLastName,
                 c.cltFirstName AS cltFirstName
                 FROM $_zrp_command cmd
                 LEFT JOIN cmd.zrpProducts p
                 LEFT JOIN cmd.zrpClient c";

        $_and_where = 'WHERE cmd.id IS NOT NULL';

        $_having = "HAVING cmd.cmdNumber LIKE :search
                        OR cmdCreateAt LIKE {$_date_value}
                        OR cmd.cmdAmount LIKE :search
                        OR cmd.cmdDescription LIKE :search
                        OR p.pdLibelle LIKE :search
                        OR c.cltLastName LIKE :search
                        OR c.cltFirstName LIKE :search
                        ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_numero_one = trim($_custom_filter['numero_one']);
        $_numero_two = trim($_custom_filter['numero_two']);
        if ($_numero_one != '' && $_numero_two != '')
            $_and_where .= " AND (cmd.cmdNumber BETWEEN '" . $_numero_one . "' AND '" . $_numero_two . "')";
        elseif ($_numero_one && !$_numero_two)
            return ['result' => [], 'all_result' => []];
        elseif (!$_numero_one && $_numero_two)
            return ['result' => [], 'all_result' => []];

        if (array_key_exists('date_command_one', $_custom_filter) && !empty($_custom_filter['date_command_one'])
            && array_key_exists('date_command_two', $_custom_filter) && !empty($_custom_filter['date_command_two'])
        ) {
            $_current_date_one = (\DateTime::createFromFormat('d/m/Y', $_custom_filter['date_command_one']))->format('Y-m-d');
            $_current_date_two = (\DateTime::createFromFormat('d/m/Y', $_custom_filter['date_command_two']))->format('Y-m-d');
            $_and_where        .= " AND (DATE(cmd.cmdCreateAt) BETWEEN '$_current_date_one' AND '$_current_date_two')";
        } elseif ((array_key_exists('date_command_one', $_custom_filter) && !empty($_custom_filter['date_command_one']))
            && (!array_key_exists('date_command_two', $_custom_filter) || !$_custom_filter['date_command_two'])
        ) {
            return ['result' => [], 'all_result' => []];
        } elseif ((array_key_exists('date_command_one', $_custom_filter) && !empty($_custom_filter['date_command_one']))
            && (!array_key_exists('date_command_two', $_custom_filter) || !$_custom_filter['date_command_two'])
        ) {
            return ['result' => [], 'all_result' => []];
        }

        $_montant_one = trim($_custom_filter['montant_one']);
        $_montant_two = trim($_custom_filter['montant_two']);
        if ($_montant_one != '' && $_montant_two != '')
            $_and_where .= " AND (cmd.cmdAmount BETWEEN '" . $_montant_one . "' AND '" . $_montant_two . "')";
        elseif ($_montant_one && !$_montant_two)
            return ['result' => [], 'all_result' => []];
        elseif (!$_montant_one && $_montant_two)
            return ['result' => [], 'all_result' => []];

        $_decription_term = trim($_custom_filter['decription']);

        if ($_decription_term != '') $_and_where .= " AND cmd.cmdDescription LIKE '%{$_decription_term}%'";

        if ($_custom_filter['product_id'] != '') {
            $_product_id = $_custom_filter['product_id'];
            $_and_where   .= " AND p.id = $_product_id";
        }

        $_result = $this->_entity_manager->createQuery($_dql.' '.$_and_where.' '.$_having)
            ->setParameter('search', "%{$_search}%")
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();

        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)
            ->getResult();

        return ['result' => $_result, 'all_result' => $_all_result];
    }
}
<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCashRegister;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

class ZrpCashRegisterRepository extends ServiceEntityRepository
{
    private $_entity_manager;
    private $_utils_manager;

    /**
     * ZrpCashRegisterRepository constructor.
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     * @param ServiceMetierUtils $_utils_manager
     */
    public function __construct(ManagerRegistry $_registry, EntityManagerInterface $_entity_manager,
                                ServiceMetierUtils $_utils_manager)
    {
        $this->_entity_manager = $_entity_manager;
        $this->_utils_manager  = $_utils_manager;
        parent::__construct($_registry, ZrpCashRegister::class);
    }

    /**
     * get all cash register
     * @param $_company_id
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllCashRegister($_company_id, $_page, $_nb_max, $_search, $_order_by)
    {
        $_cash_register = EntityName::ZRP_CASH_REGISTER;
        $_order_by      = $_order_by ? $_order_by : "cashregister.id DESC";

        /** get role id*/
        $_role_id = $this->_utils_manager->getUserRoleConnected();
        $_role_id = $_role_id ? $_role_id->getId() : 0;

        $_where = '';
        if ($_role_id == RoleName::ID_ROLE_ENTREPRISE)
            $_where = "JOIN cashregister.zrpCompany company WHERE company.id = $_company_id";

        $_having = "HAVING cashregister.csRgtNum LIKE :search 
        OR cashregister.csRgtName LIKE :search 
        OR cmp.cmpName  LIKE :search 
        OR cmp.cmpCp LIKE :search 
        OR cmp.cmpSiren  LIKE :search 
        OR cmp.cmpZeropCheckoutNbr  LIKE :search 
        ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_dql = "SELECT cashregister.id AS id, cashregister.csRgtNum AS csRgtNum ,
                    cashregister.csRgtName AS csRgtName,
                    cmp.cmpName AS cmpName, 
                    cmp.cmpCp AS cmpCp, 
                    cmp.cmpSiren AS cmpSiren, 
                    cmp.cmpZeropCheckoutNbr AS cmpZeropCheckoutNbr
                    FROM $_cash_register cashregister
                    LEFT JOIN cashregister.zrpCompany cmp
                    $_where";

        $_result     = $this->_entity_manager->createQuery($_dql . ' ' . $_having)
            ->setParameter("search", "%{$_search}%")
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }

    /**
     * get all cash register num
     * @param $_company_id
     * @return array
     */
    public function getAllCashRegisterNum($_company_id)
    {
        $_cash_register = EntityName::ZRP_CASH_REGISTER;

        /** get role id*/
        $_role_id = $this->_utils_manager->getUserRoleConnected();
        $_role_id = $_role_id ? $_role_id->getId() : 0;

        $_where = '';
        if ($_role_id == RoleName::ID_ROLE_ENTREPRISE)
            $_where = " WHERE company.id = $_company_id";

        $_dql = "SELECT 
                    cashregister.id AS id, 
                    CONCAT(cashregister.csRgtNum,' (',COALESCE (company.cmpName,' '),')') AS text
                FROM $_cash_register cashregister
                LEFT JOIN cashregister.zrpCompany company
                    $_where 
                ORDER BY cashregister.id DESC";

        $_results = $this->_entity_manager->createQuery($_dql)->getResult();

        return [
            'results' => $_results
        ];
    }
}
<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpRoleRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpRoleRepository extends ServiceEntityRepository
{
    /**
     * ZrpRoleRepository constructor.
     * @param ManagerRegistry $_registry
     */
    public function __construct(ManagerRegistry $_registry)
    {
        parent::__construct($_registry, ZrpRole::class);
    }
}

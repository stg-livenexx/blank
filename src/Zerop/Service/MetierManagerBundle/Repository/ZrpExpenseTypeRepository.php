<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpExpenseType;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpExpenseTypeRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpExpenseTypeRepository extends ServiceEntityRepository
{
    private $_entity_manager;

    /**
     * ExpenseTypeRepository constructor.
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     */
    public function __construct(ManagerRegistry $_registry, EntityManagerInterface $_entity_manager)
    {
        $this->_entity_manager = $_entity_manager;
        parent::__construct($_registry, ZrpExpenseType::class);
    }

    /**
     * get all expense type
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllExpenseType($_page, $_nb_max, $_search, $_order_by)
    {
        $_expense_type = EntityName::ZRP_EXPENSE_TYPE;
        $_order_by     = $_order_by ? $_order_by : "expensetype.id DESC";

        $_having = "HAVING expensetype.expTpName LIKE '%$_search%'
                    ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_dql = "SELECT expensetype.id AS id, expensetype.expTpName AS expTpName
                 FROM $_expense_type expensetype";

        $_result     = $this->_entity_manager->createQuery($_dql . ' ' . $_having)
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)
            ->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }
}
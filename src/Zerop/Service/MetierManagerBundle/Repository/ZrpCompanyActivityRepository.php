<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyActivity;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpCompanyActivityRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpCompanyActivityRepository extends ServiceEntityRepository
{
    private $_entity_manager;

    /**
     * ZrpCompanyActivityRepository constructor.
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     */
    public function __construct(ManagerRegistry $_registry, EntityManagerInterface $_entity_manager)
    {
        $this->_entity_manager = $_entity_manager;
        parent::__construct($_registry, ZrpCompanyActivity::class);
    }

    /**
     * get all company activity
     * @param $_page
     * @param $_nb_max_page
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllcompanyActivity($_page, $_nb_max_page, $_search, $_order_by)
    {
        $_zrp_company_activity = EntityName::ZRP_COMPANY_ACTIVITY;
        $_order_by             = $_order_by ? $_order_by : "cmpAct.id DESC";

        $_having = "HAVING cmpAct.cmpActName LIKE :search
                    ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_dql = "SELECT cmpAct.id AS id, cmpAct.cmpActName AS cmpActName FROM $_zrp_company_activity cmpAct ";

        $_result     = $this->_entity_manager->createQuery($_dql . ' ' . $_having)
            ->setParameter("search", "%{$_search}%")
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max_page)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }
}
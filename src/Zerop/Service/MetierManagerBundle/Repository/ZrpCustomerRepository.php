<?php

namespace App\Zerop\Service\MetierManagerBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCustomer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ZrpCustomerRepository
 * @package App\Zerop\Service\MetierManagerBundle\Repository
 */
class ZrpCustomerRepository extends ServiceEntityRepository
{
    private $_entity_manager;

    /**
     * ZrpCustomerRepository constructor.
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     */
    public function __construct(ManagerRegistry $_registry, EntityManagerInterface $_entity_manager)
    {
        $this->_entity_manager = $_entity_manager;
        parent::__construct($_registry, ZrpCustomer::class);
    }
}
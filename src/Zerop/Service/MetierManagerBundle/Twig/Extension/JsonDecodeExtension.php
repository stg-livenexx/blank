<?php

namespace App\Zerop\Service\MetierManagerBundle\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class JsonDecodeExtension
 * @package App\Zerop\Service\MetierManagerBundle\Twig\Extension
 */
class JsonDecodeExtension extends AbstractExtension
{
    public function getName()
    {
        return 'twig';
    }

    public function getFilters()
    {
        return [
            new TwigFilter('json_decode', [$this, 'jsonDecode']),
        ];
    }

    public function jsonDecode($str) {
        return json_decode($str);
    }
}

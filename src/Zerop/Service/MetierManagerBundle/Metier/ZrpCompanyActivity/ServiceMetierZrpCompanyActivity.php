<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyActivity;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCompanyActivityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class ServiceMetierZrpCompanyActivity
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyActivityRepository
 */
class ServiceMetierZrpCompanyActivity
{
    private $_entity_manager;
    private $_container;
    private $_company_activity_repository;
    private $_utils_manager;

    /**
     * ServiceMetierZrpCompanyActivity constructor.
     * @param EntityManagerInterface $_entity_manager
     * @param ServiceMetierUtils $_utils_manager
     * @param ContainerInterface $_container
     * @param ZrpCompanyActivityRepository $_company_activity_repository
     */
    public function __construct(EntityManagerInterface $_entity_manager, ServiceMetierUtils $_utils_manager, ContainerInterface $_container, ZrpCompanyActivityRepository $_company_activity_repository)
    {
        $this->_entity_manager              = $_entity_manager;
        $this->_container                   = $_container;
        $this->_utils_manager               = $_utils_manager;
        $this->_company_activity_repository = $_company_activity_repository;
    }

    /**
     * get company activity list array
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllCompanyActivity($_page, $_nb_max, $_search, $_order_by)
    {
        $_company_type = $this->_company_activity_repository->getAllcompanyActivity($_page, $_nb_max, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_company_type['result'], $_company_type['all_result']);
    }
}
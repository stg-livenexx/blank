<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompany;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCompanyRepository;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\MetierManagerBundle\Utils\ServiceName;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ServiceMetierCompany
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompany
 */
class ServiceMetierCompany
{
    private $_utils_manager;
    private $_entity_manager;
    private $_container;
    private $_request_stack;
    private $_company_repository;

    /**
     * ServiceMetierZrpCompany constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     * @param RequestStack $_request_stack
     * @param ZrpCompanyRepository $_company_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_manager,
                                ContainerInterface $_container, RequestStack $_request_stack, ZrpCompanyRepository $_company_repository)
    {
        $this->_utils_manager      = $_utils_manager;
        $this->_entity_manager     = $_entity_manager;
        $this->_container          = $_container;
        $this->_request_stack      = $_request_stack;
        $this->_company_repository = $_company_repository;
    }

    /**
     * Get a company repository
     * @return array
     */
    public function getRepository()
    {
        return $this->_entity_manager->getRepository(EntityName::ZRP_COMPANY);
    }

    /**
     * Get all company
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @param $_activity_id
     * @param $_custom_filter
     * @return array
     */
    public function getAllCompany($_page, $_nb_max, $_search, $_order_by, $_activity_id, $_custom_filter)
    {
        $_companys = $this->_company_repository->getAllCompany($_page, $_nb_max, $_search, $_order_by, $_activity_id, $_custom_filter);

        return $this->_utils_manager->datatableResponse($_companys['result'], $_companys['all_result']);
    }

    /**
     * get all city
     * @param $_order_by
     * @param $_search
     * @return array
     */
    public function getAllCity($_order_by, $_search)
    {
        $_city = $this->_company_repository->getAllCity($_order_by, $_search);
        return $_city;
    }

    /**
     * Get all companys by order
     * @param array $_order
     * @return array
     */
    public function getAllCompanyByOrder($_order)
    {
        return $this->getRepository()->findBy(array(), $_order);
    }

    /**
     * Get company by id
     * @param Integer $_id
     * @return array
     */
    public function getCompanyById($_id)
    {
        return $this->getRepository()->find($_id);
    }

    /**
     * Test the existence companyname
     * @param string $_companyname
     * @return boolean
     */
    public function isCompanynameExist($_companyname)
    {
        $_exist = $this->getRepository()->findByCompanyname($_companyname);
        if ($_exist) {
            return true;
        }

        return false;
    }

    /**
     * Test the existence email
     * @param string $_email
     * @return boolean
     */
    public function isEmailExist($_email)
    {
        $_exist = $this->getRepository()->findByEmail($_email);
        if ($_exist) {
            return true;
        }

        return false;
    }

    /**
     *  Update company
     * @param $_company
     * @param $_form
     * @return Company
     */
    public function updateCompany($_company, $_form)
    {
        return $this->saveCompany($_company, 'update');
    }

    /**
     * Save company
     * @param Company $_company
     * @param string $_action
     * @return Company
     */
    public function saveCompany($_company, $_action)
    {
        if ($_action == 'new') {
            $this->_entity_manager->persist($_company);
        }
        $this->_entity_manager->flush();

        return $_company;
    }

    /**
     * Deleting company
     * @param Company $_company
     * @return boolean
     */
    public function deleteCompany($_company)
    {
        $this->_entity_manager->remove($_company);
        $this->_entity_manager->flush();

        return true;
    }

    /**
     * Deleting multiple company
     * @param array $_ids
     * @return boolean
     */
    public function deleteGroupCompany($_ids)
    {
        $_company_upload_manager = $this->_container->get(ServiceName::SRV_METIER_USER_UPLOAD);

        if (count($_ids)) {
            foreach ($_ids as $_id) {
                // Deleting image
                $_company_upload_manager->deleteImageById($_id);

                // Deleting company
                $_company = $this->getCompanyById($_id);
                $this->deleteCompany($_company);
            }
        }

        return true;
    }

    /**
     * Get company by mail
     * @param string $_email
     * @return array
     */
    public function findByEmail($_email)
    {
        return $this->getRepository()->findByEmail($_email);
    }

    /**
     * Get company by companyname
     * @param string $_companyname
     * @return array
     */
    public function findByCompanyname($_companyname)
    {
        return $this->getRepository()->findByCompanyname($_companyname);
    }

    /**
     * count company
     * @return mixed
     */
    public function getNbrCompany()
    {
        return $this->_company_repository->getNbrCompany();
    }
}

<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpTransaction;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCashRegister;
use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity;
use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany;
use App\Zerop\Service\MetierManagerBundle\Entity\ZrpTransaction;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCashRegisterRepository;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCityRepository;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCompanyRepository;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpTransactionRepository;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\UserBundle\Manager\UploadManager;
use App\Zerop\Service\UserBundle\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ServiceMetierZrpTransaction
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpDashboard
 */
class ServiceMetierZrpTransaction
{
    private $_entity_manager;
    private $_container;
    private $_token_storage;
    private $_web_root;
    private $_city_repository;
    private $_company_repository;
    private $_utils_manager;
    private $_user_manager;
    private $_transaction_repository;
    private $_upload_manager;
    private $_cash_register_repository;

    /**
     * ServiceMetierZrpTransaction constructor.
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     * @param TokenStorageInterface $_token_storage
     * @param ParameterBagInterface $_root_dir
     * @param ZrpTransactionRepository $_transaction_repository
     * @param ServiceMetierUtils $_utils_manager
     * @param ZrpCityRepository $_city_repository
     * @param ZrpCompanyRepository $_company_repository
     * @param ZrpCashRegisterRepository $_cash_register_repository
     * @param UserManager $_user_manager
     * @param UploadManager $_upload_manager
     */
    public function __construct(EntityManagerInterface $_entity_manager, ContainerInterface $_container,
                                TokenStorageInterface $_token_storage, ParameterBagInterface $_root_dir,
                                ZrpTransactionRepository $_transaction_repository, ServiceMetierUtils $_utils_manager,
                                ZrpCityRepository $_city_repository, ZrpCompanyRepository $_company_repository,
                                ZrpCashRegisterRepository $_cash_register_repository, UserManager $_user_manager,
                                UploadManager $_upload_manager)
    {
        $this->_entity_manager           = $_entity_manager;
        $this->_utils_manager            = $_utils_manager;
        $this->_container                = $_container;
        $this->_token_storage            = $_token_storage;
        $this->_web_root                 = realpath($_root_dir->get('kernel.project_dir') . '/public');
        $this->_transaction_repository   = $_transaction_repository;
        $this->_city_repository          = $_city_repository;
        $this->_company_repository       = $_company_repository;
        $this->_user_manager             = $_user_manager;
        $this->_upload_manager           = $_upload_manager;
        $this->_cash_register_repository = $_cash_register_repository;
    }

    /**
     * get all transaction
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllTransaction($_page, $_nb_max, $_search, $_order_by)
    {
        $_transaction = $this->_transaction_repository->getAllTransaction($_page, $_nb_max, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_transaction['result'], $_transaction['all_result']);
    }

    /**
     * get admin all transaction
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_custom_filter
     * @param $_order_by
     * @return array
     */
    public function getAdminAllTransaction($_page, $_nb_max, $_search, $_custom_filter, $_order_by)
    {
        $_transaction = $this->_transaction_repository->getAdminAllTransaction($_page, $_nb_max, $_search, $_custom_filter, $_order_by);

        return $this->_utils_manager->datatableResponse($_transaction['result'], $_transaction['all_result']);
    }

    /**
     * get all ticket by folder
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @param $_folder
     * @return array
     */
    public function getAllTicketByFolder($_page, $_nb_max, $_search, $_order_by, $_folder, $_time_zone)
    {
        $_transaction = $this->_transaction_repository->getAllTicketByFolder($_page, $_nb_max, $_search, $_order_by, $_folder, $_time_zone);


        return $this->_utils_manager->datatableResponse($_transaction['result'], $_transaction['all_result'], $_transaction['total_amount'] ) ;
    }

    /**
     *  get all transaction api
     * @param $_company_id
     * @return array
     * @throws \HTTP_Request2_LogicException
     */
    public function getAllTransactionApi($_company_id)
    {
        $_odoo_url_transaction = 'dataset/search_read';
        $_body_api             = '{"jsonrpc":"2.0","method":"call","params":{"model":"pos.order","domain":[],"fields":
        ["currency_id","name","session_id","date_order","pos_reference","partner_id","user_id","amount_total","state"],
        "limit":80,"sort":"","context":{"lang":"fr_FR","tz":"Africa/Nairobi","uid":2,"allowed_company_ids":[1],"params":
        {"action":350,"cids":1,"menu_id":209,"model":"pos.order","view_type":"list"},"bin_size":true}},"id":102972893}';

        $_result_api = $this->_utils_manager->getApiOdoo($_odoo_url_transaction, $_body_api, $_company_id);

        $_api_all_transaction = $_result_api && is_object($_result_api) ? $_result_api->records : [];
        return $_api_all_transaction;
    }

    /**
     * get customer information
     * @param $_customer_id
     * @param int $_company_id
     * @return mixed|null
     * @throws \HTTP_Request2_LogicException
     */
    public function getCustomerInformation($_customer_id, $_company_id = 0)
    {
        $_odoo_url_customer = 'dataset/call_kw/res.partner/read';
        $_body_api          = '{"jsonrpc":"2.0","method":"call","params":{"args":[[' . $_customer_id . '],
        ["name","email","email","city","state_id","zip","country_id"]],"model":"res.partner","method":"read","kwargs":
        {"context":{"bin_size":true,"lang":"fr_FR","tz":"Africa/Nairobi","uid":2,"allowed_company_ids":[1],
        "search_default_customer":1,"res_partner_search_mode":"customer","default_is_company":true,
        "default_customer_rank":1}}}}';
        $_api_customer      = $this->_utils_manager->getApiOdoo($_odoo_url_customer, $_body_api, $_company_id);

        return $_api_customer;
    }

    /**
     * fech all transaction fromm odoo
     * @param int $_company_id
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \HTTP_Request2_LogicException
     */
    public function fetchAllTransactionFromOdoo($_company_id = 0)
    {
        ini_set('max_execution_time', 0);
        if ($_company_id > 0) {
            $_company_connected = $this->_utils_manager->getEntityById(EntityName::ZRP_COMPANY, $_company_id);
        } else {
            $this->_utils_manager->getUserConnected();
            $_company_connected = $this->_utils_manager->getCompanyConnected();
        }

        // get all transaction from api ODOO
        $_api_all_transactions = $this->getAllTransactionApi($_company_id);
        $_flash                = false;
        $_message              = '';
        if ($_api_all_transactions && $_company_connected) {
            foreach ($_api_all_transactions as $_api_all_transaction) {
                $_transaction_id = $_api_all_transaction ? $_api_all_transaction->id : 0;

                $_articles = $this->getDetailCommandeByTransactionId($_transaction_id);

                $_num_transaction = $_api_all_transaction->pos_reference;
                $_transaction     = $this->_transaction_repository->findTransactionByNameAndCompany($_num_transaction, $_company_connected->getId());

                // if transaction not exist
                if (!$_transaction) {
                    $_zrp_transaction = new ZrpTransaction();

                    $this->setTransaction($_zrp_transaction, $_api_all_transaction, $_company_connected, $_articles);

                    $_message = 'new';
                    $_flash   = true;
                } else {
                    break;
                }
//                  else {
//                    $_transaction_id  = array_shift($_transaction)->getId();
//                    $_zrp_transaction = $this->_utils_manager->getEntityById(EntityName::ZRP_TRANSACTION, $_transaction_id);
//
//                    $this->setTransaction($_zrp_transaction, $_api_all_transaction, $_company_connected, $_articles);
//
//                    $_message = 'update';
//                    $_flash   = true;
//                }
            }
            $this->_entity_manager->flush();
        }
        return ['flash' => $_flash, 'message' => $_message];
    }

    /**
     * set transaction
     * @param ZrpTransaction $_zrp_transaction
     * @param $_api_all_transaction
     * @param ZrpCompany $_company_connected
     * @param $_articles
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \HTTP_Request2_LogicException
     */
    public function setTransaction(ZrpTransaction $_zrp_transaction, $_api_all_transaction, ZrpCompany $_company_connected, $_articles)
    {

        //recuperation customer id depuis api odd
        $_customer_api    = $_api_all_transaction->partner_id;
        $_customer_api_id = $_customer_api ? $_customer_api[0] : 0;

        /** Recover cash number from odoo*/
        $_cash_number = trim(explode('/', $_api_all_transaction->name)[0]);

        /** find cash number from database*/
        $_zrp_cash = $this->_cash_register_repository->findOneBycsRgtNum($_cash_number);
        if (!$_zrp_cash && $_cash_number) {
            $_zrp_cash = new ZrpCashRegister();
            $_zrp_cash->setCsRgtNum($_cash_number);
            $_zrp_cash->setZrpCompany($_company_connected ? $_company_connected : 0);
            $this->_entity_manager->persist($_zrp_cash);
            $this->_utils_manager->saveEntity($_zrp_cash, 'new');
        }

        /** Recover customer's info from odoo*/
        $_api_customer = $this->getCustomerInformation($_customer_api_id, $_company_connected->getId());

        $_api_customer  = array_shift($_api_customer);
        $_city_invoiced = $_api_customer->city ? $_api_customer->city : '';
        $_city          = $this->_city_repository->findOneByctName($_city_invoiced);

        if (!$_city && $_city_invoiced) {
            $_city = new ZrpCity();
            $_city->setCtName($_city_invoiced);
            $this->_entity_manager->persist($_city);
        }

//        $_timezone  = DefaultHoraire::DEFAULT_LOCAL_SERVER_TIMEZONE;
        $_date_fact = new \DateTime($_api_all_transaction->date_order);
        $_date_fact = \DateTime::createFromFormat('Y-m-d H:i', $_date_fact->format('Y-m-d H:i'));

        /*
        if ($_timezone)
            $_date_fact->setTimeZone(new DateTimeZone($_timezone));
        */

        $_postal_code        = $_api_customer ? $_api_customer->zip : null;
        $_amount_total       = $_api_all_transaction ? $_api_all_transaction->amount_total : null;
        $_postal_code_biller = $_company_connected ? $_company_connected->getCmpCp() : null;

        if ($_city)
            $_zrp_transaction->setZrpCityInvoiced($_city);

        if ($_postal_code)
            $_zrp_transaction->setTrxPostalCodeInvoiced($_postal_code);


        if ($_postal_code_biller)
            $_zrp_transaction->setTrxPostalCodeBiller($_postal_code_biller);

        if ($_articles)
            $_zrp_transaction->setTrxDesc(json_encode($_articles));

        $_zrp_transaction->setTrxAmount($_amount_total);
        $_zrp_transaction->setTrxIssueDate($_date_fact);
        $_zrp_transaction->setTrxNum($_api_all_transaction->pos_reference);
        $_zrp_transaction->setZrpCompany($_company_connected);

        $_company_address = $_company_connected->getZrpCity() ? $_company_connected->getZrpCity()->getCtName() : '';
        $_zrp_transaction->setTrxAddressBiller($_company_address);
        if ($_company_connected->getZrpCompanyActivity())
            $_zrp_transaction->setZrpCompanyActivity($_company_connected->getZrpCompanyActivity());

        $_customer_name  = $_api_customer->name;
        $_customer_email = $_api_customer->email ? $_api_customer->email : $_customer_name . '@zerop.com';

        $_filter_customer = ['email' => $_customer_email];
        $_user_customer   = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_USER, $_filter_customer);
        if ($_user_customer) {
            $_customer = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER,
                ['zrpUser' => $_user_customer->getId()]);
            if ($_customer)
                $_zrp_transaction->setZrpCustomer($_customer);
        } else {
            // create customer
            $_customer = $this->_user_manager->addUserCustomerFromApiTransaction($_api_customer, $_city, $_company_connected);
            $_zrp_transaction->setZrpCustomer($_customer);
        }
        $_zrp_transaction->setTrxRef($this->setReferenceTransactionByCombany($_company_connected, $_zrp_transaction));
        $_zrp_transaction->setZrpCashRegister($_zrp_cash);
        $_zrp_transaction = $this->_utils_manager->saveEntity($_zrp_transaction, 'new');
        $this->_utils_manager->generateQrCodeByTransaction($_zrp_transaction);
    }

    /**
     * set reference transaction
     * @param ZrpCompany $_company
     * @param ZrpTransaction $_transaction
     * @return string
     */
    private function setReferenceTransactionByCombany(ZrpCompany $_company, ZrpTransaction $_transaction)
    {
        $_company_rs = $_company->getCmpName();
        $_date_now   = new \DateTime();
        $_transact   = $_transaction ? intval($_transaction->getTrxAmount()) : 0;
        $_ref        = $_date_now->format('Ymdhm') . strtoupper($_company_rs) . $_transact;

        return $_ref;
    }

    /**
     * get all details commande by transaction id
     * @param $_transaction_id
     * @return array|bool|mixed
     * @throws \HTTP_Request2_LogicException
     */
    public function getDetailCommandeByTransactionId($_transaction_id)
    {
        $_odd_url_get_list_article = 'dataset/call_kw/pos.order/read';
        $_body                     = '{"jsonrpc":"2.0","method":"call","params":{"args":[[' . $_transaction_id . '],
                    ["name","date_order","partner_id","lines","amount_tax","amount_total","amount_paid","pos_reference"]],
                    "model":"pos.order","method":"read","kwargs":{"context":{"bin_size":true,"lang":"fr_FR",
                    "tz":"Indian/Antananarivo","params":{"model":"pos.order"}}}}}';
        $_api_list_article         = $this->_utils_manager->getApiOdoo($_odd_url_get_list_article, $_body);
        $_data                     = $_api_list_article ? $_api_list_article[0]->lines : [];
        $_data                     = implode(", ", $_data);

        $_api_article = [];
        if ($_data) {
            $_odoo_url_article = 'dataset/call_kw/res.partner/read';
            $_body_api_article = '{"jsonrpc":"2.0","method":"call","params":{"args":[[' . $_data . '],
                        ["full_product_name","qty","price_unit","price_subtotal","price_subtotal_incl"]],"model":
                        "pos.order.line","method":"read","kwargs":{"context":{"lang":"fr_FR","tz":"Indian/Antananarivo"}}}}';
            $_api_article      = $this->_utils_manager->getApiOdoo($_odoo_url_article, $_body_api_article);
        }

        return $_api_article;
    }

    /**
     * get api timezone
     * @return array|mixed|null
     * @throws \HTTP_Request2_LogicException
     */
    public function getApiTimezone()
    {
        $_odd_url_get_timezone = 'dataset/call_kw/pos.order/read';
        $_body                 = '{"jsonrpc":"2.0","method":"call","params":{"args":[[2],["tz","tz_offset"]],"model":"res.users",
        "method":"read","kwargs":{}}}';
        $_api_timezone         = $this->_utils_manager->getApiOdoo($_odd_url_get_timezone, $_body);
        return $_api_timezone ? $_api_timezone : [];
    }

    /**
     * Set ip view transaction
     * @param array $_data
     * @return boolean
     */
    public function setIpView($_data)
    {
        $_transaction = $this->_utils_manager->getEntityById(EntityName::ZRP_TRANSACTION, $_data['id']);

        if ($_transaction) {
            $_addresse_ip = $this->_container->get('request_stack')->getMasterRequest()->getClientIp();
            $_transaction->setTrxIpView($_addresse_ip);

            $this->_utils_manager->saveEntity($_transaction, 'update');
            return true;
        }
        return true;
    }

    /**
     * $_customer_ticket
     * @param ZrpTransaction $_customer_ticket
     * @param $_form
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveCustomerTicket(ZrpTransaction $_customer_ticket, $_form)
    {
        $_customer = $this->_utils_manager->getCustomerConnected();

        $_cp_company = $_customer->getZrpCompany() ? $_customer->getZrpCompany()->getCmpCp() : null;
        $_customer_ticket->setTrxPostalCodeBiller($_cp_company);

        $_label                = $_form['trxLabel']->getData();
        $_date_issue           = $_form['trxIssueDate']->getData();
        $_montant              = $_form['trxAmount']->getData();
        $_fichier              = $_form['trxFile']->getData();
        $_code_postal_invoiced = $_form['trxPostalCodeInvoiced']->getData();

        if ($_montant)
            $_montant = str_replace(' ','',$_montant);

        if ($_label) $_customer_ticket->setTrxLabel($_label);
        if ($_code_postal_invoiced) $_customer_ticket->setTrxPostalCodeInvoiced($_code_postal_invoiced);
        if ($_date_issue) $_customer_ticket->setTrxIssueDate($_date_issue);
        if ($_montant) $_customer_ticket->setTrxAmount($_montant);
        $_customer_ticket->setTrxDateCreated(new \DateTime());
        if ($_customer)
            $_customer_ticket->setZrpCustomer($_customer);
        if ($_fichier) {
            $this->_upload_manager->uploadCustomerTicket($_customer_ticket, $_fichier);
        }

        $_company = !empty($_customer) && $_customer->getZrpCompany() ? $_customer->getZrpCompany() : [];
        if (!empty($_company)) {
            $_company_id = $_company ? $_company->getId() : [];
            $_company    = $this->_utils_manager->getEntityById(EntityName::ZRP_COMPANY, $_company_id);
            $_customer_ticket->setZrpCompany($_company);
            $_company_activity_id = $_company->getZrpCompanyActivity() ? $_company->getZrpCompanyActivity()->getId() : null;
            if ($_company_activity_id > 0) {
                $_company_activity = $this->_utils_manager->getEntityById(EntityName::ZRP_COMPANY_ACTIVITY, $_company_activity_id);
                $_customer_ticket->setZrpCompanyActivity($_company_activity);
            }

            $_address_biller = $_company->getCmpName() ? $_company->getCmpName() : null;
            if ($_address_biller) $_customer_ticket->setTrxAddressBiller($_address_biller);

            $_company_rs = $_company->getCmpName();
            $_date_now   = new \DateTime();
            $_transact   = $_customer_ticket ? intval($_montant) : 0;
            $_ref        = $_date_now->format('Ymdhm') . strtoupper($_company_rs) . $_transact;
            $_customer_ticket->setTrxRef($_ref);

        }

        $_customer_ticket = $this->_utils_manager->saveEntity($_customer_ticket, 'new');

        $this->_utils_manager->generateQrCodeByTransaction($_customer_ticket);

        return true;
    }
}
<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpErp;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpErpRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class ServiceMetierZrpErp
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpErp
 */
class ServiceMetierZrpErp
{
    private $_entity_manager;
    private $_container;
    private $_erp_repository;
    private $_utils_manager;

    /**
     * ServiceMetierZrpErp constructor.
     * @param EntityManagerInterface $_entity_manager
     * @param ServiceMetierUtils $_utils_manager
     * @param ContainerInterface $_container
     * @param ZrpErpRepository $_erp_repository
     */
    public function __construct(EntityManagerInterface $_entity_manager, ServiceMetierUtils $_utils_manager, ContainerInterface $_container, ZrpErpRepository $_erp_repository)
    {
        $this->_entity_manager = $_entity_manager;
        $this->_container      = $_container;
        $this->_utils_manager  = $_utils_manager;
        $this->_erp_repository = $_erp_repository;
    }

    /**
     * get all erp
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllErp($_page, $_nb_max, $_search, $_order_by)
    {
        $_erp_type = $this->_erp_repository->getAllErp($_page, $_nb_max, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_erp_type['result'], $_erp_type['all_result']);
    }
}
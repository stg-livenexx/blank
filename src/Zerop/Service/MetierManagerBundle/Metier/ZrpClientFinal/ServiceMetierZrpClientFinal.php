<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpClientFinal;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCustomer;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpClientFinalRepository;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\UserBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class ServiceMetierZrpClientFinal
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpClentFinal
 */
class ServiceMetierZrpClientFinal
{
    private $_utils_manager;
    private $_entity_manager;
    private $_container;
    private $_client_manager_repository;

    /**
     * ServiceMetierZrpClientFinal constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     * @param ZrpClientFinalRepository $_client_manager_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_manager, ContainerInterface $_container, ZrpClientFinalRepository $_client_manager_repository)
    {
        $this->_utils_manager             = $_utils_manager;
        $this->_entity_manager            = $_entity_manager;
        $this->_container                 = $_container;
        $this->_client_manager_repository = $_client_manager_repository;
    }

    /**
     * Get all client final
     * @param $_page
     * @param $_nb_max
     * @param $_order_by
     * @param $_data_filter
     * @return array
     */
    public function getAllClientFinal($_page, $_nb_max, $_order_by, $_data_filter)
    {
        $_client_finals = $this->_client_manager_repository->getAllClientFinal($_page, $_nb_max, $_order_by, $_data_filter);

        return $this->_utils_manager->datatableResponse($_client_finals['result'], $_client_finals['all_result']);
    }

    /**
     * get all city
     * @param $_order_by
     * @param $_search
     * @return array
     */
    public function getAllCity($_order_by, $_search)
    {
        $_list_city = $this->_client_manager_repository->getAllCityName($_order_by, $_search);
        return $_list_city;
    }

    /**
     * add user
     * @param User $_user
     * @param $_form
     * @param $_city_id
     * @return User
     */
    public function addUser(User $_user, $_form, $_city_id)
    {
        $_city = $this->_utils_manager->getEntityById(EntityName::ZRP_CITY, $_city_id);
        $_user->setZrpCity($_city);
        $_user->setPassword(uniqid());
        return $this->saveUser($_user, 'new');
    }

    /**
     * Update User
     * @param User $_user
     * @param $_form
     * @param $_city_id
     * @return User
     * @throws \Exception
     */
    public function updateUser(User $_user, $_form, $_city_id)
    {
        $_city = $this->_utils_manager->getEntityById(EntityName::ZRP_CITY, $_city_id);
        $_user->setZrpCity($_city);
        $_user->setUsrDateUpdate(new \DateTime());

        // Update password
        /*$_password = $_user->getPassword();
        if ($_password) {
            $_encoded = $this->_encoded->encodePassword($_user, $_password);
            $_user->setPassword($_encoded);
        }*/

        return $this->saveUser($_user, 'update');
    }

    /**
     * Save user
     * @param User $_user
     * @param string $_action
     * @return User
     */
    public function saveUser($_user, $_action)
    {
        if ($_action == 'new') {
            $this->_entity_manager->persist($_user);
        }
        $this->_entity_manager->flush();

        return $_user;
    }

    /**
     * Deleting user
     * @param $_user
     * @return bool
     */
    public function deleteUser($_user)
    {
        $this->_entity_manager->remove($_user);
        $this->_entity_manager->flush();

        return true;
    }

    /**
     * Get user has customer
     * @return mixed
     */
    public function getUserHasCustomer()
    {
        $_user_not_in_customer = $this->_client_manager_repository->getUserHasCustomer();

        return $_user_not_in_customer;
    }

    /**
     * add user customer
     * @param ZrpCustomer $_customer
     * @param $_data_post
     * @param $_is_new
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addUserCustomer(ZrpCustomer $_customer, $_data_post, $_is_new)
    {
        $_user_id = $_data_post['zrp_service_metiermanagerbundle_client_final']['zrpUser'];
        $_user    = $this->_utils_manager->getEntityById(EntityName::ZRP_USER, $_user_id);

        $_nom     = "";
        $_prenom  = "";
        $_adresse = "";
        $_phone   = 0;

        if (isset($_data_post['selection_ville'])) {
            $_city_id = (int)$_data_post['selection_ville'];
            $_city    = $this->_utils_manager->getEntityById(EntityName::ZRP_CITY, $_city_id);
            $_user->setZrpCity($_city);
        }

        if (isset($_data_post['name'])) $_nom = $_data_post['name'];
        if (isset($_data_post['lastname'])) $_prenom = $_data_post['lastname'];
        if (isset($_data_post['adress'])) $_adresse = $_data_post['adress'];
        if (isset($_data_post['date_naissance'])) $_user->setUsrDateBirth(\DateTime::createFromFormat('d/m/Y', $_data_post['date_naissance']));;
        if (isset($_data_post['phone'])) $_phone = $_data_post['phone'];

        $_user->setUsrFirstname($_nom);
        $_user->setUsrLastname($_prenom);
        $_user->setUsrAddress($_adresse);
        $_user->setUsrPhoneNumber($_phone);

        $this->_utils_manager->saveEntity($_user, 'update');

        if ($_is_new) $this->_utils_manager->saveEntity($_customer, 'new');
        else $this->_utils_manager->saveEntity($_customer, 'update');
    }
}

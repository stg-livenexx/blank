<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpWsResponse;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class ServiceMetierZrpWsResponse
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpWsResponse
 */
class ServiceMetierZrpWsResponse
{
    private $_entity_manager;
    private $_container;

    /**
     * ServiceMetierZrpWsResponse constructor.
     * @param EntityManager $_entity_manager
     * @param Container $_container
     */
    public function __construct(EntityManager $_entity_manager, Container $_container)
    {
        $this->_entity_manager = $_entity_manager;
        $this->_container      = $_container;
    }

    /**
     * Retourner la réponse pour chaque webservice
     * @param $_status
     * @param $_message
     * @param $_data
     * @return array
     */
    public function getGsWsResponse($_status, $_message, $_data = null)
    {
        $_response = [
            'status'  => $_status,
            'message' => $_message
        ];

        if ($_data !== null)
            $_response['data'] = $_data;

        return $_response;
    }

    /**
     * Vérification validation token
     * @param array $_post
     * @return boolean
     */
    public function isTokenValid($_post)
    {
        $_token = $this->_container->getParameter('token_web_service');

        if (isset($_post['token_ws'])) {
            if ($_post['token_ws'] == $_token)
                return true;
        }

        return false;
    }
}
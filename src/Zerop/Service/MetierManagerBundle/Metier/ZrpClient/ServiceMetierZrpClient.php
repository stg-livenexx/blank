<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpClient;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpClient;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class ServiceMetierZrpClient
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpClient
 */
class ServiceMetierZrpClient
{
    /**
     * @var ServiceMetierUtils
     */
    private $_utils_manager;

    /**
     * @var EntityManagerInterface
     */
    private $_entity_manager;

    /**
     * @var ContainerInterface
     */
    private $_container;

    /**
     * @var ZrpClientRepository
     */
    private $_client_repository;

    /**
     * ServiceMetierZrpClient constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     * @param ZrpClientRepository $_client_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_manager,
                                ContainerInterface $_container, ZrpClientRepository $_client_repository)
    {
        $this->_utils_manager = $_utils_manager;
        $this->_entity_manager = $_entity_manager;
        $this->_container = $_container;
        $this->_client_repository = $_client_repository;
    }

    /**
     * Recupère all client
     * @param $_page
     * @param $_nb_max_page
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllClient($_page, $_nb_max_page, $_search, $_order_by)
    {
        $_clients = $this->_client_repository->getAllClient($_page, $_nb_max_page, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_clients['result'], $_clients['all_result']);
    }

    /**
     * Mapping client
     * @param $_request_content_json
     * @param $_client_exist
     * @return ZrpClient|mixed
     */
    public function mappingClient($_request_content_json, $_client_exist = null)
    {
        $_request_content = json_decode($_request_content_json);

        $_client = $_client_exist === null ? new ZrpClient() : $_client_exist;

        $_client_current_cin_is_equals_client_update_cin = ($_client_exist !== null && $_client_exist->getCltCin() === $_request_content->clt_cin) ? true : false;

        $_client->setCltCin($_request_content->clt_cin);
        $_client->setCltLastName($_request_content->clt_last_name);
        $_client->setCltFirstName($_request_content->clt_first_name);
        $_client->setCltPhoneNumber($_request_content->clt_phone_number);

        return [
            'client' => $_client,
            'client_current_cin_is_equals_client_update_cin' => $_client_current_cin_is_equals_client_update_cin
        ];
    }
}

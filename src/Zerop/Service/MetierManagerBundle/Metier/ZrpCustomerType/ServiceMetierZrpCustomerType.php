<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpCustomerType;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCustomerTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class ServiceMetierZrpCustomerType
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpCustomerType
 */
class ServiceMetierZrpCustomerType
{
    private $_utils_manager;
    private $_entity_manager;
    private $_container;
    private $_customer_type_repository;

    /**
     * ServiceMetierZrpCustomerType constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     * @param ZrpCustomerTypeRepository $_customer_type_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_manager,
                                ContainerInterface $_container, ZrpCustomerTypeRepository $_customer_type_repository)
    {
        $this->_utils_manager            = $_utils_manager;
        $this->_entity_manager           = $_entity_manager;
        $this->_container                = $_container;
        $this->_customer_type_repository = $_customer_type_repository;
    }

    /**
     * get all customer type
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllCustomerType($_page, $_nb_max, $_search, $_order_by)
    {
        $_users = $this->_customer_type_repository->getAllCustomerType($_page, $_nb_max, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_users['result'], $_users['all_result']);
    }
}
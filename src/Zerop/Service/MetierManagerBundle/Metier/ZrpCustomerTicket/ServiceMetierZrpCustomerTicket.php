<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpCustomerTicket;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\MetierManagerBundle\Utils\PathName;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * Class ServiceMetierZrpCustomerTicket
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpCustomerTicket
 */
class ServiceMetierZrpCustomerTicket
{
    private $_utils_manager;
    private $_entity_manager;
    private $_container;
    private $_translator;
    private $_twig;
    private $_web_root;

    /**
     * ServiceMetierZrpCustomerTicket constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     * @param TranslatorInterface $_translator
     * @param Environment $_twig
     * @param ParameterBagInterface $_root_dir
     */
    public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_manager,
                                ContainerInterface $_container, TranslatorInterface $_translator,
                                Environment $_twig, ParameterBagInterface $_root_dir)
    {
        $this->_utils_manager  = $_utils_manager;
        $this->_entity_manager = $_entity_manager;
        $this->_container      = $_container;
        $this->_translator     = $_translator;
        $this->_twig           = $_twig;
        $this->_web_root       = realpath($_root_dir->get('kernel.project_dir') . '/public');
    }

    /**
     * Send mail ticket
     * @param array $_data
     * @return boolean
     */
    public function sendMail($_data)
    {
        $_data['transaction'] = $this->_utils_manager->getEntityById(EntityName::ZRP_TRANSACTION, $_data['id']);

        // PDF ticket generation
        $_ticket_id = $_data['id'];
        $_pdf       = $this->generatePdf($_data['id']);
        $_output    = $_pdf->output();
        $_file_path = $this->_web_root . PathName::UPLOAD_PDF . "ticket-$_ticket_id.pdf";
        file_put_contents($_file_path, $_output);

        // Send mail
        $_project        = $this->_translator->trans('bo.sidebar.title.project');
        $_subject        = $_project . ' | ' . $this->_translator->trans('bo.ticket.email.subject');
        $_receiver       = $_data['email'];
        $_data_params    = ['data' => $_data, 'attachs' => [$_file_path]];
        $_template       = '@Admin/ZrpCustomerTicket/email.html.twig';
        $_is_mail_sended = $this->_utils_manager->sendMail($_receiver, $_subject, $_template, $_data_params);

        if ($_is_mail_sended)
            return true;

        return false;
    }

    /**
     * Générate pdf
     * @param integer $_transaction_id
     * @return boolean
     */
    public function generatePdf($_transaction_id)
    {
        $_transaction = $this->_utils_manager->getEntityById(EntityName::ZRP_TRANSACTION, $_transaction_id);

        // PDF ticket generation
        $_pdf_options = new \Dompdf\Options();
        $_pdf_options->set('defaultFont', 'Arial')->setIsRemoteEnabled(true);
        $_pdf = new Dompdf($_pdf_options);

        $_json_details = $_transaction->getTrxDesc() ? json_decode($_transaction->getTrxDesc()) : [];
        $_template     = $this->_twig->render('@Admin/ZrpCustomerTicket/template_pdf.html.twig', array(
            'transaction'  => $_transaction,
            'json_details' => $_json_details
        ));
        $_pdf->loadHtml($_template);
        $_pdf->setPaper('A4', 'portrait');
        $_pdf->render();

        return $_pdf;
    }
}
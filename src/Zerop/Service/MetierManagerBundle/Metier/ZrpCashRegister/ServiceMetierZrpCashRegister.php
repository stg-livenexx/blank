<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpCashRegister;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCashRegisterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class ServiceMetierZrpCashRegister
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpErp
 */
class ServiceMetierZrpCashRegister
{
    private $_entity_manager;
    private $_container;
    private $_cash_register_repository;
    private $_utils_manager;

    /**
     * ServiceMetierZrpCashRegister constructor.
     * @param EntityManagerInterface $_entity_manager
     * @param ServiceMetierUtils $_utils_manager
     * @param ContainerInterface $_container
     * @param ZrpCashRegisterRepository $_cash_register_repository
     */
    public function __construct(EntityManagerInterface $_entity_manager, ServiceMetierUtils $_utils_manager,
                                ContainerInterface $_container, ZrpCashRegisterRepository $_cash_register_repository)
    {
        $this->_entity_manager           = $_entity_manager;
        $this->_container                = $_container;
        $this->_utils_manager            = $_utils_manager;
        $this->_cash_register_repository = $_cash_register_repository;
    }

    /**
     * get all cash register
     * @param $_company_id
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllCashRegister($_company_id, $_page, $_nb_max, $_search, $_order_by)
    {
        $_erp_type = $this->_cash_register_repository->getAllCashRegister($_company_id, $_page, $_nb_max, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_erp_type['result'], $_erp_type['all_result']);
    }
}
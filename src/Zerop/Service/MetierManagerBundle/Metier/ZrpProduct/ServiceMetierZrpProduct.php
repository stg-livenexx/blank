<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpProduct;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpProductRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ServiceMetierZrpProduct
 * @package App\Zerop\Service\MetierManagerBundle\Metier
 */
class ServiceMetierZrpProduct
{
    /**
     * @var ServiceMetierUtils
     */
    private $_utils_manager;

    /**
     * @var EntityManagerInterface
     */
    private $_entity_mananger;

    /**
     * @var ZrpProductRepository
     */
    private $_product_repository;

    /**
     * ServiceMetierZrpProduct constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param EntityManagerInterface $_entity_mananger
     * @param ZrpProductRepository $_product_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_mananger, ZrpProductRepository $_product_repository)
    {
        $this->_utils_manager = $_utils_manager;
        $this->_entity_mananger = $_entity_mananger;
        $this->_product_repository = $_product_repository;
    }

    /**
     * Get All product
     * @param $_page
     * @param $_nb_max_page
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllProduct($_page, $_nb_max_page, $_search, $_order_by)
    {
        $_products = $this->_product_repository->getAllProduct($_page, $_nb_max_page, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_products['result'], $_products['all_result']);
    }
}
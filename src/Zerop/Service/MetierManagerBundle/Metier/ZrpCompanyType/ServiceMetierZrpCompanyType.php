<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyType;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCompanyTypeRepository;

/**
 * Class ServiceMetierZrpCompanyType
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyTypeType
 */
class ServiceMetierZrpCompanyType
{
    private $_utils_manager;
    private $_company_Type_Repository;


    /**
     * ServiceMetierZrpCompanyType constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ZrpCompanyTypeRepository $_company_Type_Repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ZrpCompanyTypeRepository $_company_Type_Repository)
    {
        $this->_utils_manager           = $_utils_manager;
        $this->_company_Type_Repository = $_company_Type_Repository;
    }

    /**
     * company type list array
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return mixed
     */
    public function getAllCompanyType($_page, $_nb_max, $_search, $_order_by)
    {
        $_company_type = $this->_company_Type_Repository->getAllCompanyType($_page, $_nb_max, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_company_type['result'], $_company_type['all_result']);
    }
}

<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpDashboard;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class ServiceMetierZrpDashboard
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpDashboard
 */
class ServiceMetierZrpDashboard
{
    private $_entity_manager;
    private $_container;
    private $_utils_manager;

    /**
     * ServiceMetierZrpDashboard constructor.
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     * @param ServiceMetierUtils $_utils_manager
     */
    public function __construct(EntityManagerInterface $_entity_manager, ContainerInterface $_container,
                                ServiceMetierUtils $_utils_manager)
    {
        $this->_utils_manager  = $_utils_manager;
        $this->_entity_manager = $_entity_manager;
        $this->_container      = $_container;
    }
}

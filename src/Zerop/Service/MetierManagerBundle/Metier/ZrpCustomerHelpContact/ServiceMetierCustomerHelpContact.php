<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpCustomerHelpContact;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ServiceMetierCustomerHelpContact
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpCustomerHelpContact
 */
class ServiceMetierCustomerHelpContact
{
    private $_container;
    private $_utils_manager;
    private $_translator;

    public function __construct(ContainerInterface $_container, ServiceMetierUtils $_utils_manager, TranslatorInterface $_translator)
    {
        $this->_container     = $_container;
        $this->_utils_manager = $_utils_manager;
        $this->_translator    = $_translator;
    }

    /**
     * send mail
     * @param $_data
     * @return bool
     * @throws \Twig\Error\Error
     */
    public function sendMail($_data)
    {
        // Send mail
        $_project = $this->_translator->trans('bo.sidebar.title.project');
        $_subject = $_project . ' | ' . $this->_translator->trans('contact.email.subject');

        // Recover the recipient email address
        $_recipient = $this->_container->getParameter('to_email_address');

        $_data_params = ['data' => $_data];

        $_template       = '@Admin/ZrpCustomerHelpContact/email.html.twig';
        $_is_mail_sended = $this->_utils_manager->sendMail($_recipient, $_subject, $_template, $_data_params);

        if ($_is_mail_sended)
            return true;

        return false;
    }
}
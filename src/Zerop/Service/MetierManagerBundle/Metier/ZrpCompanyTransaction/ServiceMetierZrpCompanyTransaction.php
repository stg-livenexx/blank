<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyTransaction;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyTransaction;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCompanyTransactionRepository;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ServiceMetierZrpCompanyTransaction
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyTransaction
 */
class ServiceMetierZrpCompanyTransaction
{
    private $_entity_manager;
    private $_utils_manager;
    private $_company_transaction_repository;

    /**
     * ServiceMetierZrpCompanyTransaction constructor.
     * @param EntityManagerInterface $_entity_manager
     * @param ServiceMetierUtils $_utils_manager
     * @param ZrpCompanyTransactionRepository $_company_transaction_repository
     */
    public function __construct(EntityManagerInterface $_entity_manager, ServiceMetierUtils $_utils_manager,
                                ZrpCompanyTransactionRepository $_company_transaction_repository)
    {
        $this->_entity_manager                 = $_entity_manager;
        $this->_utils_manager                  = $_utils_manager;
        $this->_company_transaction_repository = $_company_transaction_repository;
    }

    /**
     * Save company transaction
     * @param $_company_id
     * @param $_nbr_transaction
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveCompanyTransaction($_company_id, $_nbr_transaction)
    {
        $_company_transaction = new ZrpCompanyTransaction();

        $_company = $this->_utils_manager->getEntityById(EntityName::ZRP_COMPANY, $_company_id);
        $_company_transaction->setZrpCompany($_company);
        $_company_transaction->setCmpTrxNbr($_nbr_transaction);

        $_now = new \DateTime();
        $_company_transaction->setCmpTrxDate($_now);

        $this->_utils_manager->saveEntity($_company_transaction, 'new');

        return $_nbr_transaction;
    }

    /**
     * Get last company transaction
     * @param integer $_company_id
     * @return integer
     */
    public function getLastCompanyTransaction($_company_id)
    {
        return $this->_company_transaction_repository->getLastCompanyTransaction($_company_id);
    }
}
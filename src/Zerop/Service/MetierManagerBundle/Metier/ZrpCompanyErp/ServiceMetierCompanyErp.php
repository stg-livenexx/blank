<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyErp;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCompanyErpRepository;

class ServiceMetierCompanyErp
{
    private $_utils_manager;
    private $_company_erp_Repository;

    /**
     * ServiceMetierCompanyErp constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ZrpCompanyErpRepository $_company_erp_Repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ZrpCompanyErpRepository $_company_erp_Repository)
    {
        $this->_utils_manager          = $_utils_manager;
        $this->_company_erp_Repository = $_company_erp_Repository;
    }

    /**
     * get all company erp
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return mixed
     */
    public function getAllCompanyErp($_page, $_nb_max, $_search, $_order_by)
    {
        $_company_type = $this->_company_erp_Repository->getAllCompanyErp($_page, $_nb_max, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_company_type['result'], $_company_type['all_result']);
    }
}
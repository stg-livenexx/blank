<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpFolder;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpFolder;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpFolderRepository;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ServiceMetierFolder
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpFolder
 */
class ServiceMetierZrpFolder
{
    private $_utils_manager;
    private $_entity_manager;
    private $_container;
    private $_request_stack;
    private $_folder_repository;

    /**
     * ServiceMetierZrpFolder constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     * @param RequestStack $_request_stack
     * @param ZrpFolderRepository $_folder_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_manager,
                                ContainerInterface $_container, RequestStack $_request_stack, ZrpFolderRepository $_folder_repository)
    {
        $this->_utils_manager     = $_utils_manager;
        $this->_entity_manager    = $_entity_manager;
        $this->_container         = $_container;
        $this->_request_stack     = $_request_stack;
        $this->_folder_repository = $_folder_repository;
    }

    /**
     * get all folder
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllFolder($_page, $_nb_max, $_search, $_order_by)
    {
        $_folders = $this->_folder_repository->getAllFolder($_page, $_nb_max, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_folders['result'], $_folders['all_result']);
    }

    /**
     * Get ticket has not folder
     * @return mixed
     */
    public function getTicketHasNotFolder()
    {
        $_tickets_has_not_foder = $this->_folder_repository->getTicketHasNotFolder();

        return $_tickets_has_not_foder;
    }

    /**
     * Get ticket on folder
     * @param $_folder_id
     * @return mixed
     */
    public function getTicketOnFolder($_folder_id)
    {
        $_tickets_has_not_foder = $this->_folder_repository->getTicketOnFolder($_folder_id);

        return $_tickets_has_not_foder;
    }

    /**
     * add filliation
     * @param $_data_post
     * @param ZrpFolder $_folder
     * @return bool
     */
    public function addFilliation($_data_post, ZrpFolder $_folder)
    {
        // Delete old file
        $_filter  = ['zrpFolder' => $_folder->getId()];
        $_tickets = $this->_utils_manager->getEntityByFilter(EntityName::ZRP_TRANSACTION, $_filter);
        foreach ($_tickets as $_ticket) {
            $_ticket->setZrpFolder(null);
            $this->_utils_manager->saveEntity($_ticket, 'new');
        }

        if (array_key_exists('selection_ticket', $_data_post)) {
            foreach ($_data_post['selection_ticket'] as $_id) {
                $_ticket = $this->_utils_manager->getEntityById(EntityName::ZRP_TRANSACTION, $_id);
                $_ticket->setZrpFolder($_folder);
                $this->_entity_manager->persist($_ticket);
            }
            $this->_entity_manager->flush();
        }

        return true;
    }

}

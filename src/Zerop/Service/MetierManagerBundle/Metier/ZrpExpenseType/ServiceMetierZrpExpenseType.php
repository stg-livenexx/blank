<?php
namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpExpenseType;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpExpenseTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class ServiceMetierZrpExpenseType
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpExpenseType
 */
class ServiceMetierZrpExpenseType
{
    private $_utils_manager;
    private $_entity_manager;
    private $_container;
    private $_expense_type_repository;

    /**
     * ServiceMetierZrpExpenseType constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     * @param ZrpExpenseTypeRepository $_expense_type_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_manager,
                                ContainerInterface $_container, ZrpExpenseTypeRepository $_expense_type_repository)
    {
        $this->_utils_manager           = $_utils_manager;
        $this->_entity_manager          = $_entity_manager;
        $this->_container               = $_container;
        $this->_expense_type_repository = $_expense_type_repository;
    }

    /**
     * get all expense type
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllExpenseType($_page, $_nb_max, $_search, $_order_by)
    {
        $_users = $this->_expense_type_repository->getAllExpenseType($_page, $_nb_max, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_users['result'], $_users['all_result']);
    }
}
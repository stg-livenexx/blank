<?php

namespace App\Zerop\Service\MetierManagerBundle\Metier\ZrpCommand;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpClient;
use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCommand;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpClientRepository;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCommandRepository;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpProductRepository;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Twig\Environment;

/**
 * Class ServiceMetierZrpCommand
 * @package App\Zerop\Service\MetierManagerBundle\Metier\ZrpCommand
 */
class ServiceMetierZrpCommand
{
    private $_utils_manager;

    private $_entity_manager;

    private $_command_repository;

    private $_product_repository;

    private $_client_repository;

    private $_twig;

    /**
     * ServiceMetierZrpCommand constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param EntityManagerInterface $_entity_manager
     * @param ZrpCommandRepository $_command_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_manager, ZrpCommandRepository $_command_repository,
                                ZrpProductRepository $_product_repository, ZrpClientRepository $_client_repository, Environment $_twig)
    {
        $this->_utils_manager = $_utils_manager;
        $this->_entity_manager = $_entity_manager;
        $this->_command_repository = $_command_repository;
        $this->_product_repository = $_product_repository;
        $this->_client_repository = $_client_repository;
        $this->_twig = $_twig;
    }

    /**
     * get all command
     * @param $_page
     * @param $_nb_max_page
     * @param $_search
     * @param $_order_by
     * @return array
     */
    public function getAllCommand($_page, $_nb_max_page, $_search, $_order_by)
    {
        $_commands = $this->_command_repository->getAllCommand($_page, $_nb_max_page, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_commands['result'], $_commands['all_result']);
    }

    public function getAdminAllCommand($_page, $_nb_max, $_search, $_custom_filter, $_order_by)
    {
        $_command = $this->_command_repository->getAdminAllCommand($_page, $_nb_max, $_search, $_custom_filter, $_order_by);

        return $this->_utils_manager->datatableResponse($_command['result'], $_command['all_result']);
    }

    /**
     * mapping command
     * @param $_request
     * @param null $_command_current
     * @return array
     */
    public function mapCommand($_request, $_command_current = null)
    {
        $_command = $_command_current === null ? new ZrpCommand() : $_command_current;

        $_command_current_number_is_equals_command_update_number = ($_command_current !== null && (int) $_command_current->getCmdNumber() === (int)$_request->request->get("number")) ? true : false;

        $_command->setCmdNumber($_request->request->get('number'));
        $_command->setCmdCreateAt($_request->request->get('createAt'));
        $_command->setCmdAmount($_request->request->get('amount'));
        $_command->setCmdDescription($_request->request->get('description'));
        $_command->addZrpProduct($this->_product_repository->find($_request->request->get('product_id')));
        $_command->setZrpClient($this->_client_repository->find($_request->request->get('client_id')));

        return [
            'command' => $_command,
            'command_current_number_is_equals_command_update_number' => $_command_current_number_is_equals_command_update_number
        ];
    }


    public function generatePdf($_command_id)
    {
        $_command = $this->_utils_manager->getEntityById(EntityName::ZRP_COMMAND, $_command_id);

        // PDF command generation
        $_pdf_options = new \Dompdf\Options();
        $_pdf_options->set('defaultFont', 'Arial')->setIsRemoteEnabled(true);
        $_pdf = new Dompdf($_pdf_options);

        $_product_details = $_command->getZrpProducts() ? $_command->getZrpProducts()->toArray() : [];
        $_template     = $this->_twig->render('@Admin/ZrpCommand/template_pdf.html.twig', array(
            'command'  => $_command,
            'product_details' => $_product_details
        ));
        $_pdf->loadHtml($_template);
        $_pdf->setPaper('A4', 'portrait');
        $_pdf->render();

        return $_pdf;
    }
}
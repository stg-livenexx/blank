<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Zerop\Service\UserBundle\Entity;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity;
use App\Zerop\Service\MetierManagerBundle\Entity\ZrpRole;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Zerop\Service\UserBundle\Repository\UserRepository")
 *
 * @ORM\Table(name="zrp_user")
 *
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Email déjà pris", errorPath="port")
 * @UniqueEntity(fields="username", message="Identifiant déjà pris", errorPath="port")
 *
 * Defines the properties of the User entity to represent the application users.
 * See https://symfony.com/doc/current/doctrine.html#creating-an-entity-class
 *
 * Tip: if you have an existing database, you can generate these entity class automatically.
 * See https://symfony.com/doc/current/doctrine/reverse_engineering.html
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_firstname", type="string", length=255, nullable=true)
     */
    private $usrFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_lastname", type="string", length=255, nullable=true)
     */
    private $usrLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_address", type="string", length=255, nullable=true)
     */
    private $usrAddress;

    /**
     * @var string
     * @ORM\Column(name="usr_phone_number", type="string", length=20, nullable=true)
     */
    private $usrPhoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_token", type="string", length=255, nullable=true)
     */
    private $usrToken;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_img_url", type="string", length=255, nullable=true)
     */
    private $usrImgUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="usr_date_birth", type="datetime", nullable=true)
     */
    private $usrDateBirth;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="usr_date_registration", type="datetime", nullable=true)
     */
    private $usrDateRegistration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="usr_date_last_connected", type="datetime", nullable=true)
     */
    private $usrDateLastConnected;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="usr_date_create", type="datetime", nullable=true)
     */
    private $usrDateCreate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="usr_date_update", type="datetime", nullable=true)
     */
    private $usrDateUpdate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usr_is_enabled", type="boolean")
     */
    private $usrIsEnabled = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @Assert\Length(min=2, max=50)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var ZrpRole
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="zrp_role_id", referencedColumnName="id")
     * })
     */
    private $zrpRole;

    /**
     * @var ZrpCity
     *
     * @ORM\ManyToOne(targetEntity="App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="zrp_city_id", referencedColumnName="id")
     * })
     */
    private $zrpCity;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usrDateCreate = new \DateTime();
        $this->usrDateRegistration = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getUsrFullname()
    {
        return $this->usrFirstname . ' ' . $this->usrLastname;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        if ($password)
            $this->password = $password;

        return $this;
    }

    /**
     * Returns the roles or permissions granted to the user for security.
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        // guarantees that a user always has at least one role for security
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * {@inheritdoc}
     */
    public function getSalt(): ?string
    {
        // We're using bcrypt in security.yaml to encode the password, so
        // the salt value is built-in and and you don't have to generate one
        // See https://en.wikipedia.org/wiki/Bcrypt

        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * {@inheritdoc}
     */
    public function eraseCredentials(): void
    {
        // if you had a plainPassword property, you'd nullify it here
        // $this->plainPassword = null;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize(): string
    {
        // add $this->salt too if you don't use Bcrypt or Argon2i
        return serialize([$this->id, $this->username, $this->password]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized): void
    {
        // add $this->salt too if you don't use Bcrypt or Argon2i
        [$this->id, $this->username, $this->password] = unserialize($serialized, ['allowed_classes' => false]);
    }

    /**
     * @return string
     */
    public function getUsrFirstname()
    {
        return $this->usrFirstname;
    }

    /**
     * @param string $usrFirstname
     */
    public function setUsrFirstname($usrFirstname)
    {
        $this->usrFirstname = $usrFirstname;
    }

    /**
     * @return string
     */
    public function getUsrLastname()
    {
        return $this->usrLastname;
    }

    /**
     * @param string $usrLastname
     */
    public function setUsrLastname($usrLastname)
    {
        $this->usrLastname = $usrLastname;
    }

    /**
     * @return string
     */
    public function getUsrAddress()
    {
        return $this->usrAddress;
    }

    /**
     * @param string $usrAddress
     */
    public function setUsrAddress($usrAddress)
    {
        $this->usrAddress = $usrAddress;
    }

    /**
     * @return string
     */
    public function getUsrPhoneNumber()
    {
        return $this->usrPhoneNumber;
    }

    /**
     * @param string $usrPhoneNumber
     */
    public function setUsrPhoneNumber($usrPhoneNumber)
    {
        $this->usrPhoneNumber = $usrPhoneNumber;
    }

    /**
     * @return string
     */
    public function getUsrToken()
    {
        return $this->usrToken;
    }

    /**
     * @param string $usrToken
     */
    public function setUsrToken(string $usrToken)
    {
        $this->usrToken = $usrToken;
    }

    /**
     * @return string
     */
    public function getUsrImgUrl()
    {
        return $this->usrImgUrl;
    }

    /**
     * @param string $usrImgUrl
     */
    public function setUsrImgUrl($usrImgUrl)
    {
        $this->usrImgUrl = $usrImgUrl;
    }

    /**
     * @return \DateTime
     */
    public function getUsrDateBirth()
    {
        return $this->usrDateBirth;
    }

    /**
     * @param \DateTime $usrDateBirth
     */
    public function setUsrDateBirth($usrDateBirth)
    {
        $this->usrDateBirth = $usrDateBirth;
    }

    /**
     * @return \DateTime
     */
    public function getUsrDateRegistration(): \DateTime
    {
        return $this->usrDateRegistration;
    }

    /**
     * @param \DateTime $usrDateRegistration
     */
    public function setUsrDateRegistration(\DateTime $usrDateRegistration)
    {
        $this->usrDateRegistration = $usrDateRegistration;
    }

    /**
     * @return \DateTime
     */
    public function getUsrDateLastConnected(): \DateTime
    {
        return $this->usrDateLastConnected;
    }

    /**
     * @param \DateTime $usrDateLastConnected
     */
    public function setUsrDateLastConnected(\DateTime $usrDateLastConnected)
    {
        $this->usrDateLastConnected = $usrDateLastConnected;
    }

    /**
     * @return \DateTime
     */
    public function getUsrDateCreate(): \DateTime
    {
        return $this->usrDateCreate;
    }

    /**
     * @param \DateTime $usrDateCreate
     */
    public function setUsrDateCreate(\DateTime $usrDateCreate)
    {
        $this->usrDateCreate = $usrDateCreate;
    }

    /**
     * @return \DateTime
     */
    public function getUsrDateUpdate(): \DateTime
    {
        return $this->usrDateUpdate;
    }

    /**
     * @param \DateTime $usrDateUpdate
     */
    public function setUsrDateUpdate(\DateTime $usrDateUpdate)
    {
        $this->usrDateUpdate = $usrDateUpdate;
    }

    /**
     * @return ZrpRole
     */
    public function getZrpRole(): ?ZrpRole
    {
        return $this->zrpRole;
    }

    /**
     * @param ZrpRole $zrpRole
     */
    public function setZrpRole(ZrpRole $zrpRole)
    {
        $this->zrpRole = $zrpRole;
    }

    /**
     * @return ZrpCity
     */
    public function getZrpCity()
    {
        return $this->zrpCity;
    }

    /**
     * @param ZrpCity $zrpCity
     */
    public function setZrpCity($zrpCity)
    {
        $this->zrpCity = $zrpCity;
    }

    /**
     * @return bool
     */
    public function isUsrIsEnabled(): bool
    {
        return $this->usrIsEnabled;
    }

    /**
     * @param bool $usrIsEnabled
     */
    public function setUsrIsEnabled(bool $usrIsEnabled)
    {
        $this->usrIsEnabled = $usrIsEnabled;
    }
}
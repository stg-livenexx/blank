<?php

namespace App\Zerop\Service\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Gregwar\CaptchaBundle\Type\CaptchaType;

/**
 * Class UserNewAccountType
 * @package App\Zerop\Service\UserBundle\Form
 */
class UserNewAccountType extends AbstractType
{
    private $_translator;

    /**
     * PpProductType constructor.
     */
    public function __construct(TranslatorInterface $_translator)
    {
        $this->_translator = $_translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('usrLastname', TextType::class, array(
                'label'    => $this->_translator->trans('bo.user.field.lastname'),
                'required' => false
            ))

            ->add('usrFirstname', TextType::class, array(
                'label'    => $this->_translator->trans('bo.user.field.firstname'),
                'required' => false
            ))

            ->add('usrAddress', TextType::class, array(
                'label'    => $this->_translator->trans('bo.user.field.address'),
                'required' => false
            ))

            ->add('usrPhoneNumber', TextType::class, array(
                'label'    => $this->_translator->trans('bo.user.field.phone'),
                'required' => false,
                'attr'     => [
                    'maxlength'                    => 20,
                    'minlength'                    => 2,
                    'data-bv-stringlength-message' => 'Ce champ doit comporter plus de 2 et moins de 20 caractères. ',
                    'data-bv-regexp'               => 'true',
                    'data-bv-regexp-regexp'        => '^\+?[0-9 ]*$',
                    'data-bv-regexp-message'       => 'Veuillez fournir un numéro téléphone valide. ',
                ]
            ))

            ->add('usrDateBirth', DateTimeType::class, [
                'label'    => $this->_translator->trans('bo.user.field.date.birth'),
                'widget'   => 'single_text',
                'format'   => 'dd/MM/yyyy',
                'html5'    => false,
                'required' => false,
                'attr'     => array(
                    'autocomplete'=> 'off'
                )
            ])

            ->add('email', EmailType::class, array(
                'label'    => $this->_translator->trans('bo.user.field.email'),
                'required' => false,
                'attr'     => [
                    'data-bv-notempty-message' => 'Ce champ est requis',
                    'data-bv-regexp'           => 'true',
                    'data-bv-regexp-regexp'    => '[^@]+@[^@]+\.[a-zA-Z]{2,}',
                    'data-bv-regexp-message'   => 'Email invalide'
                ],
            ))

            ->add('password',PasswordType::class, [
                'required' => false,
                'label'    => $this->_translator->trans('bo.user.field.password')
            ])

            ->add('captcha', CaptchaType::class, [
                'label'           => $this->_translator->trans('gregwar.captcha.label'),
                'invalid_message' => $this->_translator->trans('gregwar.captcha.invalid.message'),
                'attr'            => [
                    'placeholder' => $this->_translator->trans('gregwar.captcha.input.label')
                ],
                'as_url'          => true,
                'reload'          => true
            ]);
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Zerop\Service\UserBundle\Entity\User',
            'user_role'  => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zrp_userbundle_user';
    }
}

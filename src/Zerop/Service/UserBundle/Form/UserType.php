<?php

namespace App\Zerop\Service\UserBundle\Form;

use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserType
 * @package App\Gs\Service\UserBundle\Form
 */
class UserType extends AbstractType
{
    private $_translator;

    /**
     * UserType constructor.
     * @param TranslatorInterface $_translator
     */
    public function __construct(TranslatorInterface $_translator)
    {
        $this->_translator = $_translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user_role = $options['user_role'];
        $this->is_edit   = $options['is_edit'];

        $builder
            ->add('usrLastname', TextType::class, array(
                'label'    => $this->_translator->trans('bo.user.field.lastname'),
                'required' => true
            ))
            ->add('usrFirstname', TextType::class, array(
                'label'    => $this->_translator->trans('bo.user.field.firstname'),
                'required' => true
            ))
            ->add('username', TextType::class, array(
                'label'    => $this->_translator->trans('bo.user.field.username'),
                'required' => true
            ));
        if ($this->user_role == RoleName::ID_ROLE_CLIENT) {
            $builder
                ->add('usrAddress', TextType::class, array(
                    'label'    => $this->_translator->trans('bo.user.field.address'),
                    'required' => true
                ))
                ->add('usrPhoneNumber', TextType::class, array(
                    'label'    => $this->_translator->trans('bo.user.field.phone'),
                    'required' => true,
                    'attr'     => [
                        'maxlength'                    => 20,
                        'minlength'                    => 2,
                        'data-bv-stringlength-message' => 'Ce champ doit comporter plus de 2 et moins de 20 caractères. ',
                        'data-bv-regexp'               => 'true',
                        'data-bv-regexp-regexp'        => '^\+?[0-9 ]*$',
                        'data-bv-regexp-message'       => 'Veuillez fournir un numéro téléphone valide. ',
                    ]
                ))
                ->add('usrDateBirth', DateTimeType::class, [
                    'label'    => $this->_translator->trans('bo.user.field.date.birth'),
                    'widget'   => 'single_text',
                    'format'   => 'dd/MM/yyyy',
                    'html5'    => false,
                    'required' => false,
                    'attr'     => array(
                        'placeholder'            => 'JJ/MM/AAAA',
                        'autocomplete'           => 'off',
                        'data-bv-regexp'         => 'true',
                        'data-bv-regexp-regexp'  => '^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d$',
                        'data-bv-regexp-message' => 'Veuillez fournir une format de date valide dd/mm/yyyy',
                    )
                ])
                ->add('cmpCp', NumberType::class, array(
                    'label'    => $this->_translator->trans('bo.company.index.th.code.postal'),
                    'required' => false,
                    'attr'     => [
                        'data-bv-regexp'           => 'true',
                        'data-bv-regexp-regexp'    => '^\+?[0-9]*$',
                        'data-bv-regexp-message'   => 'Veuillez fournir une valeur valide. ',
                        'data-bv-notempty-message' => 'Ce champ est requis',
                        'id'                       => 'zrp_service_metiermanagerbundle_company_cmpCp'
                    ],
                    'mapped'   => false,
                ));
            if ($this->is_edit) {
                $builder->add('usrImgUrl', FileType::class, array(
                    'label'    => $this->_translator->trans('bo.user.field.image'),
                    'mapped'   => false,
                    'attr'     => array('accept' => 'image/*'),
                    'required' => false
                ));
            } else {
                $builder->add('usrImgUrl', FileType::class, array(
                    'label'    => $this->_translator->trans('bo.user.field.image'),
                    'mapped'   => false,
                    'attr'     => array('accept' => 'image/*'),
                    'required' => false
                ));
            }
        } else {
            if ($this->user_role == RoleName::ID_ROLE_SUPERADMIN) {
                $builder
                    ->add('usrAddress', TextType::class, array(
                        'label'    => $this->_translator->trans('bo.user.field.address'),
                        'required' => true
                    ));
            } else {
                if ($this->user_role == RoleName::ID_ROLE_ENTREPRISE) {
                    $builder
                        ->add('usrAddress', TextType::class, array(
                            'label'    => $this->_translator->trans('bo.transaction.addresse.company'),
                            'required' => true
                        ))
                        ->add('cmpCp', NumberType::class, array(
                            'label'    => $this->_translator->trans('bo.company.index.th.code.postal'),
                            'required' => true,
                            'attr'     => [
                                'data-bv-regexp'           => 'true',
                                'data-bv-regexp-regexp'    => '^\+?[0-9]*$',
                                'data-bv-regexp-message'   => 'Veuillez fournir une valeur valide. ',
                                'data-bv-notempty-message' => 'Ce champ est requis',
                                'id'                       => 'zrp_service_metiermanagerbundle_company_cmpCp'
                            ],
                            'mapped'   => false,
                        ));
                }
            }
            $builder
                ->add('usrPhoneNumber', TextType::class, array(
                    'label'    => $this->_translator->trans('bo.user.field.phone'),
                    'required' => true,
                    'attr'     => [
                        'maxlength'                    => 20,
                        'minlength'                    => 2,
                        'data-bv-stringlength-message' => 'Ce champ doit comporter plus de 2 et moins de 20 caractères. ',
                        'data-bv-regexp'               => 'true',
                        'data-bv-regexp-regexp'        => '^\+?[0-9 ]*$',
                        'data-bv-regexp-message'       => 'Veuillez fournir un numéro téléphone valide. ',
                    ]
                ))
                ->add('usrDateBirth', DateTimeType::class, [
                    'label'    => $this->_translator->trans('bo.user.field.date.birth'),
                    'widget'   => 'single_text',
                    'format'   => 'dd/MM/yyyy',
                    'html5'    => false,
                    'required' => false,
                    'attr'     => array(
                        'placeholder'            => 'JJ/MM/AAAA',
                        'autocomplete'           => 'off',
                        'data-bv-regexp'         => 'true',
                        'data-bv-regexp-regexp'  => '^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d$',
                        'data-bv-regexp-message' => 'Veuillez fournir une format de date valide dd/mm/yyyy',
                    )
                ])
                ->add('usrImgUrl', FileType::class, array(
                    'label'    => $this->_translator->trans('bo.user.field.image'),
                    'mapped'   => false,
                    'attr'     => array('accept' => 'image/*'),
                    'required' => false
                ));
        }
        $builder
            ->add('usrIsEnabled', CheckboxType::class, array(
                'label'    => $this->_translator->trans('bo.user.field.enabled'),
                'required' => false
            ))
            ->add('email', EmailType::class, array(
                'label'    => $this->_translator->trans('bo.user.field.email'),
                'attr'     => [
                    'data-bv-notempty-message' => 'Ce champ est requis',
                    'data-bv-regexp'           => 'true',
                    'data-bv-regexp-regexp'    => '[^@]+@[^@]+\.[a-zA-Z]{2,}',
                    'data-bv-regexp-message'   => 'Email invalide'
                ],
                'required' => true
            ));

        if ($this->user_role != RoleName::ID_ROLE_CLIENT) {
            $builder->add('password', RepeatedType::class, [
                'type'            => PasswordType::class,
                'first_options'   => [
                    'label' => $this->_translator->trans('bo.user.field.password'),
                    'attr'  => [
                        'minlength'                    => 6,
                        'data-bv-identical'            => "false",
                        'data-bv-identical-field'      => 'zrp_userbundle_user[password][second]',
                        'data-bv-stringlength-message' => 'Ce champ doit comporter plus de 6 caractères',
                        "data-bv-identical-message"    => 'Les deux mots de passe ne sont pas identiques.',
                        'data-bv-notempty-message'     => 'Ce champ est requis'
                    ]
                ],
                'second_options'  => [
                    'label' => $this->_translator->trans('bo.user.field.password.confirm'),
                    'attr'  => [
                        'data-bv-identical'         => "true",
                        'data-bv-identical-field'   => 'zrp_userbundle_user[password][first]',
                        'data-bv-notempty-message'  => 'Ce champ est requis',
                        "data-bv-identical-message" => 'Les deux mots de passe ne sont pas identiques.',
                    ]],
                'invalid_message' => $this->_translator->trans('bo.user.field.password.confirm.incorrect'),
            ]);
        };

        $builder
            ->add('zrpRole', EntityType::class, array(
                'label'         => $this->_translator->trans('bo.user.field.role'),
                'class'         => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpRole',
                'query_builder' => function (EntityRepository $_er) {
                    $_query_builder = $_er->createQueryBuilder('rl');

                    if ($this->user_role == RoleName::ID_ROLE_CLIENT) {
                        $_query_builder
                            ->andWhere('rl.id = :id_role')
                            ->setParameter('id_role', RoleName::ID_ROLE_CLIENT);
                    }
                    if ($this->user_role == RoleName::ID_ROLE_ENTREPRISE) {
                        $_query_builder
                            ->andWhere('rl.id = :id_role')
                            ->setParameter('id_role', RoleName::ID_ROLE_ENTREPRISE);
                    }

                    $_query_builder->orderBy('rl.rlName', 'ASC');

                    return $_query_builder;
                },
                'choice_label'  => 'rlName',
                'multiple'      => false,
                'expanded'      => false,
                'required'      => true,
                'placeholder'   => $this->_translator->trans('bo.user.field.role.select')
            ))
            ->add('zrpCity', TextType::class, [
                'label'  => $this->_translator->trans('bo.company.index.th.commune'),
                'mapped' => false,
            ]);
//            ->add('zrpCity', EntityType::class, array(
//                'label'         => $this->_translator->trans('bo.user.field.city'),
//                'class'         => 'App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity',
//                'query_builder' => function (EntityRepository $_er) {
//                    return $_er
//                        ->createQueryBuilder('ct')
//                        ->orderBy('ct.ctName', 'ASC');
//                },
//                'choice_label'  => 'ctName',
//                'attr'          => array('class' => 'kl-select-city'),
//                'multiple'      => false,
//                'expanded'      => false,
//                'required'      => false,
//                'placeholder'   => $this->_translator->trans('bo.user.field.city.select')
//            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Zerop\Service\UserBundle\Entity\User',
            'user_role'  => null,
            'is_edit'    => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'zrp_userbundle_user';
    }
}
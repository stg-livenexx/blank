<?php

namespace App\Zerop\Service\UserBundle\Manager;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpTransaction;
use App\Zerop\Service\MetierManagerBundle\Utils\PathName;
use App\Zerop\Service\UserBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UploadManager
 * @package App\Gs\Service\UserBundle\Manager
 */
class UploadManager
{
    protected $_em;
    protected $_web_root;

    /**
     * UploadManager constructor.
     * @param EntityManagerInterface $_em
     * @param ParameterBagInterface $_root_dir
     */
    public function __construct(EntityManagerInterface $_em, ParameterBagInterface $_root_dir)
    {
        $this->_em       = $_em;
        $this->_web_root = realpath($_root_dir->get('kernel.project_dir') . '/public');
    }

    /**
     * Deleting file (file with entity)
     * @param integer $_id
     * @return array
     */
    public function deleteImageById($_id)
    {
        $_user = $this->_em->getRepository('UserBundle:User')->find($_id);

        if ($_user) {
            try {
                $_path = $this->_web_root.$_user->getUsrImgUrl();

                // Deleting file
                @unlink($_path);

                // Deleting in database
                $_user->setUsrImgUrl(null);
                $this->_em->persist($_user);
                $this->_em->flush();

                return array(
                    'success' => true
                );
            } catch (\Exception $_exception) {

                return array(
                    'success' => false,
                    'message' => $_exception->getTraceAsString()
                );
            }
        } else {

            return array(
                'success' => false,
                'message' => 'Image not found in database'
            );
        }
    }

    /**
     * Deleting file (file only)
     * @param integer $_id
     * @return array
     */
    public function deleteOnlyImageById($_id)
    {
        $_user = $this->_em->getRepository('UserBundle:User')->find($_id);

        if ($_user) {
            $_path = $this->_web_root.$_user->getUsrImgUrl();

            @unlink($_path);
        }
    }

    /**
     * Upload file
     * @param User $_user
     * @param file $_image
     */
    public function upload(User $_user, $_image)
    {
        // Retrieve the specific image directory
        $_directory_image = PathName::UPLOAD_IMAGE_USER;

        try {
            $_original_name = $_image->getClientOriginalName();
            $_path_part     = pathinfo($_original_name);
            $_extension     = $_path_part['extension'];
            $_filename      = md5(uniqid());

            // Upload image
            $_filename_extension = $_filename . '.' . $_extension;
            $_uri_file           = $_directory_image . $_filename_extension;
            $_dir                = $this->_web_root . $_directory_image;
            $_image->move(
                $_dir,
                $_filename_extension
            );

            $_user->setUsrImgUrl($_uri_file);
        } catch (\Exception $_exception) {
            throw new NotFoundHttpException("Error while uploading file : " . $_exception->getMessage());
        }
    }

	/**
	 * Upload file
	 * @param ZrpTransaction $_customer_ticket
	 * @param $_image
	 */
	public function uploadCustomerTicket(ZrpTransaction $_customer_ticket, $_image)
	{
		// Retrieve the specific image directory
		$_directory_image = PathName::UPLOAD_CUSTOMER;

		try {
			$_original_name = $_image->getClientOriginalName();
			$_path_part     = pathinfo($_original_name);
			$_extension     = $_path_part['extension'];
			$_filename      = md5(uniqid());

			// Upload image
			$_filename_extension = $_filename . '.' . $_extension;
			$_uri_file           = $_directory_image . $_filename_extension;
			$_dir                = $this->_web_root . $_directory_image;
			$_image->move(
				$_dir,
				$_filename_extension
			);

			$_customer_ticket->setTrxFile($_uri_file);
		} catch (\Exception $_exception) {
			throw new NotFoundHttpException("Error while uploading file : " . $_exception->getMessage());
		}
	}
}
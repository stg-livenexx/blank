<?php

namespace App\Zerop\Service\UserBundle\Manager;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity;
use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany;
use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCustomer;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCityRepository;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use App\Zerop\Service\UserBundle\Entity\User;
use App\Zerop\Service\UserBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserManager
 * @package App\Zerop\Service\UserBundle\Manager
 */
class UserManager
{
    private $_utils_manager;
    private $_entity_manager;
    private $_city_repository;
    private $_container;
    private $_request_stack;
    private $_web_root;
    private $_upload_manager;
    private $_encoded;
    private $_user_repository;
    private $_translator;

    /**
     * UserManager constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     * @param RequestStack $_request_stack
     * @param ParameterBagInterface $_root_dir
     * @param UploadManager $_upload_manager
     * @param UserPasswordEncoderInterface $_encoded
     * @param UserRepository $_user_repository
     * @param TranslatorInterface $_translator
     */
    public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_manager,
                                ContainerInterface $_container, RequestStack $_request_stack,
                                ParameterBagInterface $_root_dir, UploadManager $_upload_manager,
                                UserPasswordEncoderInterface $_encoded, UserRepository $_user_repository,
                                TranslatorInterface $_translator, ZrpCityRepository $_city_repository)
    {
        $this->_utils_manager   = $_utils_manager;
        $this->_entity_manager  = $_entity_manager;
        $this->_container       = $_container;
        $this->_request_stack   = $_request_stack;
        $this->_web_root        = realpath($_root_dir->get('kernel.project_dir') . '/public');
        $this->_upload_manager  = $_upload_manager;
        $this->_encoded         = $_encoded;
        $this->_user_repository = $_user_repository;
        $this->_translator      = $_translator;
        $this->_city_repository = $_city_repository;
    }

    /**
     * Get a user repository
     * @return array
     */
    public function getRepository()
    {
        return $this->_entity_manager->getRepository(EntityName::ZRP_USER);
    }

    /**
     * Get all user
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     * @throws \Exception
     */
    public function getAllUser($_page, $_nb_max, $_search, $_order_by)
    {
        $_users = $this->_user_repository->getAllUser($_page, $_nb_max, $_search, $_order_by);

        return $this->_utils_manager->datatableResponse($_users['result'], $_users['all_result']);
    }

    /**
     * Get all users by order
     * @param array $_order
     * @return array
     */
    public function getAllUserByOrder($_order)
    {
        return $this->getRepository()->findBy(array(), $_order);
    }

    /**
     * Get user by id
     * @param Integer $_id
     * @return array
     */
    public function getUserById($_id)
    {
        return $this->getRepository()->find($_id);
    }

    /**
     * Test the existence username
     * @param string $_username
     * @return boolean
     */
    public function isUsernameExist($_username)
    {
        $_exist = $this->getRepository()->findByUsername($_username);
        if ($_exist) {
            return true;
        }

        return false;
    }

    /**
     * Test the existence email
     * @param string $_email
     * @return boolean
     */
    public function isEmailExist($_email)
    {
        $_exist = $this->getRepository()->findByEmail($_email);
        if ($_exist) {
            return true;
        }

        return false;
    }

    /**
     * Add user
     * @param User $_user
     * @param Object $_form
     * @return User
     * @throws \Exception
     */
    public function addUser($_user, $_form)
    {
        // User role processing
        $_type      = $_form['zrpRole']->getData();
        $_user_role = RoleName::$ROLE_TYPE[$_type->getRlName()];
        $_user->setRoles(array($_user_role));

        //get city by google maps
        $_city     = $_form['zrpCity']->getData();
        $_zrp_city = $this->saveCityNotExist($_city);
        $_zrp_city = is_array($_zrp_city) ? array_shift($_zrp_city) : $_zrp_city;
        $_user->setZrpCity($_zrp_city);

        // Update password
        $_password = $_form['password']->getData();
        if ($_password) {
            $_encoded = $this->_encoded->encodePassword($_user, $_password);
            $_user->setPassword($_encoded);
        }

        // User photo processing
        $_img_photo = $_form['usrImgUrl']->getData();
        if ($_img_photo) {
            $this->_upload_manager->upload($_user, $_img_photo);
        }

        return $this->saveUser($_user, 'new');
    }

    /**
     * Save City
     * @param $_city
     * @param $_action
     * @return mixed
     */
    public function saveCityNotExist($_city)
    {
        $_zrp_city = $this->_city_repository->findByCtName($_city);
        if (!$_zrp_city) {
            $_zrp_city = new ZrpCity();
            $_zrp_city->setCtName($_city);
            $this->_entity_manager->persist($_zrp_city);
            $this->_entity_manager->flush();
        }
        return $_zrp_city;
    }

    /**
     * Update user
     * @param User $_user
     * @param Object $_form
     * @return User
     */
    public function updateUser($_user, $_form)
    {
        $_user_connected = $this->_container->get('security.token_storage')->getToken()->getUser();

        // User photo processing
        $_img_photo = $_form['usrImgUrl']->getData();
        // If there is a new file added, delete the old file and save it again
        if ($_img_photo) {
            $this->_upload_manager->deleteOnlyImageById($_user->getId());
            $this->_upload_manager->upload($_user, $_img_photo);
        }

        //get city by google maps
        $_city     = $_form['zrpCity']->getData();
        $_zrp_city = $this->saveCityNotExist($_city);
        $_zrp_city = is_array($_zrp_city) ? array_shift($_zrp_city) : $_zrp_city;
        $_user->setZrpCity($_zrp_city);

        // User role processing
        $_type      = $_form['zrpRole']->getData();
        $_user_role = RoleName::$ROLE_TYPE[$_type->getRlName()];
        $_user->setRoles(array($_user_role));

        $_user->setUsrDateUpdate(new \DateTime());

        if (($_user_connected->getZrpRole()->getId() != RoleName::ID_ROLE_CLIENT)) {
            // Update password
            $_password = $_form['password']->getData();
            if ($_password) {
                $_encoded = $this->_encoded->encodePassword($_user, $_password);
                $_user->setPassword($_encoded);
            }
        }

        return $this->saveUser($_user, 'update');
    }

    /**
     * Save user
     * @param User $_user
     * @param string $_action
     * @return User
     */
    public function saveUser($_user, $_action)
    {
        if ($_action == 'new') {
            $this->_entity_manager->persist($_user);
        }
        $this->_entity_manager->flush();

        return $_user;
    }

    /**
     * Deleting user
     * @param User $_user
     * @return boolean
     */
    public function deleteUser($_user)
    {
        // Deleting image
        $this->_upload_manager->deleteImageById($_user->getId());

        $this->_entity_manager->remove($_user);
        $this->_entity_manager->flush();

        return true;
    }

    /**
     * Get user by mail
     * @param string $_email
     * @return array
     */
    public function findByEmail($_email)
    {
        return $this->getRepository()->findByEmail($_email);
    }

    /**
     * Get user by username
     * @param string $_username
     * @return array
     */
    public function findByUsername($_username)
    {
        return $this->getRepository()->findByUsername($_username);
    }

    /**
     * @param User $_user
     * @param $_post_data
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addUserAccount(User $_user, $_post_data)
    {
        $_siren_number   = "";
        $_data_form_type = $_post_data['zrp_userbundle_user'];
        if (isset($_post_data['siren_number'])) $_siren_number = $_post_data['siren_number'];
        $_form_type_data = $_post_data['zrp_userbundle_user'];
        $_username       = $_form_type_data['email'] ? $_form_type_data['email'] : '';

        $_type_compte = $_post_data['type_compte'];
        $_role_id     = $_type_compte == 'professionel' ? RoleName::ID_ROLE_ENTREPRISE : RoleName::ID_ROLE_CLIENT;
        $_role        = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_ROLE,
            ['id' => $_role_id]);

        $_user->setUsername($_username);
        $_user->setUsrIsEnabled(true);
        if ($_role) {
            $_user->setZrpRole($_role);
        }
        $_user->setRoles([RoleName::ROLE_CLIENT]);

        // Update password
        $_password = $_data_form_type['password'] ?? false;
        if ($_password) {
            $_encoded = $this->_encoded->encodePassword($_user, $_password);
            $_user->setPassword($_encoded);
        }
        $this->_utils_manager->saveEntity($_user, 'new');
        if ($_type_compte == 'professionel') {
            $_company = new ZrpCompany();
            $_user->setRoles([RoleName::ROLE_ENTREPRISE]);
            $_company->setCmpName($_data_form_type['usrLastname']);
            $_company->setCmpSiren($_siren_number);
            $_company->setZrpUser($_user);
            $this->_utils_manager->saveEntity($_company, 'new');
        } else {
            $_customer = new ZrpCustomer();
            $_user->setRoles([RoleName::ROLE_CLIENT]);
            $_customer->setZrpUser($_user);
            $this->_utils_manager->saveEntity($_customer, 'new');
        }
        $this->_utils_manager->saveEntity($_user, 'update');

        return true;
    }

    /**
     * Récupération mot de passe oublié
     * @param array $_data
     * @return boolean
     * @throws \Twig\Error\Error
     */
    public function resettingPassword($_data)
    {
        // Récupérer l'utilisateur
        $_user = $this->findByEmail($_data['user_username']);

        if ($_user) {
            // Générer un mot de passe
            $_generated_password = uniqid();
            $_encoded            = $this->_encoded->encodePassword($_user[0], $_generated_password);
            $_user[0]->setPassword($_encoded);

            $this->saveUser($_user[0], 'update');

            $_data['user_fullname'] = $_user[0]->getUsrFullname();
            $_data['user_password'] = $_generated_password;

            // Envoyer un email contenant la récuperation du mot de passe oublié
            $_project     = $this->_translator->trans('bo.sidebar.title.project');
            $_subject     = $_project . ' | ' . $this->_translator->trans('resetting.password.email.subject');
            $_receiver    = $_data['user_username'];
            $_data_params = ['data' => $_data];
            $_template    = '@User/Email/email_resetting_password.html.twig';
            $this->_utils_manager->sendMail($_receiver, $_subject, $_template, $_data_params);

            return true;
        }

        return false;
    }

    /**
     * add customer from data api transaction
     * @param $_data_api
     * @param $_city
     * @param $_company_connected
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addUserCustomerFromApiTransaction($_data_api, $_city, $_company_connected)
    {
        $_role_client_id = RoleName::ID_ROLE_CLIENT;
        $_role_client    = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_ROLE, ['id' => $_role_client_id]);

        $_customer_name  = $_data_api->name;
        $_customer_email = $_data_api->email ? $_data_api->email : $_customer_name . '@zerop.com';

        $_user_customer = new User();
        $_user_customer->setEmail($_customer_email);
        $_user_customer->setZrpRole($_role_client);
        $_user_customer->setRoles([RoleName::ROLE_CLIENT]);
        $_user_customer->setUsername($_customer_email);
        $_user_customer->setUsrLastname($_customer_name);
        $_user_customer->setZrpCity($_city);
        $_encoded = $this->_encoded->encodePassword($_user_customer, uniqid());
        $_user_customer->setPassword($_encoded);
        $_user_customer = $this->_utils_manager->saveEntity($_user_customer, 'new');

        $_customer = new ZrpCustomer();
        $_customer->setZrpUser($_user_customer);
        $_customer->setZrpCompany($_company_connected);

        return $this->_utils_manager->saveEntity($_customer, 'new');
    }

    /**
     * update company connected
     * @param $_company
     * @param $_post
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateCompanyConnected($_company, $_post)
    {
        $_activity = $this->_utils_manager->getEntityById(EntityName::ZRP_COMPANY_ACTIVITY, $_post['zrpCompanyActivity']);
        $_city     =  $_post['zrp_userbundle_user'] ? $_post['zrp_userbundle_user']['zrpCity'] : null;
        $_code_postal     =  $_post['zrp_userbundle_user'] ? $_post['zrp_userbundle_user']['cmpCp'] : null;
        $_zrp_city = $this->saveCityNotExist($_city);
        $_zrp_city = is_array($_zrp_city) ? array_shift($_zrp_city) : $_zrp_city;
        // $_city  = $this->_utils_manager->getEntityById(EntityName::ZRP_CITY, $_post['zrpCity']);
        $_company->setCmpName($_post['cmpName']);
        $_company->setCmpCp($_code_postal);
        if (isset($_post['cmpDesc'])) $_company->setCmpDesc($_post['cmpDesc']);
        $_company->setCmpSiren($_post['cmpSiren']);
        if ($_activity) $_company->setZrpCompanyActivity($_activity);
        if ($_zrp_city) $_company->setZrpCity($_zrp_city);

        return $this->_utils_manager->saveEntity($_company, 'update');
    }
}



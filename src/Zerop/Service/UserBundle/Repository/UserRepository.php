<?php

namespace App\Zerop\Service\UserBundle\Repository;

use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\UserBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class UserRepository
 * @package App\Zerop\Service\UserBundle\Repository
 */
class UserRepository extends ServiceEntityRepository
{
    private $_entity_manager;

    /**
     * UserRepository constructor.
     * @param ManagerRegistry $_registry
     * @param EntityManagerInterface $_entity_manager
     */
    public function __construct(ManagerRegistry $_registry, EntityManagerInterface $_entity_manager)
    {
        $this->_entity_manager = $_entity_manager;
        parent::__construct($_registry, User::class);
    }

    /**
     * Get all user
     * @param $_page
     * @param $_nb_max
     * @param $_search
     * @param $_order_by
     * @return array
     * @throws \Exception
     */
    public function getAllUser($_page, $_nb_max, $_search, $_order_by)
    {
        $_user     = EntityName::ZRP_USER;
        $_order_by = $_order_by ? $_order_by : "usr.id DESC";

        $_having = "HAVING usr.usrLastname LIKE :search OR
                    usr.email LIKE :search OR
                    usr.usrAddress LIKE :search OR
                    usrDateCreate LIKE :search OR
                    rl.rlName LIKE :search
                    ORDER BY $_order_by";

        $_no_having = "ORDER BY $_order_by";

        $_dql = "SELECT usr.id AS id, usr.usrLastname AS usrLastname, usr.email AS email, usr.usrAddress AS usrAddress,
                 rl.rlName AS rlName, DATE_FORMAT(usr.usrDateCreate, '%d/%m/%Y') AS usrDateCreate
                 FROM $_user usr
                 LEFT JOIN usr.zrpRole rl";

        $_result     = $this->_entity_manager->createQuery($_dql . ' ' . $_having)
            ->setParameter("search", "%{$_search}%")
            ->setFirstResult($_page)
            ->setMaxResults($_nb_max)
            ->getResult();
        $_all_result = $this->_entity_manager->createQuery($_dql . ' ' . $_no_having)
            ->getResult();

        return [
            'result'     => $_result,
            'all_result' => $_all_result
        ];
    }
}

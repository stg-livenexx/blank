<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Zerop\Service\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * Controller used to manage the application security.
 * See https://symfony.com/doc/current/security/form_login_setup.html.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class SecurityController extends AbstractController
{
    use TargetPathTrait;

    /**
     * Login
     * @param Request $_request
     * @param Security $_security
     * @param AuthenticationUtils $_helper
     * @return Response
     */
    public function login(Request $_request, Security $_security, AuthenticationUtils $_helper): Response
    {
        // if user is already logged in, don't display the login page again
        if ($_security->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('dashboard_index');
        }

        // this statement solves an edge-case: if you change the locale in the login
        // page, after a successful login you are redirected to a page in the previous
        // locale. This code regenerates the referrer URL whenever the login page is
        // browsed, to ensure that its locale is always the current one.
        $this->saveTargetPath($_request->getSession(), 'main', $this->generateUrl('dashboard_index'));

        return $this->render('@User/Security/login.html.twig', [
            // last username entered by the user (if any)
            'last_username' => $_helper->getLastUsername(),
            // last authentication error (if any)
            'error' => $_helper->getLastAuthenticationError()
        ]);
    }

    /**
     * This is the route the user can use to logout.
     * But, this will never be executed. Symfony will intercept this first
     * and handle the logout automatically. See logout in config/packages/security.yaml
     */
    public function logout(): void
    {
        throw new \Exception('This should never be reached!');
    }
}

<?php

namespace App\Zerop\Service\UserBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use App\Zerop\Service\UserBundle\Entity\User;
use App\Zerop\Service\UserBundle\Form\UserNewAccountType;
use App\Zerop\Service\UserBundle\Form\UserType;
use App\Zerop\Service\UserBundle\Manager\UploadManager;
use App\Zerop\Service\UserBundle\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserController
 * @package App\Gs\Service\UserBundle\Controller
 */
class UserController extends AbstractController
{
    private $_utils_manager;
    private $_user_manager;
    private $_translator;
    private $_upload_manager;

    /**
     * UserController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param UserManager $_user_manager
     * @param TranslatorInterface $_translator
     * @param UploadManager $_upload_manager
     */
    public function __construct(ServiceMetierUtils $_utils_manager, UserManager $_user_manager,
                                TranslatorInterface $_translator, UploadManager $_upload_manager)
    {
        $this->_utils_manager  = $_utils_manager;
        $this->_user_manager   = $_user_manager;
        $this->_translator     = $_translator;
        $this->_upload_manager = $_upload_manager;
    }

    /**
     * List ajax
     * @param Request $_request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAjax(Request $_request)
    {
        $_page     = $_request->query->get('start');
        $_nb_max   = $_request->query->get('length');
        $_search   = $_request->query->get('search')['value'];
        $_order_by = $_request->query->get('order_by');

        $_list_user = $this->_user_manager->getAllUser($_page, $_nb_max, $_search, $_order_by);

        return new JsonResponse($_list_user);
    }

    /**
     * Display a page index
     * @return Response
     */
    public function index()
    {
        return $this->render('@User/User/index.html.twig');
    }

    /**
     * Adding
     * @param Request $_request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function add(Request $_request)
    {
        $_user = new User();
        $_form = $this->createCreateForm($_user);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $this->_user_manager->addUser($_user, $_form);
            $_flash_message = $this->_translator->trans('bo.confirmation.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('user_index'));
        }

        return $this->render('@User/User/add.html.twig', array(
            'user' => $_user,
            'form' => $_form->createView()
        ));
    }

    /**
     * Editing
     * @param Request $_request
     * @param User $_user
     * @return Response
     */
    public function edit(Request $_request, User $_user)
    {
        if (!$_user) {
            $_exception_message = $this->_translator->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        // Securisation url
        $_user_connected    = $this->container->get('security.token_storage')->getToken()->getUser();
        $_user_connected_id = $_user_connected->getId();
        if (($_user_connected->getZrpRole()->getId() != RoleName::ID_ROLE_SUPERADMIN)) {
            if ($_user_connected_id != $_user->getId())
                return $this->redirectToRoute('user_edit', ['id' => $_user_connected_id]);
        }

        $_company_connected = $this->_utils_manager->getCompanyConnected();
        $_desc_company      = "";

        $_company_activity = $this->_utils_manager->getAllEntities(EntityName::ZRP_COMPANY_ACTIVITY);
        $_city             = $this->_utils_manager->getAllEntities(EntityName::ZRP_CITY);

        //set data city
        $_city      = $_user->getZrpCity() ? $_user->getZrpCity()->getCtName() : '';
        $_edit_form = $this->createEditForm($_user);
        $_edit_form->get('zrpCity')->setData($_city);

        if (!empty($_company_connected)) {
            $_desc_company = $_company_connected->getCmpDesc();
            //set data code postal
            $_cmp_cp = $_company_connected ? $_company_connected->getCmpCp() : '';
            $_edit_form->get('cmpCp')->setData($_cmp_cp);
        }


        return $this->render('@User/User/edit.html.twig', array(
            'user'               => $_user,
            'edit_form'          => $_edit_form->createView(),
            'role_entreprise_id' => RoleName::ID_ROLE_ENTREPRISE,
            'company'            => $_company_connected,
            'city'               => $_city,
            'company_activity'   => $_company_activity,
            'desc_company'       => $_desc_company
        ));
    }

    /**
     * Updating
     * @param Request $_request
     * @param User $_user
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function update(Request $_request, User $_user)
    {
        // Get user connected
        $_user_connected = $this->container->get('security.token_storage')->getToken()->getUser();
        $_user_role      = $_user_connected->getZrpRole()->getId();

        if (!$_user) {
            $_exception_message = $this->_translator->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        $_edit_form         = $this->createEditForm($_user);
        $_company_connected = $this->_utils_manager->getCompanyConnected();
        $_company_activity  = $this->_utils_manager->getAllEntities(EntityName::ZRP_COMPANY_ACTIVITY);
        $_city              = $this->_utils_manager->getAllEntities(EntityName::ZRP_CITY);
        $_edit_form->handleRequest($_request);

        if ($_edit_form->isValid()) {
            $_post = $_request->request->all();
            if ($_user_role == RoleName::ID_ROLE_ENTREPRISE)
                $this->_user_manager->updateCompanyConnected($_company_connected, $_post);
            $this->_user_manager->updateUser($_user, $_edit_form);
            $_flash_message = $this->_translator->trans('bo.confirmation.update');
            $this->_utils_manager->setFlash('success', $_flash_message);

            if (in_array($_user_role, [RoleName::ID_ROLE_CLIENT, RoleName::ID_ROLE_ENTREPRISE]))
                return $this->redirectToRoute('user_edit', ['id' => $_user->getId()]);

            return $this->redirect($this->generateUrl('user_index'));
        }

        return $this->render('@User/User/edit.html.twig', array(
            'user'               => $_user,
            'edit_form'          => $_edit_form->createView(),
            'role_entreprise_id' => RoleName::ID_ROLE_ENTREPRISE,
            'company'            => $_company_connected,
            'city'               => $_city,
            'company_activity'   => $_company_activity
        ));
    }


    /**
     * Display a page detail user
     * @param User $_user
     * @return Response
     */
    public function show(User $_user)
    {
        if (!$_user) {
            $_exception_message = $this->_translator->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        return $this->render('@User/User/show.html.twig', array(
            'user' => $_user,
        ));
    }

    /**
     * Deleting
     * @param Request $_request
     * @param User $_user
     * @return RedirectResponse
     * @throws \Exception
     */
    public function delete(Request $_request, User $_user)
    {
        $_form = $this->createDeleteForm($_user);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid()) && !empty($_user->getZrpRole())) {
            $_user_role_id = $_user->getZrpRole() ? $_user->getZrpRole()->getId() : 0;
            if ($_user_role_id == RoleName::ID_ROLE_SUPERADMIN) {
                $_message = $this->_translator->trans('bo.security.authorisation.not.allowed');
                $this->_utils_manager->setFlash('error', $_message);

                return $this->redirectToRoute('user_index');
            }

            $this->_user_manager->deleteUser($_user);

            $_flash_message = $this->_translator->trans('bo.confirmation.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * Creation add form
     * @param User $_user
     * @return FormInterface
     */
    private function createCreateForm(User $_user)
    {
        // Get user connected
        $_user_connected = $this->container->get('security.token_storage')->getToken()->getUser();
        $_user_role      = $_user_connected->getZrpRole()->getId();

        $_form = $this->createForm(UserType::class, $_user, array(
            'action'    => $this->generateUrl('user_add'),
            'method'    => 'POST',
            'user_role' => $_user_role,
            'is_edit'   => false
        ));

        return $_form;
    }

    /**
     * Creation account form
     * @param User $_user
     * @return FormInterface
     */
    private function createCreateAcountForm(User $_user)
    {
        $_form = $this->createForm(UserNewAccountType::class, $_user, array(
            'action' => $this->generateUrl('security_user_register'),
            'method' => 'POST'
        ));

        return $_form;
    }

    /**
     * Create edit form
     * @param User $_user
     * @return FormInterface
     */
    private function createEditForm(User $_user)
    {
        // Get user connected
        $_user_connected = $this->container->get('security.token_storage')->getToken()->getUser();
        $_user_role      = $_user_connected->getZrpRole()->getId();

        $_form = $this->createForm(UserType::class, $_user, array(
            'action'    => $this->generateUrl('user_update', array('id' => $_user->getId())),
            'method'    => 'PUT',
            'user_role' => $_user_role,
            'is_edit'   => true
        ));

        return $_form;
    }

    /**
     * Creation deleting form user
     * @param User $_user
     * @return FormInterface
     */
    private function createDeleteForm(User $_user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $_user->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Ajax user image file deletion
     * @param Request $_request
     * @return JsonResponse
     */
    public function deleteImageAjax(Request $_request)
    {
        // Get all data form
        $_data = $_request->request->all();
        $_id   = $_data['id'];

        $_response = $this->_upload_manager->deleteImageById($_id);

        return new JsonResponse($_response);
    }

    /**
     * Register account
     * @param Request $_request
     * @return Response
     * @throws \Exception
     */
    public function register(Request $_request)
    {
        $_user = new User();
        $_form = $this->createCreateAcountForm($_user);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $_data_post      = $_request->request->all();
            $_create_account = $this->_user_manager->addUserAccount($_user, $_data_post);
            if ($_create_account) {
                $_flash_message = $this->_translator->trans('bo.confirmation.add');
                $this->_utils_manager->setFlash('success', $_flash_message);

                return $this->redirect($this->generateUrl('security_login'));
            }
        }

        return $this->render('@User/Register/index.html.twig', [
            'form' => $_form->createView()
        ]);
    }

    /**
     * Récuperation mot de passe oublié
     * @param Request $_request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function resettingPassword(Request $_request)
    {
        // Récupérer l'utilisateur connecté
        $_user_connected = $this->get('security.token_storage')->getToken()->getUser();

        // Redirection vers page si l'utilisateur est déjà connecté
        if ($_user_connected != 'anon.') {
            return $this->redirectToRoute('dashboard_index');
        }

        if ($_request->isMethod('POST')) {
            // Récuperer les données formulaire
            $_post = $_request->request->all();

            $_resetting_password = $this->_user_manager->resettingPassword($_post);

            if (!$_resetting_password) {
                $_flash_message = $this->_translator->trans('resetting.password.user.not.identified');
                $this->_utils_manager->setFlash('error', $_flash_message);

                return $this->redirect($this->generateUrl('security_resetting_password'));
            }

            $_flash_message = $this->_translator->trans('resetting.password.success');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('security_resetting_password'));
        }

        return $this->render('@User/Security/resetting_password.html.twig');
    }
}

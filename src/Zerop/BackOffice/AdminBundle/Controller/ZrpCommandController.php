<?php
 namespace App\Zerop\BackOffice\AdminBundle\Controller;

 use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCommand;
 use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
 use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCommand\ServiceMetierZrpCommand;
 use App\Zerop\Service\MetierManagerBundle\Repository\ZrpClientRepository;
 use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCommandRepository;
 use App\Zerop\Service\MetierManagerBundle\Repository\ZrpProductRepository;
 use Doctrine\Common\Collections\ArrayCollection;
 use Doctrine\ORM\EntityManagerInterface;
 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Contracts\Translation\TranslatorInterface;

 /**
  * Class ZrpCommandController
  * @package App\Zerop\BackOffice\AdminBundle\Controller
  */
 class ZrpCommandController extends AbstractController
 {
     /**
      * @var ServiceMetierUtils
      */
     private $_utils_manager;

     /**
      * @var EntityManagerInterface
      */
     private $_entity_manager;

     /**
      * @var ServiceMetierZrpCommand
      */
     private $_command_manager;

     /**
      * @var ZrpCommandRepository
      */
     private $_command_repository;

     /**
      * @var ZrpProductRepository
      */
     private $_product_repository;

     /**
      * @var ZrpClientRepository
      */
     private $_client_repository;

     /**
      * @var TranslatorInterface
      */
     private $_translator;

     /**
      * ZrpCommandController constructor.
      * @param ServiceMetierUtils $_utils_manager
      * @param EntityManagerInterface $_entity_manager
      * @param ServiceMetierZrpCommand $_command_manager
      * @param ZrpProductRepository $_product_repository
      * @param ZrpClientRepository $_client_repository
      * @param ZrpCommandRepository $_command_repository
      */
     public function __construct(ServiceMetierUtils $_utils_manager, EntityManagerInterface $_entity_manager, ServiceMetierZrpCommand $_command_manager,
                                  ZrpProductRepository $_product_repository, ZrpClientRepository $_client_repository, ZrpCommandRepository $_command_repository,
                                    TranslatorInterface $_translator)
     {
         $this->_utils_manager = $_utils_manager;
         $this->_entity_manager = $_entity_manager;
         $this->_command_manager = $_command_manager;
         $this->_product_repository = $_product_repository;
         $this->_client_repository = $_client_repository;
         $this->_command_repository = $_command_repository;
         $this->_translator = $_translator;
     }

     /**
      * display page index
      * @return \Symfony\Component\HttpFoundation\Response
      */
     public function index()
     {
         $_products = $this->_product_repository->findAll();
         $_clients = $this->_client_repository->findAll();

         return $this->render('@Admin/ZrpCommand/index.html.twig', [
             'products'   =>  $_products,
             'clients'    =>  $_clients
         ]);
     }

     /**
      * list client ajax
      * @param Request $_request
      * @return \Symfony\Component\HttpFoundation\JsonResponse
      */
     public function listAjax(Request $_request)
     {
         $_page     = $_request->query->get('start');
         $_nb_max   = $_request->query->get('length');
         $_search   = $_request->query->get('search')['value'];
         $_order_by = $_request->query->get('order_by');

         $_custom_filter = [
             'numero_one'       =>  $_request->query->get('numero_one'),
             'numero_two'       =>  $_request->query->get('numero_two'),
             'date_command_one' =>  $_request->query->get('date_command_one'),
             'date_command_two' =>  $_request->query->get('date_command_two'),
             'montant_one'      =>  $_request->query->get('montant_one'),
             'montant_two'      =>  $_request->query->get('montant_two'),
             'decription'       =>  $_request->query->get('decription'),
             'product_id'       =>  $_request->query->get('product_id'),
             'timezone'         =>  $_request->query->get('timezone')
         ];


         $_list_command = $this->_command_manager->getAdminAllCommand($_page, $_nb_max, $_search, $_custom_filter, $_order_by);

         return $this->json($_list_command, 200, []);
     }

     /**
      * create a new command
      * @param Request $_request
      */
     public function addAjax(Request $_request)
     {
         if($_request->isMethod('POST'))
         {
             $_data = $this->_command_manager->mapCommand($_request);
             $_command_new = $_data['command'];

             if($this->_command_repository->findOneBy(['cmdNumber' => $_command_new->getCmdNumber()]))
             {
                 return $this->json(['exist_number' => $this->_translator->trans('bo.command.number.already.exist')], 400, []);
             }
             $this->_utils_manager->saveEntity($_command_new, 'new');

             return $this->json(['success' => $this->_translator->trans('bo.confirmation.add')], 200, []);
         }

         return $this->redirectToRoute('command_index');
     }

     /**
      * get a single commande for editing
      * @param ZrpCommand $_command
      * @return \Symfony\Component\HttpFoundation\JsonResponse
      */
     public function editAjax(ZrpCommand $_command)
     {
         return $this->json($_command, 200, [], ['groups' => 'command:read']);
     }

     /**
      * update a signle commande
      * @param Request $_request
      * @param ZrpCommand $_command
      * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
      * @throws \Doctrine\ORM\ORMException
      * @throws \Doctrine\ORM\OptimisticLockException
      */
     public function updateAjax(Request $_request, ZrpCommand $_command)
     {
         if($_request->isMethod('PUT'))
         {
             $_data = $this->_command_manager->mapCommand($_request, $_command);
             $_command_update = $_data['command'];

             if($this->_command_repository->findOneBy(['cmdNumber' => $_command_update->getCmdNumber()])
                 && $_data['command_current_number_is_equals_command_update_number'] === false)
             {
                 return $this->json(['exist_number' => $this->_translator->trans('bo.command.number.already.exist')], 400, []);
             }
             $this->_utils_manager->saveEntity($_command_update, 'update');

             return $this->json(['success' => $this->_translator->trans('bo.confirmation.update')], 200, []);
         }


         return $this->redirectToRoute('command_index');
     }

     /**
      * delete a single command
      * @param Request $_request
      * @param ZrpCommand $_command
      * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
      * @throws \Doctrine\ORM\ORMException
      * @throws \Doctrine\ORM\OptimisticLockException
      */
     public function deleteAjax(Request $_request, ZrpCommand $_command)
     {
         if($_request->isMethod('DELETE'))
         {
             $this->_utils_manager->deleteEntity($_command);

             return $this->json(['success' => $this->_translator->trans('bo.confirmation.delete')], 200, []);
         }

         return $this->redirectToRoute('command_index');
     }

     /**
      * Generate pdf
      * @param Request $_request
      * @param ZrpCommand $_command
      */
     public function generatePdf(Request $_request, ZrpCommand $_command)
     {
         $_pdf = $this->_command_manager->generatePdf($_command->getId());

         $_command_id = $_command->getId();
         $_pdf->stream("ticket-$_command_id.pdf", [
             "Attachment" => false
         ]);

         exit();
     }

 }
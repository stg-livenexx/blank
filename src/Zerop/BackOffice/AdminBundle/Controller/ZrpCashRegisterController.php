<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCashRegister;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpCashRegisterType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCashRegister\ServiceMetierZrpCashRegister;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCashRegisterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpCashRegisterController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpCashRegisterController extends AbstractController
{
    private $_utils_manager;
    private $_zrp_cash_register_manager;
    private $_translator;
    private $_cash_register_repository;

    /**
     * ZrpCashRegisterController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param TranslatorInterface $_translator
     * @param ServiceMetierZrpCashRegister $_zrp_cash_register_manager
     * @param ZrpCashRegisterRepository $_cash_register_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, TranslatorInterface $_translator,
                                ServiceMetierZrpCashRegister $_zrp_cash_register_manager,
                                ZrpCashRegisterRepository $_cash_register_repository
    )
    {
        $this->_utils_manager             = $_utils_manager;
        $this->_translator                = $_translator;
        $this->_zrp_cash_register_manager = $_zrp_cash_register_manager;
        $this->_cash_register_repository  = $_cash_register_repository;
    }

    /**
     * index cash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('@Admin/ZrpCashRegister/index.html.twig');
    }

    /**
     * list ajax cash
     * @param Request $_request
     * @return JsonResponse
     */
    public function listAjax(Request $_request)
    {
        $_page        = $_request->query->get('start');
        $_nb_max_page = $_request->query->get('length');
        $_search      = $_request->query->get('search')['value'];
        $_order_by    = $_request->query->get('order_by');

        /** get company connected */
        $_company    = $this->_utils_manager->getCompanyConnected();
        $_company_id = $_company ? $_company->getId() : 0;

        $_zrp_cash_register = $this->_zrp_cash_register_manager->getAllCashRegister($_company_id, $_page, $_nb_max_page, $_search, $_order_by);

        return new JsonResponse($_zrp_cash_register);
    }

    /**
     * list num ajax
     * @return JsonResponse
     */
    public function listNumAjax()
    {
        /** get company connected */
        $_company    = $this->_utils_manager->getCompanyConnected();
        $_company_id = $_company ? $_company->getId() : 0;

        $_zrp_cash_register = $this->_cash_register_repository->getAllCashRegisterNum($_company_id);

        return new JsonResponse($_zrp_cash_register);

    }

    /**
     * add cash
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function add(Request $_request)
    {
        $_zrp_cash_register = new ZrpCashRegister();
        $_form              = $this->createCreateForm($_zrp_cash_register);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {

            /* get company connected */
            $_company          = $this->_utils_manager->getCompanyConnected();
            $_company_id       = $_company ? $_company->getId() : 0;
            $_num_exist_in_cmp = $this->numExistInCmp($_company_id, $_request, $_form);

            if ($_num_exist_in_cmp) {
                $_referer = $_request->headers->get('referer');
                return $this->redirect($_referer);
            }

            if ($_company)
                $_zrp_cash_register->setZrpCompany($_company ? $_company : 0);

            $this->_utils_manager->saveEntity($_zrp_cash_register, 'new');
            $_flash_message = $this->_translator->trans('bo.controller.message.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirectToRoute('cash_register_index');
        }

        return $this->render('@Admin/ZrpCashRegister/add.html.twig', [
            'form' => $_form->createView(),
        ]);
    }

    /**
     * edit cash
     * @param ZrpCashRegister $_zrp_cash_register
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(ZrpCashRegister $_zrp_cash_register, Request $_request)
    {
        $_form = $this->createEditForm($_zrp_cash_register);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {

            /** get company connected */
            $_company       = $this->_utils_manager->getCompanyConnected();
            $_company_id    = $_company ? $_company->getId() : 0;
            $_filter        = ['csRgtNum' => $_form->get('csRgtNum')->getData(), 'zrpCompany' => $_company_id];
            $_cash_register = $this->_utils_manager->findOneEntityByFilter(ZrpCashRegister::class, $_filter);
            if ($_cash_register && $_cash_register->getId() != $_zrp_cash_register->getId()) {
                $_referer       = $_request->headers->get('referer');
                $_flash_message = $this->_translator->trans('bo.cash.register.already.exist');
                $this->_utils_manager->setFlash('error', $_flash_message);
                return $this->redirect($_referer);
            }

            $this->_utils_manager->saveEntity($_zrp_cash_register, 'update');
            $_flash_message = $this->_translator->trans('bo.confirmation.update');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirectToRoute('cash_register_index');
        }

        return $this->render('@Admin/ZrpCashRegister/edit.html.twig', [
            'form' => $_form->createView()
        ]);
    }

    /**
     * create delete form cash
     * @param ZrpCashRegister $_zrp_cash_register
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(ZrpCashRegister $_zrp_cash_register)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cash_register_delete', ['id' => $_zrp_cash_register->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * delete cash
     * @param Request $_request
     * @param ZrpCashRegister $_zrp_cash_register
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Request $_request, ZrpCashRegister $_zrp_cash_register)
    {
        $_form = $this->createDeleteForm($_zrp_cash_register);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_zrp_cash_register);
            $_flash_message = $this->_translator->trans('bo.controller.message.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirectToRoute('cash_register_index');
        }

        return $this->redirectToRoute('cash_register_index');
    }

    /**
     * create create form
     * @param ZrpCashRegister $_zrp_cash_register
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createCreateForm(ZrpCashRegister $_zrp_cash_register)
    {
        $_user_connected = $this->container->get('security.token_storage')->getToken()->getUser();
        $_user_role      = $_user_connected->getZrpRole()->getId();

        $_form = $this->createForm(ZrpCashRegisterType::class, $_zrp_cash_register, array(
            'action'    => $this->generateUrl('cash_register_new'),
            'method'    => 'POST',
            'user_role' => $_user_role
        ));
        return $_form;
    }

    /**
     * create edit form
     * @param ZrpCashRegister $_zrp_cash_register
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createEditForm(ZrpCashRegister $_zrp_cash_register)
    {
        $_user_connected = $this->container->get('security.token_storage')->getToken()->getUser();
        $_user_role      = $_user_connected->getZrpRole()->getId();

        $_form = $this->createForm(ZrpCashRegisterType::class, $_zrp_cash_register, array(
            'action'    => $this->generateUrl('cash_register_edit', array('id' => $_zrp_cash_register->getId())),
            'method'    => 'POST',
            'user_role' => $_user_role
        ));

        return $_form;
    }

    /**
     * num exist in company
     * @param $_company_id
     * @param $_request
     * @param $_form
     * @return bool
     * @throws \Exception
     */
    public function numExistInCmp($_company_id, $_request, $_form)
    {
        if ($_company_id)
            $_filter = ['csRgtNum' => $_form->get('csRgtNum')->getData(), 'zrpCompany' => $_company_id];
        else {
            $_company_id = $_request->get('zrp_adminbundle_erp')['zrpCompany'] ?
                $_request->get('zrp_adminbundle_erp')['zrpCompany'] : 0;
            $_filter     = ['csRgtNum' => $_form->get('csRgtNum')->getData(), 'zrpCompany' => $_company_id];
        }

        if ($this->_utils_manager->getEntityByFilter(ZrpCashRegister::class, $_filter)) {
            $_flash_message = $this->_translator->trans('bo.cash.register.already.exist');
            $this->_utils_manager->setFlash('error', $_flash_message);
            return true;
        }
        return false;
    }
}
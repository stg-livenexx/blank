<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpTransaction;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpCustomerTicketType;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpTicketType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCustomerTicket\ServiceMetierZrpCustomerTicket;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpTransaction\ServiceMetierZrpTransaction;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpTransactionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpCustomerTicketController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpCustomerTicketController extends AbstractController
{
    private $_transaction_manager;
    private $_translator;
    private $_utils_manager;
    private $_customer_ticket_manager;
    private $_transaction_repository;

    /**
     * ZrpCustomerTicketController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpTransaction $_transaction_manager
     * @param TranslatorInterface $_translator
     * @param ZrpTransactionRepository $_transaction_repository
     * @param ServiceMetierZrpCustomerTicket $_customer_ticket_manager
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpTransaction $_transaction_manager,
                                TranslatorInterface $_translator, ZrpTransactionRepository $_transaction_repository,
                                ServiceMetierZrpCustomerTicket $_customer_ticket_manager)
    {
        $this->_transaction_manager     = $_transaction_manager;
        $this->_translator              = $_translator;
        $this->_utils_manager           = $_utils_manager;
        $this->_customer_ticket_manager = $_customer_ticket_manager;
        $this->_transaction_repository  = $_transaction_repository;
    }

    /**
     * Display a page index
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $_user_connected = $this->_utils_manager->getUserConnected();
        return $this->render('@Admin/ZrpCustomerTicket/index.html.twig', [
            'min_max' => $this->_transaction_repository->getMinMaxTransactionAmountAndPostalCodeBiller($_user_connected)
        ]);
    }

    /**
     * list filter ajax
     * @param Request $_request
     * @return bool|\Symfony\Component\HttpFoundation\Response
     */
    public function listFilterAjax(Request $_request)
    {
        $_sort    = $_request->query->get('sort');
        $_filters = [
            'start_date_filter' => $_request->query->get('start_date_filter'),
            'end_date_filter'   => $_request->query->get('end_date_filter'),
            'price_range'       => $_request->query->get('price_range'),
            'postal_code_range' => $_request->query->get('postal_code_range'),
            'num_caisse'        => $_request->query->get('num_caisse'),
        ];
        $_user_connected = $this->_utils_manager->getUserConnected();
        if ($_user_connected) {
            $_transactions = $this->_transaction_repository->getCustomerTransactionByFilter($_user_connected, $_filters, $_sort);
            return $this->render('@Admin/ZrpCustomerTicket/partial/list_ticket_visuel_partial.html.twig', [
                'transactions' => $_transactions
            ]);
        }
        return false;
    }

    /**
     * List ajax
     * @param Request $_request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAjax(Request $_request)
    {
        $_page     = $_request->query->get('start');
        $_nb_max   = $_request->query->get('length');
        $_search   = $_request->query->get('search')['value'];
        $_order_by = $_request->query->get('order_by');

        $_customer_ticket = $this->_transaction_manager->getAllTransaction($_page, $_nb_max, $_search, $_order_by);

        return new JsonResponse($_customer_ticket);
    }


    /**
     * add customer ticket
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(Request $_request)
    {
        $_customer_ticket = new ZrpTransaction();
        $_form            = $this->createCreateForm($_customer_ticket);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {

            $this->_transaction_manager->saveCustomerTicket($_customer_ticket, $_form);
            $_flash_message = $this->_translator->trans('bo.controller.message.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('customer_ticket_index'));
        }

        return $this->render('@Admin/ZrpCustomerTicket/add.html.twig', [
            'form' => $_form->createView()
        ]);
    }

    /**
     * show customer ticket
     * @param ZrpTransaction $_transaction
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(ZrpTransaction $_transaction)
    {
        $_json_details = $_transaction->getTrxDesc() ? json_decode($_transaction->getTrxDesc()) : [];

        return $this->render('@Admin/ZrpCustomerTicket/show.html.twig',
            ['transaction'  => $_transaction,
             'json_details' => $_json_details
            ]);
    }

    /**
     * update modal ajax
     * @param Request $_request
     * @param ZrpTransaction $_transaction
     * @return bool|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateAjax(Request $_request, ZrpTransaction $_transaction)
    {
        $_update_form = $this->createForm(ZrpCustomerTicketType::class, $_transaction, array(
            'action' => $this->generateUrl('customer_ticket_update_ajax', array('id' => $_transaction->getId())),
            'method' => 'PUT',
        ));
        $_update_form->handleRequest($_request);

        if ($_update_form->isSubmitted() && $_update_form->isValid() && $_request->isXmlHttpRequest()) {
            $this->_utils_manager->saveEntity($_transaction, 'update');
            return new JsonResponse(['status' => true]);
        }

        return $this->render('@Admin/ZrpCustomerTicket/partial/edit_ticket_form_partial.html.twig', [
            'update_form' => $_update_form->createView()
        ]);
    }

    /**
     * Send mail customer ticket
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sendMail(Request $_request)
    {
        // Récupérer les données formulaire
        $_data = $_request->request->all();

        $_is_mail_sended = $this->_customer_ticket_manager->sendMail($_data);
        $_flash_message  = $this->_translator->trans('bo.ticket.email.sent.error');
        $_type           = 'error';
        if ($_is_mail_sended) {
            $_flash_message = $this->_translator->trans('bo.ticket.email.sent.success');
            $_type          = 'success';
        }

        $this->_utils_manager->setFlash($_type, $_flash_message);

        return $this->redirect($this->generateUrl('customer_ticket_show', ['id' => $_data['id']]));
    }

    /**
     * Générer pdf ticket
     * @param Request $_request
     * @param ZrpTransaction $_transaction
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function generatePdf(Request $_request, ZrpTransaction $_transaction)
    {
        $_pdf = $this->_customer_ticket_manager->generatePdf($_transaction->getId());

        $_ticket_id = $_transaction->getId();
        $_pdf->stream("ticket-$_ticket_id.pdf", [
            "Attachment" => false
        ]);

        exit();
    }

    /**
     * Creation new form customer ticket
     * @param ZrpTransaction $_customer_ticket
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createCreateForm(ZrpTransaction $_customer_ticket)
    {
        $_form = $this->createForm(ZrpTicketType::class, $_customer_ticket, array(
            'action' => $this->generateUrl('customer_ticket_add'),
            'method' => 'POST'
        ));

        return $_form;
    }
}

<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCustomerHelpContact\ServiceMetierCustomerHelpContact;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpCustomerHelpContactController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpCustomerHelpContactController extends AbstractController
{
    private $_utils_manager;
    private $_customer_help_contact;
    private $_translator;

    /**
     * ZrpCustomerHelpContactController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierCustomerHelpContact $_customer_help_contact
     * @param TranslatorInterface $_translator
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierCustomerHelpContact $_customer_help_contact, TranslatorInterface $_translator)
    {
        $this->_utils_manager         = $_utils_manager;
        $this->_customer_help_contact = $_customer_help_contact;
        $this->_translator            = $_translator;
    }

    /**
     * Display a page index
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('@Admin/ZrpCustomerHelpContact/index.html.twig');
    }

    /**
     * send mail
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function sendMail(Request $_request)
    {
        // recovery data
        $_data = $_request->request->all();

        $_is_mail_sended = $this->_customer_help_contact->sendMail($_data);
        $_flash_message  = $this->_translator->trans('contact.mail.not.sent');
        $_type           = 'error';
        if ($_is_mail_sended) {
            $_flash_message = $this->_translator->trans('contact.mail.sent');
            $_type          = 'success';
        }

        $this->_utils_manager->setFlash($_type, $_flash_message);

        return $this->redirect($this->generateUrl('customer_help_contact_index'));

    }
}

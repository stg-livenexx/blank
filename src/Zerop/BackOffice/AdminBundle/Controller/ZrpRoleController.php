<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpRole;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpRoleType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpRoleController
 * @package App\Gs\BackOffice\AdminBundle\Controller
 */
class ZrpRoleController extends AbstractController
{
    private $_utils_manager;
    private $_translator;

    /**
     * UserController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param TranslatorInterface $_translator
     */
    public function __construct(ServiceMetierUtils $_utils_manager, TranslatorInterface $_translator)
    {
        $this->_utils_manager = $_utils_manager;
        $this->_translator    = $_translator;
    }

    /**
     * Display a page index
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $_order = ['id' => 'asc'];
        $_roles = $this->_utils_manager->getAllEntitiesByOrder(EntityName::ZRP_ROLE, [], $_order);

        return $this->render('@Admin/ZrpRole/index.html.twig', [
            'roles' => $_roles
        ]);
    }

    /**
     * new role
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(Request $_request)
    {
        $_role = new ZrpRole();

        $_form = $this->createCreateForm($_role);
        $_form->handleRequest($_request);
        if ($_form->isSubmitted() && $_form->isValid()) {
            $this->_utils_manager->saveEntity($_role, 'new');
            $_flash_message = $this->_translator->trans('bo.controller.message.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('role_index'));
        }

        return $this->render('@Admin/ZrpRole/add.html.twig', [
            'form' => $_form->createView()
        ]);
    }

    /**
     * Creation editing form role
     * @param ZrpRole $_role
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createCreateForm(ZrpRole $_role)
    {
        $_form = $this->createForm(ZrpRoleType::class, $_role, array(
            'action' => $this->generateUrl('role_add'),
            'method' => 'POST'
        ));

        return $_form;
    }

    /**
     * function create edit form
     * @param ZrpRole $_zrpRole
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createEditForm(ZrpRole $_zrpRole)
    {
        $_form = $this->createForm(ZrpRoleType::class, $_zrpRole, array(
            'action' => $this->generateUrl('role_update', array('id' => $_zrpRole->getId())),
            'method' => 'PUT'
        ));

        return $_form;
    }

    /**
     * Creation formulaire de suppression centre
     * @param ZrpRole $_zrpRole
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(ZrpRole $_zrpRole)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('role_delete', array('id' => $_zrpRole->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Edit page
     * @param ZrpRole $_zrpRole
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ZrpRole $_zrpRole)
    {
        $_form = $this->createEditForm($_zrpRole);

        return $this->render('@Admin/ZrpRole/edit.html.twig', [
            'edit_form' => $_form->createView()
        ]);
    }

    /**
     * update role
     * @param Request $_request
     * @param ZrpRole $_zrpRole
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Request $_request, ZrpRole $_zrpRole)
    {
        $_form = $this->createEditForm($_zrpRole);

        $_form->handleRequest($_request);
        if ($_form->isValid()) {
            $this->_utils_manager->saveEntity($_zrpRole, 'update');
            $_flash_message = $this->_translator->trans('bo.controller.message.edit');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('role_index'));
        }

        return $this->render('@Admin/ZrpRole/edit.html.twig', [
            'edit_form' => $_form->createView()
        ]);
    }

    /**
     * delete role
     * @param Request $_request
     * @param ZrpRole $_zrpRole
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function delete(Request $_request, ZrpRole $_zrpRole)
    {
        $_form = $this->createDeleteForm($_zrpRole);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_zrpRole);
            $_flash_message = $this->_translator->trans('bo.controller.message.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('role_index'));
        }

        return $this->redirectToRoute('role_index');
    }
}

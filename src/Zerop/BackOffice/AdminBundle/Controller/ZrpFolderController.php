<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpFolder;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpFolderType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpFolder\ServiceMetierZrpFolder;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpTransaction\ServiceMetierZrpTransaction;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpFolderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpFolderController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpFolderController extends AbstractController
{
    private $_utils_manager;
    private $_folder_manager;
    private $_translator;
    private $_folder_repository;
    private $_transaction_manager;

    /**
     * ZrpFolderController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpFolder $_folder_manager
     * @param TranslatorInterface $_translator
     * @param ZrpFolderRepository $_folder_repository
     * @param ServiceMetierZrpTransaction $_transaction_manager
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpFolder $_folder_manager,
                                TranslatorInterface $_translator, ZrpFolderRepository $_folder_repository,
                                ServiceMetierZrpTransaction $_transaction_manager)
    {
        $this->_utils_manager       = $_utils_manager;
        $this->_folder_manager      = $_folder_manager;
        $this->_transaction_manager = $_transaction_manager;
        $this->_translator          = $_translator;
        $this->_folder_repository   = $_folder_repository;
    }

    /**
     * List ajax
     * @param Request $_request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAjax(Request $_request)
    {
        $_folders = $this->_folder_repository->getFoldersAll($_request->query->get('sort'));

        return $this->render('@Admin/ZrpFolder/liste.html.twig', [
            'folders' => $_folders
        ]);
    }

    /**
     * List datatable ajax
     * @param Request $_request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listDatatableAjax(Request $_request)
    {
        $_page     = $_request->query->get('start');
        $_nb_max   = $_request->query->get('length');
        $_search   = $_request->query->get('search')['value'] ?? '';
        $_order_by = $_request->query->get('order_by');

        $_zrp_folders = $this->_folder_manager->getAllFolder($_page, $_nb_max, $_search, $_order_by);

        return new JsonResponse($_zrp_folders);
    }

    /**
     * Display a page index
     * @return Response
     */
    public function index()
    {
        return $this->render('@Admin/ZrpFolder/index.html.twig');
    }

    /**
     * Adding
     * @param Request $_request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function add(Request $_request)
    {
        $_folder = new ZrpFolder();
        $_form   = $this->createCreateForm($_folder);
        $_form->handleRequest($_request);
        $_tickets = $this->_folder_manager->getTicketHasNotFolder();

        if ($_form->isSubmitted() && $_form->isValid()) {
            $_folder_label = $_request->get('zrp_service_metiermanagerbundle_folder')['fldName'];
            // testing folder exist
            $_folder_exist = $this->_folder_repository->getNameFolderByCustomer($_folder_label, NULL);
            $_post_data    = $_request->request->all();
            $_customer     = $this->_utils_manager->getCustomerConnected();
            if ($_customer) {
                $_date_enregistrement = new \DateTime();
                $_folder->setZrpCustomer($_customer);
                $_folder->setFldUpdatedDate($_date_enregistrement);
            }
            if (!$_folder_exist) {
                $this->_utils_manager->saveEntity($_folder, 'new');
                $this->_folder_manager->addFilliation($_post_data, $_folder);
                $_flash_message = $this->_translator->trans('bo.confirmation.add');
                $this->_utils_manager->setFlash('success', $_flash_message);

                return $this->redirect($this->generateUrl('folder_index'));
            }
            $_flash_message = $this->_translator->trans('bo.folder.filter.exist');
            $this->_utils_manager->setFlash('error', $_flash_message);
            $this->redirect($this->generateUrl('folder_add'));
        }
        return $this->render('@Admin/ZrpFolder/add.html.twig', array(
            'folder'  => $_folder,
            'form'    => $_form->createView(),
            'tickets' => $_tickets
        ));
    }

    /**
     * Editing folder
     * @param ZrpFolder $_folder
     * @return Response
     */
    public function edit(ZrpFolder $_folder)
    {
        if (!$_folder) {
            $_exception_message = $this->get('translator')->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        $_form_edit = $this->createEditForm($_folder);

        $_tickets = $this->_folder_manager->getTicketOnFolder($_folder->getId());

        return $this->render('@Admin/ZrpFolder/edit.html.twig', array(
            'tickets'   => $_tickets,
            'folder'    => $_folder,
            'edit_form' => $_form_edit->createView()
        ));
    }

    /**
     * Updating
     * @param Request $_request
     * @param ZrpFolder $_folder
     * @return RedirectResponse|Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function update(Request $_request, ZrpFolder $_folder)
    {
        $_folder_id = $_folder ? $_folder->getId() : 0;
        if (!$_folder) {
            $_exception_message = $this->get('translator')->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        $_form_edit = $this->createEditForm($_folder);
        $_form_edit->handleRequest($_request);

        $_tickets = $this->_folder_manager->getTicketOnFolder($_folder->getId());

        if ($_form_edit->isValid()) {
            $_folder_label = $_request->get('zrp_service_metiermanagerbundle_folder')['fldName'];
            $_folder_exist = $this->_folder_repository->getNameFolderByCustomer($_folder_label, $_folder_id);
            if (!$_folder_exist) {
                $_date_enregistrement = new \DateTime();
                $_folder->setFldUpdatedDate($_date_enregistrement);
                $_post_data = $_request->request->all();
                $this->_utils_manager->saveEntity($_folder, 'update');
                $this->_folder_manager->addFilliation($_post_data, $_folder);
                $_flash_message = $this->_translator->trans('bo.confirmation.update');
                $this->_utils_manager->setFlash('success', $_flash_message);

                return $this->redirect($this->generateUrl('folder_index'));
            }
            $_flash_message = $this->_translator->trans('bo.folder.filter.exist');
            $this->_utils_manager->setFlash('error', $_flash_message);
        }
        return $this->render('@Admin/ZrpFolder/edit.html.twig', array(
            'tickets'   => $_tickets,
            'folder'    => $_folder,
            'edit_form' => $_form_edit->createView()
        ));
    }

    /**
     * Deleting
     * @param Request $_request
     * @param ZrpFolder $_folder
     * @return RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Request $_request, ZrpFolder $_folder)
    {
        $_form = $this->createDeleteForm($_folder);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_folder);
            $_flash_message = $this->_translator->trans('bo.controller.message.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('folder_index'));
        }

        return $this->redirectToRoute('folder_index');
    }

    /**
     * show all ticket by folder
     * @param ZrpFolder $_folder
     * @return Response
     */
    public function ticketList(ZrpFolder $_folder)
    {
        return $this->render('@Admin/ZrpFolder/ticket.html.twig',
            ['folder' => $_folder]);
    }

    /**
     *  list ajax ticket by folder
     * @param Request $_request
     * @param ZrpFolder $_folder
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function ticketListAjax(Request $_request, ZrpFolder $_folder)
    {
        $_page        = $_request->query->get('start');
        $_nb_max      = $_request->query->get('length');
        $_search      = $_request->query->get('search')['value'];
        $_time_zone   = $_request->query->get('timezone');
        $_order_by    = $_request->query->get('order_by');
        $_list_ticket = $this->_transaction_manager->getAllTicketByFolder($_page, $_nb_max, $_search, $_order_by, $_folder, $_time_zone);

        return new JsonResponse($_list_ticket);
    }

    /**
     * Creation add form
     * @param ZrpFolder $_folder
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createCreateForm(ZrpFolder $_folder)
    {
        $_form = $this->createForm(ZrpFolderType::class, $_folder, array(
            'action' => $this->generateUrl('folder_add'),
            'method' => 'POST'
        ));

        return $_form;
    }

    /**
     * Create edit form
     * @param ZrpFolder $_folder
     * @return FormInterface
     */
    private function createEditForm(ZrpFolder $_folder)
    {

        $_form = $this->createForm(ZrpFolderType::class, $_folder, array(
            'action' => $this->generateUrl('folder_update', array('id' => $_folder->getId())),
            'method' => 'PUT'
        ));

        return $_form;
    }

    /**
     * Creation deleting form folder
     * @param ZrpFolder $_folder
     * @return FormInterface
     */
    private function createDeleteForm(ZrpFolder $_folder)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('folder_delete', array('id' => $_folder->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}

<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompany\ServiceMetierCompany;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpDashboard\ServiceMetierZrpDashboard;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpClientFinalRepository;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpTransactionRepository;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use App\Zerop\Service\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ZrpDashboardController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpDashboardController extends AbstractController
{
    private $_utils_manager;
    private $_dashboard_manager;
    private $_client_final_repository;
    private $_company_manager;
    private $_transaction_repository;

    /**
     * ZrpDashboardController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpDashboard $_dashboard_manager
     * @param ZrpClientFinalRepository $_client_final_repository
     * @param ZrpTransactionRepository $_transaction_repository
     * @param ServiceMetierCompany $_company_manager
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpDashboard $_dashboard_manager,
                                ZrpClientFinalRepository $_client_final_repository, ZrpTransactionRepository $_transaction_repository,
                                ServiceMetierCompany $_company_manager)
    {
        $this->_utils_manager           = $_utils_manager;
        $this->_dashboard_manager       = $_dashboard_manager;
        $this->_client_final_repository = $_client_final_repository;
        $this->_company_manager         = $_company_manager;
        $this->_transaction_repository  = $_transaction_repository;
    }

    /**
     * Display a dashboard
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index()
    {
        /**
         * @var User $_user_connected
         */
        $_user_connected = $this->_utils_manager->getUserConnected();
        if ($_user_connected &&
            $_user_connected->getZrpRole() &&
            $_user_connected->getZrpRole()->getId() === RoleName::ID_ROLE_CLIENT) {
            return $this->render('@Admin/ZrpDashboard/index_customer.html.twig');
        }

        $_now = new \DateTime();

        $_sum_transaction_today     = $this->_transaction_repository->getSumTransactionByDateTime($_now);
        $_sum_transaction_yesterday = $this->_transaction_repository->getSumTransactionByDateTime($_now->modify('- 1 day'));
        $_sum_total_transaction     = $this->_transaction_repository->getSumTransactionAmount();
        $_nbr_client_final          = $this->_client_final_repository->getNbrClientFinaux();
        $_nbr_company               = $this->_company_manager->getNbrCompany();
        $_transaction_clients       = $this->_client_final_repository->getLastedClientTransaction();

        return $this->render('@Admin/ZrpDashboard/index.html.twig', [
            'sum_transaction_today'     => $_sum_transaction_today,
            'sum_transaction_yesterday' => $_sum_transaction_yesterday,
            'sum_total_transaction'     => $_sum_total_transaction,
            'nbr_client_final'          => $_nbr_client_final ?? 0,
            'nbr_company'               => $_nbr_company ?? 0,
            'transaction_clients'       => $_transaction_clients
        ]);

    }

    /**
     * @param Request $_request
     * @return bool|\Symfony\Component\HttpFoundation\Response
     */
    public function listTransactionCustomerAjax(Request $_request)
    {
        $_term           = trim($_request->query->get('term'));
        $_user_connected = $this->_utils_manager->getUserConnected();
        if ($_user_connected && $_user_connected->getZrpRole() &&
            $_user_connected->getZrpRole()->getId() === RoleName::ID_ROLE_CLIENT) {
            $_filter   = ['zrpUser' => $_user_connected->getId()];
            $_customer = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_CUSTOMER, $_filter);

            $_transactions = $this->_transaction_repository->getCustomerTransactionByTermAndLimit($_customer, $_term, 5);
            return $this->render('@Admin/ZrpDashboard/partial/customer_list_transaction_partial.html.twig', [
                'transactions' => $_transactions
            ]);
        }
        return false;
    }

    /**
     *  ajax fetch chart
     * @param Request $_request
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function ajaxFetchChart(Request $_request)
    {
        $_type       = $_request->query->get('type');
        $_siren      = $_request->get('siren');
        $_date       = $_request->get('date');
        $_date_value = $_request->get('date_value');

        $_response = $this->_transaction_repository->fetchTransactionDataChart($_type, $_siren, $_date, $_date_value);

        return new JsonResponse($_response);
    }

    /**
     * ajax amount by type
     * @param Request $_request
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function ajaxAmountType(Request $_request)
    {
        $_response = false;
        $_type     = $_request->get('type');
        $_siren    = $_request->get('siren');

        $_response = $this->_transaction_repository->amountByType($_type, $_siren);

        return new JsonResponse($_response);
    }
}
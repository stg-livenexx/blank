<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCustomerType;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpCustomerTypeType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCustomerType\ServiceMetierZrpCustomerType;
use App\Zerop\Service\UserBundle\Manager\UploadManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpCustomerTypeController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpCustomerTypeController extends AbstractController
{
    private $_utils_manager;
    private $_translator;
    private $_upload_manager;
    private $_customer_type_manager;

    /**
     * ZrpCustomerTypeController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpCustomerType $_customer_type_manager
     * @param TranslatorInterface $_translator
     * @param UploadManager $_upload_manager
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpCustomerType $_customer_type_manager,
                                TranslatorInterface $_translator, UploadManager $_upload_manager)
    {
        $this->_utils_manager         = $_utils_manager;
        $this->_translator            = $_translator;
        $this->_upload_manager        = $_upload_manager;
        $this->_customer_type_manager = $_customer_type_manager;
    }

    /**
     * display a customer type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('@Admin/ZrpCustomerType/index.html.twig');
    }

    /**
     * List ajax
     * @param Request $_request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAjax(Request $_request)
    {
        $_page     = $_request->query->get('start');
        $_nb_max   = $_request->query->get('length');
        $_search   = $_request->query->get('search')['value'];
        $_order_by = $_request->query->get('order_by');

        $_list_user = $this->_customer_type_manager->getAllCustomerType($_page, $_nb_max, $_search, $_order_by);

        return new JsonResponse($_list_user);
    }

    /**
     * add customer type
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(Request $_request)
    {
        $_customer_type = new ZrpCustomerType();
        $_form          = $this->createNewForm($_customer_type);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $this->_utils_manager->saveEntity($_customer_type, 'new');
            $_flash_message = $this->_translator->trans('bo.confirmation.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('customer_type_index'));
        }
        return $this->render('@Admin/ZrpCustomerType/add.html.twig', array(
            'customer_type' => $_customer_type,
            'form'          => $_form->createView(),
        ));
    }

    /**
     * update customer type
     * @param Request $_request
     * @param ZrpCustomerType $_customer_type
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Request $_request, ZrpCustomerType $_customer_type)
    {
        if (!$_customer_type) {
            $_exception_message = $this->get('translator')->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        $_edit_form = $this->createEditForm($_customer_type);
        $_edit_form->handleRequest($_request);

        if ($_edit_form->isValid()) {
            $this->_utils_manager->saveEntity($_customer_type, 'update');
            $_flash_message = $this->_translator->trans('bo.confirmation.update');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('customer_type_index'));
        }
        return $this->render('@Admin/ZrpCustomerType/edit.html.twig', array(
            'customer_type' => $_customer_type,
            'edit_form'     => $_edit_form->createView(),
        ));
    }

    /**
     * edit customer type
     * @param ZrpCustomerType $_customer_type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ZrpCustomerType $_customer_type)
    {
        if (!$_customer_type) {
            $_exception_message = $this->get('translator')->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }
        $_edit_form = $this->createEditForm($_customer_type);

        return $this->render('@Admin/ZrpCustomerType/edit.html.twig', array(
            'customer_type' => $_customer_type,
            'edit_form'     => $_edit_form->createView()
        ));
    }

    /**
     * delete customer type
     * @param Request $_request
     * @param ZrpCustomerType $_customer_type
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Request $_request, ZrpCustomerType $_customer_type)
    {
        $_form = $this->createDeleteForm($_customer_type);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_customer_type);
            $_flash_message = $this->_translator->trans('bo.confirmation.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);
        }
        return $this->redirectToRoute('customer_type_index');
    }

    /**
     * create new form customer type
     * @param ZrpCustomerType $_customer_type
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createNewForm(ZrpCustomerType $_customer_type)
    {
        $_form = $this->createForm(ZrpCustomerTypeType::class, $_customer_type, array(
            'action' => $this->generateUrl('customer_type_add'),
            'method' => 'POST'
        ));
        return $_form;
    }

    /**
     * Create edit form customer type
     * @param ZrpCustomerType $_customer_type
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createEditForm(ZrpCustomerType $_customer_type)
    {
        $_form = $this->createForm(ZrpCustomerTypeType::class, $_customer_type, array(
            'action' => $this->generateUrl('customer_type_update', array('id' => $_customer_type->getId())),
            'method' => 'POST'
        ));
        return $_form;
    }

    /**
     * create delete form customer type
     * @param ZrpCustomerType $_customer_type
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createDeleteForm(ZrpCustomerType $_customer_type)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('customer_type_delete', array('id' => $_customer_type->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpClient;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpClient\ServiceMetierZrpClient;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpClientController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpClientController extends AbstractController
{
    private $_utils_manager;

    private $_client_manager;

    private $_translator;

    private $_client_repository;


    /**
     * ZrpClientController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpClient $_client_manager
     * @param TranslatorInterface $_translator
     * @param ZrpClientRepository $_client_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpClient $_client_manager, TranslatorInterface $_translator,
                                ZrpClientRepository $_client_repository)
    {
        $this->_utils_manager = $_utils_manager;
        $this->_client_manager = $_client_manager;
        $this->_translator = $_translator;
        $this->_client_repository = $_client_repository;
    }

    /**
     * List client ajax
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function listAjax(Request $_request)
    {
        $_page     = $_request->query->get('start');
        $_nb_max   = $_request->query->get('length');
        $_search   = $_request->query->get('search')['value'];
        $_order_by = $_request->query->get('order_by');

        $_list_client = $this->_client_manager->getAllClient($_page, $_nb_max, $_search, $_order_by);

        return $this->json($_list_client, 200, []);
    }

    /**
     * display a page index
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('@Admin/ZrpClient/index.html.twig');
    }

    /**
     * add a new client
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(Request $_request)
    {
        if($_request->isXmlHttpRequest() && $_request->isMethod('POST'))
        {
            $_data = $this->_client_manager->mappingClient($_request->getContent(), null);
            $_client_new = $_data['client'];

            if($this->_client_repository->findOneBy(['cltCin' => $_client_new->getCltCin()]))
            {
                return $this->json(['exist_cin' => $this->_translator->trans('bo.client.cin.already.exist')], 400, []);
            }
            $this->_utils_manager->saveEntity($_client_new, 'new');

            return $this->json(['success' => $this->_translator->trans('bo.confirmation.add')], 200, []);
        }

        return $this->redirectToRoute('client_index');
    }

    /**
     * get a single client for editing
     * @param ZrpClient $_client
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function edit(ZrpClient $_client)
    {
        return $this->json($_client, 200, [], ['groups' => 'client:read']);
    }

    /**
     * update a single client
     * @param Request $_request
     * @param ZrpClient $_client_exist
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Request $_request, ZrpClient $_client_exist)
    {
        if($_request->isXmlHttpRequest() && $_request->isMethod('PUT'))
        {
            $_data = $this->_client_manager->mappingClient($_request->getContent(), $_client_exist);
            $_client_update = $_data['client'];

            if($this->_client_repository->findOneBy(['cltCin' => $_client_update->getCltCin()])
                && $_data['client_current_cin_is_equals_client_update_cin'] === false)
            {
                return $this->json(['exist_cin' => $this->_translator->trans('bo.client.cin.already.exist')], 400, []);
            }
            $this->_utils_manager->saveEntity($_client_update, 'update');

            return $this->json(['success' => $this->_translator->trans('bo.confirmation.update')], 200, []);
        }


        return $this->redirectToRoute('client_index');
    }

    /**
     * delete a single client
     * @param Request $_request
     * @param ZrpClient $_client
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Request $_request, ZrpClient $_client)
    {
        if($_request->isXmlHttpRequest() && $_request->isMethod('DELETE'))
        {
            $this->_utils_manager->deleteEntity($_client);

            return $this->json(['success' => $this->_translator->trans('bo.confirmation.delete')], 200, []);
        }

        return $this->redirectToRoute('client_index');
    }
}
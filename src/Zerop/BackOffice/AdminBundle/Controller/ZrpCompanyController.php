<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCity;
use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompany;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpCompanyType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompany\ServiceMetierCompany;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyTransaction\ServiceMetierZrpCompanyTransaction;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCityRepository;
use App\Zerop\Service\MetierManagerBundle\Utils\Effective;
use App\Zerop\Service\MetierManagerBundle\Utils\EntityName;
use App\Zerop\Service\MetierManagerBundle\Utils\RoleName;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpCompanyController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpCompanyController extends AbstractController
{
    private $_utils_manager;
    private $_company_manager;
    private $_translator;
    private $_company_transaction_manager;
    private $_city_repository;

    /**
     * ZrpCompanyController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierCompany $_company_manager
     * @param TranslatorInterface $_translator
     * @param ServiceMetierZrpCompanyTransaction $_company_transaction_manager
     * @param ZrpCityRepository $_city_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierCompany $_company_manager,
                                TranslatorInterface $_translator, ServiceMetierZrpCompanyTransaction $_company_transaction_manager
        , ZrpCityRepository $_city_repository
    )
    {
        $this->_utils_manager               = $_utils_manager;
        $this->_company_manager             = $_company_manager;
        $this->_translator                  = $_translator;
        $this->_company_transaction_manager = $_company_transaction_manager;
        $this->_city_repository             = $_city_repository;
    }

    /**
     * List ajax
     * @param Request $_request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAjax(Request $_request)
    {
        $_page     = $_request->query->get('start');
        $_nb_max   = $_request->query->get('length');
        $_search   = $_request->query->get('search')['value'];
        $_order_by = $_request->query->get('order_by');
        $_activity = $_request->query->get('activity_id');

        $_custom_filter = [
            'raison_social'     => $_request->query->get('raison_social'),
            'code_postal'       => $_request->query->get('code_postal'),
            'type_entreprise'   => $_request->query->get('type_entreprise'),
            'commune_facturant' => $_request->query->get('commune_facturant'),
            'erp'               => $_request->query->get('erp'),
            'effectif'          => $_request->query->get('effectif'),
            'data_creation'     => $_request->query->get('data_creation'),
            'ca'                => $_request->query->get('ca'),
            'nbr_caisse'        => $_request->query->get('nbr_caisse'),
            'nbr_caisse_zerop'  => $_request->query->get('nbr_caisse_zerop'),
            'date_caisse'       => $_request->query->get('date_caisse'),
            'user'              => $_request->query->get('user'),
            'siren'             => $_request->query->get('siren'),
        ];

        $_list_company = $this->_company_manager->getAllCompany($_page, $_nb_max, $_search, $_order_by, $_activity, $_custom_filter);

        return new JsonResponse($_list_company);
    }

    /**
     * list ajax city
     * @param Request $_request
     * @return JsonResponse
     */
    public function listAjaxCity(Request $_request)
    {
        $_order_by = $_request->query->get('order_by');
        $_search   = $_request->query->get('term');
        $_city     = $this->_company_manager->getAllCity($_order_by, $_search);
        return new JsonResponse($_city);
    }

    /**
     * Display a page index
     * @return Response
     */
    public function index()
    {
        $_activities       = $this->_utils_manager->getAllEntities(EntityName::ZRP_COMPANY_ACTIVITY);
        $_type_entreprises = $this->_utils_manager->getAllEntities(EntityName::ZRP_COMPANY_TYPE);
        $_filter           = ['zrpRole' => RoleName::ID_ROLE_ENTREPRISE];
        $_users            = $this->_utils_manager->getAllEntitiesByOrder(EntityName::ZRP_USER, $_filter, ['id' => 'DESC']);
        $_erps             = $this->_utils_manager->getAllEntities(EntityName::ZRP_ERP);

        return $this->render('@Admin/ZrpCompany/index.html.twig', [
            'activities'       => $_activities,
            'type_entreprises' => $_type_entreprises,
            'users'            => $_users,
            'erps'             => $_erps
        ]);
    }

    /**
     * Adding company
     * @param Request $_request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function add(Request $_request)
    {
        $_company = new ZrpCompany();
        $_form    = $this->createCreateForm($_company);
        $_form->handleRequest($_request);
        $_erps = $this->_utils_manager->getAllEntities(EntityName::ZRP_ERP);

        //Affectif
        $_choices = Effective::$EFFECTIF_LIST;
        $_output  = [];
        foreach ($_choices as $_key => $_value) {
            $_result           = implode(' à ', $_value);
            $_output[$_result] = $_result;
        }

        if ($_form->isSubmitted() && $_form->isValid()) {
            $_data_effectif = $_request->request->get('selection_effectif');
            if (isset($_data_effectif) && !empty($_data_effectif) && $_data_effectif != 0) {
                $_data_tab = explode(' à ', $_data_effectif);
                $_company->setCmpEffectiveMin($_data_tab[0]);
                $_company->setCmpEffectiveMax($_data_tab[1]);
            }

            //get city by google maps
            $_city     = $_form['zrpCity']->getData();
            $_zrp_city = $this->saveCityNotExist($_city);
            $_zrp_city = is_array($_zrp_city) ? array_shift($_zrp_city) : $_zrp_city;
            //set object city in attribute zrpcity by company
            $_company->setZrpCity($_zrp_city);

            $this->_utils_manager->saveEntity($_company, 'new');
            $_flash_message = $this->_translator->trans('bo.confirmation.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('company_index'));
        }

        return $this->render('@Admin/ZrpCompany/add.html.twig', array(
            'company' => $_company,
            'form'    => $_form->createView(),
            'erps'    => $_erps,
            'outputs' => $_output
        ));
    }

    /**
     * Editing Company
     * @param ZrpCompany $_company
     * @return Response
     */
    public function edit(ZrpCompany $_company)
    {
        if (!$_company) {
            $_exception_message = $this->get('translator')->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        //Affectif
        $_choices = Effective::$EFFECTIF_LIST;
        $_output  = [];
        foreach ($_choices as $_key => $_value) {
            $_result           = implode(' à ', $_value);
            $_output[$_result] = $_result;
        }
        $_min_affectif  = $_company->getCmpEffectiveMin();
        $_max_affectif  = $_company->getCmpEffectiveMax();
        $_min_max_value = $_min_affectif . " à " . $_max_affectif;

        $_edit_form = $this->createEditForm($_company);
        $_city      = $_company->getZrpCity() ? $_company->getZrpCity()->getCtName() : '';
        $_edit_form->get('zrpCity')->setData($_city);

        $_last_transaction = $this->_company_transaction_manager->getLastCompanyTransaction($_company->getId());
        $_company_erp      = $this->_utils_manager->findOneEntityByFilter(EntityName::ZRP_COMPANY_ERP,
            ['zrpCompany' => $_company->getId()]);

        return $this->render('@Admin/ZrpCompany/edit.html.twig', array(
            'company'          => $_company,
            'last_transaction' => $_last_transaction,
            'edit_form'        => $_edit_form->createView(),
            'outputs'          => $_output,
            'min_max_value'    => $_min_max_value,
            'company_erp'      => $_company_erp
        ));
    }

    /**
     * Updating company
     * @param Request $_request
     * @param ZrpCompany $_company
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function update(Request $_request, ZrpCompany $_company)
    {
        if (!$_company) {
            $_exception_message = $this->get('translator')->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        $_edit_form = $this->createEditForm($_company);
        $_edit_form->handleRequest($_request);

        if ($_edit_form->isValid()) {
            $_data_effectif = $_request->request->get('selection_effectif');
            if (isset($_data_effectif) && !empty($_data_effectif) && $_data_effectif != 0) {
                $_data_tab = explode(' à ', $_data_effectif);
                $_company->setCmpEffectiveMin($_data_tab[0]);
                $_company->setCmpEffectiveMax($_data_tab[1]);
            }

            //get city by google maps
            $_city     = $_edit_form['zrpCity']->getData();
            $_zrp_city = $this->saveCityNotExist($_city);
            $_zrp_city = is_array($_zrp_city) ? array_shift($_zrp_city) : $_zrp_city;

            //set object city in attribute zrpcity by company
            $_company->setZrpCity($_zrp_city);

            $this->_utils_manager->saveEntity($_company, 'update');
            $_flash_message = $this->_translator->trans('bo.confirmation.update');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('company_index'));
        }

        return $this->render('@Admin/ZrpCompany/edit.html.twig', array(
            'company'   => $_company,
            'edit_form' => $_edit_form->createView()
        ));
    }

    /**
     * Deleting company
     * @param Request $_request
     * @param ZrpCompany $_company
     * @return RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Request $_request, ZrpCompany $_company)
    {
        $_form = $this->createDeleteForm($_company);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_company);
            $_flash_message = $this->_translator->trans('bo.controller.message.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('company_index'));
        }

        return $this->redirectToRoute('company_index');
    }

    /**
     * Creation add form
     * @param ZrpCompany $_company
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createCreateForm(ZrpCompany $_company)
    {
        $_form = $this->createForm(ZrpCompanyType::class, $_company, array(
            'action' => $this->generateUrl('company_add'),
            'method' => 'POST'
        ));

        return $_form;
    }

    /**
     * Create edit form
     * @param ZrpCompany $_company
     * @return FormInterface
     */
    private function createEditForm(ZrpCompany $_company)
    {

        $_form = $this->createForm(ZrpCompanyType::class, $_company, array(
            'action' => $this->generateUrl('company_update', array('id' => $_company->getId())),
            'method' => 'PUT'
        ));

        return $_form;
    }

    /**
     * Creation deleting form company
     * @param ZrpCompany $_company
     * @return FormInterface
     */
    private function createDeleteForm(ZrpCompany $_company)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('company_delete', array('id' => $_company->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Save City
     * @param $_city
     * @param $_action
     * @return mixed
     */
    public function saveCityNotExist($_city)
    {
        $_zrp_city = $this->_city_repository->findByCtName($_city);
        if (!$_zrp_city) {
            $_zrp_city = new ZrpCity();
            $_zrp_city->setCtName($_city);
            $this->_utils_manager->saveEntity($_zrp_city, 'new');
        }
        return $_zrp_city;
    }
}

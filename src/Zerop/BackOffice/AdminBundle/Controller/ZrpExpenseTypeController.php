<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpExpenseType;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpExpenseTypeType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpExpenseType\ServiceMetierZrpExpenseType;
use App\Zerop\Service\UserBundle\Manager\UploadManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpExpenseTypeController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpExpenseTypeController extends AbstractController
{
    private $_utils_manager;
    private $_translator;
    private $_upload_manager;
    private $_expense_type_manager;

    /**
     * ZrpExpenseTypeController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpExpenseType $_expense_type_manager
     * @param TranslatorInterface $_translator
     * @param UploadManager $_upload_manager
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpExpenseType $_expense_type_manager,
                                TranslatorInterface $_translator, UploadManager $_upload_manager)
    {
        $this->_utils_manager        = $_utils_manager;
        $this->_translator           = $_translator;
        $this->_upload_manager       = $_upload_manager;
        $this->_expense_type_manager = $_expense_type_manager;
    }

    /**
     * display a expense type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('@Admin/ZrpExpenseType/index.html.twig');
    }

    /**
     * List ajax
     * @param Request $_request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAjax(Request $_request)
    {
        $_page     = $_request->query->get('start');
        $_nb_max   = $_request->query->get('length');
        $_search   = $_request->query->get('search')['value'];
        $_order_by = $_request->query->get('order_by');

        $_list_user = $this->_expense_type_manager->getAllExpenseType($_page, $_nb_max, $_search, $_order_by);

        return new JsonResponse($_list_user);
    }

    /**
     * add expense type
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(Request $_request)
    {
        $_expense_type = new ZrpExpenseType();
        $_form         = $this->createCreateForm($_expense_type);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $this->_utils_manager->saveEntity($_expense_type, 'new');
            $_flash_message = $this->_translator->trans('bo.confirmation.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('expense_type_index'));
        }
        return $this->render('@Admin/ZrpExpenseType/add.html.twig', array(
            'expense_type' => $_expense_type,
            'form'         => $_form->createView(),
        ));
    }

    /**
     * update expense type
     * @param Request $_request
     * @param ZrpExpenseType $_expense_type
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function update(Request $_request, ZrpExpenseType $_expense_type)
    {
        if (!$_expense_type) {
            $_exception_message = $this->get('translator')->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        $_edit_form = $this->createEditForm($_expense_type);
        $_edit_form->handleRequest($_request);

        if ($_edit_form->isValid()) {
            $this->_utils_manager->saveEntity($_expense_type, 'update');
            $_flash_message = $this->_translator->trans('bo.confirmation.update');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('expense_type_index'));
        }
        return $this->render('@Admin/ZrpExpenseType/edit.html.twig', array(
            'expense_type' => $_expense_type,
            'edit_form'    => $_edit_form->createView(),
        ));
    }

    /**
     * edit expense type
     * @param ZrpExpenseType $_expense_type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ZrpExpenseType $_expense_type)
    {
        if (!$_expense_type) {
            $_exception_message = $this->get('translator')->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }
        $_edit_form = $this->createEditForm($_expense_type);
        return $this->render('@Admin/ZrpExpenseType/edit.html.twig', array(
            'expense_type' => $_expense_type,
            'edit_form'    => $_edit_form->createView()
        ));
    }

    /**
     * delete expense type
     * @param Request $_request
     * @param ZrpExpenseType $_expense_type
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function delete(Request $_request, ZrpExpenseType $_expense_type)
    {
        $_form = $this->createDeleteForm($_expense_type);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_expense_type);
            $_flash_message = $this->_translator->trans('bo.confirmation.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);
        }
        return $this->redirectToRoute('expense_type_index');
    }

    /**
     * create new form expense type
     * @param ZrpExpenseType $_expense_type
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createCreateForm(ZrpExpenseType $_expense_type)
    {
        $_form = $this->createForm(ZrpExpenseTypeType::class, $_expense_type, array(
            'action' => $this->generateUrl('expense_type_new'),
            'method' => 'POST'
        ));
        return $_form;
    }

    /**
     * Create edit form expense type
     * @param ZrpExpenseType $_expense_type
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createEditForm(ZrpExpenseType $_expense_type)
    {
        $_form = $this->createForm(ZrpExpenseTypeType::class, $_expense_type, array(
            'action' => $this->generateUrl('expense_type_update', array('id' => $_expense_type->getId())),
            'method' => 'POST'
        ));
        return $_form;
    }

    /**
     * Deleting expense type
     * @param ZrpExpenseType $_expense_type
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createDeleteForm(ZrpExpenseType $_expense_type)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expense_type_delete', array('id' => $_expense_type->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
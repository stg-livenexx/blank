<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCustomer;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpClientFinalType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpClientFinal\ServiceMetierZrpClientFinal;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyTransaction\ServiceMetierZrpCompanyTransaction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpClientFinalController
 * @package App\Gs\Service\UserBundle\Controller
 */
class ZrpClientFinalController extends AbstractController
{
    private $_utils_manager;
    private $_client_final_manager;
    private $_translator;
    private $_company_transaction_manager;


    /**
     * ZrpClientFinalController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpClientFinal $_client_final_manager
     * @param TranslatorInterface $_translator
     * @param ServiceMetierZrpCompanyTransaction $_company_transaction_manager
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpClientFinal $_client_final_manager,
                                TranslatorInterface $_translator, ServiceMetierZrpCompanyTransaction $_company_transaction_manager)
    {
        $this->_utils_manager               = $_utils_manager;
        $this->_client_final_manager        = $_client_final_manager;
        $this->_translator                  = $_translator;
        $this->_company_transaction_manager = $_company_transaction_manager;

    }

    /**
     * List ajax client final
     * @param Request $_request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAjax(Request $_request)
    {
        $_page        = $_request->query->get('start');
        $_nb_max      = $_request->query->get('length');
        $_order_by    = $_request->query->get('order_by');
        $_data_filter = $_request->query->get('data_filter');

        $_list_user = $this->_client_final_manager->getAllClientFinal($_page, $_nb_max, $_order_by, $_data_filter);

        return new JsonResponse($_list_user);
    }

    /**
     * list city ajax
     * @param Request $_request
     * @return JsonResponse
     */
    public function cityListAjax(Request $_request)
    {
        $_order_by  = $_request->query->get('order_by');
        $_search    = $_request->query->get('term');
        $_list_city = $this->_client_final_manager->getAllCity($_order_by, $_search);
        return new JsonResponse($_list_city);
    }

    /**
     * Display a page index
     * @return Response
     */
    public function index()
    {
        return $this->render('@Admin/ZrpClientFinal/index.html.twig');
    }

    /**
     * Adding client final
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function add(Request $_request)
    {
        $_customer = new ZrpCustomer();
        $_form     = $this->createCreateForm($_customer);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $_data_post = $_request->request->all();
            $this->_client_final_manager->addUserCustomer($_customer, $_data_post, true);
            $_flash_message = $this->_translator->trans('bo.confirmation.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('client_final_index'));
        }

        return $this->render('@Admin/ZrpClientFinal/add.html.twig', array(
            'customer' => $_customer,
            'form'     => $_form->createView()
        ));
    }

    /**
     * Editing client final
     * @param ZrpCustomer $_customer
     * @return Response
     */
    public function edit(ZrpCustomer $_customer)
    {
        if (!$_customer) {
            $_exception_message = $this->get('translator')->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        $_edit_form = $this->createEditForm($_customer);
        $_user      = $_customer->getZrpUser();

        return $this->render('@Admin/ZrpClientFinal/edit.html.twig', array(
            'user'      => $_user,
            'customer'  => $_customer,
            'edit_form' => $_edit_form->createView()
        ));
    }

    /**
     * Updating client final
     * @param Request $_request
     * @param ZrpCustomer $_customer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Request $_request, ZrpCustomer $_customer)
    {
        if (!$_customer) {
            $_exception_message = $this->get('translator')->trans('exception.entity');
            throw $this->createNotFoundException($_exception_message);
        }

        $_edit_form = $this->createEditForm($_customer);
        $_edit_form->handleRequest($_request);

        if ($_edit_form->isValid() && $_edit_form->isValid()) {
            $_data_post = $_request->request->all();
            $this->_client_final_manager->addUserCustomer($_customer, $_data_post, false);
            $_flash_message = $this->_translator->trans('bo.confirmation.update');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('client_final_index'));
        }

        return $this->render('@Admin/ZrpClientFinal/edit.html.twig', array(
            'customer'  => $_customer,
            'edit_form' => $_edit_form->createView()
        ));
    }

    /**
     * Creation add form client final
     * @param ZrpCustomer $_customer
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createCreateForm(ZrpCustomer $_customer)
    {
        $_user_has_customer = $this->_client_final_manager->getUserHasCustomer();
        $_form              = $this->createForm(ZrpClientFinalType::class, $_customer, array(
            'action'            => $this->generateUrl('client_final_add'),
            'method'            => 'POST',
            'user_has_customer' => $_user_has_customer
        ));

        return $_form;
    }

    /**
     * Create edit form client final
     * @param ZrpCustomer $_customer
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createEditForm(ZrpCustomer $_customer)
    {
        $_form = $this->createForm(ZrpClientFinalType::class, $_customer, array(
            'action' => $this->generateUrl('client_final_update', array('id' => $_customer->getId())),
            'method' => 'PUT'
        ));

        return $_form;
    }

    /**
     * Deleting client final
     * @param Request $_request
     * @param ZrpCustomer $_customer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function delete(Request $_request, ZrpCustomer $_customer)
    {
        $_form = $this->createDeleteForm($_customer);
        $_form->handleRequest($_request);
        $_user = $_customer->getZrpUser();

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_client_final_manager->deleteUser($_customer);
            $this->_client_final_manager->deleteUser($_user);

            $_flash_message = $this->_translator->trans('bo.confirmation.delete');
            $this->_utils_manager->setFlash('danger', $_flash_message);
        }

        return $this->redirectToRoute('client_final_index');
    }

    /**
     * create delete form client final
     * @param ZrpCustomer $_user
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(ZrpCustomer $_user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('client_final_delete', array('id' => $_user->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * fiche client
     * @param ZrpCustomer $_customer
     * @return Response
     */
    public function fiche(ZrpCustomer $_customer)
    {
        $_company          = $this->_utils_manager->getCompanyConnected();
        $_company_id       = $_company ? $_company->getId() : '';
        $_last_transaction = $this->_company_transaction_manager->getLastCompanyTransaction($_company_id);

        return $this->render('@Admin/ZrpTransaction/index.html.twig', [
            'last_transaction' => $_last_transaction,
            'customer_id'      => $_customer ? $_customer->getId() : 0
        ]);
    }
}

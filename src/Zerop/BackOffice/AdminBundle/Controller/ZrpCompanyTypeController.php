<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyType;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpCompanyTypeType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyType\ServiceMetierZrpCompanyType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpCompanyTypeController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpCompanyTypeController extends AbstractController
{
    private $_utils_manager;
    private $_company_Type;
    private $_translator;

    /**
     * ZrpCompanyTypeController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpCompanyType $_company_Type
     * @param TranslatorInterface $_translator
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpCompanyType $_company_Type, TranslatorInterface $_translator)
    {
        $this->_utils_manager = $_utils_manager;
        $this->_company_Type  = $_company_Type;
        $this->_translator    = $_translator;
    }

    /**
     * list ajax
     * @param Request $_request
     * @return JsonResponse
     */
    public function listAjax(Request $_request)
    {
        $_page     = $_request->query->get('start');
        $_nb_max   = $_request->query->get('length');
        $_search   = $_request->query->get('search')['value'];
        $_order_by = $_request->query->get('order_by');

        $_list_company_type = $this->_company_Type->getAllCompanyType($_page, $_nb_max, $_search, $_order_by);

        return new JsonResponse($_list_company_type);
    }

    /**
     * Display a page index
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('@Admin/ZrpCompanyType/index.html.twig');
    }

    /**
     * add company type
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(Request $_request)
    {
        $_company_type = new ZrpCompanyType();

        $_form = $this->createCreateForm($_company_type);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $this->_utils_manager->saveEntity($_company_type, 'new');
            $_flash_message = $this->_translator->trans('bo.controller.message.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('company_type_index'));
        }

        return $this->render('@Admin/ZrpCompanyType/add.html.twig', [
            'form' => $_form->createView()
        ]);
    }

    /**
     * edit company type
     * @param ZrpCompanyType $_company_type
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ZrpCompanyType $_company_type)
    {
        $_form = $this->createEditForm($_company_type);

        return $this->render('@Admin/ZrpCompanyType/edit.html.twig', [
            'edit_form' => $_form->createView()
        ]);
    }

    /**
     * update company type
     * @param Request $_request
     * @param ZrpCompanyType $_company_type
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Request $_request, ZrpCompanyType $_company_type)
    {
        $_form = $this->createEditForm($_company_type);

        $_form->handleRequest($_request);

        if ($_form->isValid()) {
            $this->_utils_manager->saveEntity($_company_type, 'update');
            $_flash_message = $this->_translator->trans('bo.controller.message.edit');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('company_type_index'));
        }

        return $this->render('@Admin/ZrpCompanyType/edit.html.twig', [
            'edit_form' => $_form->createView()
        ]);
    }

    /**
     * delete company type
     * @param Request $_request
     * @param ZrpCompanyType $_company_type
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Request $_request, ZrpCompanyType $_company_type)
    {
        $_form = $this->createDeleteForm($_company_type);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_company_type);
            $_flash_message = $this->_translator->trans('bo.controller.message.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('company_type_index'));
        }

        return $this->redirectToRoute('company_type_index');
    }

    /**
     * create new form company type
     * @param ZrpCompanyType $_company_type
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createCreateForm(ZrpCompanyType $_company_type)
    {
        $_form = $this->createForm(ZrpCompanyTypeType::class, $_company_type, array(
            'action' => $this->generateUrl('company_type_add'),
            'method' => 'POST'
        ));

        return $_form;
    }

    /**
     * create edit form company type
     * @param ZrpCompanyType $_company_type
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createEditForm(ZrpCompanyType $_company_type)
    {
        $_form = $this->createForm(ZrpCompanyTypeType::class, $_company_type, array(
            'action' => $this->generateUrl('company_type_update', array('id' => $_company_type->getId())),
            'method' => 'PUT'
        ));

        return $_form;
    }

    /**
     * create delete form company type
     * @param ZrpCompanyType $_company_type
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(ZrpCompanyType $_company_type)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('company_type_delete', array('id' => $_company_type->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
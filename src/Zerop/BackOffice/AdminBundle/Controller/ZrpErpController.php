<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpErp;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpErpType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpErp\ServiceMetierZrpErp;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpErpRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpErpController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpErpController extends AbstractController
{
    private $_utils_manager;
    private $_zrp_erp_manager;
    private $_zrp_erp_repository;
    private $_translator;

    /**
     * ZrpErpController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpErp $_zrp_erp_manager
     * @param ZrpErpRepository $_zrp_erp_repository
     * @param TranslatorInterface $_translator
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpErp $_zrp_erp_manager, ZrpErpRepository $_zrp_erp_repository, TranslatorInterface $_translator)
    {
        $this->_utils_manager      = $_utils_manager;
        $this->_zrp_erp_manager    = $_zrp_erp_manager;
        $this->_zrp_erp_repository = $_zrp_erp_repository;
        $this->_translator         = $_translator;
    }

    /**
     * index erp
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('@Admin/ZrpErp/index.html.twig');
    }

    /**
     * list ajax erp
     * @param Request $_request
     * @return JsonResponse
     */
    public function listAjax(Request $_request)
    {
        $_page        = $_request->query->get('start');
        $_nb_max_page = $_request->query->get('length');
        $_search      = $_request->query->get('search')['value'];
        $_order_by    = $_request->query->get('order_by');

        $_zrp_erp = $this->_zrp_erp_manager->getAllErp($_page, $_nb_max_page, $_search, $_order_by);

        return new JsonResponse($_zrp_erp);
    }

    /**
     * add erp
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(Request $_request)
    {
        $_zrp_erp = new ZrpErp();
        $_form    = $this->createForm(ZrpErpType::class, $_zrp_erp);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $this->_utils_manager->saveEntity($_zrp_erp, 'new');
            $_flash_message = $this->_translator->trans('bo.controller.message.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirectToRoute('erp_index');
        }

        return $this->render('@Admin/ZrpErp/add.html.twig', [
            'form' => $_form->createView(),
        ]);
    }

    /**
     * edit erp
     * @param ZrpErp $_zrp_erp
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(ZrpErp $_zrp_erp, Request $_request)
    {
        $_password = $_zrp_erp->getErpApiUserPwd();
        $_form     = $this->createForm(ZrpErpType::class, $_zrp_erp, ['is_edit' => true]);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $_flash_message = $this->_translator->trans('bo.confirmation.update');
            $this->_utils_manager->setFlash('success', $_flash_message);
            if (!$_zrp_erp->getErpApiUserPwd()) {
                $_zrp_erp->setErpApiUserPwd($_password);
            }
            $this->_utils_manager->saveEntity($_zrp_erp, 'update');

            return $this->redirectToRoute('erp_index');
        }

        return $this->render('@Admin/ZrpErp/edit.html.twig', [
            'form' => $_form->createView()
        ]);
    }

    /**
     * create delete form erp
     * @param ZrpErp $_zrp_erp
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(ZrpErp $_zrp_erp)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('erp_delete', ['id' => $_zrp_erp->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * delete erp
     * @param Request $_request
     * @param ZrpErp $_zrp_erp
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Request $_request, ZrpErp $_zrp_erp)
    {
        $_form = $this->createDeleteForm($_zrp_erp);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_zrp_erp);
            $_flash_message = $this->_translator->trans('bo.controller.message.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirectToRoute('erp_index');
        }

        return $this->redirectToRoute('erp_index');
    }
}
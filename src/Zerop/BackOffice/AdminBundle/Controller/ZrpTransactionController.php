<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpTransaction;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyTransaction\ServiceMetierZrpCompanyTransaction;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCustomerTicket\ServiceMetierZrpCustomerTicket;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpTransaction\ServiceMetierZrpTransaction;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpTransactionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpTransactionController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpTransactionController extends AbstractController
{
    private $_transaction_manager;
    private $_translator;
    private $_utils_manager;
    private $_company_transaction_manager;
    private $_transaction_repository;
    private $_customer_ticket_manager;

    /**
     * ZrpTransactionController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpTransaction $_transaction_manager
     * @param TranslatorInterface $_translator
     * @param ServiceMetierZrpCompanyTransaction $_company_transaction_manager
     * @param ZrpTransactionRepository $_transaction_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpTransaction $_transaction_manager,
                                TranslatorInterface $_translator, ServiceMetierZrpCompanyTransaction $_company_transaction_manager,
                                ZrpTransactionRepository $_transaction_repository, ServiceMetierZrpCustomerTicket $_customer_ticket_manager)
    {
        $this->_transaction_manager         = $_transaction_manager;
        $this->_translator                  = $_translator;
        $this->_utils_manager               = $_utils_manager;
        $this->_company_transaction_manager = $_company_transaction_manager;
        $this->_transaction_repository      = $_transaction_repository;
        $this->_customer_ticket_manager     = $_customer_ticket_manager;
    }

    /**
     * Display a page index
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $_company          = $this->_utils_manager->getCompanyConnected();
        $_company_id       = $_company ? $_company->getId() : '';
        $_last_transaction = $this->_company_transaction_manager->getLastCompanyTransaction($_company_id);

        return $this->render('@Admin/ZrpTransaction/index.html.twig', [
            'last_transaction' => $_last_transaction,
            'customer_id'      => 0
        ]);
    }

    /**
     * recover transaction
     * @param $_company_id
     * @param Request $_request
     * @return RedirectResponse
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \HTTP_Request2_LogicException
     */
    public function recoverTransaction($_company_id, Request $_request)
    {
        // get previous url
        $_url_referer     = $_request->headers->get('referer');
        $_all_transaction = $this->_transaction_manager->fetchAllTransactionFromOdoo($_company_id);

        $_company         = $this->_utils_manager->getCompanyConnected();
        $_company_id      = $_company ? $_company->getId() : $_company_id;
        $_transaction     = $this->_transaction_repository->getNbrTransaction();
        $_nbr_transaction = $_transaction ? (int)$_transaction : 0;

        if ($_nbr_transaction && $_company_id)
            $this->_company_transaction_manager->saveCompanyTransaction($_company_id, $_nbr_transaction);

        if ($_all_transaction['flash']) {
            $_flash_message = $_all_transaction['message'] = 'new' ? $this->_translator->trans('bo.transaction.api.new')
                : $this->_translator->trans('bo.transaction.api.edit');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($_url_referer);
        }
        return $this->redirect($_url_referer);
    }

    /**
     * List ajax for admin
     * @param Request $_request
     * @return JsonResponse
     * @throws \Exception
     */
    public function adminListAjax(Request $_request)
    {
        $_page             = $_request->query->get('start');
        $_nb_max           = $_request->query->get('length');
        $_search           = $_request->query->get('search')['value'];
        $_custom_filter    = [
            'montant_one'       => $_request->query->get('montant_one'),
            'montant_two'       => $_request->query->get('montant_two'),
            'date_emission_one' => $_request->query->get('date_emission_one'),
            'date_emission_two' => $_request->query->get('date_emission_two'),
            'commune_facturant' => $_request->query->get('commune_facturant'),
            'lastname_term'     => $_request->query->get('lastname_term'),
            'firstname_term'    => $_request->query->get('firstname_term'),
            'code_postal'       => $_request->query->get('code_postal'),
            'commune_facture'   => $_request->query->get('commune_facture'),
            'customer_id'       => $_request->query->get('customer_id'),
            'timezone'          => $_request->query->get('timezone'),
            'num_caisse'    => $_request->query->get('num_caisse'),
        ];

        $_order_by         = $_request->query->get('order_by');
        $_list_transaction = $this->_transaction_manager->getAdminAllTransaction($_page, $_nb_max, $_search, $_custom_filter, $_order_by);

        return new JsonResponse($_list_transaction);
    }

    /**
     * Deleting transaction
     * @param Request $_request
     * @param ZrpTransaction $_transaction
     * @return RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Request $_request, ZrpTransaction $_transaction)
    {
        $_form = $this->createDeleteForm($_transaction);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_transaction);
            $_flash_message = $this->_translator->trans('bo.controller.message.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('transaction_index'));
        }

        return $this->redirectToRoute('transaction_index');
    }

    /**
     * Creation deleting form transaction
     * @param ZrpTransaction $_transaction
     * @return FormInterface
     */
    private function createDeleteForm(ZrpTransaction $_transaction)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('transaction_delete', array('id' => $_transaction->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * show qr code transaction
     * @param ZrpTransaction $_transaction
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function showQrCode(ZrpTransaction $_transaction)
    {
        $this->_utils_manager->generateQrCodeByTransaction($_transaction);
        return $this->render('@Admin/ZrpTransaction/show_qr_code.html.twig', ['transaction' => $_transaction]);
    }

    /**
     * Générer pdf ticket
     * @param Request $_request
     * @param ZrpTransaction $_transaction
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function generatePdf(Request $_request, ZrpTransaction $_transaction)
    {
        $_pdf = $this->_customer_ticket_manager->generatePdf($_transaction->getId());

        $_ticket_id = $_transaction->getId();
        $_pdf->stream("ticket-$_ticket_id.pdf", [
            "Attachment" => false
        ]);

        exit();
    }
}

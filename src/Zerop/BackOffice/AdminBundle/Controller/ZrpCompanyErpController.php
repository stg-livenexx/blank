<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyErp;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpCompanyErpType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyErp\ServiceMetierCompanyErp;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpCompanyErpController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpCompanyErpController extends AbstractController
{

    private $_utils_manager;
    private $_translator;
    private $_company_erp_manager;

    /**
     * ZrpCompanyErpController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierCompanyErp $_company_erp_manager
     * @param TranslatorInterface $_translator
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierCompanyErp $_company_erp_manager, TranslatorInterface $_translator)
    {
        $this->_utils_manager       = $_utils_manager;
        $this->_translator          = $_translator;
        $this->_company_erp_manager = $_company_erp_manager;
    }

    /**
     * display all company erp
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('@Admin/ZrpCompanyErp/index.html.twig');
    }

    /**
     * List ajax
     * @param Request $_request
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAjax(Request $_request)
    {
        $_page     = $_request->query->get('start');
        $_nb_max   = $_request->query->get('length');
        $_search   = $_request->query->get('search')['value'];
        $_order_by = $_request->query->get('order_by');

        $_list_company = $this->_company_erp_manager->getAllCompanyErp($_page, $_nb_max, $_search, $_order_by);

        return new JsonResponse($_list_company);
    }

    /**
     * add company erp
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(Request $_request)
    {
        $_company_erp = new ZrpCompanyErp();

        $_form = $this->createCreateForm($_company_erp);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $this->_utils_manager->saveEntity($_company_erp, 'new');
            $_flash_message = $this->_translator->trans('bo.controller.message.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('company_erp_index'));
        }

        return $this->render('@Admin/ZrpCompanyErp/add.html.twig', [
            'form' => $_form->createView()
        ]);
    }

    /**
     * edit company erp
     * @param ZrpCompanyErp $_company_erp
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ZrpCompanyErp $_company_erp)
    {
        $_form = $this->createEditForm($_company_erp);

        return $this->render('@Admin/ZrpCompanyErp/edit.html.twig', [
            'edit_form' => $_form->createView()
        ]);
    }

    /**
     * update company erp
     * @param Request $_request
     * @param ZrpCompanyErp $_company_erp
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Request $_request, ZrpCompanyErp $_company_erp)
    {
        $_form = $this->createEditForm($_company_erp);

        $_form->handleRequest($_request);

        if ($_form->isValid()) {
            $this->_utils_manager->saveEntity($_company_erp, 'update');
            $_flash_message = $this->_translator->trans('bo.controller.message.edit');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('company_erp_index'));
        }

        return $this->render('@Admin/ZrpCompanyErp/index.html.twig', [
            'edit_form' => $_form->createView()
        ]);
    }

    /**
     * delete company erp
     * @param Request $_request
     * @param ZrpCompanyErp $_company_erp
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Request $_request, ZrpCompanyErp $_company_erp)
    {
        $_form = $this->createDeleteForm($_company_erp);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_company_erp);
            $_flash_message = $this->_translator->trans('bo.controller.message.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirect($this->generateUrl('company_erp_index'));
        }

        return $this->redirectToRoute('company_erp_index');
    }

    /**
     * Creation new form company erp
     * @param ZrpCompanyErp $_company_erp
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createCreateForm(ZrpCompanyErp $_company_erp)
    {
        $_form = $this->createForm(ZrpCompanyErpType::class, $_company_erp, array(
            'action' => $this->generateUrl('company_erp_add'),
            'method' => 'POST'
        ));

        return $_form;
    }

    /**
     * Creation edit form company erp
     * @param ZrpCompanyErp $_company_erp
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createEditForm(ZrpCompanyErp $_company_erp)
    {
        $_form = $this->createForm(ZrpCompanyErpType::class, $_company_erp, array(
            'action' => $this->generateUrl('company_erp_update', array('id' => $_company_erp->getId())),
            'method' => 'PUT'
        ));

        return $_form;
    }

    /**
     * Create deleting form company erp
     * @param ZrpCompanyErp $_company_erp
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(ZrpCompanyErp $_company_erp)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('company_erp_delete', array('id' => $_company_erp->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
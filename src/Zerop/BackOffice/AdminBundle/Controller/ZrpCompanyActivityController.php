<?php

namespace App\Zerop\BackOffice\AdminBundle\Controller;

use App\Zerop\Service\MetierManagerBundle\Entity\ZrpCompanyActivity;
use App\Zerop\Service\MetierManagerBundle\Form\ZrpCompanyActivityType;
use App\Zerop\Service\MetierManagerBundle\Metier\Utils\ServiceMetierUtils;
use App\Zerop\Service\MetierManagerBundle\Metier\ZrpCompanyActivity\ServiceMetierZrpCompanyActivity;
use App\Zerop\Service\MetierManagerBundle\Repository\ZrpCompanyActivityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ZrpCompanyActivityController
 * @package App\Zerop\BackOffice\AdminBundle\Controller
 */
class ZrpCompanyActivityController extends AbstractController
{
    private $_utils_manager;
    private $_zrp_company_activity_manager;
    private $_zrp_company_activity_repository;
    private $_translator;

    /**
     * ZrpCompanyActivityController constructor.
     * @param ServiceMetierUtils $_utils_manager
     * @param ServiceMetierZrpCompanyActivity $_zrp_company_activity_manager
     * @param ZrpCompanyActivityRepository $_zrp_company_activity_repository
     */
    public function __construct(ServiceMetierUtils $_utils_manager, ServiceMetierZrpCompanyActivity $_zrp_company_activity_manager, ZrpCompanyActivityRepository $_zrp_company_activity_repository, TranslatorInterface $_translator)
    {
        $this->_utils_manager                   = $_utils_manager;
        $this->_zrp_company_activity_manager    = $_zrp_company_activity_manager;
        $this->_zrp_company_activity_repository = $_zrp_company_activity_repository;
        $this->_translator                      = $_translator;
    }

    /**
     * Display a company activity
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index()
    {
        return $this->render('@Admin/ZrpCompanyActivity/index.html.twig');
    }

    /**
     * list Ajax company activity
     * @param Request $_request
     * @return JsonResponse
     */
    public function listAjax(Request $_request)
    {
        $_page        = $_request->query->get('start');
        $_nb_max_page = $_request->query->get('length');
        $_search      = $_request->query->get('search')['value'];
        $_order_by    = $_request->query->get('order_by');

        $_zrp_company_activity = $this->_zrp_company_activity_manager->getAllCompanyActivity($_page, $_nb_max_page, $_search, $_order_by);

        return new JsonResponse($_zrp_company_activity);
    }

    /**
     * add company activity
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function add(Request $_request)
    {
        $_zrp_company_activity = new ZrpCompanyActivity();
        $_form                 = $this->createForm(ZrpCompanyActivityType::class, $_zrp_company_activity);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $this->_utils_manager->saveEntity($_zrp_company_activity, 'new');
            $_flash_message = $this->_translator->trans('bo.controller.message.add');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirectToRoute('company_activity_index');
        }

        return $this->render('@Admin/ZrpCompanyActivity/add.html.twig', [
            'form' => $_form->createView(),
        ]);
    }

    /**
     * edit company activity
     * @param ZrpCompanyActivity $_zrp_company_activity
     * @param Request $_request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(ZrpCompanyActivity $_zrp_company_activity, Request $_request)
    {
        $_form = $this->createForm(ZrpCompanyActivityType::class, $_zrp_company_activity);
        $_form->handleRequest($_request);

        if ($_form->isSubmitted() && $_form->isValid()) {
            $_flash_message = $this->_translator->trans('bo.confirmation.update');
            $this->_utils_manager->setFlash('success', $_flash_message);
            $this->_utils_manager->saveEntity($_zrp_company_activity, 'update');

            return $this->redirectToRoute('company_activity_index');
        }

        return $this->render('@Admin/ZrpCompanyActivity/edit.html.twig', [
            'form' => $_form->createView()
        ]);
    }

    /**
     * Create delete form company activity
     * @param ZrpCompanyActivity $_zrp_company_activity
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(ZrpCompanyActivity $_zrp_company_activity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('company_activity_delete', array('id' => $_zrp_company_activity->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * delete company activity
     * @param Request $_request
     * @param ZrpCompanyActivity $_zrp_company_activity
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Request $_request, ZrpCompanyActivity $_zrp_company_activity)
    {
        $_form = $this->createDeleteForm($_zrp_company_activity);
        $_form->handleRequest($_request);

        if ($_request->isMethod('GET') || ($_form->isSubmitted() && $_form->isValid())) {
            $this->_utils_manager->deleteEntity($_zrp_company_activity);
            $_flash_message = $this->_translator->trans('bo.controller.message.delete');
            $this->_utils_manager->setFlash('success', $_flash_message);

            return $this->redirectToRoute('company_activity_index');
        }

        return $this->redirectToRoute('company_activity_index');
    }
}
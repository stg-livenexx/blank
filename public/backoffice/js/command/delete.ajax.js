$(document).ready(function () {
    $("#id-command-list tbody").on('click', '.kl-remove-elt-ajax', function (e) {
        e.preventDefault();
        if (confirm('Etes vous sûr de vouloir supprimer ?'))
        {
            deleteCommandAjax($(this).attr('href'));
        }
    });
})

/**
 * delete a single command with method ajax
 * @param href
 */
function deleteCommandAjax(href){
    $.ajax({
        url: href,
        method: "DELETE",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType : "json",
    })
    .done(function(response)
    {
        $('.alert.alert-success.alert-dismissible').append(`<span class="kl-success-message">${response.success}</span>`).fadeIn(2000);
        datatable.ajax.reload( function () {
            $('.alert.alert-success.alert-dismissible').fadeOut(4000);
            window.setTimeout(function () {$('.kl-success-message').remove();}, 4010);
        } );
    })
    .fail(function(error)
    {
        $('.alert.alert-danger.alert-dismissible').append('<span class="kl-error-message">${error_message}</span>').fadeIn(2000);
        window.setTimeout(function () {
            $('.alert.alert-danger.alert-dismissible').fadeOut(2000);
            window.setTimeout(function () {$('.kl-error-message').remove();}, 2010);
        }, 3000)
    })
    .always(function()
    {
        console.log('delete command completed!!!');
    });
}
$(document).ready(function (){
    $("#id-command-list tbody").on('click', '.kl-edit-ajax', function (e) {
        e.preventDefault();
        getCommandAjax($(this).attr('href'));
        console.log('Click edit');
    });
});

/**
 * edit a single command
 * @param href
 */
function getCommandAjax(href)
{
    $.ajax({
        url: href,
        method: "GET",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType : "json"
    })
    .done(function(response)
    {
        const command_ajax = JSON.parse(JSON.stringify(response));
        $('#id-command-edit-id').val(command_ajax.id)
        $('#id-command-edit-number').val(command_ajax.cmdNumber);
        $('#id-command-edit-createAt').val(command_ajax.cmdCreateAt);
        $('#id-command-edit-amount').val(command_ajax.cmdAmount);
        $('#id-command-edit-description').val(command_ajax.cmdDescription);
        $('#id-command-edit-product').val(command_ajax.zrpProducts[0].id).trigger('change');
        $('#id-command-edit-full-name').val(command_ajax.zrpClient.id).trigger('change');
        $('#id-command-edit-form-modal').modal('show');

    }).fail(function(error)
    {
        console.log("Error " + JSON.stringify(error));
    })
    .always(function(){

        console.log('request completed!!!');
    });
}
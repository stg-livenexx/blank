$(document).ready(function () {

    $('.kl-customer-date-of-creation').datepicker({
        format: "dd/mm/yyyy",
        clearBtn: true,
        autoclose: true,
        language: locale
    });

    $('#id-show-modal-add').click(function (e) {
        e.preventDefault();
        $('#id-form-add').get(0).reset();
        $('.kl-error-span-validate').text('');
        $('#id-command-add-product').val('').trigger('change');
        $('#id-command-add-full-name').val('').trigger('change');
    })

    /*hide sprin*/
    $('.kl-sprin').hide();

    formReset();
});

var column_defs = [
    {
        name: "cltLastName",
        orderable: true,
        targets: 6
    },
    {
        name: "pdLibelle",
        orderable: true,
        targets: 5
    },
    {
        name: "cmdDescription",
        orderable: true,
        targets: 4
    },
    {
        name: "cmdAmount",
        orderable: true,
        targets: 3
    },
    {
        name: "cmdCreateAt",
        orderable: true,
        targets: 2
    },
    {
        name: "cmdNumber",
        orderable: true,
        targets: 1
    },
    {
        name: "id",
        orderable: false,
        render: function (data, type, row) {
            var href_edit = data ? url_command_edit_default : "javascript:void(0)";
            var href_delete = data ? url_command_delete_default : "javascript:void(0)";
            href_edit = href_edit.replace('0', data);
            href_delete = href_delete.replace('0', data);
            var url_generate_pdf_default = data ? url_command_generate_pdf_default : "javascript:void(0)";
            url_generate_pdf_default = url_generate_pdf_default.replace('0', data);
            return '<td>' +
                        '<a class="kl-edit-ajax" href="' + href_edit + '"><i class="feather icon-edit"></i></a>' +
                        '<a class="kl-remove-elt-ajax" href="' + href_delete + '">' + '<i class="feather icon-trash-2"></i></a>' +
                        '<a href="' + url_generate_pdf_default + '">' + '<i class="fas fa-file-pdf"></i></a>' +
                   '</td>';
        },
        targets: 0
    }
];

$(document).ready(function () {
        datatable = $('#id-command-list').DataTable({
        bProcessing: true,
        bFilter: true,
        bServerSide: true,
        iDisplayLength: 10,
        order: [],
        ajax: {
            url: url_command_list_ajax,
            data: function (data) {
                data.order_by = data.order[0] ? data.columns[data.order[0].column].name + ' ' + data.order[0].dir : '';
                for (var i = 0; i < params_filter_datatable.length; i++) {
                    data[params_filter_datatable[i]['key']] = params_filter_datatable[i]['value'];
                }
                var cmd_date = new Date();
                var cmd_timezone = cmd_date.getTimezoneOffset() / 60;

                data.numero_one = $('#id-form-command-filter input[name="numero_one"]').val();
                data.numero_two = $('#id-form-command-filter input[name="numero_two"]').val();
                data.date_command_one = $('#id-form-command-filter input[name="date_command_one"]').val();
                data.date_command_two = $('#id-form-command-filter input[name="date_command_two"]').val();
                data.montant_one =  $('#id-form-command-filter input[name="montant_one"]').val();
                data.montant_two = $('#id-form-command-filter input[name="montant_two"]').val();
                data.decription = $('#id-form-command-filter input[name="decription"]').val();
                data.product_id = $('#id-form-command-filter input[name="product_id"]').val();
                data.timezone = cmd_timezone;
            }
        },
        columnDefs: column_defs,
        language: {url: url_datatable_language},
        searching: true

        })
});

/**
 *  filter
 */
$(document).on('click', '#id-btn-filter', function (e) {
    datatable.draw();
});

/**
 * Reset form
 */
function formReset()
{
    $('.kl-command-reset').val('');
    $('#id-filter-product').val('').trigger('change');
}

/**
 *  on click reinitialize
 */
$(document).on('click', '#id-btn-reset', function () {
    formReset();
    datatable.draw();
});


/**
 *  number one
 */
$(document).on('input', '#id-filter-numero_one', function (e) {
    var number = e.target.value;
    $('#id-form-command-filter input[name="numero_one"]').val(number);
});
/**
 *  number two
 */
$(document).on('input', '#id-filter-numero-two', function (e) {
    var number = e.target.value;
    $('#id-form-command-filter input[name="numero_two"]').val(number);
});

/**
 *  date create one
 */
$(document).on('change.datetimepicker', '#id-filter-date-command-one', function (e) {
    var date_create = e.target.value;
    $('#id-form-command-filter input[name="date_command_one"]').val(date_create);
});
/**
 *  date create two
 */
$(document).on('change.datetimepicker', '#id-filter-date_command_two', function (e) {
    var date_create = e.target.value;
    $('#id-form-command-filter input[name="date_command_two"]').val(date_create);
});

/**
 *   amount one
 */
$(document).on('input', '#id-filter-montant_one', function (e) {
    var amount = e.target.value;
    $('#id-form-command-filter input[name="montant_one"]').val(amount);
});
/**
 *  amount two
 */
$(document).on('input', '#id-filter-montant_two', function (e) {
    var amount = e.target.value;
    $('#id-form-command-filter input[name="montant_two"]').val(amount);
});

/**
 *   description
 */
$(document).on('input', '#id-filter-decription', function (e) {
    var description = e.target.value;
    $('#id-form-command-filter input[name="decription"]').val(description);
});


/**
 *  on change product
 */
$(document).on('change', '#id-filter-product', function (e) {
    var product_id = e.target.value;
    $('#id-form-command-filter input[name="product_id"]').val(product_id);
});

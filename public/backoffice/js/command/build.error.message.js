function buildErrorMessage(input, form)
{
    switch ($(input).attr('id'))
    {
        case 'id-command-'+form+'-number':
            $(input).next().text('Veuillez saisir un numéro de commande valide');
            break;
        case 'id-command-'+form+'-createAt':
            $(input).next().text('Veuillez saisir une date valide');
            break;
        case 'id-command-'+form+'-amount':
            $(input).next().text('Veuillez saisir un montant valide');
            break;
        case 'id-command-'+form+'-product':
            $(`#id-message-error-${form}-product`).text('Veuillez Séléctionner un produit');
            break;
        case 'id-command-'+form+'-full-name':
            $(`#id-message-error-${form}-full-name`).text('Veuillez Séléctionner un client');
            break;
    }
}
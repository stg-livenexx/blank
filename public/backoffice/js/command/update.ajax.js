$(document).ready(function (){
    $('#id-button-save-edit').click(function(e){
        e.preventDefault();
        if(!isInValidateEdit())
        {
            var href = url_command_update_default.replace('0', $('#id-command-edit-id').val());
            updateCommandAjax(href);
        }
    });
});

/**
 * update a single command with method ajax
 * @param href
 */
function updateCommandAjax(href)
{

    $.ajax({
        url: href,
        method: "PUT",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType : "json",
        data: $('#id-form-edit').serialize()
    })
    .done(function(response)
    {
        $('#id-command-edit-form-modal').modal('hide');
        $('.alert.alert-success.alert-dismissible').append(`<span class="kl-success-message">${response.success}</span>`).fadeIn(2000);

        datatable.ajax.reload(function() {
            $('.alert.alert-success.alert-dismissible').fadeOut(4000);
            window.setTimeout(function () {$('.kl-success-message').remove();}, 4010);
        } );

    })
    .fail(function(error)
    {
        if(error.status === 400 && error.responseJSON["exist_number"])
        {
            $('#id-command-edit-number').next().text(error.responseJSON["exist_number"]);
        }
        else
        {

            $('#id-command-edit-form-modal').modal('hide');
            $('.alert.alert-danger.alert-dismissible').append(`<span class="kl-error-message">${error_message}</span>`).fadeIn(2000);
            window.setTimeout(function () {
                $('.alert.alert-danger.alert-dismissible').fadeOut(2000);
                window.setTimeout(function () {$('.kl-error-message').remove();}, 2010);
            }, 3000);

        }
    })
    .always(function()
    {
        console.log('update command completed!!!');
    });
}

/**
 * validate form
 * @returns {boolean}
 */
function isInValidateEdit()
{
    var error = false;
    $('.kl-command-edit').each(function () {
        switch($(this).val())
        {
            case '':
                buildErrorMessage(this, 'edit');
                error = true;
                break;
            default:
                $(this).next().hasClass('kl-error-span-validate') ? $(this).next().text('') : $('.kl-error-span-validate.select-error').text('');
        }
    })
    return error;
}

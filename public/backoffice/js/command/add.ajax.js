$(document).ready(function (){
    $('.kl-error-span-validate').css('color', 'red');
    $('#id-button-save-add').click(function (e) {
        e.preventDefault();
        if(!isInValidate()){
            /*show sprin*/
            $('.kl-sprin').show(300);
            addCommandAjax();
        }
    });
});

/**
 * add a single command with method ajax
 */
function addCommandAjax()
{
    $.ajax({

        url: url_command_add_default,
        method: "POST",
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        dataType : "json",
        data: $('#id-form-add').serialize()

    }).done(function(response){

        $('#id-command-add-form-modal').modal('hide');
        $('.alert.alert-success.alert-dismissible').append(`<span class="kl-success-message">${response.success}</span>`).fadeIn(2000);

        datatable.ajax.reload(function() {
            $('.alert.alert-success.alert-dismissible').fadeOut(4000);
            window.setTimeout(function () {$('.kl-success-message').remove();}, 4010);
        } );

    }).fail(function(error){

        if(error.status === 400 && error.responseJSON["exist_number"])
        {
            $('#id-command-add-number').next().text(error.responseJSON["exist_number"]);
        }
        else
        {

            $('#id-command-add-form-modal').modal('hide');
            $('.alert.alert-danger.alert-dismissible').append(`<span class="kl-error-message">${error_message}</span>`).fadeIn(2000);
            window.setTimeout(function () {
                $('.alert.alert-danger.alert-dismissible').fadeOut(2000);
                window.setTimeout(function () {$('.kl-error-message').remove();}, 2010);
            }, 3000);

        }

    })
    .always(function()
    {
        /*hide sprin*/
        $('.kl-sprin').hide(10);
        console.log('add command completed!!!');

    });
}

/**
 * validate form
 * @returns {boolean}
 */
function isInValidate()
{
    var error = false;
    $('.kl-command-add').each(function () {
        switch($(this).val())
        {
            case '':
                buildErrorMessage(this, 'add');
                error = true
                break;
            default:
                $(this).next().hasClass('kl-error-span-validate') ? $(this).next().text('') : $('.kl-error-span-validate.select-error').text('');
        }
    })
    return error;
}
var column_defs = [
    {
        name: "cmpTpName",
        orderable: true,
        targets: 1
    },
    {
        name: "id",
        orderable: false,
        render: function (data, type, row) {
            var href_edit = data ? href_edit_company_type_default : "javascript:void(0)";
            var href_delete = data ? href_delete_company_type_default : "javascript:void(0)";
            href_edit = href_edit.replace('0', data);
            href_delete = href_delete.replace('0', data);
            return '<td>' +
                '<a href="' + href_edit + '"><i class="feather icon-edit"></i></a>' +
                '<a class="kl-remove-elt" href="' + href_delete + '">' + '<i class="feather icon-trash-2"></i></a>' +
                '</td>';
        },
        targets: 0
    }
];

var datatable = generateDatatable($('#id-company-type-list'), url_company_type_list_ajax, column_defs);
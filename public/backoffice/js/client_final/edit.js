$(document).ready(function () {
    $(".kl-select-ville").select2({
        ajax: {
            url: url_city_ajax,
            dataType: 'json',
            delay: 250,
            minimumInputLength: 1,
            data: function (params) {
                return {
                    order_by: 'DESC',
                    term: params.term
                };
            },
            cache: true
        },
        language: locale,
    });
});
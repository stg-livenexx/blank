var column_defs = [
    {
        name: "id",
        orderable: false,
        render: function (data, type, row) {
            var href_edit = data ? href_edit_default : "javascript:void(0)";
            var href_delete = data ? href_delete_default : "javascript:void(0)";
            var href_fiche = data ? href_fiche_default : "javascript:void(0)";
            href_edit = href_edit.replace('0', data);
            href_delete = href_delete.replace('0', data);
            href_fiche = href_fiche.replace('0', data);
            var action = '<td>';
            var action_sa = '';
            if (role_company_id > 0) action_sa += '<a href="' + href_edit + '"><i class="feather icon-edit"></i></a>';
            action += '<a class="kl-remove-elt" href="' + href_delete + '">' + '<i class="feather icon-trash-2"></i></a></td>';
            action += '<a href="' + href_fiche + '">' + '<i class="feather icon-file"></i></a></td>';
            return action_sa + action;
        },
        targets: 0
    },
    {
        name: "usrFirstname",
        orderable: true,
        targets: 1
    },
    {
        name: "usrLastname",
        orderable: true,
        targets: 2
    },
    {
        name: "usrAddress",
        orderable: true,
        targets: 3
    },
    {
        name: "ct.ctName",
        orderable: true,
        targets: 4
    },
    {
        name: "usrDateBirth",
        orderable: true,
        targets: 5
    },
    {
        name: "usrPhoneNumber",
        orderable: true,
        targets: 6
    },
    {
        name: "email",
        orderable: true,
        targets: 7
    },
    {
        name: "postalCode",
        orderable: true,
        render: function(data){
            var value =  thousandSeparator(data);
            if ( value == 'NaN')
                return '';
            return value;
        },
        targets: 8
    }
];

var datatable = generateDatatable($('#id-client-final-list'), url_client_final_list_ajax, column_defs, false);
updateDataParams();

$(document).ready(function () {
    $('.kl-select-city').select2({
        ajax:
            {
                url: url_city_list_ajax,
                data: function (params) {
                    return {
                        term: params.term
                    };
                },
            },
        allowClear: true,
        placeholder: $('.kl-select-city').data('placeholder'),
        language: locale
    });

    $('.kl-customer-date-of-birth').datepicker({
        format: "dd-mm-yyyy",
        clearBtn: true,
        autoclose: true,
        language: locale
    });

    $(document).on('click', '.kl-btn-customer-filter', function () {
        updateDataParams();
        datatable.draw();
    });

    $(document).on('click', '.kl-btn-customer-clear-filter', function () {
        clearDataParams();
        updateDataParams();
        datatable.draw();
    });
});

function updateDataParams() {
    params_filter_datatable = [
        {
            key: 'data_filter',
            value: [
                {
                    key: 'usrFirstname',
                    value: $('.kl-customer-firstname').val()
                },
                {
                    key: 'usrLastname',
                    value: $('.kl-customer-lastname').val()
                },
                {
                    key: 'usrAddress',
                    value: $('.kl-customer-address').val()
                },
                {
                    key: 'ctName',
                    value: $('#id-filter-city').val()
                },
                {
                    key: 'usrDateBirth',
                    value: $('.kl-customer-date-of-birth').val()
                },
                {
                    key: 'usrPhoneNumber',
                    value: $('.kl-customer-phone').val()
                },
                {
                    key: 'email',
                    value: $('.kl-customer-email').val()
                },
                {
                    key: 'postalCode',
                    value: $('.kl-postal-code').val()
                }
            ]
        }
    ];
}

function clearDataParams() {
    $('.kl-customer-firstname').val('');
    $('.kl-customer-lastname').val('');
    $('.kl-customer-address').val('');
    $('#id-filter-city').val('');
    $('.kl-customer-date-of-birth').datepicker('update', '');
    $('.kl-customer-phone').val('');
    $('.kl-postal-code').val('');
    $('.kl-customer-email').val('');
}
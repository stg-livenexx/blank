$(document).ready(function () {
    if($('#zrp_userbundle_user_password_first').parent().parent().hasClass('has-error')) {
        var ul_error = $('#zrp_userbundle_user_password_first').parent();
        var ul = ul_error.next();
        ul.hide();
        $('#zrp_userbundle_user_password_first').parent().parent().next().append(ul.css({'display':'block'}));
    }
    $('#id-btn-submit').click(function (e) {
        $('form[name=zrp_userbundle_user]').bootstrapValidator();
    });
    
    $('#zrp_userbundle_user_usrDateBirth').focusout(function() {
        $('#zrp_userbundle_user_usrDateBirth').trigger('change');
    })
});
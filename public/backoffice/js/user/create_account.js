$(document).ready(function () {
    $('#id-btn-next').on('click', function () {
        validatePartOne();
       if (validatePartOne()) {
           $('.part-one').addClass('d-none');
           $('.part-two').removeClass('d-none');
       }
    });

    //Validation first part
    $('#id-professionel').click(function () {
        if ($('#id-professionel:checkbox:checked').length > 0) $('#id-serial-number-parent').addClass('d-none');
        else $('#id-serial-number-parent').removeClass('d-none');
    });
    $('#id-particular').click(function () {
        $('#id-serial-number-parent').addClass('d-none');
    });

    //Validation second part
    $('#id-btn-submit').click(function (e) {
         $('form[name=zrp_userbundle_user]').bootstrapValidator();
        validatePartTwo();
        if (!validatePartTwo()) e.preventDefault();
    });
     // $('form[name=zrp_userbundle_user]').bootstrapValidator('destroy');
});

function validatePartOne() {
    var name = $('#zrp_userbundle_user_usrLastname').val();
    var lastname = $('#zrp_userbundle_user_usrFirstname').val();
    var password = $('#zrp_userbundle_user_password').val();
    var email = $('#zrp_userbundle_user_email').val();
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var check_general = $('#id-contrat-general').prop('checked');
    var check_mail = $('#id-contrat-mail').prop('checked');

    if (name == '') {
        $('#id-error-message').removeClass('d-none');
        $('#id-error-message').text(error_message_input_empty_name);
        return false;
    } else if(lastname == '') {
        $('#id-error-message').removeClass('d-none');
        $('#id-error-message').text(error_message_input_empty_lastname);
        return false;
    } else if(email == '') {
        $('#id-error-message').removeClass('d-none');
        $('#id-error-message').text(error_message_input_empty_mail);
        return false;
    } else if(!emailReg.test(email)) {
        $('#id-error-message').removeClass('d-none');
        $('#id-error-message').text(error_message_mail_not_valid);
        return false;
    } else if(password == '') {
        $('#id-error-message').removeClass('d-none');
        $('#id-error-message').text(error_message_input_empty_password);
        return false;
    } else if(password.length < 6) {
        $('#id-error-message').removeClass('d-none');
        $('#id-error-message').text(error_message_password);
        return false;
    } else if ((check_general == false) || (check_mail == false)) {
        $('#id-error-message').removeClass('d-none');
        $('#id-error-message').text(error_message_contrat);
        return false;
    }
    return true;
}

function validatePartTwo() {
    var type_compte = $('input[name=type_compte]:checked').val();
    var adresse = $('#zrp_userbundle_user_usrAddress').val();
    var datebirth = $('#zrp_userbundle_user_usrDateBirth').val();
    var phone = $('#zrp_userbundle_user_usrPhoneNumber').val();
    var siren_number = $('#siren_number').val();
    var captcha_value = $('#zrp_userbundle_user_captcha').val();
    if (typeof type_compte == "undefined") {
        $('#id-error-message-part-two').removeClass('d-none');
        $('#id-error-message-part-two').text(error_message_error_type_compte);
        return false;
    } else if (adresse == '') {
        $('#id-error-message-part-two').removeClass('d-none');
        $('#id-error-message-part-two').text(error_message_error_adress);
        return false;
    } else if (datebirth == ''){
        $('#id-error-message-part-two').removeClass('d-none');
        $('#id-error-message-part-two').text(error_message_error_date_birth);
        return false;
    } else if (phone == ''){
        $('#id-error-message-part-two').removeClass('d-none');
        $('#id-error-message-part-two').text(error_message_error_phone);
        return false;
    } else if (siren_number == ''){
        if (type_compte != 'particular')
        {
            $('#id-error-message-part-two').removeClass('d-none');
            $('#id-error-message-part-two').text(error_message_error_empty_siren);
            return false;
        }
    } /*else if (!$.isNumeric(phone)){
        $('#id-error-message-part-two').removeClass('d-none');
        $('#id-error-message-part-two').text(error_message_error_phone_number);
        return false;
    }*/ else if ((!$.isNumeric(siren_number)) && siren_number.length > 0){
        $('#id-error-message-part-two').removeClass('d-none');
        $('#id-error-message-part-two').text(error_message_error_siren_number);
        return false;
    } else if (captcha_value == '') {
        $('#id-error-message-part-two').removeClass('d-none');
        $('#id-error-message-part-two').text(error_message_error_captcha);
        return false;
    }
    return true;
}

//Chose one checkbox
var allIds = [ "id-professionel", "id-particular"];
function uncheck( event )
{
    var id = event.target.id;
    allIds.forEach( function( id ){
        if ( id != event.target.id )
        {
            document.getElementById( id ).checked = false;
        }
    });
}
$("#id-professionel").click(uncheck);
$("#id-particular").click(uncheck);

//Datetime
var isRtl = $('html').attr('dir') === 'rtl';
$('.kl-date-time-picker-birthday').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
    orientation: isRtl ? 'auto right' : 'auto left',
    language: 'fr'
});
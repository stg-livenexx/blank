var column_defs = [
    {
        name: "id",
        orderable: false,
        render: function (data, type, row) {
            var href_edit = data ? href_edit_default : "javascript:void(0)";
            var href_delete = data ? href_delete_default : "javascript:void(0)";
            var href_show = data ? href_show_default : "javascript:void(0)";
            href_edit = href_edit.replace('0', data);
            href_delete = href_delete.replace('0', data);
            href_show = href_show.replace('0', data);
            var action = '<td>' +'<a href="' + href_edit + '">' + '<i class="feather icon-edit"></i>' + '</a> ';
            action += '<a href="' + href_show + '">' + '<i class="feather icon-user"></i>' + '</a>'
                + '</td>'
            if (user_id != data)
                action += '<a class="kl-remove-elt"' +' href="' + href_delete + '">' + '<i class="feather icon-trash-2"></i>' + '</a>'


                ;
            return action;
        },
        targets: 0
    },
    {
        name: "usrLastname",
        orderable: true,
        targets: 1
    },
    {
        name: "email",
        orderable: true,
        targets: 2
    },
    {
        name: "usrAddress",
        orderable: true,
        targets: 3
    },
    {
        name: "rlName",
        orderable: true,
        targets: 4
    },
    {
        name: "usrDateCreate",
        orderable: true,
        targets: 5
    },
];

var datatable = generateDatatable($('#id-user-list'), url_user_list_ajax, column_defs);
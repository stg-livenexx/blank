var geocoder = new google.maps.Geocoder();

/**
 * geocodePosition
 * @param pos
 */
function geocodePosition(pos)
{
    geocoder.geocode({
        latLng: pos
    }, function(responses) {
        if (responses && responses.length > 0) {
            updateMarkerAddress(responses[0].formatted_address);
        } else {
            updateMarkerAddress('Cannot determine address at this location.');
        }
    });
}

/**
 * updateMarkerAddress
 * @param str
 */
function updateMarkerAddress(str) {
    document.getElementById('zrp_userbundle_user_zrpCity').value = str;
}

/**
 * initialize Google map
 */
function initialize()
{
    var lat_long = new google.maps.LatLng(48.856614, 2.3522219);
    var map = new google.maps.Map(document.getElementById('id-map-canvas'), {
        zoom: 16,
        center: lat_long,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var options = {
        componentRestrictions: { country: europe_country },
    };

    var input = document.getElementById('zrp_userbundle_user_zrpCity');
    var autocomplete = new google.maps.places.Autocomplete(input,options);
    // Set initial restriction to the greater list of countries.
   /* autocomplete.setComponentRestrictions({
        country: ["fr", "es", "it", "gu", "mp"],
    });*/
    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        position: lat_long,
        title: 'Position du centre',
        map: map,
        draggable: true,
    });
    google.maps.event.addListener(marker, 'dragstart', function() {
        updateMarkerAddress('Dragging...');
    });
    google.maps.event.addListener(marker, 'dragend', function() {
        geocodePosition(marker.getPosition());
    });
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        infowindow.close();
        var place = autocomplete.getPlace();
        for (var i = 0; i < place.address_components.length; i++) {
            for (var j = 0; j < place.address_components[i].types.length; j++) {
                if (place.address_components[i].types[j] == "postal_code") {
                    document.getElementById('zrp_userbundle_user_cmpCp').value
                        = place.address_components[i].long_name ;
                    $('#zrp_userbundle_user_cmpCp').trigger('input');
                }else {
                    var code_postal_field = document.getElementById('zrp_userbundle_user_cmpCp') ;
                    if(code_postal_field) code_postal_field.value = '';
                }
            }
        }
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        var image = new google.maps.MarkerImage(
            place.icon,
            new google.maps.Size(71, 71),
            new google.maps.Point(0, 0),
            new google.maps.Point(17, 34),
            new google.maps.Size(35, 35));
        marker.setIcon(image);
        marker.setPosition(place.geometry.location);
        var address = '';
        if (place.address_components) {
            address = [(place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')].join(' ');
        }
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address + "<br>" + place.geometry.location);
        infowindow.open(map, marker);
    });
}

google.maps.event.addDomListener(window, 'load', initialize);
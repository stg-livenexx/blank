$(document).ready(function () {
    $('.kl-select-city').each(function () {
        $(this)
            .wrap('<div class="position-relative"></div>')
            .select2({
                ajax: {
                    url: url_city_list_ajax,
                    data: function (params) {
                        return {
                            term: params.term
                        };
                    },
                },
                dropdownParent: $(this).parent(),
                language: locale
            });
        $('.kl-select-city-no-search').each(function () {
            $(this)
                .wrap('<div class="position-relative"></div>')
                .select2({
                    dropdownParent: $(this).parent(),
                    language: locale,
                    minimumResultsForSearch: -1
                });
        });
    });

    if(is_not_client != true) {
        $('#zrp_userbundle_user_password_first, #zrp_userbundle_user_password_second').removeAttr('required');

        $('form').bootstrapValidator('enableFieldValidators', 'zrp_userbundle_user[password][first]', false);
        $('form').bootstrapValidator('enableFieldValidators', 'zrp_userbundle_user[password][second]', false);

        $('#zrp_userbundle_user_password_second').on('input', function () {
            $('#zrp_userbundle_user_password_first').attr('data-bv-identical', true);
        });
    }
});

$(document).on('change.datetimepicker', '.kl-date-time-picker', function (e) {
    $("#zrp_userbundle_user_usrDateBirth").trigger("input")
});

$(document).ready(function () {

    $('#id-datetime-picker1').datetimepicker({
        format : 'DD/MM/YYYY HH:mm',
        locale: 'fr',
        useCurrent: false,
        sideBySide : true
    });

    $('.kl-select-city').each(function () {
        $(this)
            .wrap('<div class="position-relative"></div>')
            .select2(
                {
                    ajax:
                        {
                            url: url_city_list_ajax,
                            data: function (params) {
                                return {
                                    term: params.term
                                };
                            },
                        },
                    dropdownParent: $(this).parent(),
                    language: locale
                });
        $('.kl-select-city-no-search').each(function () {
            $(this)
                .wrap('<div class="position-relative"></div>')
                .select2({
                    dropdownParent: $(this).parent(),
                    language: locale,
                    minimumResultsForSearch: -1
                });
        });
    });

    $('#zrp_service_metiermanagerbundle_company_effective').val();

});

$('#zrp_service_metiermanagerbundle_ticket_zrpCashRegister').select2();

$(document).on('change', '#zrp_service_metiermanagerbundle_company_cmpDateCreation', function (e) {
    $("#zrp_service_metiermanagerbundle_company_cmpDateCreation").trigger('input');
});

$(document).on('change', '#zrp_service_metiermanagerbundle_company_cmpZeropCheckoutDate', function (e) {
    $("#zrp_service_metiermanagerbundle_company_cmpZeropCheckoutDate").trigger('input');
});

$(document).on('click','.kl-open-dtm', function (e) {
    $('[data-bv-for="zrp_service_metiermanagerbundle_ticket[trxIssueDate]"]').attr('style','display: none')
});

/* On input */
$("#zrp_service_metiermanagerbundle_ticket_trxAmount, #zrp_service_metiermanagerbundle_company_cmp_ca").on('input', function () {
    zrpMaskInput($(this));
});

/** on key up cash number Zerop */
$(document).on('keyup','#zrp_service_metiermanagerbundle_company_cmpZeropCheckoutNbr', function (e) {
    var number = $('#zrp_service_metiermanagerbundle_company_cmpCheckoutNbr').val();
    var cash_number_zerop = parseInt(isNaN($(this).val()) ? 0 : $(this).val());
    var cash_number = parseInt(isNaN(number) ? 0 : number);
    deleteErrorMessage();

    if ((cash_number && cash_number_zerop) && cash_number < cash_number_zerop)
    {
        addErrorMessage('#zrp_service_metiermanagerbundle_company_cmpZeropCheckoutNbr', 'cmpZerop', error_message_zerop, cash_number);
    }
    else{
        deleteErrorMessage();
        if (is_edit)
            $('.kl-btn-save').removeAttr('disabled','disabled');
    }
});

/** on key up cash number */
$(document).on('keyup','#zrp_service_metiermanagerbundle_company_cmpCheckoutNbr', function (e) {
    var number_zerop = $('#zrp_service_metiermanagerbundle_company_cmpZeropCheckoutNbr').val();
    var cash_number_zerop = parseInt(isNaN(number_zerop) ? 0 : number_zerop);
    var cash_number = parseInt(isNaN($(this).val()) ? 0 : $(this).val());
    deleteErrorMessage();

    if ((cash_number && cash_number_zerop) && cash_number < cash_number_zerop)
    {
        addErrorMessage('#zrp_service_metiermanagerbundle_company_cmpCheckoutNbr', 'cmp', error_message, cash_number_zerop);
    }
    else {
        deleteErrorMessage();
        if (is_edit)
            $('.kl-btn-save').removeAttr('disabled','disabled');
    }
});

/** on submit save*/
$(document).on('click','.kl-btn-save', function (e) {
    var number = $('#zrp_service_metiermanagerbundle_company_cmpCheckoutNbr').val();
    var number_zerop = $('#zrp_service_metiermanagerbundle_company_cmpZeropCheckoutNbr').val();
    var cash_number_zerop = parseInt(isNaN(number_zerop) ? 0 : number_zerop);
    var cash_number = parseInt(isNaN(number) ? 0 : number);
    if ((cash_number && cash_number_zerop) && (cash_number < cash_number_zerop))
    {
        $('form[name=zrp_service_metiermanagerbundle_company]').bootstrapValidator();
        addErrorMessage('#zrp_service_metiermanagerbundle_company_cmpZeropCheckoutNbr', 'cmpZerop', error_message_zerop, cash_number);
        e.preventDefault();
    }
});

/**
 * delete error message
 */
function deleteErrorMessage() {
    $('[data-bv-for="zrp_service_metiermanagerbundle_company[cmpZerop]"]').attr('style','display: none');
    $('[data-bv-for="zrp_service_metiermanagerbundle_company[cmp]"]').attr('style','display: none');
}

/**
 * add error message
 * @param id
 * @param name
 * @param error_message
 * @param cash
 */
function addErrorMessage(id, name, error_message, cash) {
    $(id).parent().find('div.clearfix').append('<small class="help-block" data-bv-validator="lessthan" ' +
        'data-bv-for="zrp_service_metiermanagerbundle_company[' + name + ']" data-bv-result="INVALID" ' +
        'style="">' + error_message + ' ('+ cash +').</small>');
    $('.kl-btn-save').attr('disabled','disabled');
}


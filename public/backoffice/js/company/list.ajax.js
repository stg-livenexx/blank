var column_defs = [

    {
        name: "id",
        orderable: false,
        render: function (data, type, row) {
            var href_edit = data ? href_edit_default : "javascript:void(0)";
            var href_delete = data ? href_delete_default : "javascript:void(0)";
            href_edit = href_edit.replace('0', data);
            href_delete = href_delete.replace('0', data);
            var action = '<td>' +
                '<a href="' + href_edit + '"><i class="feather icon-edit"></i></a>' ;

            action += '<a class="kl-remove-elt" href="' + href_delete + '">' + '<i class="feather icon-trash-2"></i></a>' +
                '</td>'
            ;
            return action;
        },
        targets: 0
    },
    {
        name: "activity",
        orderable: true,
        targets: 1,
        visible:false
    },
    {
        name: "cmp_name",
        orderable: true,
        targets: 2
    },
    {
        name: "cmp_cp",
        orderable: true,
        render: function(data){
            var value =  thousandSeparator(data);
            if ( value == 'NaN')
                return '';
            return value;
        },
        targets: 3
    },
    {
        name: "ct_name",
        orderable: true,
        targets: 4
    },
    {
        name: "cmpTpName",
        orderable: true,
        targets: 5
    },
    {
        name: "erp_name",
        orderable: true,
        targets: 6
    },
    {
        name: "cmp_effective",
        orderable: true,
        targets: 7
    },
    {
        name: "dateCreation",
        orderable: true,
        targets: 8
    },
    {
        name: "cmp_ca",
        orderable: true,
        render: function(data){
            var value =  thousandSeparator(data);
            if ( value == 'NaN')
                return '';
            return value;
        },
        targets: 9
    },
    {
        name: "cmpCheckoutNbr",
        orderable: true,
        render: function(data){
            var value =  thousandSeparator(data);
            if ( value == 'NaN')
                return '';
            return value;
        },
        targets: 10
    },
    {
        name: "cmpZeropCheckoutNbr",
        orderable: true,
        render: function(data){
            var value =  thousandSeparator(data);
            if ( value == 'NaN')
                return '';
            return value;
        },
        targets: 11
    },
    {
        name: "dateZeropCheckout",
        orderable: true,
        targets: 12
    },
    {
        name: "user_name",
        orderable: true,
        targets: 13
    },
    {
        name: "num_siren",
        orderable: true,
        render: function(data){
            var value =  thousandSeparator(data);
            if ( value == 'NaN')
                return '';
            return value;
        },
        targets: 14
    }


];

var datatable = $('#id-company-list').DataTable(
    {
        "bProcessing": true,
        "bFilter": true,
        "bServerSide": true,
        "iDisplayLength": 10,
        "order": [],
        "ajax": {
            url: url_company_list_ajax,
            data: function (data) {
                data.order_by = data.order[0] ? data.columns[data.order[0].column].name + ' ' + data.order[0].dir : 'id desc';
                data.activity_id = $('.kl-activity').data('id');
                for (var i = 0; i < params_filter_datatable.length; i++) {
                    data[params_filter_datatable[i]['key']] = params_filter_datatable[i]['value'];
                }

                data.raison_social = $('#id-filter-rs').val();
                data.code_postal = $('#id-filter-code-postal').val();
                data.type_entreprise = $('#id-select-type-entreprise').val();
                data.commune_facturant = $('#id-filter-commune-facturant').val();
                data.erp = $('#id-select-type-erp').val();
                data.effectif = $('#id-filter-effectif').val();
                data.data_creation = $('#id-filter-date-creation').val();
                data.ca = $('#id-filter-ca').val();
                data.nbr_caisse = $('#id-filter-nbr-caisse').val();
                data.nbr_caisse_zerop = $('#id-filter-nbr-caisse-zerop').val();
                data.date_caisse = $('#id-filter-date-caisse').val();
                data.user = $('#id-select-user').val();
                data.siren = $('#id-filter-siren').val();
            }
        },columnDefs: typeof column_defs != "undefined" ? column_defs : [],
        language: {url: url_datatable_language}
    }
);

/**
 *  filter
 */
$(document).on('click', '#id-btn-filter', function (e) {
    datatable.draw();
});

/**
 *  on click reinitialize
 */
$(document).on('click', '#id-btn-reset', function () {

    $('.kl-select-city').val(0).trigger('change');
    $('#id-select-type-entreprise').val(0).trigger('change');
    $('#id-select-type-erp').val(0).trigger('change');
    $('#id-select-user').val(0).trigger('change');

    $('#id-filter-rs').val('');
    $('#id-filter-code-postal').val('');
    $('#id-filter-effectif').val('');
    $('#id-filter-date-creation').val('');
    $('#id-filter-ca').val('');
    $('#id-filter-nbr-caisse').val('');
    $('#id-filter-nbr-caisse-zerop').val('');
    $('#id-filter-date-caisse').val('');
    $('#id-filter-siren').val('');
    $('#id-filter-commune-facturant').val('');

    datatable.draw();
});

$('#id-select-activity').on('change', function(){
    var initialize_datatable = datatable;
    $('.kl-activity').data('id', this.value);
    initialize_datatable.draw();
});
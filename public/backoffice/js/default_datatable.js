var params_filter_datatable = [];

/**
 * Generate datatable
 * @param selector_datatable
 * @param datatable_ajax_url
 * @param column_defs
 * @param searching
 * @returns {*|jQuery}
 */
function generateDatatable(selector_datatable, datatable_ajax_url, column_defs, searching = true) {
    return $(selector_datatable).DataTable({
        bProcessing: true,
        bFilter: true,
        bServerSide: true,
        iDisplayLength: 10,
        order: [],
        ajax: {
            url: datatable_ajax_url,
            data: function (data) {
                data.order_by = data.order[0] ? data.columns[data.order[0].column].name + ' ' + data.order[0].dir : 'id desc';
                for (var i = 0; i < params_filter_datatable.length; i++) {
                    data[params_filter_datatable[i]['key']] = params_filter_datatable[i]['value'];
                }
            }
        },
        columnDefs: typeof column_defs != "undefined" ? column_defs : [],
        language: {url: url_datatable_language},
        searching: searching
    });
}
$(document).ready(function () {
    var transaction_datatable = $('#id-transaction-list').DataTable({
        bProcessing: true,
        bFilter: true,
        bServerSide: true,
        iDisplayLength: 10,
        order: [],
        ajax: {
            url: url_admin_transaction_list_ajax,
            data: function (data) {
                data.order_by = data.order[0] ? data.columns[data.order[0].column].name + ' ' + data.order[0].dir : '';
                for (var i = 0; i < params_filter_datatable.length; i++) {
                    data[params_filter_datatable[i]['key']] = params_filter_datatable[i]['value'];
                }
                var trx_date = new Date();
                var trx_timezone = trx_date.getTimezoneOffset() / 60;

                data.montant_one = $('#id-form-transaction input[name="montant_one"]').val();
                data.montant_two = $('#id-form-transaction input[name="montant_two"]').val();
                data.date_emission_one = $('#id-form-transaction input[name="date_emission_one"]').val();
                data.date_emission_two = $('#id-form-transaction input[name="date_emission_two"]').val();
                data.commune_facturant =  $('#id-filter-commune-facturant').val();
                data.lastname_term = $('#id-form-transaction input[name="lastname_term"]').val();
                data.firstname_term = $('#id-form-transaction input[name="firstname_term"]').val();
                data.code_postal = $('#id-form-transaction input[name="code_postal"]').val();
                data.commune_facture = $('#id-filter-commune').val();
                data.num_caisse = $('#id-form-transaction input[name="num_caisse"]').val();
                data.customer_id = customer_id;
                data.timezone = trx_timezone;
            }
        },
        columnDefs: [
            {
                name: "id",
                orderable: false,
                render: function (data, type, row) {
                    var href_delete = data ? href_delete_default : "javascript:void(0)";
                    href_delete = href_delete.replace('0', data);

                    /*
                                             -- qr_code --
                    var href_show_qr_code = data ? href_show_qr_code_default : "javascript:void(0)";
                    href_show_qr_code = href_show_qr_code.replace('0', data);

                    var action = '<a class="kl-remove-elt" href="' + href_delete + '">'
                        + '<i class="feather icon-trash-2"></i></a>' +
                        '<a  href="' + href_show_qr_code + '">'
                        + '<i class="fa fa-qrcode"></i></a>' +
                        '</td>'
                    ;

                     */

                    var href_transaction_ticket_generate_pdf = data ? href_transaction_ticket_generate_pdf_default : "javascript:void(0)";
                    href_transaction_ticket_generate_pdf = href_transaction_ticket_generate_pdf.replace('0', data);

                    var action = '<a class="kl-remove-elt" href="' + href_delete + '">'
                        + '<i class="feather icon-trash-2"></i></a>' +
                        '<a  href="' + href_transaction_ticket_generate_pdf + '">'
                        + '<i class="fas fa-file-pdf"></i></a>' +
                        '</td>'
                    ;

                    return action;
                },
                targets: 0
            },
            {
                name: "amount",
                orderable: true,
                render: function (data) {
                    var value = thousandSeparator(data);
                    if (value == 'NaN')
                        return '';
                    return value;
                },
                targets: 1
            },
            {
                name: "date_transaction",
                orderable: true,
                render: function (data) {
                    var format = "DD/MM/YYYY HH:mm";
                    var trx_data = moment(data, format).format("MM/DD/YYYY HH:mm");
                    var trx_date = new Date(trx_data);
                    var trx_timezone = trx_date.getTimezoneOffset() / 60;
                    trx_date.setHours(trx_date.getHours() - trx_timezone);

                    return moment(trx_date, format).format(format);
                },
                targets: 2
            },
            {
                name: "city_biller",
                orderable: true,
                targets: 3
            },
            {
                name: "lastname",
                orderable: true,
                targets: 4
            },
            {
                name: "firstname",
                orderable: true,
                targets: 5
            },
            {
                name: "code_postal",
                orderable: true,
                render: function (data) {
                    var value = thousandSeparator(data);
                    if (value == 'NaN')
                        return '';
                    return value;
                },
                targets: 6
            },
            {
                name: "city_invoiced",
                orderable: true,
                targets: 7
            },
            {
                name : "num_caisse",
                orderable: true,
                targets : 8
            }
        ],
        language: {url: url_datatable_language}
    });

    /**
     *  filter
     */
    $(document).on('click', '#id-btn-filter', function (e) {
        transaction_datatable.draw();
    });

    /**
     *  montant
     */
    $(document).on('input', '#id-filter-montant-one', function (e) {
        var montant = e.target.value;
        $('#id-form-transaction input[name="montant_one"]').val(montant);
    });
    /**
     *  montant
     */
    $(document).on('input', '#id-filter-montant-two', function (e) {
        var montant = e.target.value;
        $('#id-form-transaction input[name="montant_two"]').val(montant);
    });

    /**
     *  date emission
     */
    $(document).on('change.datetimepicker', '#id-filter-date-emission-one', function (e) {
        var date_emission = e.target.value;
        $('#id-form-transaction input[name="date_emission_one"]').val(date_emission);
    });
    /**
     *  date emission
     */
    $(document).on('change.datetimepicker', '#id-filter-date-emission-two', function (e) {
        var date_emission = e.target.value;
        $('#id-form-transaction input[name="date_emission_two"]').val(date_emission);

    });

    /**
     *  on change num caisse
     */
    $(document).on('change', '#id-filter-num-caisse', function (e) {
         var num_caisse = e.target.value;
        $('#id-form-transaction input[name="num_caisse"]').val(num_caisse);
    });

    /**
     *  lastname
     */
    $(document).on('input', '#id-filter-lastname', function (e) {
        var lastname_term = e.target.value;
        $('#id-form-transaction input[name="lastname_term"]').val(lastname_term);
    });

    /**
     *  firstname
     */
    $(document).on('input', '#id-filter-firstname', function (e) {
        var firstname_term = e.target.value;
        $('#id-form-transaction input[name="firstname_term"]').val(firstname_term);
    });

    /**
     *  code postal
     */
    $(document).on('input', '#id-filter-code-postal', function (e) {
        var code_postal = e.target.value;
        $('#id-form-transaction input[name="code_postal"]').val(code_postal);
    });

    /**
     * ref num caisse
     */
    $(document).on('select2:select', '#id-filter-num-caisse', function (e) {
        var num_caisse = e.target.value;
        $('#id-form-transaction input[name="num_caisse"]').val(num_caisse);
    });

    /**
     *  on click reinitialize
     */
    $(document).on('click', '#id-btn-reset', function () {
        $('#id-filter-montant-one').val('');
        $('#id-form-transaction input[name="montant_one"]').val('');

        $('#id-filter-montant-two').val('');
        $('#id-form-transaction input[name="montant_two"]').val('');

        $('#id-filter-commune').val('');
        $('#id-form-transaction input[name="commune_facture]').val('');

        $('#id-filter-commune-facturant').val('');
        $('#id-form-transaction input[name="commune_facturant]').val('');


        $('#id-filter-date-emission-one').val('');
        $('#id-form-transaction input[name="date_emission_one"]').val('');

        $('#id-filter-date-emission-two').val('');
        $('#id-form-transaction input[name="date_emission_two"]').val('');

        $('#id-form-transaction input[name="commune_facturant"]').val('');

        $('#id-filter-lastname').val('');
        $('#id-form-transaction input[name="lastname_term"]').val('');

        $('#id-filter-firstname').val('');
        $('#id-form-transaction input[name="firstname_term"]').val('');

        $('#id-filter-code-postal').val('');
        $('#id-form-transaction input[name="code_postal"]').val('');

        $('#id-filter-num-caisse').select2({
            placeholder: "- Numéro de caisse -",
            ajax: {
                url: url_cash_register_list_num_ajax,
                data: function (params) {
                    return {
                        term: params.term
                    };
                },
            },
            dropdownParent: $(this).parent(),
            language: locale
        });
        $('.kl-select-cash-register').val(0).trigger('change');
        transaction_datatable.draw();
    });

    /* gestion min et max date filtre*/

    var isRtl = $('html').attr('dir') === 'rtl';

    $('#id-filter-date-emission-one').datepicker({
        autoclose: true,
        orientation: isRtl ? 'auto right' : 'auto left',
        language: 'fr',
    }).on('changeDate', function (e) {
        $('#id-filter-date-emission-two').datepicker('setStartDate', e.date);

    });

    $('#id-filter-date-emission-two').datepicker({
        autoclose: true,
        orientation: isRtl ? 'auto right' : 'auto left',
        language: 'fr'
    }).on('changeDate', function (e) {
        $('#id-filter-date-emission-one').datepicker('setEndDate', e.date);
    });

    /*fin gestion filtre date*/
});

var form_validate;
// var europe_country = ["fr", "de", "es","at","be","bg","hr","cy","cz","dk","ee","fi","it","gr","hu","ie","lv","lt","lu",
//     "mt","nl","po","pt","ro","sk","se"];
var europe_country = "fr";

$(document).ready(function () {

    $("form:not([name=zrp_userbundle_user])").bootstrapValidator();
});
/**
 * Javascript general
 */
$(function () {

    // Confirmation suppression
    $(document).on('click', '.kl-delete-btn-custom, .kl-remove-elt', function (event) {
        if (!confirm('Etes vous sûr de vouloir supprimer ?'))
            event.preventDefault();
    });
    $('form .kl-delete-btn').click(function (event) {
        var length_checked = $('[name="delete[]"]:checked').length;
        if (length_checked == 0) {
            alert('Veuillez sélectionner un élément à supprimer');
            event.preventDefault();
        } else {
            if (!confirm('Etes vous sûr de vouloir supprimer ?'))
                event.preventDefault();
        }
    });

    // Supprimer la classe Error séléctionnée
    $("input").focus(function () {
        $(this).parents('.form-group').removeClass('has-error');
    });
    $("select").focus(function () {
        $(this).parents('.form-group').removeClass('has-error');
    });
    $("textarea").focus(function () {
        $(this).parents('.form-group').removeClass('has-error');
    });
    $('.kl-login-button').click(function () {
        $('#login-form-submit').trigger('submit');
    });

    /* read only and can require */
    $(document).on('keydown paste', '.kl-readonly', function (e) {
        e.preventDefault();
    });
});

/*
 * Mettre une erreur sur le champ spécifique
 */
function setErrorClass($this) {
    $this.parents('.form-group').addClass('has-error');
}

/**
 * separateur
 * @param n_str
 * @returns {string}
 */
function thousandSeparator(n_str) {
    var y = parseFloat(n_str).toFixed(2).replace(/\.0+$/, '');
    y += '';
    var x = y.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + ' ' + '$2');
    return x1 + x2;
}

// Select2
$(function () {
    $('.kl-select-two').each(function () {
        $(this)
            .wrap('<div class="position-relative"></div>')
            .select2({
                dropdownParent: $(this).parent(),
                language: locale
            });
    });
    $('.kl-select-two-no-search').each(function () {
        $(this)
            .wrap('<div class="position-relative"></div>')
            .select2({
                dropdownParent: $(this).parent(),
                language: locale,
                minimumResultsForSearch: -1
            });
    });
});

//DatePicker
$(function () {
    var isRtl = $('html').attr('dir') === 'rtl';
    $('.kl-date-time-picker').datepicker({
        autoclose: true,
        orientation: isRtl ? 'auto right' : 'auto left',
        language: 'fr'
    }).on('changeDate', function(e) {
        console.log('change');
    });

    $('.kl-date-time-picker-limited-tommorow').datepicker({
        autoclose: true,
        orientation: isRtl ? 'auto right' : 'auto left',
        language: 'fr',
        startDate: "+1d"
    });

    $('.kl-date-picker-wrapper').each(function(i,el){
        var dpk = $(el).find('input.kl-date-time-picker');
        dpk.on('click', function(){
            $(this).datepicker('destroy');
        });     
        $(el).find('.kl-open-dtm').on('click', function(){
            dpk.datepicker('update').datepicker('show');
        }); 
    });

});

// tooltip
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

$(document).on('input', '#gs_service_metiermanagerbundle_customer_cstEmail, #gs_userbundle_user_email, #gs_userbundle_user_username', function () {
    $(this).closest('.form-group.row').find('ul').remove();
});

$(document).on('click', '.kl-form-required ~ label:not(.error):not(.file-ok)', function () {
    if ($(this).closest('div').find('input').length > 0)
        $(this).closest('div').find('input').focus();
    else
        $(this).closest('div').find('textarea').focus();
});

/**
 * @param data_element
 * @param methods
 * @param value
 * @returns {string}
 */
function createRulesForValidate(data_element, methods, value = null) {
    var data = '';
    var method = '{';

    for (var i = 0; i < methods.length; i++) {
        method += '"' + methods[i] + '": ' + (value ? '"' + value + '"' : 'true') + (i == methods.length - 1 ? '}' : ',');
    }
    for (var i = 0; i < data_element.length; i++) {
        data += '"' + data_element[i] + '": ' + method + (i != data_element.length - 1 ? ',' : '');
    }

    return data;
}

/**
 * @param data_array
 * @returns {any}
 */
function generateRules(data_array) {
    var rules_start = '{"rules": {';
    var rules_end = '},"errorElement": "span"}';

    return JSON.parse(rules_start + data_array.join(',') + rules_end);
}

/**
 * mask input
 * @param current_value
 */
function zrpMaskInput(current_value) {
    var value = $(current_value).val();
    var formated_val = value.replace(',', '.');
    var array_value = formated_val.split('.');
    var decimal = array_value[0].replace(/ /g, '');
    var new_val = zrpFormatMillier(decimal);
    if (array_value.length > 1) {
        var entier = array_value[1].replace(/ /g, '');
        new_val += '.' + entier.substr(0, 2);
    }

    $(current_value).val(new_val);
}

/**
 * format millier
 * @param nombre
 * @returns {string}
 */
function zrpFormatMillier(nombre) {
    nombre += '';
    var sep = ' ';
    var reg = /(\d+)(\d{3})/;
    while (reg.test(nombre)) {
        nombre = nombre.replace(reg, '$1' + sep + '$2');
    }
    return nombre;
}
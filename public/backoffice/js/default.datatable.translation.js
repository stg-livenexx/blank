var  datatable_translation = {
    'sEmptyTable' : bo_datatable_no_enregistrement,
    'sInfo' : bo_datatable_see + ' _TOTAL_ ' + bo_datatable_of + ' _PAGE_ ' + bo_datatable_for + ' _PAGES_ ' + bo_datatable_entrees ,
    'sInfoEmpty' : bo_datatable_empty,
    'sInfoFiltered' : '',
    'sInfoPostFix' : '',
    'sInfoThousands' : ',',
    'sLengthMenu' : bo_datatable_display + ' _MENU_' + bo_datatable_entrees,
    'sLoadingRecords' : bo_datatable_loading,
    'sProcessing' : bo_datatable_loading,
    'sSearch' : bo_datatable_search + ':',
    'sZeroRecords' : bo_datatable_zero_records,
    'oPaginate' : {
        'sFirst' : bo_datatable_first,
        'sLast' : bo_datatable_last,
        "sPrevious": bo_datatable_previous,
        "sNext": bo_datatable_next
    },
    'oAria' :  {
        'sSortAscending' : ':' + bo_datatable_activate_sort_asc,
        'sSortDescending' : ':' + bo_datatable_activate_sort_desc,
    },
    'select' : {
        'rows' : {
            '_' : '%d' + bo_datatable_rows_selected,
            0 : bo_datatable_no_row_selected,
            1 : '1' + bo_datatable_row_selected,
        },
    },
};

$.extend( true, $.fn.dataTable.defaults, {
    "iDisplayLength": 10,
    "oLanguage": datatable_translation
});


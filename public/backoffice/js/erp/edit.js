$(document).ready(function () {
    if($('#zrp_adminbundle_erp_erpApiUserPwd_first').parent().parent().hasClass('has-error')) {
        var ul_error = $('#zrp_adminbundle_erp_erpApiUserPwd_first').parent();
        var ul = ul_error.next();
        ul.hide();
        $('#zrp_adminbundle_erp_erpApiUserPwd_first').parent().parent().next().append(ul.css({'display':'block'}));
    }
});
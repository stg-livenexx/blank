var column_defs = [
    {
        name: "id",
        orderable: false,
        render: function (data, type, row) {
            var href_edit = data ? href_edit_default : "javascript:void(0)";
            var href_delete = data ? href_delete_default : "javascript:void(0)";
            href_edit = href_edit.replace('0', data);
            href_delete = href_delete.replace('0', data);
            var action = '<td>' +
                '<a href="' + href_edit + '"><i class="feather icon-edit"></i></a>' +
                '<a class="kl-remove-elt" href="' + href_delete + '">' + '<i class="feather icon-trash-2"></i></a>' +
                '</td>'
            ;
            return action;
        },
        targets: 0
    },
    {
        name: "erpName",
        orderable: true,
        targets: 1
    },
    {
        name: "erpApiUrl",
        orderable: true,
        render: function (data){
            var url = data.substr(0, 50);
            if (url.length >= 50){
                url = url+'...';
            }
           return url;
        },
        targets: 2
    },
    {
        name: "erpApiDatabase",
        orderable: true,
        targets: 3
    },
    {
        name: "erpApiUserEmail",
        orderable: true,
        targets: 4
    }
];

var datatable = generateDatatable($('#id-erp-list'), url_erp_list_ajax, column_defs);
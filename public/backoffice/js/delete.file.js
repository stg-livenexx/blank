/**
 * Suppression du fichier dans la base et dans le dossier
 */
function deleteFile(id_file, this_element) {
    // Récuperation url ajax
    var url_ajax = $("#file-" + id_file).attr("ajax-url");

    bootbox.confirm({
        message: "Etes-vous sûr de vouloir supprimer ce fichier ?",
        swapButtonOrder:true,
        buttons: {
            confirm: {
                label: 'OK',
                className: 'btn btn-primary'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn btn-default'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: url_ajax,
                    data: {'id': id_file},
                    cache: false,
                    success: function (response) {
                        if (response.success) {
                            this_element.parents('.blc-image').remove();
                            bootbox.alert("Suppression effectuée avec succès !");
                        } else {
                            if (response.message) {
                                bootbox.alert(response.message);
                            }
                        }
                    }
                });
            }
        }
    });
}
$(document).ready(function (){
    $("#id-client-list tbody").on('click', '.kl-edit-ajax', function (e) {
        e.preventDefault();
        getClientAjax($(this).attr('href'));
    });
});

function getClientAjax(href)
{
    $.ajax({
        url: href,
        method: "GET",
        contentType: 'application/json; charset=utf-8',
        dataType : "json"
    }).done(function(response){
        const client_ajax = JSON.parse(JSON.stringify(response));
        $('#id-client-edit-id').val(client_ajax.id)
        $('#id-client-edit-cin').val(client_ajax.cltCin);
        $('#id-client-edit-last-name').val(client_ajax.cltLastName);
        $('#id-client-edit-first-name').val(client_ajax.cltFirstName);
        $('#id-client-edit-phone').val(client_ajax.cltPhoneNumber);
        $('#id-client-edit-form-modal').modal('show');
    }).fail(function(error){
        console.log("Error " + JSON.stringify(error));
    }).always(function(){
        console.log('request completed!!!');
    });
}
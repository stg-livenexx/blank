var column_defs = [
    {
        name: "cltPhoneNumber",
        orderable: true,
        targets: 4
    },
    {
        name: "cltFirstName",
        orderable: true,
        targets: 3
    },
    {
        name: "cltLastName",
        orderable: true,
        targets: 2
    },
    {
        name: "cltCin",
        orderable: true,
        targets: 1
    },
    {
        name: "id",
        orderable: false,
        render: function (data, type, row) {
            var href_edit = data ? href_client_edit_default : "javascript:void(0)";
            var href_delete = data ? href_client_delete_default : "javascript:void(0)";
            href_edit = href_edit.replace('0', data);
            href_delete = href_delete.replace('0', data);
            return '<td>' +
                        '<a class="kl-edit-ajax" href="' + href_edit + '"><i class="feather icon-edit"></i></a>' +
                        '<a class="kl-remove-elt-ajax" href="' + href_delete + '">' + '<i class="feather icon-trash-2"></i></a>' +
                   '</td>';
        },
        targets: 0
    }
];

datatable = generateDatatable($('#id-client-list'), url_client_list_ajax, column_defs);
$(document).ready(function (){
    $('#id-button-save-edit').click(function(e){
        e.preventDefault();
        if(!isInValidateEdit())
        {
            var href = href_client_update_default.replace('0', $('#id-client-edit-id').val());
            updateClientAjax(href);
        }
    });
});

function updateClientAjax(href)
{
    const client_updated = {
        clt_cin:  $('#id-client-edit-cin').val(),
        clt_last_name:  $('#id-client-edit-last-name').val(),
        clt_first_name:  $('#id-client-edit-first-name').val(),
        clt_address: $('#id-client-edit-address').val(),
        clt_phone_number: $('#id-client-edit-phone').val()
    };

    $.ajax({
        url:href,
        method: "PUT",
        contentType: 'application/json; charset=utf-8',
        dataType : "json",
        data: JSON.stringify(client_updated)
    }).done(function(response){
        $('#id-client-edit-form-modal').modal('hide');
        $('.alert.alert-success.alert-dismissible').append(`<span class="kl-success-message">${response.success}</span>`).show(2000);
        datatable.ajax.reload(function() {
            $('.alert.alert-success.alert-dismissible').fadeOut(4000);
            window.setTimeout(function () {$('.kl-success-message').remove();}, 4010);
        } );
    }).fail(function(error){

        var message = error.status === 400 && error.responseJSON["exist_cin"] ?
            error.responseJSON['exist_cin'] : 'Une erreur est survenue pendant le traitement.';

        $('#id-client-edit-form-modal').modal('hide');
        $('.alert.alert-danger.alert-dismissible').append(`<span class="kl-error-message">${message}</span>`).show(1000);
        window.setTimeout(function () {
            $('.alert.alert-danger.alert-dismissible').fadeOut(2000);
            window.setTimeout(function () {$('.kl-error-message').remove();}, 2010);
        }, 3000);

        console.log("Error " + JSON.stringify(error));
    }).always(function(){
        console.log('request completed!!!');
    });
}

function isInValidateEdit()
{
    var error = false;
    $('.kl-client-edit').each(function () {
        switch($(this).val())
        {
            case '':
                buildErrorMessage(this, 'edit');
                error = true;
                break;
            default:
                $(this).next().text('');
        }
    })
    return error;
}

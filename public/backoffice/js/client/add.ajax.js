$(document).ready(function (){
    $('.kl-error-span-validate').css('color', 'red');
    $('#id-button-save-add').click(function (e) {
        e.preventDefault();
        if(!isInValidate()){
            addClient();
        }
    })
});

function addClient()
{
    const client_new = {
        clt_cin:  $('#id-client-add-cin').val(),
        clt_last_name:  $('#id-client-add-last-name').val(),
        clt_first_name:  $('#id-client-add-first-name').val(),
        clt_address: $('#id-client-add-address').val(),
        clt_phone_number: $('#id-client-add-phone').val()
    };

    $.ajax({
        url: href_client_add_default,
        method: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType : "json",
        data: JSON.stringify(client_new)
    }).done(function(response){
        $('#id-client-add-form-modal').modal('hide');
        $('#id-form-add').get(0).reset();
        $('.alert.alert-success.alert-dismissible').append(`<span class="kl-success-message">${response.success}</span>`).show(2000);
        datatable.ajax.reload(function() {
            $('.alert.alert-success.alert-dismissible').fadeOut(4000);
            window.setTimeout(function () {$('.kl-success-message').remove();}, 4010);
        } );
    }).fail(function(error){

        var message = error.status === 400 && error.responseJSON["exist_cin"] ?
                        error.responseJSON['exist_cin'] : 'Une erreur est survenue pendant le traitement.';

        $('#id-client-add-form-modal').modal('hide');
        $('.alert.alert-danger.alert-dismissible').append(`<span class="kl-error-message">${message}</span>`).show(1000);
        window.setTimeout(function () {
            $('.alert.alert-danger.alert-dismissible').fadeOut(2000);
            window.setTimeout(function () {$('.kl-error-message').remove();}, 2010);
        }, 3000);

    }).always(function(){
        console.log('request completed!!!');
    });
}

function isInValidate()
{
    console.log('is_validate');
    var error = false;
    $('.kl-client-add').each(function () {
        switch($(this).val())
        {
            case '':
                buildErrorMessage(this, 'add');
                error = true
                break;
            default:
                $(this).next().text('');

        }
    })
    return error;
}


/*if($(input_client_cin).val() === '' || !(new RegExp("^[1-9][0-9]+$").test($(input_client_cin).val().trim())))
    {
        $(input_client_cin).next().text('Veuillez saisir un numéro de CIN valide');
        error = true;
    }
    else {
        $(input_client_cin).next().text('');
    }

    if($(input_client_last).val() === '' || !(new RegExp("([a-zA-Z]+)").test($(input_client_last).val().trim())))
    {
        $(input_client_last).next().text('Veuillez saisir un nom valide');
        error = true;
    }
    else{
        $(input_client_last).next().text('');
    }

    if($(input_client_first).val() === '' || !(new RegExp("([a-zA-Z]+)").test($(input_client_first).val().trim())))
    {
        $(input_client_first).next().text('Veuillez saisir un prénom valide');
        error = true;
    }
    else{
        $(input_client_first).next().text('');
    }

    if($(input_client_address).val() === '' || !(new RegExp("([a-zA-Z0-9]+)").test($(input_client_address).val().trim())))
    {
        $(input_client_address).next().text('Veuillez saisir une adresse valide');
        error = true;
    }
    else{
        $(input_client_address).next().text('');
    }

    if($(input_client_phone).val() === '' || !(new RegExp("([0-9]+)").test($(input_client_phone).val().trim())))
    {
        $(input_client_phone).next().text('Veuillez saisir un numéro de téléphone valide');
        error = true;
    }
    else{
        $(input_client_phone).next().text('');
    }*/
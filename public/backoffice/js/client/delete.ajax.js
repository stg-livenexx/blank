$(document).ready(function () {
    $("#id-client-list tbody").on('click', '.kl-remove-elt-ajax', function (e) {
        e.preventDefault();
        console.log('Click!!! remove');
        if (confirm('Etes vous sûr de vouloir supprimer ?'))
        {
            deleteClient($(this).attr('href'));
        }
    });
})

function deleteClient(href){
    $.ajax({
        url: href,
        method: "DELETE",
        contentType: 'application/json; charset=utf-8',
        dataType : "json",
    }).done(function(response){
        $('.alert.alert-success.alert-dismissible').append(`<span class="kl-success-message">${response.success}</span>`).show(2000);
        datatable.ajax.reload( function () {
            $('.alert.alert-success.alert-dismissible').fadeOut(4000);
            window.setTimeout(function () {$('.kl-success-message').remove();}, 4010);
        } );
    }).fail(function(error){
        $('.alert.alert-danger.alert-dismissible').append('<span class="kl-error-message">Une erreur est survenue pendant le traitement.</span>').show(1000);
        window.setTimeout(function () {
            $('.alert.alert-danger.alert-dismissible').fadeOut(2000);
            window.setTimeout(function () {$('.kl-error-message').remove();}, 2010);
        }, 3000)
        console.log("Error " + JSON.stringify(error));
    }).always(function(){
        console.log('request completed!!!');
    });
}
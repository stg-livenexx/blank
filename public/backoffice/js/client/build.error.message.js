function buildErrorMessage(input, form)
{
    switch ($(input).attr('id'))
    {
        case 'id-client-'+form+'-cin':
            $(input).next().text('Veuillez saisir un numéro de CIN valide');
            break;
        case 'id-client-'+form+'-last-name':
            $(input).next().text('Veuillez saisir un nom valide');
            break;
        case 'id-client-'+form+'-first-name':
            $(input).next().text('Veuillez saisir un prénom valide');
            break;
        case 'id-client-'+form+'-phone':
            $(input).next().text('Veuillez saisir un numéro de téléphone valide');
            break;
    }
}
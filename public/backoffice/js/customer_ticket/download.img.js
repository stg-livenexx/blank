$(document).on('click', '#id-btn-Convert-html-to-jpeg', function () {
    var name = '';
    window.scrollTo(0,0);
    name = filename.replaceAll(/ |-/g, '_');
    name = name + '.jpg';
    html2canvas(document.querySelector('#id-content-page')).then(function(canvas) {
        saveAs(canvas.toDataURL(), name);
    });
});

function saveAs(uri, filename) {
    var link = document.createElement('a');

    if (typeof link.download === 'string') {
        link.href = uri;
        link.download = filename;

        //Firefox requires the link to be in the body
        document.body.appendChild(link);

        //simulate click
        link.click();

        //remove the link when done
        document.body.removeChild(link);

    } else {
        window.open(uri);
    }
}
var start_date_filter;
var end_date_filter;
$(document).ready(function () {
    var ticket_datatable = $('#id-customer-ticket-list').DataTable({
        bProcessing: true,
        bFilter: true,
        bServerSide: true,
        iDisplayLength: 10,
        order: [],
        ajax: {
            url: url_customer_ticket_list,
            data: function (data) {
                data.order_by = data.order[0] ? data.columns[data.order[0].column].name + ' ' + data.order[0].dir : '';
                for (var i = 0; i < params_filter_datatable.length; i++) {
                    data[params_filter_datatable[i]['key']] = params_filter_datatable[i]['value'];
                }
            }
        },
        columnDefs: [
            {
                name: "id",
                orderable: false,
                render: function (data, type, row) {

                    var href_show = data ? href_show_default : "javascript:void(0)";
                    href_show = href_show.replace('0', data);

                    var action = '<a  href="' + href_show + '">' + '<i class="feather icon-eye"></i></a>'+'</td>' ;
                    return action;
                },
                targets: 0
            },
            {
                name: "label",
                orderable: true,
                render: function (data , type, row) {
                    var value = data ? data : '';
                    return '<input type="text" data-ticket-id ="'+ row[0] +'" ' +
                        'class="kl-edit-ticket-ajax-modal form-control" data-toggle="modal" ' +
                        'readonly data-target="#id-modal-edit-ticket" value="'+value+'"/>';
                },
                targets: 1
            },
            {
                name: "amount",
                orderable: true,
                targets: 2
            },
            {
                name: "date_transaction",
                orderable: true,
                targets: 3
            },
            {
                name: "postal_code_biller",
                orderable: true,
                targets: 4
            },
            {
                name: "city_biller",
                orderable: true,
                targets: 5
            },
            {
                name: "address_biller",
                orderable: true,
                targets: 6
            },
            {
                name: "activity_biller",
                orderable: true,
                targets: 7
            },
            {
                name: "usrAddress",
                orderable: true,
                targets: 8
            }
        ],
        language: {url: url_datatable_language}
    });

    /**
     *  filter
     */
    $('.kl-toggle-filter .dropdown-menu *').not('.kl-validate-filter').on('click', function(e){
        e.stopPropagation();
    });


    /* $(document).on('click', '#id-btn-filter', function (e) {
        ticket_datatable.draw();
    }); */


    $(document).on('change','#id-price-range',function(){
        var val = $(this).val().split(',');
        /**set indication**/
        $('#id-indicateur-price-range-min').text('$ '+val[0]);
        $('#id-indicateur-price-range-max').text('$ '+val[1]);
        /**set indication**/
    });


    $(document).on('change','#id-postal-code-range',function(){
        var val = $(this).val().split(',');
        /**set indication**/
        $('#id-indicateur-postal-code-range-min').text(val[0]);
        $('#id-indicateur-postal-code-range-max').text(val[1]);
        /**set indication**/
    });
    $('#id-price-range').val('');
    drawListFilter();
    /**
     * daterangepicker
     */
    var isRtl = $('body').attr('dir') === 'rtl' || $('html').attr('dir') === 'rtl';
    $('#id-date-range-filter').daterangepicker({
        locale: {
            cancelLabel: 'Retirer',
            applyLabel: 'Appliquer',
            daysOfWeek: ['Di', 'Lu', 'Ma', 'Mer', 'Jeu', 'Ven', 'Sa'],
            monthNames: [
                "Janvier",
                "Février",
                "Mars",
                "Avril",
                "Mai",
                "Juin",
                "Juillet",
                "Août",
                "Septembre",
                "Octobre",
                "Novembre",
                "Décembre"
            ],            format: 'DD/MM/YYYY',
        },
        opens: (isRtl ? 'left' : 'right'),
        showWeekNumbers: true,
        autoUpdateInput: false,
        parentEl: "#id-dropdown-menu-filter-by"
    });
    $('#id-date-range-filter').val(label_date_interval);


    /**
     * on apply click charge variable
     */
    $('#id-date-range-filter').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        start_date_filter = picker.startDate.format('YYYY-MM-DD');
        end_date_filter = picker.endDate.format('YYYY-MM-DD');
    });
    /**
     * on cancel click charge variable
     */
    $('#id-date-range-filter').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val(label_date_interval);
        start_date_filter = null;
        end_date_filter = null;
    });

    // /**
    //  * daterangepicker
    //  */
    // $('#id-date-range-filter').focus(function (e) {
    //     $(this).val('');
    // });

    /**
     * on click active
     */
    $(document).on('click', '[aria-labelledby=id-dropdown-trie-by] a', function () {
        /* toggle icon sort && value (asc, desc) if item active*/
        if ($(this).hasClass('active'))
            if ($(this).find('.kl-icon-sort').attr('class').indexOf('-up-alt') >= 0) {
                /* icon */
                $(this).find('.kl-icon-sort').attr('class', $(this).find('.kl-icon-sort').attr('class').replace('-up-alt', '-down-alt'));
                /* value */
                $(this).attr('data-value', $(this).data('value').replace('asc', 'desc'));
                $(this).data('value', $(this).data('value').replace('asc', 'desc'));
            } else {
                /* icon */
                $(this).find('.kl-icon-sort').attr('class', $(this).find('.kl-icon-sort').attr('class').replace('-down-alt', '-up-alt'));
                /* value */
                $(this).attr('data-value', $(this).data('value').replace('desc', 'asc'));
                $(this).data('value', $(this).data('value').replace('desc', 'asc'));
            }

        /* toggle item active */
        $('[aria-labelledby=id-dropdown-trie-by] a').removeClass('active');
        $(this).addClass('active');

        /* ajax filter */
        drawListFilter();
    });

    /**
     * apply all filter
     */
    $(document).on('click', '#id-btn-apply-all-filter', function () {
        /* ajax filter */
        drawListFilter();
        $(this).closest('.dropdown-menu').removeClass('show');
    });

    /**
     *  on change num caisse
     */
    $(document).on('change', '#id-filter-num-caisse', function (e) {
        var num_caisse = e.target.value;
        $('#id-input-num-caisse').val(num_caisse);
    });

    /**
     * ref num caisse
     */
    $(document).on('select2:select', '#id-filter-num-caisse', function (e) {
        var num_caisse = e.target.value;
        $('#id-input-num-caisse').val(num_caisse);
    });

    /**
     * reset all filter
     */
    $(document).on('click', '#id-btn-reset-all-filter', function () {
        $('#id-date-range-filter').val(label_date_interval);
        start_date_filter = null;
        end_date_filter = null;
        $('#id-postal-code-range').val('');
        // Setter
        $('#id-price-range').val('');
        $('#id-filter-num-caisse').select2({
            placeholder: "- Numéro de caisse -",
            ajax: {
                url: url_cash_register_list_num_ajax,
                data: function (params) {
                    return {
                        term: params.term
                    };
                },
            },
            dropdownParent: $(this).parent(),
            language: locale
        });
        $('.kl-select-cash-register').val(0).trigger('change');
        //initSliderPostalCode();
        /* ajax filter */
        drawListFilter(true);
    });
});
/**
 * Filter by
 */
function drawListFilter(is_reset = false)  {

    $.ajax({
        url: url_customer_ticket_list_filter_ajax,
        type: 'GET',
        dataType: 'html',
        data: {
            sort: $('[aria-labelledby=id-dropdown-trie-by] a.active').data('value'),
            start_date_filter:start_date_filter,
            end_date_filter:end_date_filter,
            price_range: is_reset ? 0 : $('#id-price-range').val(),
            postal_code_range:$('#id-postal-code-range').val(),
            num_caisse: $('#id-input-num-caisse').val()
        },
        beforeSend: function () {
            $('#id-list-ticket-visuel').html(
                '<div class="d-flex justify-content-center kl-list-loading">' +
                '    <div class="spinner-border text-success" style="width: 3rem; height: 3rem;" role="status">' +
                '        <span class="visually-hidden"></span>' +
                '    </div>' +
                '</div>'
            );
        },
        success: function (result) {
            $('#id-list-ticket-visuel').html(result);
        },
        complete: function () {
            if ($('.kl-list-loading').length > 0)
                $('.kl-list-loading').remove();
        }
    });
}

/**
 * init price slider
 */
function initSliderPrice() {
    /**get min max**/
    var min_price = isNaN($('#id-input-min-price').val()) ?  0 : Math.floor(parseFloat($('#id-input-min-price').val()));
    var max_price = isNaN($('#id-input-max-price').val()) ?  0 : Math.ceil(parseFloat($('#id-input-max-price').val()));
    /**set indication**/
    $('#id-indicateur-price-range-min').text('$ '+min_price);
    $('#id-indicateur-price-range-max').text('$ '+max_price);
    /**set indication**/
    /* $('#id-price-range').slider({
        min: min_price,
        max: max_price,
        formatter: function(value) {
            return value + '$' ;
            },
    });
    $('#id-price-range').slider().val(0); */

}

/**
 * init postal code slider
 */
function initSliderPostalCode() {
    /**get min max**/
    var min_postal_code = isNaN($('#id-input-min-postal-code').val()) ?  0 : parseInt($('#id-input-min-postal-code').val());
    var max_postal_code = isNaN($('#id-input-max-postal-code').val()) ?  0 : parseInt($('#id-input-max-postal-code').val());
    /**set indication**/
    $('#id-indicateur-postal-code-range-min').text(min_postal_code);
    $('#id-indicateur-postal-code-range-max').text(max_postal_code);
    /**set indication**/
    $('#id-postal-code-range').slider({
        min: min_postal_code,
        max: max_postal_code,
        value: [ min_postal_code, max_postal_code ]
    });

}

/**
 * is number key
 * @param evt
 * @returns {boolean}
 */
function isNumberKey(evt){
    var char_code = (evt.which) ? evt.which : event.keyCode
    if (char_code > 31 && (char_code < 48 || char_code > 57))
        return false;
    return true;
}

/**
 * is amount key
 * @param evt
 * @returns {boolean}
 */
function isAmountKey(evt){
    var char_code = (evt.which) ? evt.which : event.keyCode;
    var is_point_exist = false;
    var amount = $('#id-price-range').val();
    if (!amount && char_code === 46) return false;
    if (amount.includes('.'))
        is_point_exist = true;
    if (char_code !== 46)
    {
        is_point_exist = false;
    }
    if (is_point_exist) return false;

    return !(char_code > 31 && (char_code < 46 || char_code === 47 || char_code > 57));
}
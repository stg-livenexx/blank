$(document).ready(function() {
    //show and initiate modal
    $(document).on('click','.kl-edit-ticket-ajax-modal' , function () {
        $.ajax({
            url: url_customer_ticket_update_ajax.replace('0', $(this).data('ticket-id')),
            dataType: 'html',
            async: false,
            type: 'GET',
            success: function (response) {
                $('#id-edit-ticket-form-content').html(response);
                $('form[name=zrp_service_metiermanagerbundle_customer_ticket]').bootstrapValidator();
            }
        });
    });
    //on submit ajax form
    $(document).on('submit', 'form[name=zrp_service_metiermanagerbundle_customer_ticket]' , function (e) {
        e.preventDefault();
        if($(this).data('bootstrapValidator').isValid()) {
            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                async: true,
                beforeSend: function() {
                   $('#id-modal-edit-ticket').modal('hide');
                },
                success: function(data)
                {
                    var exist_alert = $('.layout-content').find('.alert.alert-success');
                    if(exist_alert.length > 0) {
                        exist_alert.html('<button type="button" class="close" data-dismiss="alert">×</button>' +
                            txt_success_edit +'</div>');
                    } else {
                        $('.layout-content').prepend('<div class="alert alert-success alert-dismissible fade show">' +
                            '<button type="button" class="close" data-dismiss="alert">×</button>' +
                            txt_success_edit +'</div>');
                    }
                    drawListFilter();
                }
            });
        }
    });
});

$(document).on('click', '#id-btn-Convert-html-to-jpeg', function (e) {
    var name ;
    var content_page = $(this).parent().find('.kl-content-page').attr('id');
    var filename = $(this).parent().find('#id-name-ticket').val();
    var id_content_page = `#${content_page}`;

    $(this).parent().find('.kl-content-page').removeClass('d-none');

    if (filename === undefined)
        filename = 'Ticket';

    window.scrollTo(0,0);
    name = filename.replaceAll(/ |-/g, '_');
    name = name + '.jpg';
    html2canvas(document.querySelector(id_content_page)).then(function(canvas) {
        saveAs(canvas.toDataURL(), name);
    });
    $(this).parent().find('.kl-content-page').addClass('d-none');
});

function saveAs(uri, filename) {
    var link = document.createElement('a');

    if (typeof link.download === 'string') {
        link.href = uri;
        link.download = filename;

        //Firefox requires the link to be in the body
        document.body.appendChild(link);

        //simulate click
        link.click();

        //remove the link when done
        document.body.removeChild(link);

    } else {
        window.open(uri);
    }
}
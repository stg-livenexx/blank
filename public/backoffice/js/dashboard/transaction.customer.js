$(document).ready(function () {
    drawListCustomerTransaction();
    $(document).on('click', '#id-btn-search', function () {
        drawListCustomerTransaction();
    });

    $(document).on('keypress', '#id-input-search', function (event) {
        if (event.keyCode == 13)
            drawListCustomerTransaction();
    });

    $(document).on('input', '#id-input-search', function (event) {
        var search = $(this).val();
        if(search == '')
        {
            drawListCustomerTransaction();
        }
    });

     $(document).on('click', '.kl-link-acc', function () {       
         $('.card').removeClass('kl-notShow-acc');
        $(this).parents('.card').addClass('kl-notShow-acc'); 
    });


    /**
     * Search ajax
     */
    function drawListCustomerTransaction() {
        $.ajax({
            beforeSend: function () {
                $('#id-customer-transaction-list').html(
                    '<div class="d-flex justify-content-center">' +
                    '    <div class="spinner-border text-success" style="width: 3rem; height: 3rem;" role="status">' +
                    '        <span class="visually-hidden"></span>' +
                    '    </div>' +
                    '</div>'
                );
            },
            url: url_dashboard_list_transaction_customer_ajax,
            dataType: 'html',
            data: {term: $('#id-input-search').val()},
            type: 'GET',
            success: function (response) {
                $('#id-customer-transaction-list').html(response);

                var $collaspe = $('.collapse');

                $collaspe.each(function() {

                     if($(this).hasClass("show")) {

                        $('.card').removeClass('kl-notShow-acc');
                         $(this).parent('.card').addClass('kl-notShow-acc'); 
                     }
                    else{
                         $(this).parent('.card').removeClass('kl-notShow-acc'); 
                    }
                });

            }
        });
    }
});
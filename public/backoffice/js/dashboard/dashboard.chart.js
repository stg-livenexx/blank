$(document).ready(function () {
    var siren, date;
    var type = 'month';
    var date_value = 0;
    var date_range;

    /** init chart to type of month */
    rateTransaction(type, '', 0, date_value);

    /** on click month */
    $('.kl-month').on('click', function (e) {
        $('#id-daterange-filter').val(label_date_interval);
        $('#id-stat-month').addClass('show active');
        $('#id-stat-week').removeClass('show active');
        $('#id-stat-day').removeClass('show active');
        date_range = 0;
        date_value = 0;
        type = 'month';
        rateTransaction(type, siren, date_range, date_value);
    });

    /** on click week */
    $('.kl-week').on('click', function (e) {
        $('#id-daterange-filter').val(label_date_interval);
        $('#id-stat-week').addClass('show active');
        $('#id-stat-month').removeClass('show active');
        $('#id-stat-day').removeClass('show active');
        date_range = 0;
        date_value = 0;
        type = 'week';
        rateTransaction(type, siren, date_range, date_value);
    });

    /** on click day */
    $('.kl-day').on('click', function (e) {
        $('#id-daterange-filter').val(label_date_interval);
        $('#id-stat-day').addClass('show active');
        $('#id-stat-month').removeClass('show active');
        $('#id-stat-week').removeClass('show active');
        date_range = 0;
        date_value = 0;
        type = 'day';
        rateTransaction(type, siren, date_range, date_value);
    });

    /** on change input search siren **/
    $('.kl-input-search').on('keyup change', function (e) {
        var value = $(this).val();

        if (value) siren = value;
        else siren = '';

        var chart_id = "chart-line-" + type;
        destroyChartById(chart_id);
        amountByType(type, siren, date);
        rateTransaction(type, siren, date_range, date_value);
    });

    /** on click next **/
    $('.kl-next').on('click', function (e) {
        $('#id-daterange-filter').val(label_date_interval);
        date_range = 0;

        if (type === 'week') date_value += 7;
        else date_value++;

        var chart_id = "chart-line-" + type;
        destroyChartById(chart_id);
        amountByType(type, siren, date);
        rateTransaction(type, siren, date_range, date_value);
    });

    /** on click preview **/
    $('.kl-preview').on('click', function (e) {
        $('#id-daterange-filter').val(label_date_interval);
        date_range = 0;

        if (type === 'week') date_value -= 7;
        else date_value--;
        
        var chart_id = "chart-line-" + type;
        destroyChartById(chart_id);
        amountByType(type, siren, date);
        rateTransaction(type, siren, date_range, date_value);
    });

    /** on change date range **/
    $('#id-daterange-filter').on('apply.daterangepicker', function (e, picker) {
        var value = picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY');
        date_range = value ? value : '';
        var chart_id = "chart-line-" + type;

        destroyChartById(chart_id);
        amountByType(type, siren, date);
        rateTransaction(type, siren, date_range, date_value);
    });

    // /** on focus date range **/
    // $('#id-daterange-filter').focus(function (e) {
    //     $(this).val('');
    // });

    var isRtl = $('body').attr('dir') === 'rtl' || $('html').attr('dir') === 'rtl';

    $('#id-daterange-filter').daterangepicker({
        opens: (isRtl ? 'left' : 'right'),
        showWeekNumbers: true,
        locale: {
            cancelLabel: 'Retirer',
            applyLabel: 'Appliquer',
            daysOfWeek: ['Di', 'Lu', 'Ma', 'Mer', 'Jeu', 'Ven', 'Sa'],
            monthNames: ['Janv', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sep', 'Oct', 'Nov', 'Déc'],
            format: 'DD/MM/YYYY'
        },
    });
    $('#id-daterange-filter').val(label_date_interval);
});

/**
 * Show amount by type
 * @param type
 * @param siren
 * @param date
 */
function amountByType(type, siren, date) {
    $.ajax({
        url: url_ajax_amount_type,
        dataType: 'json',
        data: {type: type, siren: siren, date: date},
        async: false,
        type: 'GET',
        success: function (response) {
            var result = response + " €";
            $('#id-amount-type').text(result)
        }
    });
}

/**
 * Show chart transaction by type
 * @param type
 * @param siren
 * @param date
 * @param date_value
 */
function rateTransaction(type, siren, date, date_value) {
    $.ajax({
        url: url_ajax_fetch_chart,
        dataType: 'json',
        data: {type: type, siren: siren, date: date, date_value: date_value},
        async: false,
        type: 'GET',
        success: function (response) {

            var chart_type = "chart-line-" + type;
            var label = [];
            var rate = [];
            var text = '';
            var date_default = new Date();
            var format_month = new Intl.DateTimeFormat('fr', {month: 'long'});

            /** if chart is type month */
            if (type === 'month' && !date) {
                date_value = date_value ? date_value : 0;
                date_default = date_default.setMonth(date_default.getMonth() + date_value);
                text = format_month.format(date_default);
                text = (text + '').charAt(0).toUpperCase() + text.substr(1);
            }

            /** if chart is type week */
            if (type === 'week' && !date) {
                date_value = date_value ? date_value : 0;
                date_default = date_default.setDate(date_default.getDate() + date_value);
                var week_number = Math.floor((new Date(date_default)).getDate() / 7) + 1;
                var year_number = (new Date(date_default)).getFullYear();
                text = 'Semaine ' + week_number + ' (' + format_month.format(date_default) + ' ' + year_number + ')';
            }

            response['rate'].map(function (value) {
                label.push(value.label);
                rate.push(value.rate);
            });
            var ctx = document.getElementById(chart_type);
            new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: label,
                    datasets: [{
                        label: "",
                        backgroundColor: "rgba(2, 186, 236, 0.3)",
                        backgroundStyle: "linéaire",
                        borderColor: "#54b48e",
                        pointBorderColor: "transparent",
                        pointBackgroundColor: "transparent",
                        pointHoverBackgroundColor: "#fff",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointBorderWidth: 0,
                        data: rate
                    }]
                },
                options: {
                    title: {
                        display: true,
                        // text: year ? year.getFullYear() : ''
                        text: text ? text : ''
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem) {
                                return amount_total + ': ' + thousandSeparator(tooltipItem.yLabel);
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                padding: 20,
                                callback: function (value, index, values) {
                                    return value.toLocaleString();
                                }
                            },
                            gridLines: {
                                borderDash: [2, 5],
                                color: '#cacdce'
                            },
                        }],
                        xAxes: [{
                            gridLines: {
                                color: '#fff',
                                lineWidth: 0,
                                drawTicks: true,
                            },
                        }]
                    },
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                        display: false
                    }
                }
            });
        }
    });
}

/**
 * destroy chart by id
 * @param id
 */
function destroyChartById(id) {
    var parent_chart = $("#" + id).parent();
    $("#" + id).remove();
    parent_chart.append('<canvas id="' + id + '"></canvas>');
}

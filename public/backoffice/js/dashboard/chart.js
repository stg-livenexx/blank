$(document).ready(function () {
    var default_commercial_id = 0;
    var default_pqr_id = 0;
    var default_percent_value = 0;

    /*Séléction pourcentage*/
    $('.kl-select-pourcentage').select2({
        allowClear: true,
        placeholder: select_choice_pourcentage,
        language: locale
    }).on('change', function (e) {
        var percent_value = $(this).val();
        var commercial_id = $(".kl-select-commercial").val();
        var pqr_id = $(".kl-select-pqr").val();
        fetchNumberProjectByUser(pqr_id, commercial_id, percent_value);
    });

    /* Initialisation select2 pour selection PQR*/
    $(".kl-select-pqr").select2({
        allowClear: true,
        placeholder: select_choice_pqr,
        language: locale
    }).on('change', function (e) {
        var pqr_id = $(this).val();
        var percent_value = $(".kl-select-pourcentage").val();
        $(".kl-select-commercial").empty();
        fetchNumberProjectByUser(pqr_id, default_commercial_id, percent_value);
    });

    /* Initialisation select2 pour selection commercial*/
    $(".kl-select-commercial").select2({
        language: locale,
        allowClear: true,
        placeholder: select_choice_commercial,
        ajax: {
            url: list_commercial_by_pqr,
            dataType: 'json',
            data: function (params) {
                return {
                    search: params.term,
                    user_pqr: $('select[name=selection_pqr]').val()
                }
            }
        }
    }).on('change', function (e) {
        var commercial_id = $(this).val();
        default_pqr_id = $('select[name=selection_pqr]').val();
        default_percent_value = $('select[name=selection_pourcentage]').val();
        fetchNumberProjectByUser(default_pqr_id, commercial_id, default_percent_value);
    });

    /* Initialisation select2 pour selection commercial user pqr*/
    $(".kl-select-commercial-pqr").select2({
        placeholder: select_choice_commercial,
        allowClear: true,
        language: locale
    }).on('change', function (e) {
        var commercial_id = $(this).val();
        default_percent_value = $('select[name=selection_pourcentage]').val();
        fetchNumberProjectByUser(default_pqr_id, commercial_id, default_percent_value);
    });

    fetchNumberProjectByUser(default_pqr_id, default_commercial_id, default_percent_value);
});

/**
 * fetch all chart
 * @param pqr_id
 * @param commercial_id
 */
function fetchNumberProjectByUser(pqr_id = 0, commercial_id = 0, percentvalue = 0) {
    fetchCanvasByType('year', pqr_id, commercial_id, percentvalue);
    fetchCanvasByType('month', pqr_id, commercial_id, percentvalue);
    fetchCanvasByType('week', pqr_id, commercial_id, percentvalue);
    fetchCanvasByType('day', pqr_id, commercial_id, percentvalue);
}

/**
 *
 * @param type
 * @param pqr_id
 * @param commercial_id
 * @param pourcentage
 */
function fetchCanvasByType(type, pqr_id, commercial_id = 0, pourcentage = 0) {
    var _loading = '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>';
    var chart_type_project = "id-chart-project-" + type;

    $('#' + chart_type_project).closest('div').find('.kl-loading').html(_loading);

    $.ajax({
        url: url_dashboard_fetch_chart,
        dataType: 'json',
        data: {
            type: type,
            commercial_user: commercial_id,
            pqr_user: pqr_id,
            pourcentage: pourcentage
        },
        type: 'GET',
        success: function (response) {
            var label_project = [];
            var rate_project = [];

            response['project'].map(function (value) {
                label_project.push(value.label);
                rate_project.push(value.rate);
            });

            canvasInit(chart_type_project, label_project, rate_project, 'projet(s)');

        },
        complete: function () {
            $('#' + chart_type_project).closest('div').find('.kl-loading').html('');
        }
    });
}

/**
 * initialisation chart
 * @param chart_type
 * @param label
 * @param rate
 * @param designation
 */
function canvasInit(chart_type, label, rate, designation) {
    //destroy chart by id
    destroyChartById(chart_type);
    var ctx = document.getElementById(chart_type).getContext('2d');
    new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: label,
            datasets: [{
                backgroundColor: '#007AFF',
                borderColor: 'rgb(255, 99, 132)',
                data: rate
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false,
            },
            tooltips: {
                // Disable the on-canvas tooltip
                enabled: false,

                custom: function (tooltipModel) {
                    // Tooltip Element
                    var tooltipEl = document.getElementById('chartjs-tooltip');

                    // Create element on first render
                    if (!tooltipEl) {
                        tooltipEl = document.createElement('div');
                        tooltipEl.id = 'chartjs-tooltip';
                        tooltipEl.innerHTML = '<table style="background:rgb(0, 0, 0, 0.5); color: #fff; padding:5px;"></table>';
                        document.body.appendChild(tooltipEl);
                    }

                    // Hide if no tooltip
                    if (tooltipModel.opacity === 0) {
                        tooltipEl.style.opacity = 0;
                        return;
                    }

                    // Set caret Position
                    tooltipEl.classList.remove('above', 'below', 'no-transform');
                    if (tooltipModel.yAlign) {
                        tooltipEl.classList.add(tooltipModel.yAlign);
                    } else {
                        tooltipEl.classList.add('no-transform');
                    }

                    function getBody(bodyItem) {
                        return bodyItem.lines;
                    }

                    // Set Text
                    if (tooltipModel.body) {
                        var titleLines = tooltipModel.title || [];
                        var bodyLines = tooltipModel.body.map(getBody);

                        var innerHtml = '<thead>';

                        titleLines.forEach(function (title) {
                            innerHtml += '<tr><th>&nbsp;' + title + '&nbsp;</th></tr>';
                        });
                        innerHtml += '</thead><tbody>';

                        bodyLines.forEach(function (body, i) {
                            var colors = tooltipModel.labelColors[i];
                            var style = 'background:red' + colors.backgroundColor;
                            style += '; border-color:' + colors.borderColor;
                            style += '; border-width: 2px';
                            var tooltip_label = 'Nombre de ' + designation + ': ' + body[i];
                            var span = '<span style="' + style + '">' + tooltip_label + '</span>';
                            innerHtml += '<tr><td>' + span + '</td></tr>';
                        });
                        innerHtml += '</tbody>';

                        var tableRoot = tooltipEl.querySelector('table');
                        tableRoot.innerHTML = innerHtml;
                    }

                    // `this` will be the overall tooltip
                    var position = this._chart.canvas.getBoundingClientRect();

                    // Display, position, and set styles for font
                    tooltipEl.style.opacity = 1;
                    tooltipEl.style.position = 'absolute';
                    tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX - 27 + 'px';
                    tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY - 5 + 'px';
                    tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
                    tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                    tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
                    tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                    tooltipEl.style.pointerEvents = 'none';
                }
            }
        }
    });
}

/**
 * destroy chart by id
 * @param id
 */
function destroyChartById(id) {
    var parent_chart = $("#" + id).parent();
    $("#" + id).remove();
    parent_chart.append('<canvas id="' + id + '"></canvas>');
}

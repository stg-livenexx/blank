var column_defs = [
    {
        name: "id",
        orderable: false,
        render: function (data, type, row) {
            if (is_company_role)
            {
                var href_edit = data ? href_edit_default : "javascript:void(0)";
                var href_delete = data ? href_delete_default : "javascript:void(0)";
                href_edit = href_edit.replace('0', data);
                href_delete = href_delete.replace('0', data);
                return '<td>' +
                    '<a href="' + href_edit + '"><i class="feather icon-edit"></i></a>' +
                    '<a class="kl-remove-elt" href="' + href_delete + '">' + '<i class="feather icon-trash-2"></i></a>' +
                    '</td>';
            }
            return '';
        },
        targets: 0,
        visible: is_company_role == 1 ? true : false,
    },
    {
        name: "csRgtNum",
        orderable: true,
        targets: 1

    } ,
    {
        name: "csRgtName",
        orderable: true,
        targets: 2
    },
    {
        name: "cmpName",
        orderable: true,
        targets: 3,
        visible: is_user_role == 1 ? true : false,
    },
    {
        name: "cmpCp",
        orderable: true,
        targets: 4,
        visible: is_user_role == 1 ? true : false,
    },
    {
        name: "cmpSiren",
        orderable: true,
        targets: 5,
        visible: is_user_role == 1 ? true : false,
    },
    {
        name: "cmpZeropCheckoutNbr",
        orderable: true,
        targets: 6,
        visible: is_user_role == 1 ? true : false,
    }
];

var datatable = generateDatatable($('#id-cash-register-list'), url_cash_register_list_ajax, column_defs);
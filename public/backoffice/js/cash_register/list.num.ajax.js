$(document).ready(function () {
    $('.kl-select-cash-register').each(function () {
        $(this)
            .wrap('<div class="position-relative"></div>')
            .select2({
                ajax: {
                    url: url_cash_register_list_num_ajax,
                    data: function (params) {
                        return {
                            term: params.term
                        };
                    },
                },
                dropdownParent: $(this).parent(),
                language: locale
            });
    });
});

$(document).ready(function () {
        $("form[name=zrp_adminbundle_erp]").bootstrapValidator({
                feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                }
        });
});

$(document).on('click', '.kl-btn-create' , function () {
        $("form[name=zrp_adminbundle_erp]").bootstrapValidator('resetForm', true);
});

$(document).on('click', '.kl-btn-cancel' , function () {
        $("form[name=zrp_adminbundle_erp]").bootstrapValidator('resetForm', true);
});
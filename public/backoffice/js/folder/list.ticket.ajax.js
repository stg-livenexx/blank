$(document).ready(function () {
    var ticket_datatable = $('#id-ticket-list').DataTable({
        bProcessing: true,
        bFilter: true,
        bServerSide: true,
        iDisplayLength: 10,
        order: [],
        ajax: {
            url: url_folder_ticket_list_ajax,
            data: function (data) {
                data.order_by = data.order[0] ? data.columns[data.order[0].column].name + ' ' + data.order[0].dir : '';
                var trx_date = new Date();
                var trx_timezone = trx_date.getTimezoneOffset() / 60;
                data.timezone = trx_timezone;
            }
        },
        columnDefs: [
            {
                name: "label",
                orderable: true,
                targets: 0,
            },
            {
                name: "reference",
                orderable: true,
                targets: 1,
            },
            {
                name: "amount",
                orderable: true,
                render: function (data) {
                    var value = thousandSeparator(data);
                    if (value == 'NaN')
                        return '';
                    return value;
                },
                targets: 2
            },
            {
                name: "date_transaction",
                orderable: true,
                render: function (data) {
                    var format = "DD/MM/YYYY HH:mm";
                    var trx_data = moment(data, format).format("MM/DD/YYYY HH:mm");
                    var trx_date = new Date(trx_data);
                    var trx_timezone = trx_date.getTimezoneOffset() / 60;
                    trx_date.setHours(trx_date.getHours() - trx_timezone);

                    return moment(trx_date, format).format(format);
                },
                targets: 3
            }
        ],
        language: {url: url_datatable_language},
        fnDrawCallback: function (settings) {
            var total_amount = settings.json.otherData;
            $('#id-total-amount').html(thousandSeparator(total_amount));
        }
    });
});
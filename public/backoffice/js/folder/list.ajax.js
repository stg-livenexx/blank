var column_defs = [
    {
        name: "id",
        orderable: false,
        render: function (data, type, row) {
            var href_edit = data ? href_edit_default : "javascript:void(0)";
            var href_delete = data ? href_delete_default : "javascript:void(0)";
            href_edit = href_edit.replace('0', data);
            href_delete = href_delete.replace('0', data);
            return '<td>' +
                '<a href="' + href_edit + '"><i class="feather icon-edit"></i></a>' +
                '<a class="kl-remove-elt" href="' + href_delete + '">' + '<i class="feather icon-trash-2"></i></a>' +
                '</td>';
        },
        targets: 0
    },
    {
        name: "name",
        orderable: true,
        targets: 1
    },
    {
        name: "nbr_transaction",
        orderable: true,
        targets: 2
    },
    {
        name: "date_enregistrement",
        orderable: true,
        targets: 3
    }
];

var datatable = generateDatatable($('#id-folder-list'), url_folder_list_datatable_ajax, column_defs);

$(document).ready(function () {
    filterBy();

    $(document).on('click', '[aria-labelledby=id-dropdown-filter-by] a', function () {
        /* toggle icon sort && value (asc, desc) if item active*/
        if ($(this).hasClass('active'))
            if ($(this).find('.kl-icon-sort').attr('class').indexOf('-up-alt') >= 0) {
                /* icon */
                $(this).find('.kl-icon-sort').attr('class', $(this).find('.kl-icon-sort').attr('class').replace('-up-alt', '-down-alt'));
                /* value */
                $(this).attr('data-value', $(this).data('value').replace('asc', 'desc'));
                $(this).data('value', $(this).data('value').replace('asc', 'desc'));
            } else {
                /* icon */
                $(this).find('.kl-icon-sort').attr('class', $(this).find('.kl-icon-sort').attr('class').replace('-down-alt', '-up-alt'));
                /* value */
                $(this).attr('data-value', $(this).data('value').replace('desc', 'asc'));
                $(this).data('value', $(this).data('value').replace('desc', 'asc'));
            }

        /* toggle item active */
        $('[aria-labelledby=id-dropdown-filter-by] a').removeClass('active');
        $(this).addClass('active');

        /* ajax filter */
        filterBy();
    });

    /**
     * Filter by
     */
    function filterBy() {
        $.ajax({
            url: url_folder_list_ajax,
            type: 'GET',
            dataType: 'html',
            data: {
                sort: $('[aria-labelledby=id-dropdown-filter-by] a.active').data('value')
            },
            beforeSend: function () {
                $('#id-list-content').html(
                    '<div class="d-flex justify-content-center kl-list-loading">' +
                    '    <div class="spinner-border text-success" style="width: 3rem; height: 3rem;" role="status">' +
                    '        <span class="visually-hidden"></span>' +
                    '    </div>' +
                    '</div>'
                );
            },
            success: function (result) {
                $('#id-list-content').html(result);
            },
            complete: function () {
                if ($('.kl-list-loading').length > 0)
                    $('.kl-list-loading').remove();
            }
        });
    }
});